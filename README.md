##Makestar Universal
The universal Javascript frontend application for www.makestar.co

###Development
The project now has a docker based development workflow.  To get up and
running ensure you have the following tools installed.
- docker (engine)
- docker-compose
- docker-machine

You can grab the tools above
[here](https://www.docker.com/products/docker-toolbox) or if you want to
use the newer Mac OSX native tools go
[here](https://docs.docker.com/docker-for-mac/)

To get started it should be as simple as

```sh
docker-compose up
```

This process can take a few minutes to complete as the images have to be
downloaded and the application dependencies installed and the javascript
+ css compiled.  This process is not necessary to run each time and it
  will use a cached version and incrementally add changes.

The application can be accessed at localhost:3000.  It will require the
[java api](https://bitbucket.org/make_star/crowdfunding-www) to be started and accessible by the universal application.

#Nginx
If you want to test out the nginx config then you will need to add
makestar.local to your /etc/hosts file.

#Autoreload
To enable a watch build for development then you need to run a seperate container with the
following command:
```sh
docker-compose run --rm universal ./node_modules/webpack/bin/webpack.js -p
```

#Testing
Test are present where possible.
Single run test execution
```sh
docker-compose run --rm universal npm run test
```
Watch and run on file update
```sh
docker-compose run --rm universal npm run test:watch
```

