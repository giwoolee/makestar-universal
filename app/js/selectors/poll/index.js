import { chunk, take } from 'lodash'
import { createSelector } from 'reselect'
import { getIsInitialLoad } from '../ui/index'

export const getPolls = state => state.polls.polls

export const getPollIdxs = state => state.polls.idxs

export const getPoll = (state,idx) => idx && state.polls.polls && state.polls.polls[idx] ? state.polls.polls[idx] : undefined

export const getSelectedStatus = state => state.polls.selectedStatus

export const getChunkedPollIdxs = createSelector(
  getPollIdxs,
  (idxs) => chunk(idxs,2)
)

export const getPollRanks = createSelector(
  getPoll,
  (poll) => poll ? poll.rank : []
)

export const getPollTopFiveRanks = createSelector(
  getPollRanks,
  (ranks) => take(ranks,5)
)

export const getPollTopThreeRanks = createSelector(
  getPollRanks,
  (ranks) => take(ranks,3)
)

export const getPollTopRank = createSelector(
  getPollRanks,
  (ranks) => take(ranks,1)
)

export const getRankImageByIdx = (state,idx) => idx && state.polls.ranks && state.polls.ranks[idx] ? state.polls.ranks[idx] : undefined

export const getPollRankTwoChunk = createSelector(
  getPollRanks,
  (ranks) => chunk(ranks,2)
)

export const getPollSns = createSelector(
  getPoll,
  (poll) => {
    if(poll) {
      return {
        idx: poll.idx,
        title: poll.title,
        description: poll.title,
        snsimageUrl: poll.snsimageUrl,
        canonical: poll.links[1].href
      }
    } else {
      return undefined
    }
  }
)
