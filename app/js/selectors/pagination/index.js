import { createSelector } from 'reselect'

export const getPagination = (state) => state.pagination

export const getPaginationIsLast = createSelector(
  getPagination,
  (pagination) => pagination.isLast
)

export const getPaginationNextPage = createSelector(
  getPagination,
  (pagination) => pagination.currentPage + 1
)

export const getPaginationCurrentPage = createSelector(
  getPagination,
  (pagination) => pagination.currentPage
)

export const getPaginationTotalPages = createSelector(
  getPagination,
  (pagination) => pagination.totalPages
)