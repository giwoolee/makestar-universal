import { createSelector } from 'reselect'

export const getCommentUi = state => state.comments.ui

export const getIsReplyOpen = (state,idx) => {
  const ui = getCommentUi(state)
  const { replyOpen } = ui
  return replyOpen.indexOf(idx) !== -1 ? true : false
}

export const getIsReplyEditOpen = (state,idx) => {
  const ui = getCommentUi(state)
  const { editOpen: { replies } } = ui
  return replies.indexOf(idx) !== -1 ? true : false
}

export const getIsCommentEditOpen = (state,idx) => {
  const ui = getCommentUi(state)
  const { editOpen: { comments } } = ui
  return comments.indexOf(idx) !== -1 ? true : false
}

export const getCommentUiDeletePrompt = createSelector(
  getCommentUi,
  (ui) => ui.deletePrompt
)

export const getCommentUiDeletePromptIdx = createSelector(
  getCommentUiDeletePrompt,
  (deletePrompt) => deletePrompt.idx
)

