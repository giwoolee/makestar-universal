import { createSelector } from 'reselect'
import moment from 'moment'

export const getComments = state => state.comments.comments

export const getCommentsIdxs = state => state.comments.idxs

export const getComment = (state,idx) => state.comments.comments[idx]

export const getCommentPagination = state => state.comments.pagination

export const getCommentNextPage = createSelector(
  getCommentPagination,
  (pagination) => pagination.currentPage + 1
)

export const getCommentIsLast = createSelector(
  getCommentPagination,
  (pagination) => pagination.isLast
)

export const getInputOpen = createSelector(
  getComment,
  comment => comment.inputOpen
)

export const getReplyPagination = createSelector(
  getComment,
  comment => comment && comment.replies ? comment.replies.pagination : undefined
)

export const getRepliesIdxs = createSelector(
  getComment,
  comment => { return comment && comment.replies ? comment.replies.content : undefined }
)

export const getReply = (state,idx) => state.replies[idx]

export const getCommentCreatedDate = createSelector(
  getComment,
  comment => {
    if(comment) {
      return moment(comment.createdDate).toDate()
    } else {
      return moment().toDate()
    }
  }
)

export const getCommentUpdatedDate = createSelector(
  getComment,
  comment => {
    if(comment) {
      return moment(comment.updatedDate).toDate()
    } else {
      return moment().toDate()
    }
  }
)

export const getReplyCreatedDate = createSelector(
  getReply,
  reply => {
    if(reply) {
      return moment(reply.createdDate).toDate()
    } else {
      return moment().toDate()
    }
  }
)

export const getReplyUpdatedDate = createSelector(
  getReply,
  reply => {
    if(reply) {
      return moment(reply.updatedDate).toDate()
    } else {
      return moment().toDate()
    }
  }
)

export const getCommentIsDeleted = createSelector(
  getComment,
  comment => comment.status === "Deleted"
)

export const getReplyIsDeleted = createSelector(
  getReply,
  reply => reply.status === "Deleted"
)
