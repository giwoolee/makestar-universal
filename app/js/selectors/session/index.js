import { createSelector } from 'reselect'

export const getSession = (state) => state.session.session

export const getIsLoggedIn = createSelector(
  getSession,
  (session) => session.isLoggedIn
)

export const getLocale = createSelector(
  getSession,
  (session) => session.locale
)

export const getUserIdx = createSelector(
  getSession,
  (session) => session.idx
)

export const getUserProfileImageUrl = createSelector(
  getSession,
  (session) => session.profileImageUrl
)

export const getUserEmail = createSelector(
  getSession,
  (session) => session.email
)

export const getUserNickname = createSelector(
  getSession,
  (session) => session.nickName
)

export const getUserGrade = createSelector(
  getSession,
  (session) => session.grade
)

export const getLocaleDisplayName = createSelector(
  getLocale,
  (locale) => {
    switch(locale) {
      case('ko'):
        return '한국어'
      case('en'):
        return 'English'
      case('zh'):
        return '中文(简体)'
      case('ja'):
        return '日本語'
    }
  }
)
