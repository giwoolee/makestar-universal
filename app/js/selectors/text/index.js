import { createSelector } from 'reselect'

export const getText = (state,idx,type) => state.text[type][idx] ? state.text[type][idx] : ''

export const getTextLength = createSelector(
  getText,
  (text) => text ? text.length : 0
)

export const getTextIsOverLength = createSelector(
  getTextLength,
  (length) => length >= 1000
)
