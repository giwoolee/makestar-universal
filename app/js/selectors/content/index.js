import { createSelector } from 'reselect'

export const getContents = state => state.contents.contents

export const getContent = (state,idx) => idx && state.contents.contents[idx] ? state.contents.contents[idx] : undefined

export const getContentsIdxs = (state) => state.contents.idxs ? state.contents.idxs : []

export const getContentIsFetching = (state) => state.contents.isFetching

export const getContentSns = createSelector(
  getContent,
  (content) => {
    if(content) {
      return {
        idx: content.idx,
        title: content.subject,
        description: content.description,
        snsimageUrl: content.snsimageUrl,
        canonical: content.links[1].href
      }
    } else {
      return undefined
    }
  }
)
