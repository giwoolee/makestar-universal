import { createSelector } from 'reselect'

export const getUi = (state) => state.ui

export const getIsInitialLoad = createSelector(
  getUi,
  (ui) => ui.isInitialLoad
)

export const getDeletePromptShow = createSelector(
  getUi,
  (ui) => ui.comments.deletePrompt.show
)

