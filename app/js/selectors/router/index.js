
export const getRouter = state => state.routing

export const getRouterPathname = state => state.routing.locationBeforeTransitions.pathname

export const getRouterQuery = state => state.routing.locationBeforeTransitions.query

