import { createSelector } from 'reselect'
import { chunk } from 'lodash'

export const getProjects = (state) => state.projects.projects

export const getProjectIdxs = (state) => state.projects.idxs ? state.projects.idxs : []

export const getChunkedProjectIdxs = createSelector(
  getProjectIdxs,
  (idxs) => chunk(idxs,2)
)

export const getProject = (state,idx) => idx && state.projects.projects ? state.projects.projects[idx] : undefined

export const getProjectPercentage = createSelector(
  getProject,
  (project) => project ? project.percent.toFixed(0) : 0
)
