import { createSelector } from 'reselect'
import { chain, head, pick, truncate, filter } from 'lodash'
import moment from 'moment'

export const getProjectUpdateIdxs = (state) => state.projectUpdates.idxs ? state.projectUpdates.idxs : []

export const getProjectUpdate = (state,idx) => idx && state.projectUpdates.projectUpdates ? state.projectUpdates.projectUpdates[idx] : undefined

export const getProjectUpdateTitle = createSelector(getProjectUpdate,
  (update) => {
    if(update) {
      return truncate(update.title,{ length: 25, seperator: ' ' })
    } else {
      return ''
    }
  }
)

export const getProjectUpdateDate = createSelector(getProjectUpdate,
  (update) => {
    if(update) {
      return moment(update.date).toDate()
    } else {
      return new Date()
    }
  }
)

export const getProjectUpdateWebLink = createSelector(getProjectUpdate,
  (update) => {
    if(update) {
      const { href } = pick(head(filter(update.links,(l) => l.rel === 'web')),'href')
      return href
    } else {
      return ''
    }
  }
)

export const getProjectUpdateShortName = createSelector(getProjectUpdate,
  (update) => update && update.projectShortName ? update.projectShortName : ''
)

export const getProjectUpdateIconImageUrl = createSelector(getProjectUpdate,
  (update) => update && update.iconImageUrl ? `url(${update.iconImageUrl})` : 'url(/public/images/icon/index/update-placeholder.png)'
)

export const getProjectUpdateImageUrl = createSelector(getProjectUpdate,
  (update) => update && update.imageUrl ? `url(${update.imageUrl})` : ''
)

