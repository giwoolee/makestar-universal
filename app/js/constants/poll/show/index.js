export const POLL_SHOW_FETCH = 'POLL_SHOW_FETCH'
export const POLL_SHOW_FETCH_REQUEST = 'POLL_SHOW_FETCH_REQUEST'
export const POLL_SHOW_FETCH_SUCCESS = 'POLL_SHOW_FETCH_SUCCESS'
export const POLL_SHOW_FETCH_FAILURE = 'POLL_SHOW_FETCH_FAILURE'
export const POLL_VOTE = 'POLL_VOTE'
export const POLL_VOTE_REQUEST = 'POLL_VOTE_REQUEST'
export const POLL_VOTE_SUCCESS = 'POLL_VOTE_SUCCESS'
export const POLL_VOTE_FAILURE = 'POLL_VOTE_FAILURE'
export const POLL_VOTE_UNAVAILABLE = 'POLL_VOTE_UNAVAILABLE'
export const POLL_VOTE_CLOSED = 'POLL_VOTE_CLOSED'
