import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://twitter.com/intent/tweet?'

  return `${BASEURL}${qs.stringify({ text: title, url: canonical })}`
}
