import qs from 'qs'

export default ({ canonical, title, description, snsImageUrl }) => {
  const BASEURL = 'http://story.kakao.com/s/share?'
  let queryParams = {
    url: canonical,
    text: title,
    desc: description,
    imageurl: snsImageUrl
  }
  return `${BASEURL}${qs.stringify(queryParams)}`
}
