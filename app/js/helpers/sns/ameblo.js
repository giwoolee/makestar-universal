import qs from 'qs'

export default ({ title, description, canonical, snsimageUrl }) => {
  const BASEURL = "http://blog.ameba.jp/ucs/entry/srventryinsertinput.do?"
  let share = { entry_text: `${title}\r\n${description}\r\n<a href="${canonical}"><img src=${snsimageUrl}></a>` }

  return `${BASEURL}${qs.stringify((share))}`
}
