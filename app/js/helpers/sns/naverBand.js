import qs from 'qs'

export default ({ canonical, title, description }) => {
  const BASEURL = 'http://band.us/plugin/share?'
  let body
  if(title === description) {
    body = `${title}`
  } else {
    body = `${title} ${description}`
  }
  return `${BASEURL}${qs.stringify({ route: canonical, body })}`
}
