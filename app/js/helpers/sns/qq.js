import qs from 'qs'

export default ({ canonical, title, description, snsimageUrl }) => {
  const BASEURL = 'http://connect.qq.com/widget/shareqq/index.html?'
  let queryParams = {
    url: canonical,
    title,
    desc: description,
    pics: snsimageUrl
  }

  return `${BASEURL}${qs.stringify(queryParams)}`
}
