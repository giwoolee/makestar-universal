import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?'
  let queryParams = {
    url: canonical,
    title
  }

  return `${BASEURL}${qs.stringify(queryParams)}`
}
