export default ({ canonical, description }) => {
  const BASEURL = 'http://line.me/R/msg/text/?'
  let parameter = `${description} ${canonical}`
  return `${BASEURL}${encodeURIComponent(parameter)}`
}
