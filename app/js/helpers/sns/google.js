import qs from 'qs'

export default ({ canonical, locale }) => {
  const BASEURL = 'https://plus.google.com/share?'
  return `${BASEURL}${qs.stringify({url: canonical,hl: locale})}`
}
