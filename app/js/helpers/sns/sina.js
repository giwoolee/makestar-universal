import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://service.weibo.com/share/share.php?'
  let queryParams = Object.assign({},{ url: canonical, title },{ content: 'utf8' })

  return `${BASEURL}${qs.stringify(queryParams)}`
}
