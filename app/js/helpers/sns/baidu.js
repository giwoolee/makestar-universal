import qs from 'qs'

export default ({ canonical, title, snsimageUrl, description }) => {
  const BASEURL = 'http://tieba.baidu.com/f/commit/share/openShareApi?'
  let queryParams = {
    url: canonical,
    title,
    desc: description,
    pic: snsimageUrl
  }
  return `${BASEURL}${qs.stringify(queryParams)}`
}
