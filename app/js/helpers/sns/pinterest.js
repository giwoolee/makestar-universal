import qs from 'qs'

export default ({ canonical, title, snsimageUrl }) => {
  const BASEURL = 'https://www.pinterest.com/pin/create/button/?'
  let queryParams = {
    url: canonical,
    description: title,
    media: snsimageUrl
  }

  return `${BASEURL}${qs.stringify(queryParams)}`
}
