import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://v.t.qq.com/share/share.php?'

  return `${BASEURL}${qs.stringify({url: canonical,title})}`
}
