import ameblo from './ameblo'
import baidu from './baidu'
import facebook from './facebook'
import google from './google'
import hatena from './hatena'
import kakaoStory from './kakaoStory'
import mixi from './mixi'
import naverBand from './naverBand'
import naverLine from './naverLine'
import pinterest from './pinterest'
import qq from './qq'
import qzone from './qzone'
import reddit from './reddit'
import sina from './sina'
import tencent from './tencent'
import twitter from './twitter'

export default {
  en: {
    facebook,
    google,
    pinterest,
    reddit,
    twitter
  },
  ja: {
    ameblo,
    facebook,
    google,
    hatena,
    mixi,
    'naver-line': naverLine,
    twitter
  },
  ko: {
    facebook,
    google,
    'kakao-story': kakaoStory,
    'naver-line': naverLine,
    'naver-band': naverBand,
    pinterest,
    twitter
  },
  zh: {
    baidu,
    facebook,
    qq,
    sina,
    qzone,
    tencent,
    twitter
  }
}
