import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://mixi.jp/share.pl?'
  return `${BASEURL}${qs.stringify({u: canonical, t: title })}`
}
