import qs from 'qs'

export default ({ canonical }) => {
  const BASEURL = 'http://www.facebook.com/sharer/sharer.php?'
  return `${BASEURL}${qs.stringify({u: canonical})}`
}
