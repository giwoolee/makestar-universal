export default ({ canonical}) => {
  const BASEURL = 'http://b.hatena.ne.jp/entry/'
  return `${BASEURL}${encodeURIComponent(canonical)}`
}
