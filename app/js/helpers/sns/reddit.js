import qs from 'qs'

export default ({ canonical, title }) => {
  const BASEURL = 'http://www.reddit.com/submit/?'
  let queryParams = {
    url: canonical,
    title
  }

  return `${BASEURL}${qs.stringify(queryParams)}`
}
