export default class ApiError extends Error {
  constructor({ payload, method, pathname, status }) {
    let message
    switch(status) {
      case 400:
        message = `${method} ${pathname} bad request`
        break
      case 401:
        message = `${method} ${pathname} not authorised`
        break
      case 404:
        message = `${method} ${pathname} resource doesn't exist`
        break
      case 412:
        message = `${method} ${pathname} precondition failed`
        break
      case 500:
        message = `${method} ${pathname} internal server error`
        break
    }
    super(message)
    this.message = message
    this.name = 'ApiError'
    this.payload = payload ? payload : undefined
    this.status = status ? status : undefined
  }
}
