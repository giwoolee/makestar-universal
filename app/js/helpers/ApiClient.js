import process from 'process'
import { isEmpty } from 'lodash'
import ApiError from './api/ApiError'

const METHODS = ['get','del','post','put']
class ApiClient {

  formatUrl(path) {
    if (process.browser) {
      return '/v1' + path
    } else {
    // Prepend `/api` to relative URL, to proxy to API server.
      return `http://${process.env.API_HOST}:${process.env.API_PORT}/v1` + path
    }
  }

  constructor(superagent,req) {
    METHODS.forEach((method) => {
      this[method] = (path, { query, data } = {}) => new Promise((resolve,reject) => {
        const request = superagent[method](this.formatUrl(path))
        if(!isEmpty(query)) {
          request.query(query)
        }

        if(req) {
          let cookie = req.get('cookie')
          if (cookie) {
            request.set('cookie',cookie)
          }
          let acceptLang = req.get('Accept-Language')
          if(acceptLang) {
            request.set('Accept-Language',acceptLang)
          }
          let xForwardedFor = req.get('X-Forwarded-For')
          if(xForwardedFor) {
            request.set('X-Forwarded-For',xForwardedFor)
          }
        }

        if(data) {
          request.send(data)
        }

        request.set('Content-Type','application/json')
        request.end((err, { body } = {}) => {
          if(err) {
            const error = new ApiError({ status: err.status, pathname: this.formatUrl(path), method, payload: body })
            return reject(error)
          } else {
            return resolve(body)
          }
        })
      })
    })
  }
}

export default ApiClient
