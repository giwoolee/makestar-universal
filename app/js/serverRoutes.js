import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import IndexContainer from './containers/index/IndexContainer'
import PollsContainer from './containers/poll/index/PollsContainer'
import PollContainer from './containers/poll/show/PollContainer'
import ContentsContainer from './containers/content/index/ContentsContainer'
import ContentContainer from './containers/content/show/ContentContainer'
import ProjectsContainer from './containers/project/index/ProjectsContainer'
import AboutPageContainer from './containers/about/AboutPageContainer'
import StarMessageContainer from './containers/main/StarMessageContainer'
import App from './containers/App'

export default () => {
  return (
    <Router>
      <Route path="/" component={App}>
        <IndexRoute component={IndexContainer} />
        <Route path="/polls" component={PollsContainer}/>
        <Route path="/polls/:idx" component={PollContainer}/>
        <Route path="/contents/:resource" component={ContentsContainer}/>
        <Route path="/contents/:resource/:idx" component={ContentContainer}/>
        <Route path="/projects" component={ProjectsContainer}/>
        <Route path="/about/:section" component={AboutPageContainer}/>
        <Route path="/main/:section" component={StarMessageContainer}/>
      </Route>
    </Router>
  )
}
