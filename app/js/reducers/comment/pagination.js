import { COMMENT_FETCH_SUCCESS } from '../../constants/comment/index'

const pagination = (state={},action) => {
  switch(action.type) {
    case COMMENT_FETCH_SUCCESS:
      return action.result.pagination
    default:
      return state
  }
}

export default pagination

