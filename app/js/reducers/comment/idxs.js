import { COMMENT_FETCH_SUCCESS, COMMENT_CLEAR, COMMENT_CREATE_SUCCESS } from '../../constants/comment/index'
import { union } from 'lodash'

const idxs = (state=[], action) => {
  switch(action.type) {
    case COMMENT_FETCH_SUCCESS:
      return union(state,action.result.content)
    case COMMENT_CREATE_SUCCESS:
      return union([action.result.content],state)
    case COMMENT_CLEAR:
      return []
    default:
      return state
  }
}

export default idxs

