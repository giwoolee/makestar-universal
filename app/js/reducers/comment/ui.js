import { without, union } from 'lodash'
import { combineReducers } from 'redux'
import {
  COMMENT_UI_REPLY_OPEN,
  COMMENT_UI_REPLY_CLOSE,
  COMMENT_UI_EDIT_OPEN,
  COMMENT_UI_EDIT_CLOSE,
  COMMENT_UI_DELETE_PROMPT_CLOSE,
  COMMENT_UI_DELETE_PROMPT_OPEN
} from '../../constants/comment/index'

const editOpen = (state = { comments: [], replies: [] },action) => {
  switch(action.type) {
    case COMMENT_UI_EDIT_CLOSE:
      if(action.payload.type === 'comments') return Object.assign({}, state, { comments: without(state.comments,action.payload.idx) })
      if (action.payload.type === 'replies') return Object.assign({}, state, { replies: without(state.replies,action.payload.idx) })
    case COMMENT_UI_EDIT_OPEN:
      if(action.payload.type === 'comments') return Object.assign({}, state, { comments: union(state.comments,[action.payload.idx]) })
      if(action.payload.type === 'replies') return Object.assign({}, state, { replies: union(state.replies,[action.payload.idx]) })
    default:
      return state
  }
}

const replyOpen = (state = [], action) => {
  switch(action.type) {
    case COMMENT_UI_REPLY_OPEN:
      return union(state,[action.payload.idx])
    case COMMENT_UI_REPLY_CLOSE:
      return without(state,action.payload.idx)
    default:
      return state
  }
}

const deletePrompt = (state={ show: false, idx: undefined, type: undefined }, action) => {
  switch(action.type) {
    case COMMENT_UI_DELETE_PROMPT_OPEN:
      return Object.assign({},{ show: true, idx: action.payload.idx, type: action.payload.type })
    case COMMENT_UI_DELETE_PROMPT_CLOSE:
      return Object.assign({},{ show: false, idx: undefined, type: undefined })
    default:
      return state
  }
}

const ui = combineReducers({
  deletePrompt,
  editOpen,
  replyOpen
})


export default ui

