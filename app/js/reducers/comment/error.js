import { COMMENT_FETCH_FAILURE, COMMENT_CREATE_FAILURE } from '../../constants/comment/index'

const error = (state=null,action) => {
  switch(action.type) {
    case COMMENT_CREATE_FAILURE:
      return action.error
    case COMMENT_FETCH_FAILURE:
      return action.error
    default:
      return state
  }
}

export default error

