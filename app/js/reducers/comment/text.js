import * as textTypes from '../../constants/comment/index'
import { omit } from 'lodash'
import { combineReducers } from 'redux'

export const text = (type,actions) => {
  const [UPDATE,CLEAR,CREATE_SUCCESS] = actions
  return (state = {},action) => {
    switch(action.type) {
      case UPDATE:
        return Object.assign({},state,{ [action.idx]: action.text })
      case CLEAR:
      case CREATE_SUCCESS:
        return omit(state,[action.idx])
      default:
        return state
    }
  }
}
const reducer = combineReducers({
  comments: text('comment',[textTypes.COMMENT_TEXT_UPDATE,textTypes.COMMENT_CLEAR,textTypes.COMMENT_CREATE_SUCCESS]),
  replies: text('reply',[textTypes.REPLY_TEXT_UPDATE,textTypes.REPLY_CLEAR,textTypes.REPLY_CREATE_SUCCESS])
})

export default reducer
