import { combineReducers } from 'redux'
import idxs from './idxs'
import isFetching from './isFetching'
import pagination from './pagination'
import comments from './comments'
import ui from './ui'

const reducer = combineReducers({
  ui,
  idxs,
  comments,
  pagination,
  isFetching
})

export default reducer

