import * as types from '../../../constants/comment/index'
import { concat, uniqBy, reduce, uniq, at, map, chain, set, keyBy } from 'lodash'

export default function replies (state = { error: undefined },action) {
  switch(action.type) {
    case types.COMMENT_FETCH_SUCCESS:
      return Object.assign({}, state, action.entities.reply)
    case types.REPLY_FETCH_SUCCESS:
    case types.REPLY_CREATE_SUCCESS:
    case types.REPLY_UPDATE_SUCCESS:
    case types.REPLY_DELETE_SUCCESS:
      return Object.assign({},state, action.entities.comment)
    case types.REPLY_CLEAR:
      return {}
    default:
      return state
  }
}
