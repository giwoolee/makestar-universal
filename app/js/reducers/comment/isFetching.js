import { COMMENT_FETCH_REQUEST, COMMENT_FETCH_SUCCESS, COMMENT_FETCH_FAILURE, COMMENT_CREATE_REQUEST, COMMENT_CREATE_SUCCESS, COMMENT_CREATE_FAILURE } from '../../constants/comment/index'

const isFetching = (state=false,action) => {
  switch(action.type) {
    case COMMENT_CREATE_REQUEST:
    case COMMENT_FETCH_REQUEST:
      return true
    case COMMENT_CREATE_SUCCESS:
    case COMMENT_FETCH_SUCCESS:
      return false
    case COMMENT_CREATE_FAILURE:
    case COMMENT_FETCH_FAILURE:
      return false
    default:
      return state
  }
}

export default isFetching
