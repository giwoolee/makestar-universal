import { COMMENT_FETCH_SUCCESS, COMMENT_CREATE_SUCCESS, COMMENT_UPDATE_SUCCESS, REPLY_FETCH_SUCCESS, REPLY_CREATE_SUCCESS, COMMENT_DELETE_SUCCESS } from '../../constants/comment/index'
import { union, set } from 'lodash'

const comments = (state = {}, action) => {
  switch(action.type) {
    case COMMENT_FETCH_SUCCESS:
    case COMMENT_CREATE_SUCCESS:
    case COMMENT_UPDATE_SUCCESS:
    case COMMENT_DELETE_SUCCESS:
      return Object.assign({}, state, action.entities.comment)
    case REPLY_FETCH_SUCCESS:
      return Object.assign({}, set(state,action.idx,Object.assign({},state[action.idx],{ pagination: action.result.pagination, replies: { content: union(action.result.content,state[action.idx].replies.content) } })))
    case REPLY_CREATE_SUCCESS:
      return Object.assign({}, set(state,action.idx,Object.assign({},state[action.idx],{ replies: { content: union([action.result.content],state[action.idx].replies ? state[action.idx].replies.content : []) } })))
    default:
      return state
  }
}

export default comments
