import { INDEX_FETCH_SUCCESS, INDEX_TOP_NOTICE_CLOSE } from '../../constants/index/index'

export default function topNotice(state = null,action) {
  switch(action.type) {
    case INDEX_FETCH_SUCCESS:
      if(action.content.topNotice !== null) {
        return Object.assign({},action.content.topNotice,{ closed: false })
      } else {
        return null
      }
    case INDEX_TOP_NOTICE_CLOSE:
      return Object.assign({},state.topNotice, { closed: true })
    default:
      return state
  }
}
