import { PROJECT_UPDATE_INDEX_FETCH_REQUEST, PROJECT_UPDATE_INDEX_FETCH_SUCCESS, PROJECT_UPDATE_INDEX_FETCH_FAILURE } from '../../../constants/project/update/index'

export default function projectUpdates(state = { idxs: [], projectUpdates: {}, isFetching: true },action) {
  switch(action.type) {
    case PROJECT_UPDATE_INDEX_FETCH_REQUEST:
      return Object.assign({},state, { isFetching: true })
    case PROJECT_UPDATE_INDEX_FETCH_SUCCESS:
      return Object.assign({},state,{ projectUpdates: action.entities.projectUpdate, idxs: action.result.content, isFetching: false })
    case PROJECT_UPDATE_INDEX_FETCH_FAILURE:
      return Object.assign({},state,{ error: action.error, isFetching: false })
    default:
      return state
  }
}
