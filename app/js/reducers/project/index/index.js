import { PROJECT_INDEX_FETCH_REQUEST, PROJECT_INDEX_FETCH_SUCCESS, PROJECT_INDEX_FETCH_FAILURE } from '../../../constants/project/index'
import { isEmpty } from 'lodash'


export default function projects(state = { projects: {}, idxs: [], isFetching: false, selectedStatus: 'All' }, action) {
  switch(action.type) {
    case PROJECT_INDEX_FETCH_REQUEST:
      return Object.assign({}, state, { isFetching: true, selectedStatus: isEmpty(action.query) ? 'All' : action.query.status })
    case PROJECT_INDEX_FETCH_SUCCESS:
      return Object.assign({}, state, { isFetching: false, idxs: action.result.content, projects: action.entities.project })
    case PROJECT_INDEX_FETCH_FAILURE:
      return Object.assign({},state,{ isFetching: false, error: action.error })
    default:
      return state
  }
}
