import { SESSION_FETCH_REQUEST, SESSION_FETCH_SUCCESS, SESSION_FETCH_FAILURE, SESSION_LOGIN_SUCCESS } from '../../constants/session/index'

export default function session(state = { session: { isLoggedIn: false, locale: 'ko' } },action) {
  switch(action.type) {
    case SESSION_FETCH_SUCCESS:
    case SESSION_LOGIN_SUCCESS:
      return Object.assign({},state,{ session: action.content })
    case SESSION_FETCH_FAILURE:
      let { payload } = action.error
      return Object.assign({},state, { session: { isLoggedIn: false, locale: payload.content.locale } })
    default:
      return state
  }
}
