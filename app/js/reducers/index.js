import { combineReducers } from 'redux'
import polls from './poll/index'
import poll from './poll/show'
import session from './session/index'
import comments from './comment/index'
import contents from './content/index'
import content from './content/show'
import ui from './ui/index'
import pagination from './pagination/index'
import projects from './project/index'
import projectUpdates from './project/update/index'
import map from './map/index'
import banners from './banner/index'
import topNotice from './topNotice/index'
import text from './comment/text'
import replies from './comment/reply/index'
import { routerReducer } from 'react-router-redux'
import index from './index/index'
import starMessage from './main/starMessage'

const main = starMessage([
  'kim_woo_bin',
  'bangtan_boys',
  'kim_jong_kook_haha',
  'lee_kwang_soo',
  't_ara',
  'joo_won',
  'aoa',
  'jang_na_ra',
  'seo_gang_joon',
  'got7',
  'girls_day',
  'b1a4',
  'kang_nam',
  'april',
  'vixx',
  'g_friend',
  'btob',
  'lovelyz',
  'lim_ji_yeon',
  'junjin',
  'mamamoo',
  'ulala_session',
  'bigstar',
  'stellar',
  'ben',
  'myb',
  '2eyes',
  'jjarimongttang',
  'unicorn',
  'sonamoo'
])

const rootReducer = combineReducers({
  routing: routerReducer,
  polls: polls,
  poll: poll,
  session: session,
  comments,
  index,
  text,
  replies,
  contents: contents,
  content: content,
  ui: ui,
  projects,
  projectUpdates,
  pagination: pagination,
  banners,
  topNotice,
  map,
  main
})

export default rootReducer
