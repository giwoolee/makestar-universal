import { POLL_VOTE_FAILURE,POLL_VOTE_CLOSED, POLL_VOTE_UNAVAILABLE } from '../../constants/poll/show/index'
import { COMMENT_CREATE_FAILURE } from '../../constants/comment/index'
import { UI_ERROR_MODAL_RESET, UI_NOTIFICATION_RESET, UI_OPEN_TOP_NAV, UI_CLOSE_TOP_NAV } from '../../constants/ui/index'
import { SESSION_LOGIN_FAILURE, SESSION_LOGIN_SUCCESS } from '../../constants/session/index'
import { GLOBAL_INITIAL_LOAD } from '../../constants/global/index'

export default function ui(state = { modal: { error: undefined, status: undefined, type: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false, isInitialLoad: true, comments: { deletePromptShow: undefined } },action) {
  switch(action.type) {
    case POLL_VOTE_FAILURE:
    case COMMENT_CREATE_FAILURE:
    case SESSION_LOGIN_FAILURE:
      return Object.assign({},state, { modal: { error: true, status: action.error.status, type: action.type } })
    case UI_ERROR_MODAL_RESET:
      return Object.assign({}, state, { modal: { error: undefined, status: undefined, type: undefined } })
    case SESSION_LOGIN_SUCCESS:
      return Object.assign({},state, { modal: { error: undefined, status: undefined, type: action.type }, notification: { show: true, type: action.type } })
    case UI_NOTIFICATION_RESET:
      return Object.assign({},state, { notification: { show: undefined, type: undefined } })
    case POLL_VOTE_CLOSED:
      return Object.assign({},state, { notification: { show: true, type: action.type }})
    case POLL_VOTE_UNAVAILABLE:
      return Object.assign({},state, { notification: { show: true, type: action.type }})
    case UI_OPEN_TOP_NAV:
      return Object.assign({},state, { topNavOpen: true })
    case UI_CLOSE_TOP_NAV:
      return Object.assign({},state, { topNavOpen: false })
    case GLOBAL_INITIAL_LOAD:
      return Object.assign({},state, { isInitialLoad: false })
    default:
      return state
  }
}
