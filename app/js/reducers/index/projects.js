import { INDEX_FETCH_SUCCESS } from '../../constants/index/index'

export default (state = [],action) => {
  switch(action.type) {
    case INDEX_FETCH_SUCCESS:
      return Object.assign({},state,{ projects: action.content.projects })
    default:
      return state
  }
}

