import { combineReducers } from 'redux'
import contents from './contents'
import polls from './polls'
import projects from './projects'
import * as types from '../../constants/index/index'

const isFetching = (state = false,action) => {
  switch(action.type) {
    case types.INDEX_FETCH_REQUEST:
      return true
    case types.INDEX_FETCH_FAILURE:
      return false
    case types.INDEX_FETCH_SUCCESS:
      return false
    default:
      return state
  }
}

const error = (state = null,action) => {
  switch(action.type) {
    case types.INDEX_FETCH_FAILURE:
      return action.error
    default:
      return state
  }
}


const index = combineReducers({
  isFetching,
  error,
  contents,
  polls,
  projects
})

export default index
