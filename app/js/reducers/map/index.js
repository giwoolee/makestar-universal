import { INDEX_FETCH_SUCCESS } from '../../constants/index/index'

export default (state = { countries: [] },action) => {
  switch(action.type) {
    case INDEX_FETCH_SUCCESS:
      return Object.assign({},state, { countries: action.content.map })
    default:
      return state
  }
}
