import { MAIN_STAR_MESSAGE_SHOW_VIDEO } from '../../constants/main/starMessage'
import { LOCATION_CHANGE } from 'react-router-redux'
import { map } from 'lodash'

const main = (artists = []) => {
  const initialState = map(artists,(v,k) => {
    return {
      id: k,
      artist: v,
      showVideo: false
    }
  })
  return (state = initialState, action) => {
    switch(action.type) {
      case MAIN_STAR_MESSAGE_SHOW_VIDEO:
        return map(state,(v,k) => {
          if(v.artist === action.artist) {
            return {
              id: k,
              artist: v.artist,
              showVideo: true
            }
          } else {
            return v
          }
        })
      case LOCATION_CHANGE:
        return map(state,(v,k) => {
          return {
            id: k,
            artist: v.artist,
            showVideo: false
          }
        })
      default:
        return state
    }
  }
}

export default main
