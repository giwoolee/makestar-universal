import { CONTENT_INDEX_FETCH_SUCCESS } from '../../constants/content/index'
import { PROJECT_INDEX_FETCH_SUCCESS } from '../../constants/project/index'
import { POLL_INDEX_FETCH_SUCCESS } from '../../constants/poll/index'
import { PROJECT_UPDATE_INDEX_FETCH_SUCCESS } from '../../constants/project/update/index'

export default function pagination(state={ isLast: undefined, currentPage: undefined, totalElements: undefined, totalElements: undefined, totalPages: undefined }, action) {
  switch(action.type) {
    case CONTENT_INDEX_FETCH_SUCCESS:
    case PROJECT_INDEX_FETCH_SUCCESS:
    case POLL_INDEX_FETCH_SUCCESS:
      return Object.assign({}, state, action.result.pagination)
    case PROJECT_UPDATE_INDEX_FETCH_SUCCESS:
      return Object.assign({}, state, action.pagination)
    default:
      return state
  }
}