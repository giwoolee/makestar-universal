import { INDEX_FETCH_SUCCESS } from '../../constants/index/index'

export default (state = { banners: {} },action) => {
  switch(action.type) {
    case INDEX_FETCH_SUCCESS:
      return Object.assign({},state,{ banners: action.content.banners })
    default:
      return state
  }
}
