import { find, pick, values, first } from 'lodash'
import { POLL_SHOW_FETCH_REQUEST, POLL_SHOW_FETCH_SUCCESS, POLL_SHOW_FETCH_FAILURE, POLL_RESET_STATUS } from '../../../constants/poll/show'


export default function poll(state = { idx: undefined, isFetching: false, error: undefined },action) {
  switch(action.type) {
    case POLL_SHOW_FETCH_REQUEST:
      return Object.assign({},state,{ isFetching: true })
    case POLL_SHOW_FETCH_SUCCESS:
      return Object.assign({},state,{ isFetching: false, idx: action.result.content })
    case POLL_SHOW_FETCH_FAILURE:
      return Object.assign({},state,{ isFetching: false, error: action.error })
    default:
      return state
  }
}
