import { POLL_INDEX_FETCH_REQUEST, POLL_INDEX_FETCH_SUCCESS, POLL_INDEX_FETCH_FAILURE } from '../../../constants/poll/index'
import { POLL_SHOW_FETCH_SUCCESS, POLL_VOTE_SUCCESS } from '../../../constants/poll/show'
import { isEmpty } from 'lodash'

export default function polls(state = { polls: {}, idxs: [], ranks: {}, isFetching: false, selectedStatus: 'All' }, action) {
  switch(action.type) {
    case POLL_INDEX_FETCH_REQUEST:
      return Object.assign({},state,{ isFetching: true, selectedStatus: isEmpty(action.query) ? 'All' : action.query.status })
    case POLL_INDEX_FETCH_SUCCESS:
      return Object.assign({},state,{ isFetching: false, idxs: action.result.content, polls: action.entities.poll, ranks: action.entities.rank })
    case POLL_INDEX_FETCH_FAILURE:
      return Object.assign({}, state, { isFetching: false, error: action.error })
    case POLL_SHOW_FETCH_SUCCESS:
      return Object.assign({}, state, { polls: Object.assign({},state.polls,action.entities.poll), ranks: Object.assign({}, state.ranks, action.entities.rank) })
    case POLL_VOTE_SUCCESS:
      return Object.assign({}, state, { ranks: Object.assign({},state.ranks,action.entities.rank), isFetching: false })
    default:
      return state
  }
}