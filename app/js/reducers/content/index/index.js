import { CONTENT_INDEX_FETCH_REQUEST, CONTENT_INDEX_FETCH_SUCCESS, CONTENT_INDEX_FETCH_FAILURE } from '../../../constants/content/index'
import { CONTENT_SHOW_FETCH_SUCCESS } from '../../../constants/content/show'
import { concat, union, uniqBy } from 'lodash'

export default (state = { contents: {}, idxs: [], isFetching: false },action) => {
  switch(action.type) {
    case CONTENT_INDEX_FETCH_REQUEST:
      return Object.assign({},state,{ isFetching: true })
    case CONTENT_INDEX_FETCH_SUCCESS:
      return Object.assign({},state,{ contents: Object.assign({},state.contents, action.entities.content), idxs: union(state.idxs,action.result.content), isFetching: false })
    case CONTENT_INDEX_FETCH_FAILURE:
      return Object.assign({},state,{ error: action.error, isFetching: false })
    case CONTENT_SHOW_FETCH_SUCCESS:
      return Object.assign({},state,{ contents: Object.assign({}, state.contents, action.entities.content) })
    default:
      return state
  }
}
