import { CONTENT_SHOW_FETCH_REQUEST, CONTENT_SHOW_FETCH_SUCCESS, CONTENT_SHOW_FETCH_FAILURE } from '../../../constants/content/show/'

export default (state = { idx: undefined, isFetching: false, error: undefined },action) => {
  switch(action.type) {
    case CONTENT_SHOW_FETCH_REQUEST:
      return Object.assign({},state,{ isFetching: true })
    case CONTENT_SHOW_FETCH_SUCCESS:
      return Object.assign({},state,{ isFetching: false, idx: action.result.content })
    case CONTENT_SHOW_FETCH_FAILURE:
      return Object.assign({},state,{ isFetching: false, error: action.error })
    default:
      return state
  }
}
