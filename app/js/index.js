import 'babel-polyfill'
import 'normalize.css/normalize.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css'
import '../styles/main.scss'
import React from 'react'
import superagent from 'superagent'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { browserHistory, match, Router } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { trigger } from 'redial'
import { isEmpty } from 'lodash'
import configureStore from './store/configureStore'
import ApiClient from './helpers/ApiClient'
import { AppContainer } from 'react-hot-loader'
import routes from './clientRoutes'

const client = new ApiClient(superagent)
const initialState = window.__INITIAL_STATE__
export const store = configureStore(initialState,client,browserHistory)
const { dispatch, getState } = store
const history = syncHistoryWithStore(browserHistory,store)

function renderApp() {
  history.listen(location => {
    match({ routes, location }, (error, redirectLocation, renderProps) => {
      const locals = {
        path: renderProps.location.pathname,
        query: renderProps.location.query,
        params: renderProps.params,
        dispatch,
        getState
      }
      const { components } = renderProps
      if(!isEmpty(window.__INITIAL_STATE__)) {
        delete window.__INITIAL_STATE__
        trigger('defer',components,locals)
        return render((
          <Provider store={store} key="provider">
            <Router history={ history } routes={routes}/>
          </Provider>),document.getElementById('root')
        )
      } else {
        trigger('fetch',components,locals)
        trigger('defer',components,locals)
        .then(() => { return trigger('done',components,locals) })
        window.scrollTo(0,0)
        return render((
          <Provider store={store}>
            <Router history={ history } routes={routes} />
          </Provider>),document.getElementById('root'))
      }
    })
  })
}

if(module.hot) {
  module.hot.accept('./reducers/index',() => {
    store.replaceReducer(require('./reducers/index').default)
  })

  module.hot.accept()
}

if(window.addEventListener) {
  window.addEventListener('load',renderApp, false)
} else if (window.attachEvent) {
  window.attachEvent('onload', renderApp)
} else {
  window.onload = renderApp()
}

