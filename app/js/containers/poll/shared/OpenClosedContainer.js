import { getPoll } from '../../../selectors/poll/index'
import { connect } from 'react-redux'
import moment from 'moment'
import OpenClosed from '../../../components/poll/shared/OpenClosed'
import { getLocale } from '../../../selectors/session/index'

export const mapStateToProps = (state, {idx}) => {
  const poll = getPoll(state, idx)

  if (poll) {    
    const {isEnd, startDate, endDate} = poll    
    return {
      isEnd,
      timeRemaining: {
        messageId: isEnd ? `${getLocale(state)}.poll.off` : `${getLocale(state)}.poll.on`,
        values: {
          days: isEnd ? undefined : Math.abs(moment().diff(moment(endDate),'days') - 1) 
        }
      }
    }    
  } else {
    return {
      isEnd: undefined,
      timeRemaining: {
        messageId: `${getLocale(state)}.poll.off`,
        values: {
          days: undefined
        }
      }
    }
  }
}

export default connect(mapStateToProps)(OpenClosed)