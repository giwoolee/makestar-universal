import { getPoll } from '../../../selectors/poll/index'
import { connect } from 'react-redux'
import Vote from '../../../components/poll/shared/Vote'

export const mapStateToProps = (state, {idx}) => {
  const poll = getPoll(state, idx)

  if (poll) {
    const { myVoteCount } = poll
    return {
      id: 'en.poll.voted',
      myVoteCount,
      iconClass: myVoteCount > 0 ? 'poll-vote-count-on' : 'poll-vote-count-off',
      textClass: myVoteCount === 0 ? 'poll-off-text' : 'poll-on-text'
    }
  } else {
    return {
      id: 'en.poll.voted',
      myVoteCount: 0,
      iconClass: 'poll-vote-count-off',
      textClass: 'poll-off-text'
    }
  }
}

export default connect(mapStateToProps)(Vote)