import { getPoll } from '../../../selectors/poll/index'
import { connect } from 'react-redux'
import Status from '../../../components/poll/shared/Status'

export const mapStateToProps = (state, {idx}) => {
  const poll = getPoll(state, idx)

  if (poll) {
    const { myVoteCount,  voteCount } = poll
    return {
      idx,
      myVoteCount,
      voteCount
    }
  } else {
    return {
      idx: undefined,
      myVoteCount: undefined,
      voteCount: undefined
    }
  }
}

export default connect(mapStateToProps)(Status)
