import VoteType from '../../../components/poll/shared/VoteType'
import { connect } from 'react-redux'
import { getPoll } from '../../../selectors/poll/index'
import { getLocale } from '../../../selectors/session/index'


export const mapStateToProps = (state, {idx}) => {
  const poll = getPoll(state, idx)

  const propsToObject = {
    'ONCE': {
      'className': 'poll-possible-votes-once',
      'messageId': `${getLocale(state)}.poll.voteOnce`
    },
    'RE1': {
      'className': 'poll-possible-votes-re1',
      'messageId': `${getLocale(state)}.poll.oneVoteOneHour`
    },
    'RE6': {
      'className': 'poll-possible-votes-re6',
      'messageId': `${getLocale(state)}.poll.oneVoteSixHours`
    },
    'RE12': {
      'className': 'poll-possible-votes-re12',
      'messageId': `${getLocale(state)}.poll.oneVoteTwelveHours`
    },
    'RE24': {
      'className':'poll-possible-votes-re24',
      'messageId': `${getLocale(state)}.poll.oneVoteOneDay`
    }
  }

  if (poll) {
    return propsToObject[poll.votingType]
  } else {
    return {
      className: undefined,
      messageId: `${getLocale(state)}.poll.oneVoteOneDay`
    }
  }
}

export default connect(mapStateToProps)(VoteType)
