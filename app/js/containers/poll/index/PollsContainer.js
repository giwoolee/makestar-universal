import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { provideHooks } from 'redial'
import Index from '../../../components/poll/index/Index'
import * as PollsActions from '../../../actions/poll/index'
import { getChunkedPollIdxs, getSelectedStatus, getPollIdxs } from '../../../selectors/poll/index'

export const mapStateToProps = state => {
  return {
    idxs: getChunkedPollIdxs(state),
    selectedStatus: getSelectedStatus(state) ? getSelectedStatus(state) : 'All',
    numOfPolls: getPollIdxs(state).length
  }
}

@provideHooks({
  fetch: ({ path, query, params, dispatch }) => {
    const promises = []
    promises.push(dispatch(PollsActions.fetchPolls({ pathname: path, query })))
    return Promise.all(promises)
  }
})

@connect(mapStateToProps)

export default class PollsContainer extends Component {
  render() {
    return(<Index {...this.props } />)
  }
}


