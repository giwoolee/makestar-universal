import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getPoll } from '../../../selectors/poll/index'
import { gaEvent } from '../../../actions/shared/ga/index'
import Poll from '../../../components/poll/index/Poll'

export const mapStateToProps = (state,ownProps) => {
  const poll = getPoll(state,ownProps.idx)
  if(poll) {
    return {
      ...poll
    }
  } else {
    return {
      rank: []
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ gaEvent },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Poll)

