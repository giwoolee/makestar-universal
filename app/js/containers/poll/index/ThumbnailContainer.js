import { connect } from 'react-redux'
import { getPoll } from '../../../selectors/poll/index'
import { getIsInitialLoad } from '../../../selectors/ui/index'
import Thumbnail from '../../../components/poll/index/Thumbnail'

export const mapStateToProps = (state,{ idx }) => {
  const poll = getPoll(state,idx)
  const isInitialLoad = getIsInitialLoad(state)

  return {
    thumbnailUrl: poll ? `url(${poll.thumbnailUrl})` : undefined,
    isInitialLoad
  }
}

export default connect(mapStateToProps)(Thumbnail)

