import { connect } from 'react-redux'
import { getPollRanks, getPollTopFiveRanks, getPollTopThreeRanks, getPollTopRank } from '../../../selectors/poll/index'
import RankImages from '../../../components/poll/index/RankImages'

export const mapStateToProps = (state,ownProps) => {
  const allRanks = getPollRanks(state,ownProps.idx)
  let ranks
  if (allRanks.length >= 5) {
    ranks = getPollTopFiveRanks(state,ownProps.idx)
  } else if (allRanks.length >= 3 && allRanks.length < 5) {
    ranks = getPollTopThreeRanks(state,ownProps.idx)
  } else if (allRanks.length < 3) {
    ranks = getPollTopRank(state,ownProps.idx)
  }
  return {
    ranks
  }
}

export default connect(mapStateToProps)(RankImages)

