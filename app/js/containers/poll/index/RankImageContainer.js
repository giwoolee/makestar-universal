import { connect } from 'react-redux'
import { getRankImageByIdx } from '../../../selectors/poll/index'
import { getIsInitialLoad } from '../../../selectors/ui/index'
import RankImage from '../../../components/poll/index/RankImage'

export const mapStateToProps = (state,{ index, length, idx }) => {
  const isInitialLoad = getIsInitialLoad(state)
  const rankImage = getRankImageByIdx(state,idx)
  let imageClass
  if(index < 2 && length === 5) {
    imageClass = 'col-xs-6 col-sm-6 col-md-6 col-lg-6 poll-rank-image-split-large'
  } else if(index >= 2 && length === 5) {
    imageClass = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 poll-rank-image-split-small'
  } else if(length === 3) {
    imageClass = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 poll-rank-image-3-way-split'
  } else {
    imageClass = 'col-xs-6 col-sm-6 col-md-6 col-lg-6 poll-rank-image-large'
  }

  return {
    backgroundImage: `url(${rankImage.imageUrl})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    imageClass
  }
}

export default connect(mapStateToProps)(RankImage)

