import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import i18n from '../../../i18n/index'
import { getLocale } from '../../../selectors/session/index'

export const mapStateToProps = (state,ownProps) => {
  const props = {
    title: 'Polls',
    meta: [
      {
        'name': 'description', 'content': i18n[getLocale(state)].share.ogDescription
      },
      {
        'name': 'og:description', 'content': i18n[getLocale(state)].share.ogDescription
      }
    ],
    link: [
      {
        'rel': 'canonical', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/polls`
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/polls?locale=ko`, 'hreflang':'ko'
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/polls?locale=ja`, 'hreflang':'ja'
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/polls?locale=zh`, 'hreflang':'zh'
      }
    ]
  }

  return props
}

export default connect(mapStateToProps)(Helmet)
