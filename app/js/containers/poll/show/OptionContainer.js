import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getIsLoggedIn } from '../../../selectors/session/index'
import { getRankImageByIdx, getPoll } from '../../../selectors/poll/index'
import { pollVote } from '../../../actions/poll/show/index'
import Option from '../../../components/poll/show/Option'

export const mapStateToProps = (state,{ idx, pollIdx }) => {
   const isLoggedIn = getIsLoggedIn(state)
   const rank = getRankImageByIdx(state,idx)
   const poll = getPoll(state,pollIdx)

   return {
     ...rank,
     isAvailable: poll.isAvailable,
     totalVoteCount: poll.voteCount,
     pollIdx: poll.idx,
     isEnd: poll.isEnd,
     isLoggedIn
   }
}

export const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ pollVote },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Option)

