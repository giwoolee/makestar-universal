import { connect } from 'react-redux'
import { getLocale } from '../../../selectors/session/index'
import { getPollSns } from '../../../selectors/poll/index'
import i18n from '../../../i18n/index'
import Helmet from 'react-helmet'

export const mapStateToProps = (state,ownProps) => {
  const { poll: { idx } } = state
  const meta = getPollSns(state,idx)
  if(meta) {
    const props = {
      title: meta.title,
      meta: [
        {
          'name': 'og:title', 'content': meta.title
        },
        {
          'name': 'description', 'content': i18n[getLocale(state)].share.ogDescription,
        },
        {
          'name': 'og:description', 'content': i18n[getLocale(state)].share.ogDescription,
        },
        {
          'name': 'og:image', 'content': meta.snsimageUrl
        }
      ],
      link: [
        {
          'rel': 'canonical', 'href': meta.canonical
        },
        {
          'rel': 'alternate',
          'href': `${meta.canonical}?locale=ko`,
          'hreflang': 'ko'
        },
        {
          'rel': 'alternate',
          'href': `${meta.canonical}?locale=ja`,
          'hreflang': 'ja'
        },
        {
          'rel': 'alternate',
          'href': `${meta.canonical}?locale=zh`,
          'hreflang': 'zh'
        }
      ]
    }
    return props
  } else {
    return {}
  }
}

export default connect(mapStateToProps)(Helmet)

