import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { provideHooks } from 'redial'
import * as CommentActions from '../../../actions/comment/index'
import * as PollActions from '../../../actions/poll/show'
import * as SnsActions from '../../../actions/shared/sns'
import Show from '../../../components/poll/show/Show'
import { getPoll, getPollRankTwoChunk } from '../../../selectors/poll/index'
import { getLocale } from '../../../selectors/session/index'

export const mapStateToProps = state => {
  const poll = getPoll(state,state.poll.idx)

  if(poll) {
    return {
      ...poll,
      ranks: getPollRankTwoChunk(state,state.poll.idx),
      locale: state.session.session.locale
    }
  } else {
    return {
      poll: {},
      ranks: [],
      locale: getLocale(state)
    }
  }
}

@provideHooks(
  {
    fetch: ({ path, query, params, dispatch, getState }) => {
      const promises = []
      const { idx } = params
      promises.push(dispatch(CommentActions.clear('comments')))
      promises.push(dispatch(CommentActions.clear('replies')))
      promises.push(dispatch(PollActions.pollFetch({ pathname: path, query })))
      promises.push(dispatch(CommentActions.fetch({ pathname: `${path}/comments`, type: 'comments', query })))

      return Promise.all(promises)
    }
  }
)

@connect(mapStateToProps,mapDispatchToProps)

export default class Poll extends Component {
  render() {
    return(<Show {...this.props}/>)
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},bindActionCreators(PollActions,dispatch),bindActionCreators(SnsActions,dispatch))
  }
}

