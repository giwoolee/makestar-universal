import React, { Component } from 'react'
import 'velocity-react/lib/velocity-animate-shim'
import { connect } from 'react-redux'
import { provideHooks } from 'redial'
import * as SessionActions from '../actions/session/index'
import TopNavContainer from './shared/TopNavContainer'
import { IntlProvider, addLocaleData } from 'react-intl'
import Footer from '../containers/shared/Footer'
import Helmet from 'react-helmet'
import NotificationContainer from './shared/NotificationContainer'
import i18n from '../i18n'
import flatten from 'flat'
import { GA_RENDER_COMPLETE } from '../constants/shared/ga/index'
import { GLOBAL_INITIAL_LOAD } from '../constants/global/index'
import en from 'react-intl/locale-data/en'
import ja from 'react-intl/locale-data/ja'
import ko from 'react-intl/locale-data/ko'
import zh from 'react-intl/locale-data/zh'

addLocaleData(en)
addLocaleData(ja)
addLocaleData(ko)
addLocaleData(zh)

@provideHooks({
  fetch: ({ query, params, dispatch }) => {
    const promises = []
    promises.push(dispatch(SessionActions.fetchSession({ pathname: '/user/me', query: { locale: query.locale } })))
    promises.push(dispatch({ type: GLOBAL_INITIAL_LOAD }))
    return Promise.all(promises)
  },
  done: ({ dispatch }) => dispatch({ type: GA_RENDER_COMPLETE })
})

@connect(mapStateToProps)

export default class App extends Component {
  render() {
    const { children, locale, messages, prefixed } = this.props
    return (
      <IntlProvider messages={ flatten(Object.assign({},messages,prefixed)) } locale={ locale }>
        <app>
          <Helmet titleTemplate="Makestar : %s" />
          <TopNavContainer/>
          <NotificationContainer/>
          { children }
          <Footer/>
        </app>
      </IntlProvider>
    )
  }
}

function messages(state) {
  const { locale } = state.session.session
  return i18n[locale]
}

function mapStateToProps(state) {
  return {
    messages: messages(state),
    prefixed: i18n,
    locale: state.session.session.locale
  }
}
