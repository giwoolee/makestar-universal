import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import i18n from '../../../i18n/index'
import { getLocale } from '../../../selectors/session/index'

export const mapStateToProps = (state,ownProps) => {
  const props = {
    title: 'Projects',
    meta: [
      {
        'name': 'og:title', 'content': 'Projects',
      },
      {
        'name': 'description', 'content': i18n[getLocale(state)].share.ogDescription
      },
      {
        'name': 'og:description', 'content': i18n[getLocale(state)].share.ogDescription
      }
    ],
    link: [
      {
        'rel': 'canonical', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/projects`
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/projects?locale=ko`, 'hreflang':'ko'
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/projects?locale=ja`, 'hreflang':'ja'
      },
      {
        'rel': 'alternate', 'href': `http://${typeof window === 'undefined' ? process.env.HOST_NAME : window.location.host}/projects?locale=zh`, 'hreflang':'zh'
      }
    ]
  }

  return props
}

export default connect(mapStateToProps)(Helmet)
