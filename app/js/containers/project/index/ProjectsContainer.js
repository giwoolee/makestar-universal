import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { provideHooks } from 'redial'
import Index from '../../../components/project/index/Index'
import * as ProjectsActions from '../../../actions/project/index'
import * as GaActions from '../../../actions/shared/ga'
import React, { Component } from 'react'
import { getChunkedProjectIdxs } from '../../../selectors/project/index'

export const mapStateToProps = state => {
  return {
    idxs: getChunkedProjectIdxs(state),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(GaActions,dispatch)
  }
}

@provideHooks({
  fetch: ({ path, query, params, dispatch }) => {
    const promises = []
    promises.push(dispatch(ProjectsActions.fetchProjects({ pathname: path, query })))
    return Promise.all(promises)
  }
})
@connect(mapStateToProps,mapDispatchToProps)
export default class Projects extends Component {
  render() {
    return(<Index {...this.props } />)
  }
}
