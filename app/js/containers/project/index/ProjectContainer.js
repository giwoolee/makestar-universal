import { getProject } from '../../../selectors/project/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { gaEvent } from '../../../actions/shared/ga/index'
import Project from '../../../components/project/index/Project'

export const mapStateToProps = (state,{ idx }) => {
  const project = getProject(state,idx)
  if(project) {
    return {
      ...project
    }
  } else {
    return {
      percent: 0,
      sumLocal: 0,
      timeRemaining: {},
      sum: {
        currency: 'USD'
      }
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ gaEvent },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Project)
