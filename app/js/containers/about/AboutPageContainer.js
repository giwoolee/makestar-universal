import { connect } from 'react-redux'
import AboutPage from '../../components/about/AboutPage'
import { getLocale } from '../../selectors/session/index'
import { getRouterPathname } from '../../selectors/router/index'

const mapStateToProps = state => {
  const locale = getLocale(state)
  const pathname = getRouterPathname(state)
  let page = pathname.split('/')[2]
  let html = (page === "corp" || page === "howto" ? `${locale}.aboutpage.${page}.html` : undefined)

  return {
    titleMessageId: `${locale}.aboutpage.${page}.title`,
    htmlMessageId: html,
    markdownMessageId: `${locale}.aboutpage.${page}.markdown`,
  }
}

export default connect(mapStateToProps)(AboutPage)
