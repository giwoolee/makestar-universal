import { slice, map, last, isEqual } from 'lodash'
import { bindActionCreators } from 'redux'
import * as ga from '../../actions/shared/ga/index'
import { connect } from 'react-redux'
import Polls from '../../components/index/Polls'

export function mapStateToProps(state,ownProps) {
  const numToRender = ownProps.numToRender ? ownProps.numToRender : 3
  return {
    polls: map(slice(state.index.polls.polls,0,numToRender),(v,k,c) => { return Object.assign({},v,{ isLast: isEqual(last(c),v) }) }),
    isInitialLoad: state.ui.isInitialLoad
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(ga,dispatch) }
}

export default connect(mapStateToProps,mapDispatchToProps)(Polls)
