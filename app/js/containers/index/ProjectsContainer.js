import { connect } from 'react-redux'
import { chunk } from 'lodash'
import { bindActionCreators } from 'redux'
import * as ga from '../../actions/shared/ga/index'
import Projects from '../../components/index/Projects'

export function mapStateToProps(state,{ type }) {
  return {
    projects: chunk(state.index.projects.projects,3),
    type,
    isInitialLoad: state.ui.isInitialLoad
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ga,dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Projects)
