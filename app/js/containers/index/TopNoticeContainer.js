import { connect } from 'react-redux'
import { camelCase, chunk, reduce, first, last } from 'lodash'
import { bindActionCreators } from 'redux'
import * as ga from '../../actions/shared/ga/index'
import TopNotice from '../../components/index/TopNotice'

export function mapStateToProps(state) {
  const { topNotice } = state
  let stylesFromString
  if(topNotice !== null) {
    if(topNotice.backgroundCSS) {
      stylesFromString = topNotice.backgroundCSS.match(/([a-z]+)/g)
    } else {
      stylesFromString = []
    }
    let style = reduce(chunk(stylesFromString,2), (result,value) => {
      result[camelCase(first(value))] = last(value)
      return result
    },{})
    return {
      style,
      text: topNotice.text,
      iconImageUrl: topNotice.iconImageUrl,
      buttonText: topNotice.buttonText,
      idx: topNotice.idx,
      linkUrl: topNotice.linkUrl
    }
  } else {
    return {}
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ga,dispatch)
  }
}

export default connect(mapStateToProps)(TopNotice)
