import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { provideHooks } from 'redial'
import * as ProjectUpdateActions from '../../actions/project/update/index'
import * as IndexActions from '../../actions/index/index'
import * as ga from '../../actions/shared/ga/index'
import Index from '../../components/index/Index'
import { getLocale } from '../../selectors/session/index'

@provideHooks({
  fetch: ({ path, query, params, dispatch }) => {
    const promises = []
    promises.push(dispatch(IndexActions.fetchIndex({ pathname: '/index', query })))
    promises.push(dispatch(ProjectUpdateActions.fetchProjectUpdates({ pathname: '/projects/updates', query: {size: 12}, ga: false })))
    return Promise.all(promises)
  }
})

@connect(mapStateToProps,mapDispatchToProps)

export default class IndexContainer extends Component {
  render() {
    return(<Index {...this.props} />)
  }
}

export function mapStateToProps(state) {
  return {
    banners: state.banners.banners,
    isInitialLoad: state.ui.isInitialLoad,
    projects: state.index.projects.projects,
    countries: state.map.countries,
    projectUpdates: state.projectUpdates.projectUpdates,
    viewAllMessageId: `${getLocale(state)}.common.viewAll`,
    projectUpdateMessageId: `${getLocale(state)}.project.update`,
    projectTitleMessageId: `${getLocale(state)}.project.title`,
    pollUpdateMessageId: `${getLocale(state)}.poll.update`,
    contentLatestMessageId: `${getLocale(state)}.content.latest`
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ga,dispatch)
  }
}
