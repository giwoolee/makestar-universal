import ProjectUpdates from '../../components/index/ProjectUpdates'
import { connect } from 'react-redux'
import { getProjectUpdateIdxs } from '../../selectors/project/update/index'

export function mapStateToProps(state) {
  return {
    idxs: getProjectUpdateIdxs(state),
  }
}

export default connect(mapStateToProps)(ProjectUpdates)
