import { slice, last, map, isEqual } from 'lodash'
import { connect } from 'react-redux'
import Contents from '../../components/index/Contents'

export function mapStateToProps(state,ownProps) {
  const numToRender = ownProps.numToRender ? ownProps.numToRender : 3
  return {
    contents: map(slice(state.index.contents.contents,0,numToRender),(v,k,c) => { return Object.assign({},{ isLast: isEqual(last(c),v)},v) }),
    isInitialLoad: state.ui.isInitialLoad
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: { gaEvent: (gaData) => { return dispatch({ type: 'GA_EVENT', gaData }) } } }
}

export default connect(mapStateToProps,mapDispatchToProps)(Contents)
