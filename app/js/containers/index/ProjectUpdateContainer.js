import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  getProjectUpdateDate,
  getProjectUpdateTitle,
  getProjectUpdateWebLink,
  getProjectUpdateShortName,
  getProjectUpdateIconImageUrl,
  getProjectUpdateImageUrl
} from '../../selectors/project/update/index'
import { gaEvent } from '../../actions/shared/ga/index'
import { getLocale } from '../../selectors/session/index'
import ProjectUpdate from '../../components/index/ProjectUpdate'

export function mapStateToProps(state,{ idx }) {
  const locale = getLocale(state)
  const dateMessageId = `${locale}.project.date`
  return {
    date: getProjectUpdateDate(state,idx),
    title: getProjectUpdateTitle(state,idx),
    link: getProjectUpdateWebLink(state,idx),
    projectShortName: getProjectUpdateShortName(state,idx),
    iconImageUrl: getProjectUpdateIconImageUrl(state,idx),
    imageUrl: getProjectUpdateImageUrl(state,idx),
    dateMessageId
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ gaEvent },dispatch)
  }
}

export function mergeProps(stateProps,dispatchProps,ownProps) {
  const mergedProps = {
    actions: {
      onClick: () => {
        dispatchProps.actions.gaEvent({ category: 'Home Updates', action: 'View', label: stateProps.title })
      }
    }
  }

  return Object.assign({},stateProps,dispatchProps,mergedProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(ProjectUpdate)

