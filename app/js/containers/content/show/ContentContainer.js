import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { provideHooks } from 'redial'
import * as ContentActions from '../../../actions/content/show'
import * as CommentActions from '../../../actions/comment'
import * as SnsActions from '../../../actions/shared/sns'
import Show from '../../../components/content/show/Show'
import { getContent } from '../../../selectors/content/index'

export const mapStateToProps = state => {
  const content = getContent(state,state.content.idx)
  if(content) {
    return {
      ...content
    }
  } else {
    return {
      idx: undefined,
      subject: undefined,
      description: undefined,
      date: undefined,
      author: {
        name: undefined,
        description: undefined,
        imageUrl: undefined
      },
      textUrl: undefined
    }
  }
}

@provideHooks({
  fetch: ({ query, params: { idx }, dispatch, getState, path }) => {
    const state = getState()
    const promises = []
    promises.push(dispatch(CommentActions.clear('comments')))
    promises.push(dispatch(CommentActions.clear('replies')))
    promises.push(dispatch(ContentActions.contentFetch({ pathname: path, query })))
    promises.push(dispatch(CommentActions.fetch({ pathname: `${path}/comments`, type: 'comments', query })))
    return Promise.all(promises)
  }
})

@connect(mapStateToProps,mapDispatchToProps)

export default class Content extends Component {
  render() {
    return(<Show {...this.props}/>)
  }
}


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(SnsActions,dispatch)
  }
}
