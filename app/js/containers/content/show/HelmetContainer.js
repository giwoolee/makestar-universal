import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { getLocale } from '../../../selectors/session/index'
import { getContentSns } from '../../../selectors/content/index'

export const mapStateToProps = (state,ownProps) => {
  const { content: { idx } } = state
  const meta = getContentSns(state,idx)
  if(meta) {

    const props = {
      title: meta.title,
      meta: [
        {
          'name': 'description', 'content': meta.description
        },
        {
          'name': 'og:title', 'content': meta.title
        },
        {
          'name': 'og:description', 'content': meta.description
        },
        {
          'name': 'og:image', 'content': meta.snsimageUrl
        }
      ],
      link: [
        {
          'rel': 'canonical', 'href': meta.canonical
        },
        {
          'rel': 'alternate', 'href': `${typeof window === 'undefined' ? process.env.HOST_NAME :meta.canonical}?locale=ko`, 'hreflang': 'ko'
        },
        {
          'rel': 'alternate', 'href': `${typeof window === 'undefined' ? process.env.HOST_NAME :meta.canonical}?locale=ja`, 'hreflang': 'ja'
        },
        {
          'rel': 'alternate', 'href': `${typeof window === 'undefined' ? process.env.HOST_NAME :meta.canonical}?locale=zh`, 'hreflang': 'zh'
        }
      ]
    }
    return props
  } else {
    return {}
  }
}

export default connect(mapStateToProps)(Helmet)

