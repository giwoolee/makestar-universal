import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Index from '../../../components/content/index/Index'
import { pick } from 'lodash'
import { provideHooks } from 'redial'
import { getContentsIdxs, getContentIsFetching } from '../../../selectors/content/index'
import { getPaginationIsLast, getPaginationNextPage } from '../../../selectors/pagination/index'
import { getRouterPathname } from '../../../selectors/router/index'
import { getIsInitialLoad } from '../../../selectors/ui/index'
import { getLocale } from '../../../selectors/session/index'
import * as ContentsActions from '../../../actions/content/index'

export const mapStateToProps = (state) => {
  const locale = getLocale(state)
  return {
    idxs: getContentsIdxs(state),
    isLast : getPaginationIsLast(state),
    query: {
      page: getPaginationNextPage(state),
    },
    isInitialLoad: getIsInitialLoad(state),
    pathname: getRouterPathname(state),
    buttonMessageId: `${locale}.common.more`,
    isFetching: getContentIsFetching(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(ContentsActions,dispatch)
  }
}

@provideHooks({
  defer: ({ path, query, dispatch, getState }) => {
    const promises = []
    const isFetching = getContentIsFetching(getState())
    if(!isFetching) {
      promises.push(dispatch(ContentsActions.fetchContents({ pathname: path, query })))
    }
    return Promise.all(promises)
  }
})

@connect(mapStateToProps,mapDispatchToProps)

export default class Contents extends Component {
  render() {
    return(<Index {...this.props } />)
  }
}



