import { getContent } from '../../../selectors/content/index'
import { getIsInitialLoad } from '../../../selectors/ui/index'
import { connect } from 'react-redux'
import Content from '../../../components/content/index/Content'

export const mapStateToProps = (state,{ idx }) => {
  return {
    ...getContent(state,idx),
    isInitialLoad: getIsInitialLoad(state)
  }
}

export default connect(mapStateToProps)(Content)
