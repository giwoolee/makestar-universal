import { getLocale, getLocaleDisplayName } from '../../../selectors/session/index'

export const mapStateToProps = (state) => {
  return {
    locale: getLocale(state),
    localeDisplayName: getLocaleDisplayName(state)
  }
}

export const mergeProps = (stateProps,dispatchProps = {}) => {
  return Object.assign({},stateProps,{
    localeChange: (lang) => {
      if(typeof window !== 'undefined' && typeof document !== 'undefined') {
        window.location = `/change_locale.do?lang=${lang}&redirect_url=${encodeURIComponent(document.location)}`
      }
    }
  })
}

