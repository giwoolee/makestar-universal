import { connect } from 'react-redux'
import LocaleButton from '../../../components/shared/header/LocaleButton'
import { mapStateToProps, mergeProps } from './LocaleCommon'

export default connect(mapStateToProps,{},mergeProps)(LocaleButton)

