import { connect } from 'react-redux'
import LocaleDropdown from '../../../components/shared/header/LocaleDropdown'
import { mapStateToProps, mergeProps } from './LocaleCommon'

export default connect(mapStateToProps,{},mergeProps)(LocaleDropdown)

