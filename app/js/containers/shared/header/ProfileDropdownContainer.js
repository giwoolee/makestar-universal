import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getLocale, getUserNickname, getUserEmail, getUserProfileImageUrl } from '../../../selectors/session/index'
import ProfileDropdown from '../../../components/shared/header/ProfileDropdown'
import { gaEvent } from '../../../actions/shared/ga/index'

export const mapStateToProps = (state) => {
  return {
    email: getUserEmail(state),
    nickName: " " + getUserNickname(state),
    profileImageUrl: getUserProfileImageUrl(state),
    projectsMessageId: `${getLocale(state)}.topnav.projects`,
    profileMessageId: `${getLocale(state)}.topnav.profile`,
    logoutMessageId: `${getLocale(state)}.topnav.logout`
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ gaEvent },dispatch)
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  return Object.assign({},stateProps,{
    actions: {
      profileClick: () => { dispatchProps.actions.gaEvent({ category: 'Header', action: 'Profile' }) },
      fundingListClick: () => { dispatchProps.actions.gaEvent({ category: 'Header', action: 'My Pledges' }) },
      logoutClick: () => { dispatchProps.actions.gaEvent({ category: 'Account Activity', action: 'LogOut' }) },
    }
  })
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(ProfileDropdown)
