import { routerActions } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import segmentize from 'segmentize'
import { connect } from 'react-redux'
import Paginator from '../../components/shared/Paginator'
import * as gaActions from '../../actions/shared/ga/index'
import { getPagination, getPaginationIsLast, getPaginationCurrentPage, getPaginationNextPage, getPaginationTotalPages } from '../../selectors/pagination/index'
import { getRouter, getRouterPathname, getRouterQuery } from '../../selectors/router/index'

export function mapStateToProps(state,ownProps) {
  const { defaultQuery, centered } = ownProps
  const query = Object.assign({},defaultQuery,getRouterQuery(state))
  const { type } = ownProps

  return {
    ...segmentize({
      page: getPaginationNextPage(state),
      pages: getPaginationTotalPages(state),
      sidePages: 2,
      beginPages: 2,
      endPages: 2
    }),
    query,
    type,
    centered: centered !== undefined ? centered : true,
    pathname: ownProps.pathname ? ownProps.pathname : getRouterPathname(state)
  }
}

export function mapDispatchToProps(dispatch,ownProps) {
  if(ownProps.pushType === true) {
    return {
      changeLink: (url) => { dispatch(routerActions.push(url)) },
      ...bindActionCreators(gaActions,dispatch)
    }
  } else {
    return {
      changeLink: (url) => { ownProps.changeLink({ pathname: url }) },
      ...bindActionCreators(gaActions,dispatch)
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Paginator)

