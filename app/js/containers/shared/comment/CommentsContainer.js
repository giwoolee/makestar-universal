import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetch } from '../../../actions/comment/index'
import Comments from '../../../components/shared/comment/Comments'
import { getRouterPathname } from '../../../selectors/router/index'

export function mapStateToProps(state,{ idx }) {
  return {
    idxs: state.comments.idxs,
    idx,
    isLast: state.comments.pagination.isLast,
    nextPage: state.comments.pagination.isLast === true ? null : { pathname: `${getRouterPathname(state)}/comments`, type: 'comments', query: { page: state.comments.pagination.currentPage + 1 } },
    moreMessage: `${state.session.session.locale}.common.more`
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ fetch },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Comments)
