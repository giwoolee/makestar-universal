import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TextInput from '../../../components/shared/comment/TextInput'
import * as actions from '../../../actions/comment/index'
import { getIsLoggedIn, getLocale } from '../../../selectors/session/index'
import { getText, getTextLength, getTextIsOverLength } from '../../../selectors/text/index'
import { getRouterPathname } from '../../../selectors/router/index'

export function mapStateToProps(state,{ type, idx }) {
  const isLoggedIn = getIsLoggedIn(state)
  const locale = getLocale(state)
  const text = isLoggedIn ? getText(state,idx,type) : ''
  const textLength = isLoggedIn ? getTextLength(state,idx,type) : 0
  const isTextOverLength = getTextIsOverLength(state,idx,type)
  const basePathname = getRouterPathname(state)

  const data = { text }
  const textAreaProps = {
    disabled: isLoggedIn === false ? true : false,
    value: text
  }
  const inputBtnProps = {
    disabled: isLoggedIn === false || isTextOverLength === true  || textLength === 0 ? true : false,
  }
  const pathname = type === 'replies' ? `${basePathname}/comments/${idx}/replies` : `${basePathname}/comments`
  return {
    data,
    idx,
    pathname,
    isTextOverLength,
    textLength,
    textAreaProps,
    isLoggedIn,
    leaveComment: type === 'replies' ? `${locale}.comment.leaveReply` : `${locale}.comment.leaveComment`,
    inputBtnProps,
    loginPromptMessageId: `${locale}.comment.loginPrompt`,
    postMessageId: `${locale}.comment.post`,
    type
  }
}

const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    actions: bindActionCreators(actions,dispatch)
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  const actionProps = {
    actions: {
      submit: () => {
          return dispatchProps.actions.create({ type: stateProps.type, data: stateProps.data, idx: stateProps.idx, pathname: stateProps.pathname }).then(() => {
        if(stateProps.type === 'replies') return dispatchProps.actions.closeReply(stateProps.idx)
       })
      },
      promptLogin: () => { if(!stateProps.isLoggedIn) dispatchProps.actions.promptLogin() },
      updateText: (text) => { dispatchProps.actions.updateText({ text, idx: stateProps.idx, type: stateProps.type }) }
    }
  }
  return Object.assign({},stateProps,dispatchProps,actionProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(TextInput)

