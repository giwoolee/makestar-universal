import moment from 'moment'
import { connect } from 'react-redux'
import DateInfo from '../../../components/shared/comment/DateInfo'
import { getLocale } from '../../../selectors/session/index'
import {
  getCommentCreatedDate,
  getCommentUpdatedDate,
  getReplyCreatedDate,
  getReplyUpdatedDate,
  getCommentIsDeleted,
  getReplyIsDeleted
} from '../../../selectors/comment/index'

export const mapStateToProps = (state,{ idx, type }) => {
  let createdDate
  let updatedDate
  let isDeleted
  const locale = getLocale(state)
  if(type === 'comments') {
    createdDate = getCommentCreatedDate(state,idx)
    updatedDate = getCommentUpdatedDate(state,idx)
    isDeleted = getCommentIsDeleted(state, idx)
  } else {
    createdDate = getReplyCreatedDate(state,idx)
    updatedDate = getReplyUpdatedDate(state,idx)
    isDeleted = getReplyIsDeleted(state, idx)
  }
  const isEdited = moment(createdDate).isSame(updatedDate)

  if (isDeleted) {
    return {
      dateMessageId: `${locale}.comment.deletedOn`,
      date: updatedDate
    }
  } else {
    return {
      dateMessageId: isEdited ? `${locale}.comment.postedOn` : `${locale}.comment.editedOn`,
      date: updatedDate
    }
  }
}


export default connect(mapStateToProps)(DateInfo)
