import { connect } from 'react-redux'
import { getLocale } from '../../../selectors/session/index'
import { getComment, getReply } from '../../../selectors/comment/index'
import TextDisplay from '../../../components/shared/comment/TextDisplay'

export const mapStateToProps = (state,{ idx, type }) => {
  let comment
  if(type === 'comments') {
    comment = getComment(state,idx)
  } else {
    comment = getReply(state,idx)
  }
  return {
    commentDeletedMessageId: `${getLocale(state)}.comment.deleted`,
    isVisible: comment.status === 'Visible',
    text: comment.text
  }
}

export default connect(mapStateToProps)(TextDisplay)

