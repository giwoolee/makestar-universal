import DeleteButton from '../../../components/shared/comment/DeleteButton'
import { toggleDeletePrompt } from '../../../actions/comment/index'
import { bindActionCreators } from 'redux'
import { getComment, getReply } from '../../../selectors/comment/index'
import { connect } from 'react-redux'
import { getLocale, getIsLoggedIn, getUserIdx } from '../../../selectors/session/index'
import { getCommentUiDeletePromptIdx } from '../../../selectors/comment/ui'

export const mapStateToProps = (state,{ idx, type }) => {
  const deleteMessageId = `${getLocale(state)}.comment.delete`
  const isLoggedIn = getIsLoggedIn(state)
  const userIdx = getUserIdx(state)
  let comment
  if(type === 'comments') {
    comment = getComment(state,idx)
  } else {
    comment = getReply(state,idx)
  }
  if(comment) {
    const isOwner = comment.userIdx === userIdx
    const isDeleted = comment.status === 'Deleted'
    return {
      deleteMessageId,
      idx,
      type,
      isDisabled: getCommentUiDeletePromptIdx(state) === idx,
      isVisible: isLoggedIn === true && isOwner && !isDeleted
    }
  } else {
    return {
      deleteMessageId,
      idx,
      type,
      isDisabled: true,
      isVisible: false
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ toggleDeletePrompt },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(DeleteButton)
