import { connect } from 'react-redux'
import Replies from '../../../components/shared/comment/Replies'
import { bindActionCreators } from 'redux'
import { fetch } from '../../../actions/comment/index'
import { getRepliesIdxs, getReplyPagination } from '../../../selectors/comment/index'
import { getIsReplyOpen } from '../../../selectors/comment/ui'
import { getLocale } from '../../../selectors/session/index'
import { getRouterPathname } from '../../../selectors/router/index'


export function mapStateToProps(state,{ parentIdx }) {
  const idxs = getRepliesIdxs(state,parentIdx)
  const pagination = getReplyPagination(state,parentIdx)
  const isLoggedIn = state.session.session.isLoggedIn
  const pathname = getRouterPathname(state)
  const locale = getLocale(state)
  const isReplyOpen = getIsReplyOpen(state,parentIdx)
  if(idxs !== undefined) {
    return {
      idxs,
      isLast: pagination ? pagination.isLast : true,
      nextPage: pagination ? { idx: parentIdx, type: 'replies', pathname: `${pathname}/comments/${parentIdx}/replies`, query: { page: pagination.currentPage + 1 } } : undefined,
      moreMessage: `${locale}.common.more`,
      isLoggedIn,
      isReplyOpen,
      parentIdx
    }
  } else {
    return {
      idxs: [],
      isLast: true,
      isLoggedIn,
      moreMessage: `${locale}.common.more`,
      nextPage: undefined,
      isLoggedIn,
      isReplyOpen,
      parentIdx
    }
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators({ fetch },dispatch) }
}

export default connect(mapStateToProps,mapDispatchToProps)(Replies)

