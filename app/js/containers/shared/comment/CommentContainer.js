import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Comment from '../../../components/shared/comment/Comment'
import { getComment, getReplyPagination } from '../../../selectors/comment/index'
import { getLocale } from '../../../selectors/session/index'
import { getIsCommentEditOpen } from '../../../selectors/comment/ui'
import { getRouterPathname } from '../../../selectors/router/index'
import { omit,pick } from 'lodash'

export function mapStateToProps(state,{ idx }) {
  let comment = getComment(state,idx)

  if(!comment) {
    return {
      nextPage: undefined,
      isEditOpen: false,
      type: 'comments'
    }
  } else {
    const pagination = getReplyPagination(state,idx)
    const pathname = `${getRouterPathname(state)}/comments/${idx}/reply`
    const isEditOpen = getIsCommentEditOpen(state,idx)

    return {
      ...omit(comment,['replies']),
      isEditOpen,
      nextPage: !pagination || pagination.isLast ? undefined : {
        pathname,
        query: { page: pagination.currentPage + 1 },
        type: 'replies'
      },
      type: 'comments'
    }
  }
}

export default connect(mapStateToProps)(Comment)
