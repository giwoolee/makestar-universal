import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getIsReplyOpen } from '../../../selectors/comment/ui'
import { getIsLoggedIn, getLocale } from '../../../selectors/session/index'
import { promptLogin, openReply } from '../../../actions/comment/index'
import ReplyButton from '../../../components/shared/comment/ReplyButton'

export const mapStateToProps = (state,{ idx }) => {
  const isReplyOpen = getIsReplyOpen(state,idx)
  const isLoggedIn = getIsLoggedIn(state)
  const replyMessageId = `${getLocale(state)}.comment.reply`

  return {
    idx,
    replyMessageId,
    isReplyOpen,
    isLoggedIn
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ promptLogin, openReply },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ReplyButton)

