import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Reply from '../../../components/shared/comment/Reply'
import { fetch } from '../../../actions/comment/index'
import { getReply } from '../../../selectors/comment/index'
import { getIsReplyEditOpen } from '../../../selectors/comment/ui'
import { getUserIdx } from '../../../selectors/session/index'

export function mapStateToProps(state,{ idx }) {
  const reply = getReply(state,idx)
  const isEditOpen = getIsReplyEditOpen(state,idx)
  const userIdx = getUserIdx(state)
  return {
    ...reply,
    isEditOpen,
    isLoggedIn: state.session.session.isLoggedIn,
    isDeleted: reply.status === 'Deleted',
    isMine: reply.userIdx === userIdx,
    type: 'replies'
  }
}

export function mapDispatchToProps(dispatch,ownProps) {
  return {
    actions: bindActionCreators({ fetch },dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Reply)
