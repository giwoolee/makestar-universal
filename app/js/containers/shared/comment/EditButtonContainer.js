import { getLocale, getUserIdx } from '../../../selectors/session/index'
import { getComment, getReply } from '../../../selectors/comment/index'
import { getIsReplyEditOpen, getIsCommentEditOpen } from '../../../selectors/comment/ui'
import EditButton from '../../../components/shared/comment/EditButton'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { editOpen, editClose, updateText } from '../../../actions/comment/index'

export const mapStateToProps = (state,{ idx, type }) => {
  const locale = getLocale(state)
  const editMessageId = `${locale}.comment.edit`
  const cancelMessageId = `${locale}.common.cancel`
  let isVisible
  let text
  if (type === 'comments') {
    isVisible = getUserIdx(state) === getComment(state, idx).userIdx && getComment(state, idx).status === 'Visible'
    text = getComment(state, idx).text
  } else if (type === 'replies') {
    isVisible = getUserIdx(state) === getReply(state, idx).userIdx && getReply(state, idx).status === 'Visible'
    text = getReply(state, idx).text
  }

  return {
    editMessageId,
    cancelMessageId,
    idx,
    type,
    text,
    isVisible,
    isDisabled: getIsCommentEditOpen(state,idx) || getIsReplyEditOpen(state,idx)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ editOpen, updateText, editClose },dispatch)
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  const actionProps = {
    actions: {
      editOpen: () => {
        if(!stateProps.isDisabled) {
          dispatchProps.actions.editOpen({ idx: stateProps.idx, type: stateProps.type })
          dispatchProps.actions.updateText({ text: stateProps.text, idx: stateProps.idx, type: stateProps.type })
        } else {
          dispatchProps.actions.editClose({ idx: stateProps.idx, type: stateProps.type })
        }
      }
    }
  }
  return Object.assign({},stateProps,actionProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(EditButton)

