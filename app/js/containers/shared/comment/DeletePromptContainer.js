import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getLocale } from '../../../selectors/session/index'
import { getRouterPathname } from '../../../selectors/router/index'
import { getComment, getReply } from '../../../selectors/comment/index'
import { getCommentUiDeletePrompt } from '../../../selectors/comment/ui'
import { toggleDeletePrompt, del, editClose } from '../../../actions/comment/index'
import DeletePrompt from '../../../components/shared/comment/DeletePrompt'

export const mapStateToProps = (state,{ type }) => {
  const locale = getLocale(state)
  let pathname
  let comment
  const promptMessageId = `${locale}.comment.commentDeletePrompt`
  const confirmMessageId = `${locale}.common.confirm`
  const cancelMessageId = `${locale}.common.cancel`
  const uiState = getCommentUiDeletePrompt(state)

  if(type === 'comments') {
    comment = getComment(state,uiState.idx)
    pathname = comment ? `${getRouterPathname(state)}/comments/${comment.idx}` : undefined
  } else {
    comment = getReply(state,uiState.idx)
    pathname = comment ? `${getRouterPathname(state)}/comments/${comment.parentIdx}/replies/${comment.idx}` : undefined
  }

  return {
    idx: comment ? comment.idx : undefined,
    type,
    show: uiState.show && uiState.type === type,
    pathname,
    promptMessageId,
    confirmMessageId,
    cancelMessageId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ toggleDeletePrompt, del, editClose },dispatch)
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  const actionProps = {
    actions: {
      confirm: () => {
        return dispatchProps.actions.del({ idx: stateProps.idx, type: stateProps.type, pathname: stateProps.pathname }).then(() => {
          dispatchProps.actions.toggleDeletePrompt({ show: true })
          dispatchProps.actions.editClose({ idx: stateProps.idx, type: stateProps.type })
        })
      },
      cancel: () => {
        dispatchProps.actions.toggleDeletePrompt({ show: stateProps.show })
      }
    }
  }

  return Object.assign({},stateProps,dispatchProps,actionProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(DeletePrompt)

