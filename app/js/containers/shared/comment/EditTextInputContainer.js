import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TextInput from '../../../components/shared/comment/TextInput'
import { updateText, update, promptLogin, editClose } from '../../../actions/comment/index'
import { getLocale, getIsLoggedIn } from '../../../selectors/session/index'
import { getText, getTextIsOverLength, getTextLength } from '../../../selectors/text/index'
import { getReply } from '../../../selectors/comment/index'
import { getRouterPathname } from '../../../selectors/router/index'

export const mapStateToProps = (state,{ idx, type }) => {
  const isLoggedIn = getIsLoggedIn(state)
  const locale = getLocale(state)
  const text = getText(state,idx,type)
  const textLength = getTextLength(state,idx,type)
  const isTextOverLength = getTextIsOverLength(state,idx,type)
  const basePathname = getRouterPathname(state)

  const data = { text }
  const textAreaProps = {
    disabled: isTextOverLength,
    value: text,
    message : undefined
  }

  const inputBtnProps = {
    disabled: textLength === 0 || isTextOverLength,
    message: `${locale}.comment.edit`
  }
  const pathname = type === 'replies' ? `${basePathname}/comments/${getReply(state,idx).parentIdx}/replies/${idx}` : `${basePathname}/comments/${idx}`
  const leaveComment = `${locale}.comment.editPost`
  const postMessageId = `${locale}.comment.post`
  return {
    data,
    idx,
    pathname,
    isTextOverLength,
    textLength,
    textAreaProps,
    isLoggedIn,
    postMessageId,
    leaveComment,
    inputBtnProps,
    type
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ update, updateText, editClose, promptLogin },dispatch)
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  const actionProps = {
    actions: {
      submit: () => {
        return dispatchProps.actions.update({ pathname: stateProps.pathname, idx: stateProps.idx, data: stateProps.data, type: stateProps.type })
        .then(() => {
          dispatchProps.actions.editClose({ idx: stateProps.idx, type: stateProps.type })
        })
      },
      updateText: (text) => {
        dispatchProps.actions.updateText({ idx: stateProps.idx, text, type: stateProps.type })
      }
    }
  }
  return Object.assign({},stateProps,dispatchProps,actionProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(TextInput)
