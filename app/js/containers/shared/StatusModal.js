import StatusModal from '../../components/shared/StatusModal'
import { resetStatus } from '../../actions/ui/index'
import { POLL_VOTE_FAILURE } from '../../constants/poll/show/index'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

function buildProps(modal,session) {
  let props
  const { type, status } = modal
  const { locale } = session
  if(type === POLL_VOTE_FAILURE && status === 403) {
    props = {
      titleMessageId: `${locale}.common.verifyFirstTitle`,
      bodyMessageId: `${locale}.common.verifyFirstMessage`,
      links: {
        primary: '/user/profile.do'
      },
      show: true
    }
  }
  return props
}

export function mapStateToProps(state) {
  const { ui: { modal }, session: { session } } = state
  return {
    ...buildProps(modal,session)
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(resetStatus,dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusModal)
