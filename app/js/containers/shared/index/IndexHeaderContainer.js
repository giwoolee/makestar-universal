import { connect } from 'react-redux'
import IndexHeader from '../../../components/shared/index/IndexHeader'
import { getLocale } from '../../../selectors/session/index'
import { getRouterPathname } from '../../../selectors/router/index'

export function mapStateToProps(state) {
  const pathname = getRouterPathname(state)
  const locale = getLocale(state)
  switch(pathname) {
    case '/polls':
      return { titleMessageId: `${locale}.poll.title`, subtitleMessageId: `${locale}.poll.subtitle` }
    case '/projects':
      return { titleMessageId: `${locale}.project.title`, subtitleMessageId: `${locale}.project.subtitle` }
    default:
      return { titleMessageId: undefined, subtitleMessageId: undefined }
  }
}

export default connect(mapStateToProps)(IndexHeader)

