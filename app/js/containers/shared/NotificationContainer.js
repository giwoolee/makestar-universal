import Notification from '../../components/shared/Notification'
import { resetNotification } from '../../actions/ui/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { SESSION_LOGIN_SUCCESS } from '../../constants/session/index'
import { POLL_VOTE_CLOSED, POLL_VOTE_UNAVAILABLE } from '../../constants/poll/show'
import { getPoll } from '../../selectors/poll/index'

export function mapStateToProps(state) {
  const poll = getPoll(state,state.poll.idx)
  const votingType = poll ? poll.votingType : undefined
  const { ui: { notification }, session: { session: { locale } } } = state
  let props
  if(notification.type === SESSION_LOGIN_SUCCESS) {
    props = {
      messageId: `${locale}.login.success`,
      show: notification.show,
      type: 'info'
    }
  } else if(notification.type === POLL_VOTE_UNAVAILABLE) {
    props = {
      messageId: votingType === 'ONCE' ? `${locale}.poll.voteUnavailable` : `${locale}.poll.voteLimitReached`,
      show: notification.show,
      type: 'alert'
    }
  } else if(notification.type === POLL_VOTE_CLOSED) {
    props = {
      messageId: `${locale}.poll.voteClosed`,
      show: notification.show,
      type: 'alert'
    }
  } else {
    props = {
      messageId: undefined,
      show: notification.show
    }
  }
  return props
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      resetNotification: bindActionCreators(resetNotification,dispatch)
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notification)

