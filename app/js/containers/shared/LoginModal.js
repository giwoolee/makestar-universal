import { resetStatus } from '../../actions/ui/index'
import { login, refreshContent } from '../../actions/session/index'
import { COMMENT_CREATE_FAILURE } from '../../constants/comment/index'
import { SESSION_LOGIN_FAILURE } from '../../constants/session/index'
import { POLL_VOTE_FAILURE } from '../../constants/poll/show/index'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import LoginModal from '../../components/shared/LoginModal'

function buildProps({ type, status },{ locale }) {
  let props
  const messages = {
      titleMessageId: `${locale}.common.loginFirst`,
      bodyMessageId: `${locale}.login.prompt`,
      emailMessageId: `${locale}.login.email`,
      passwordMessageId: `${locale}.login.password`,
      signUpMessageId: `${locale}.login.signUp`,
      closeMessageId: `${locale}.common.close`,
      loginMessageId: `${locale}.login.login`,
      failedMessageId: `${locale}.login.failed`
  }
  if (type === POLL_VOTE_FAILURE && status === 401){
    props = {
      ...messages,
      failed: false,
      links: {
        primary: '/login.do',
        secondary: '/signup.do'
      },
      type: 'poll',
      show: true
    }
  } else if(type === COMMENT_CREATE_FAILURE && status === 401) {
    props = {
      ...messages,
      failed: false,
      links: {
        primary: '/login.do',
        secondary: '/signup.do'
      },
      type: 'comment',
      show: true
    }
  } else if(type === SESSION_LOGIN_FAILURE && status === 400) {
    props = {
      ...messages,
      failed: true,
      links: {
        primary: '/login.do',
        secondary: '/signup.do'
      },
      type: 'session',
      show: true
    }
  }
  return props
}

export function mapStateToProps(state) {
  const { ui: { modal }, session: { session: locale } } = state
  return {
    ...buildProps(modal,locale)
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      resetStatus: bindActionCreators(resetStatus,dispatch),
      loginSubmit: bindActionCreators(login,dispatch),
      refreshContent: bindActionCreators(refreshContent,dispatch)
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginModal)
