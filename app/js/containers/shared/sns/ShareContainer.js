import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Share from '../../../components/shared/sns/Share'
import sns from '../../../helpers/sns/index'
import * as actions from '../../../actions/shared/sns/index'
import { getContentSns } from '../../../selectors/content/index'
import { getPollSns } from '../../../selectors/poll/index'
import { getLocale } from '../../../selectors/session/index'
import { reduce } from 'lodash'

export const mapStateToProps = (state,{ type }) => {
  const defaultProps = { locale: getLocale(state), idx: undefined, title: undefined, description: undefined, snsimageUrl: undefined, canonical: undefined }
  switch(type) {
    case 'polls':
      const pollSnsProps = getPollSns(state,state.poll.idx)
      if(pollSnsProps) {
        return { ...pollSnsProps, locale: getLocale(state) }
      } else {
        return defaultProps
      }
    case 'contents':
      const contentSnsProps = getContentSns(state,state.content.idx)
      if(contentSnsProps) {
        return { ...contentSnsProps, locale: getLocale(state) }
      } else {
        return defaultProps
      }
    default:
      return defaultProps
  }
}

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators(actions,dispatch) }
}

export const mergeProps = ({ idx, locale, title, description, snsimageUrl, canonical},{ actions },ownProps) => {
  return reduce(sns[locale],(result,v,k) => {
    result[k] = {
      icon: `share-${k}-icon`,
      key: k,
      onClick: () => {
        actions.snsShare({ idx, category: ownProps.type === 'polls' ? 'Poll Detail' : 'Content Detail', action: 'SNS', sns: k })
        window.open(v({ locale, title, description, snsimageUrl, canonical }),'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=900')
      }
    }
    return result
  },{})
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(Share)
