import { connect } from 'react-redux'
import { getLocale } from '../../selectors/session/index'
import Footer from '../../components/shared/Footer'

const mapStateToProps = (state) => {
  const locale = getLocale(state)
  return {
    aboutUsMessageId: `${locale}.footer.aboutUs`,
    partnershipMessageId: `${locale}.footer.partnership`,
    termsOfUseMessageId: `${locale}.footer.termsOfUse`,
    howToMessageId: `${locale}.footer.howTo`,
    enquiriesMessageId: `${locale}.footer.enquiries`,
    privacyPolicyMessageId: `${locale}.footer.privacyPolicy`,
    faqMessageId: `${locale}.footer.faq`,
    companyInfoMessageId: `${locale}.footer.companyInfo`,
    companyRegistrationNumberMessageId: `${locale}.footer.companyRegistrationNumber`,
    emailMessageId: `${locale}.footer.email`,
    phoneNumberMessageId: `${locale}.footer.phoneNumber`,
    salesLicenseMessageId: `${locale}.footer.salesLicense`,
    addressMessageId: `${locale}.footer.address`,
    link: locale === 'ko' ? 'http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1208824134' : undefined
  }
}

export default connect(mapStateToProps)(Footer)
