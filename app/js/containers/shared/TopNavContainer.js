import { bindActionCreators } from 'redux'
import TopNav from '../../components/shared/TopNav'
import { connect } from 'react-redux'
import * as SessionActions from '../../actions/session/index'
import * as gaActions from '../../actions/shared/ga/index'
import { push } from 'react-router-redux'
import { topNavToggle } from '../../actions/ui/index'
import { getLocale, getIsLoggedIn, getUserGrade } from '../../selectors/session/index'

export const mapStateToProps = state => {
  return {
    locale: getLocale(state),
    isLoggedIn: getIsLoggedIn(state),
    grade: getUserGrade(state),
    topNavOpen: state.ui.topNavOpen,
    confirmAccountMessageId: `${getLocale(state)}.topnav.confirmAccount`,
    projectMessageId: `${getLocale(state)}.topnav.projects`,
    contentMessageId: `${getLocale(state)}.topnav.contents`,
    eventMessageId: `${getLocale(state)}.topnav.events`,
    loginMessageId: `${getLocale(state)}.topnav.login`,
    signupMessageId: `${getLocale(state)}.topnav.signup`,
    pollMessageId: `${getLocale(state)}.topnav.polls`
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: Object.assign({},bindActionCreators(SessionActions,dispatch),bindActionCreators({ push },dispatch),bindActionCreators({topNavToggle},dispatch),bindActionCreators(gaActions,dispatch))
  }
}

export const mergeProps = (stateProps,dispatchProps) => {
  const mergeProps = {
    actions: {
      homeClick: (e) => {
        e.preventDefault()
        if(stateProps.topNavOpen) dispatchProps.actions.topNavToggle(stateProps.topNavOpen)
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Home' })
        dispatchProps.actions.push('/')
      },
      onToggle: () => {
        dispatchProps.actions.topNavToggle(stateProps.topNavOpen)
      },
      signUpClick: () => {
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Sign Up' })
      },
      loginClick: () => {
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Login' })
      },
      projectClick: (e) => {
        e.preventDefault()
        if(stateProps.topNavOpen) dispatchProps.actions.topNavToggle(stateProps.topNavOpen)
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Projects' })
        dispatchProps.actions.push('/projects')
      },
      contentClick: (e) => {
        e.preventDefault()
        if(stateProps.topNavOpen) dispatchProps.actions.topNavToggle(stateProps.topNavOpen)
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Magazine' })
        dispatchProps.actions.push('/contents/articles')
      },
      pollClick: (e) => {
        e.preventDefault()
        if(stateProps.topNavOpen) dispatchProps.actions.topNavToggle(stateProps.topNavOpen)
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Polls' })
        dispatchProps.actions.push('/polls')
      },
      eventClick: () => {
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Events' })
      },
      faqClick: () => {
        dispatchProps.actions.gaEvent({ category: 'Header', action: 'Faqs' })
      }
    }
  }
  return Object.assign({},stateProps,mergeProps)
}

export default connect(mapStateToProps,mapDispatchToProps,mergeProps)(TopNav)
