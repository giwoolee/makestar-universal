import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as gaActions from '../../../actions/shared/ga/index'
import Filter from '../../../components/shared/filter/Filter'
import { getLocale, getIsLoggedIn } from '../../../selectors/session/index'
import { getRouterPathname, getRouterQuery } from '../../../selectors/router/index'

export function mapStateToProps(state) {
  const pathname = getRouterPathname(state)
  const query = getRouterQuery(state)
  let { status } = query
  const isLoggedIn = getIsLoggedIn(state)
  const locale = getLocale(state)

  let messageId
  let filters

  if(!status) {
    status = 'All'
  }

  switch(pathname) {
    case '/polls':
      messageId = `${locale}.poll.filterLink`
      filters = [
        {
          filter: 'All',
          queryString: '/polls?status=All',
          selected: status === 'All' ? true : false,
          isLast: false,
          gaData: {
            category: 'Poll',
            action: 'Navigate',
            label: 'All'
          }
        },
        {
          filter: 'Open',
          queryString: '/polls?status=Open',
          selected: status === 'Open' ? true : false,
          isLast: false,
          gaData: {
            category: 'Poll',
            action: 'Navigate',
            label: 'Open'
          }
        },
        {
          filter: 'Closed',
          queryString: '/polls?status=Closed',
          selected: status === 'Closed' ? true : false,
          isLast: isLoggedIn === false ? true : false,
          gaData: {
            category: 'Poll',
            action: 'Navigate',
            label: 'Closed'
          }
        }
      ]
      if(isLoggedIn) {
        filters.push(
          {
            filter: 'Voted',
            queryString: '/polls?status=Voted',
            selected: status === 'Voted' ? true : false,
            isLast: true,
            gaData: {
              category: 'Poll',
              action: 'Navigate',
              label: 'Voted'
            }
          }
        )
      }
      return {
        messageId,
        filters
      }
    case '/projects':
      messageId = `${locale}.project.filterLink`
      filters = [
        {
          filter: 'All',
          queryString: '/projects?status=All',
          selected: status === 'All' ? true : false,
          isLast: false,
          gaData: {
            category: 'Project',
            action: 'Navigate',
            label: 'All'
          }
        },
        {
          filter: 'Open',
          queryString: '/projects?status=Opened',
          selected: status === 'Opened' ? true : false,
          isLast: false,
          gaData: {
            category: 'Project',
            action: 'Navigate',
            label: 'Open'
          }
        },
        {
          filter: 'Closed',
          queryString: '/projects?status=Closed',
          selected: status === 'Closed' ? true: false,
          isLast: true,
          gaData: {
            category: 'Project',
            action: 'Navigate',
            label: 'Closed'
          }
        }
      ]
      return {
        messageId,
        filters
      }
    default:
      return {
        messageId: undefined,
        filters: []
      }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(gaActions,dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Filter)
