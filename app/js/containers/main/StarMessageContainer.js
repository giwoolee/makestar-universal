import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getLocale } from '../../selectors/session/index'
import StarMessage from '../../components/main/StarMessage'
import { showVideo } from '../../actions/main/starMessage'
import { map } from 'lodash'

export const mapStateToProps = state => {
  const locale = getLocale(state)

  const artists = map(state.main,(v,k) => {
    return Object.assign({},v,{
      artistNameMessageId: `${locale}.main.starMessage.artists.${v.id}.artistName`,
      imageUrlMessageId: `${locale}.main.starMessage.artists.${v.id}.imageUrl`,
      videoUrlMessageId: `${locale}.main.starMessage.artists.${v.id}.videoUrl`
    })
  })

  return {
    artists,
    starMessageTitleMessageId: `${locale}.main.starMessage.starMessageTitle`,
    iFrameUrlMessageId: `${locale}.main.starMessage.iFrameUrl`
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ showVideo },dispatch)
  }
}

export const mergeProps = (mappedProps,dispatchProps) => {
  return Object.assign({},mappedProps,{
    artists: map(mappedProps.artists,(v) => {
      return Object.assign({},v,{
        playVideo: () => dispatchProps.actions.showVideo(v.artist)
      })
    })
  })
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(StarMessage)
