import * as types from '../../constants/session'

export function fetchSession({ pathname, query }) {
  return {
    types: [types.SESSION_FETCH_REQUEST,types.SESSION_FETCH_SUCCESS,types.SESSION_FETCH_FAILURE],
    promise: (client) => client.get(pathname,{ query })
  }
}

export function login({ pathname, data }) {
  return {
    types: [types.SESSION_LOGIN_REQUEST,types.SESSION_LOGIN_SUCCESS,types.SESSION_LOGIN_FAILURE],
    promise: (client) => client.post(pathname,{ data })
  }
}

export function refreshContent() {
  return {
    type: types.SESSION_LOGIN_REFRESH_CONTENT
  }
}
