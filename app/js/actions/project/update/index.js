import * as types from '../../../constants/project/update/'
import { receiveAll } from '../../../schemas/project/update/index'

export function fetchProjectUpdates({ pathname, query, ga }) {
  return {
    types: [types.PROJECT_UPDATE_INDEX_FETCH_REQUEST,types.PROJECT_UPDATE_INDEX_FETCH_SUCCESS,types.PROJECT_UPDATE_INDEX_FETCH_FAILURE],
    query,
    schema: receiveAll,
    promise: (client) => client.get(pathname,{ query })
  }
}
