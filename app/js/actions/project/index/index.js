import * as types from '../../../constants/project/index'
import { receiveAll } from '../../../schemas/project/index'

export function fetchProjects({ pathname, query, ga }) {
  return {
    types: [types.PROJECT_INDEX_FETCH_REQUEST,types.PROJECT_INDEX_FETCH_SUCCESS,types.PROJECT_INDEX_FETCH_FAILURE],
    query,
    schema: receiveAll,
    promise: (client) => client.get(pathname,{
      query
    })
  }
}
