import * as types from '../../constants/index/index'

export function fetchIndex({ pathname, query }) {
  return {
    types: [types.INDEX_FETCH_REQUEST,types.INDEX_FETCH_SUCCESS,types.INDEX_FETCH_FAILURE],
    query,
    promise: (client) => client.get(pathname,{ query })
  }
}

export function closeTopNotice() {
  return {
    type: types.INDEX_CLOSE_TOP_NOTICE
  }
}

