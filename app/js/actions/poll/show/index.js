import * as types from '../../../constants/poll/show'
import pathToRegexp from 'path-to-regexp'
import { filter } from 'lodash'
import { receiveOne } from '../../../schemas/poll/index'

function extractIds(pathPattern,pathname) {
  let keys = []
  let re = pathToRegexp(pathPattern,keys)
  return filter(re.exec(pathname),(v) => { return /^[^a-zA-Z\/]?[0-9]+/g.test(v) } )
}

export function pollVote({ pathname, isEnd, isAvailable }) {
  let [pollIdx, candidateIdx] = extractIds('/polls/:idx/candidates/:candidateIdx/vote',pathname)
  if(isEnd === false && isAvailable === false) {
    return {
      type: types.POLL_VOTE_UNAVAILABLE
    }
  } else if(isEnd === true && isAvailable === false){
    return {
      type: types.POLL_VOTE_CLOSED
    }
  } else {
    return {
      types: [types.POLL_VOTE_REQUEST,types.POLL_VOTE_SUCCESS,types.POLL_VOTE_FAILURE],
      promise: (client) => client.post(pathname),
      schema: receiveOne,
      gaData: {
        action: 'Vote',
        category: 'Poll Detail',
        label: pollIdx
      }
    }
  }
}

export function pollFetch({ pathname, query }) {
  return {
    types: [types.POLL_SHOW_FETCH_REQUEST,types.POLL_SHOW_FETCH_SUCCESS,types.POLL_SHOW_FETCH_FAILURE],
    schema: receiveOne,
    promise: (client) => client.get(pathname, { query })
  }
}
