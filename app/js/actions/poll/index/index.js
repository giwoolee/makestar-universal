import * as types from '../../../constants/poll/index'
import { receiveAll } from '../../../schemas/poll/index'

export function fetchPolls({ pathname, query, ga }) {
  return {
    types: [types.POLL_INDEX_FETCH_REQUEST,types.POLL_INDEX_FETCH_SUCCESS,types.POLL_INDEX_FETCH_FAILURE],
    query,
    schema: receiveAll,
    promise: (client) => client.get(pathname, {
      query
    })
  }
}

