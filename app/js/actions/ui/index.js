import * as types from '../../constants/ui/index'

export function resetStatus() {
  return {
    type: types.UI_ERROR_MODAL_RESET
  }
}

export function resetNotification() {
  return {
    type: types.UI_NOTIFICATION_RESET
  }
}

export function topNavToggle(topNavOpen) {
  return {
    type: topNavOpen === true ? types.UI_CLOSE_TOP_NAV : types.UI_OPEN_TOP_NAV
  }
}
