import * as types from '../../../constants/content/show'
import { receiveOne } from '../../../schemas/content/index'

export function contentFetch({ pathname, query }) {
  return {
    types: [types.CONTENT_SHOW_FETCH_REQUEST,types.CONTENT_SHOW_FETCH_SUCCESS,types.CONTENT_SHOW_FETCH_FAILURE],
    schema: receiveOne,
    promise: (client) => client.get(pathname,{ query })
  }
}
