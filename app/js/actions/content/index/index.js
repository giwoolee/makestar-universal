import * as types from '../../../constants/content/index'
import { receiveAll } from '../../../schemas/content/index'

export function fetchContents({ pathname, query, ga }) {
  return {
    types: [types.CONTENT_INDEX_FETCH_REQUEST,types.CONTENT_INDEX_FETCH_SUCCESS,types.CONTENT_INDEX_FETCH_FAILURE],
    query,
    schema: receiveAll,
    promise: (client) => client.get(pathname,{
      query
    })
  }
}
