import url from 'url'
import { SNS_SHARE, SNS_REFRESH } from '../../../constants/shared/sns'
import pathToRegexp from 'path-to-regexp'
import { tail } from 'lodash'

function mapResourceToCategory(resource) {
  const categories = {
    'polls': 'Poll Detail'
  }
  return categories[resource]
}

export function snsShare({ idx, category, action, sns }) {
  return {
    type: SNS_SHARE,
    gaData: {
      category,
      action: 'SNS',
      label: `${idx} ${sns}`
    }
  }
}

export function snsRefresh({ key,getState }) {
  const payload = getState()[key][key]
  return { type: SNS_REFRESH, key, content: payload }
}
