import { GA_EVENT } from '../../../constants/shared/ga/index'

export const gaEvent = (gaData) => {
  return {
    type: GA_EVENT,
    gaData
  }
}
