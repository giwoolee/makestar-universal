import pathToRegexp from 'path-to-regexp'
import * as types from '../../constants/comment/index'
import { filter } from 'lodash'
import { receiveAll, receiveOne } from '../../schemas/comment/index'

function extractIds(pathPattern,pathname) {
  let keys = []
  let re = pathToRegexp(pathPattern,keys)
  return filter(re.exec(pathname),(v) => { return /^[^a-zA-Z\/]?[0-9A-Za-z]+/g.test(v) })
}

function mapResourceToCategory(resource) {
  const categories = {
    'polls': 'Poll Detail'
  }
  return categories[resource]
}

export function fetch({ pathname, query, idx, type }) {
  return {
    types: type === 'comments' ? [types.COMMENT_FETCH_REQUEST,types.COMMENT_FETCH_SUCCESS,types.COMMENT_FETCH_FAILURE] : [types.REPLY_FETCH_REQUEST,types.REPLY_FETCH_SUCCESS,types.REPLY_CREATE_FAILURE],
    query,
    idx,
    schema: receiveAll,
    promise: (client) => client.get(pathname, { query })
  }
}

export function create({ pathname, data, type, idx }) {
  let [resource, parentResourceIdx ] = extractIds('/:type/:idx/comments*',pathname)
  const actionTypes = type === 'comments' ? [types.COMMENT_CREATE_REQUEST,types.COMMENT_CREATE_SUCCESS,types.COMMENT_CREATE_FAILURE] : [types.REPLY_CREATE_REQUEST,types.REPLY_CREATE_SUCCESS,types.REPLY_CREATE_FAILURE]
  return {
    types: actionTypes,
    data,
    idx,
    promise: (client) => client.post(pathname, { data }),
    schema: receiveOne,
    gaData: {
      category: mapResourceToCategory(resource),
      action: type === 'comments' ? 'Comment' : 'Reply',
      label: parentResourceIdx
    }
  }
}

export function del({ pathname, type, idx }) {
  const actionTypes = type === 'comments' ? [types.COMMENT_DELETE_REQUEST,types.COMMENT_DELETE_SUCCESS,types.COMMENT_DELETE_FAILURE] : [types.REPLY_DELETE_REQUEST,types.REPLY_DELETE_SUCCESS,types.REPLY_CREATE_FAILURE]
  return {
    types: actionTypes,
    idx,
    schema: receiveOne,
    promise: (client) => client.del(pathname)
  }
}

export function update({ pathname, data, type, idx}) {
  const actionTypes = type === 'comments' ? [types.COMMENT_UPDATE_REQUEST,types.COMMENT_UPDATE_SUCCESS,types.COMMENT_UPDATE_FAILURE] : [types.REPLY_UPDATE_REQUEST,types.REPLY_UPDATE_SUCCESS,types.REPLY_CREATE_FAILURE]
  return {
    types: actionTypes,
    idx,
    schema: receiveOne,
    promise: (client) => client.put(pathname,{ data })
  }

}

export function updateText({ text, idx, type }) {
  return {
    type: type === 'comments' ? types.COMMENT_TEXT_UPDATE : types.REPLY_TEXT_UPDATE,
    idx,
    text
  }
}

export function clear (type,idx) {
  return { idx, type: type === 'comments' ? types.COMMENT_CLEAR : types.REPLY_CLEAR }
}

export function promptLogin () { return { type: types.COMMENT_CREATE_FAILURE, error: { status: 401 } } }

export function toggleDeletePrompt({ idx, show, type }) {
  if(!show) {
    return { type: types.COMMENT_UI_DELETE_PROMPT_OPEN, payload: { idx, type } }
  } else {
    return { type: types.COMMENT_UI_DELETE_PROMPT_CLOSE }
  }
}

export function openReply (idx) {
  return { payload: { idx }, type: types.COMMENT_UI_REPLY_OPEN }
}

export function closeReply (idx) {
  return { payload: { idx }, type: types.COMMENT_UI_REPLY_CLOSE }
}

export function editOpen ({idx, type }) {
  return {
    payload: { idx, type },
    type: types.COMMENT_UI_EDIT_OPEN
  }
}

export function editClose({ idx, type }) {
  return {
    payload: { idx, type },
    type: types.COMMENT_UI_EDIT_CLOSE
  }
}
