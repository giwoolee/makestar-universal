import * as types from '../../constants/main/starMessage'

export function showVideo(artist) {
  return {
    type: types.MAIN_STAR_MESSAGE_SHOW_VIDEO,
    artist
  }
}
