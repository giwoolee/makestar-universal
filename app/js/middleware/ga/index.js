import { omit } from 'lodash'
import { GA_RENDER_COMPLETE } from '../../constants/shared/ga/index'
import { GLOBAL_INITIAL_LOAD } from '../../constants/global/index'

export default function gaMiddleware(ga) {
  if(ga === undefined) {
    throw new Error("Argument missing to gaMiddleware.  Pass an instance of react-ga.")
  }
  if(!ga.event || !ga.pageview || !ga.modalview || !ga.outboundLink) {
    throw new Error("Argument must be a react-ga instance or have a compatible interface.")
  }
  return ({ dispatch, getState }) => {
    return next => action => {
      const { type } = action
      if(typeof action === 'function') {
        next(action)
      }

      if(type !== GLOBAL_INITIAL_LOAD && type !== GA_RENDER_COMPLETE && !action.gaData) {
        return next(action)
      }

      let { routing: { locationBeforeTransitions: { pathname } } } = getState()
      if(type === GA_RENDER_COMPLETE) {
        ga.pageview(pathname)
        return next(action)
      }
      else if(type === GLOBAL_INITIAL_LOAD) {
        ga.pageview(pathname)
        return next(action)
      } else if(action.gaData) {
        ga.event(action.gaData)
        const modifiedAction = omit(action,['gaData'])
        return next(modifiedAction)
      }
    }
  }
}
