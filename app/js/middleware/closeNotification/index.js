import { resetNotification } from '../../actions/ui/index'

const LOCATION_CHANGE = '@@router/UPDATE_LOCATION'

export default function closeNotification() {
  return ({ dispatch, getState }) => {
    return next => action => {
      const { type } = action
      const { ui: { notification: { show } } } = getState()
      if(typeof action === 'function') {
        return next(action)
      }

      if(type === LOCATION_CHANGE && show === true) {
        dispatch(resetNotification())
        return next(action)
      } else {
        return next(action)
      }
    }
  }
}
