import { omit, pick } from 'lodash'

export default function clientMiddleware(client) {
  return ({dispatch, getState}) => {
    return next => action => {
      if (typeof action === 'function') {
        return action(dispatch, getState)
      }

      const { promise, types, schema } = pick(action,['promise','types','schema'])
      const rest = omit(action,['promise','types','schema'])

      if (!promise) {
        return next(action)
      }

      const [REQUEST, SUCCESS, FAILURE] = types
      next(Object.assign(rest, { type: REQUEST }))
      return promise(client).then(
        (result) => {
          if(schema) {
            next(Object.assign(rest, schema(result), { type: SUCCESS }))
          } else {
            next(Object.assign(rest, result, { type: SUCCESS }))
          }
        },
        (error) => next(Object.assign(rest, { error, type: FAILURE }))
      ).catch((error)=> {
        console.error('MIDDLEWARE ERROR:', error)
        next({rest, error, type: FAILURE})
      })
    }
  }
}
