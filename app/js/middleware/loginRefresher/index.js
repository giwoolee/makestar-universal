import { pollFetch } from '../../actions/poll/show/index'
import { contentFetch } from '../../actions/content/show/index'
import { SESSION_LOGIN_REFRESH_CONTENT } from '../../constants/session/index'

export default function indexRefresher() {
  return ({ dispatch, getState }) => {
    return next => action => {
      if(typeof action === 'function') return action(dispatch,getState)

      const state = getState()
      if(state.router && state.router.location && state.router.location.pathname) {
        const { router: { location: { pathname } } } = getState()
        if(/polls\/[0-9]+/.test(pathname) && action.type === SESSION_LOGIN_REFRESH_CONTENT) {
          const { poll: { poll: { idx } }, session: { session: { locale } } } = getState()
          dispatch(pollFetch({ pathname: `/polls/${idx}`, query: { locale } }))
          return next(action)
        } else if(/contents\/articles\/[0-9]+/.test(pathname) && action.type === SESSION_LOGIN_REFRESH_CONTENT) {
          const { content: { content: { idx } }, session: { session: { locale } } } = getState()
          dispatch(contentFetch({ pathname: `/contents/articles/${idx}`,query: { locale } }))
          return next(action)
        } else {
          return next(action)
        }
      } else {
        return next(action)
      }
    }
  }
}
