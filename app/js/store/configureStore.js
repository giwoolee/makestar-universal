import process from 'process'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import clientMiddleware from '../middleware/clientMiddleware'
import gaMiddleware from '../middleware/ga/index'
import thunk  from 'redux-thunk'
import rootReducer from '../reducers'
import createLogger from 'redux-logger'
import ga from 'react-ga'
import loginRefresher from '../middleware/loginRefresher/index'
import closeNotification from '../middleware/closeNotification/index'
import { compact } from 'lodash'
import { POLL_INDEX_FETCH_SUCCESS } from '../constants/poll/index/index'
import { POLL_SHOW_FETCH_SUCCESS } from '../constants/poll/show/index'
import { CONTENT_INDEX_FETCH_SUCCESS } from '../constants/content/index/index'
import { CONTENT_SHOW_FETCH_SUCCESS } from '../constants/content/show/index'
import { INDEX_FETCH_SUCCESS } from '../constants/index/index'
import { PROJECT_INDEX_FETCH_SUCCESS } from '../constants/project/index/index'

export default function configureStore(initialState,client,history = null) {
  typeof window === 'object' && window.GA_TRACKING_ID !== null ? ga.initialize(window.GA_TRACKING_ID) : null
  const gaMdlwr = typeof window === 'object' && window.GA_TRACKING_ID !== null ? gaMiddleware(ga) : null

  let middleware
  if(process.browser == true) {
    middleware = compact([ thunk, clientMiddleware(client), gaMdlwr, routerMiddleware(history), createLogger(), loginRefresher(), closeNotification() ])
  } else {
    middleware = compact([ thunk, clientMiddleware(client), routerMiddleware(history), gaMdlwr ])
  }

  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware),typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f)
  )

  return store
}
