import http from 'http'
import Express from 'express'
import pollResponse from './_test/testHelpers/pollResponse'
import commentResponse from './_test/testHelpers/commentResponse'
import sessionResponse from './_test/testHelpers/sessionResponse'
import contentsResponse from './_test/testHelpers/contentsResponse'
import paginationResponse from './_test/testHelpers/paginationResponse'
import indexResponse from './_test/testHelpers/indexResponse'
import projectUpdateResponse from './_test/testHelpers/projectUpdateResponse'
import morgan from 'morgan'
import path from 'path'
import fs from 'fs'
import { fill, times } from 'lodash'

const app = new Express()
const server = new http.Server(app)

app.use(morgan('tiny'))

app.use('/v1/public',Express.static(path.join(__dirname,'./_fake/public')))
app.use('/v1/contents/articles/1/text',Express.static(path.join(__dirname,'./_fake/text')))

app.get('/v1/index', (req,res) => {
  const response = {
    content: indexResponse()
  }
  res.status(200).send(response)
})

app.get('/v1/projects/updates', (req,res) => {
  const response = {
    content: times(10,(i) => projectUpdateResponse(i + 1)),
    pagination: paginationResponse(0)
  }
  res.status(200).send(response)
})

app.get('/v1/user/me', (req,res) => {
  const response = {
    content: sessionResponse()
  }
  res.status(200).send(response)
})

app.get('/v1/polls',(req,res) => {
  const response = {
    pagination: paginationResponse(0),
    content: [pollResponse(),pollResponse('https://dl.dropboxusercontent.com/s/ezguqrhg4p7387d/2016-01-10%2014.35.32.jpg?dl=0')]
  }
  res.status(200).send(response)
})

app.get('/v1/polls/:idx',(req,res) => {
  const response = {
    content: pollResponse()
  }
  res.status(200).send(response)
})

app.get('/v1/contents/articles',(req,res) => {
  const contentsOne = contentsResponse(1)
  const contentsTwo = contentsResponse(2)
  const contentsThree = contentsResponse(3)
  const contentsFour = contentsResponse(4)
  const contentsFive = contentsResponse(5)
  const response = {
    pagination: paginationResponse(0),
    content: [contentsOne,contentsTwo,contentsThree,contentsFour,contentsFive]
  }
  res.status(200).send(response)
})

app.get('/v1/contents/articles/:idx',(req,res) => {
  const response = {
    content: contentsResponse()
  }
  res.status(200).send(response)
})

app.get('/v1/:resource/:contentType/:idx/comments', (req,res) => {
  const response = {
    pagination: paginationResponse(),
    content: [commentResponse()]
  }
  res.status(200).send(response)
})

app.get('/v1/:resource/:idx/comments', (req,res) => {
  const response = {
    pagination: paginationResponse(),
    content: [commentResponse()]
  }
  res.status(200).send(response)
})


server.listen(8081,(err) => {
  if(err) {
    console.log(err)
  } else {
    console.info('-----\n==> ✅  %s is running, listening on %s.', 'Makestar Fake API', 8081)
  }
})
