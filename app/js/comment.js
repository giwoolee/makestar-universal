import 'babel-polyfill'
import React, { Component } from 'react'
import clientMiddleware from './middleware/clientMiddleware'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import ApiClient from './helpers/ApiClient'
import superagent from 'superagent'
import { IntlProvider, addLocaleData } from 'react-intl'
import { thunk } from 'redux-thunk'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { compact } from 'lodash'
import { fetchSession } from './actions/session/index'
import { fetch } from './actions/comment/index'
import comments from  './reducers/comment/index'
import session from './reducers/session/index'
import replies from './reducers/comment/reply/index'
import text from './reducers/comment/text'
import ui from './reducers/ui/index'
import * as textTypes from './constants/comment/index'
import CommentsContainer from './containers/shared/comment/CommentsContainer'
import LoginModal from './containers/shared/LoginModal'
import flatten from 'flat'
import createLogger from 'redux-logger'
import i18n from './i18n'
import { getLocale } from './selectors/session/index'
import en from 'react-intl/locale-data/en'
import ja from 'react-intl/locale-data/ja'
import ko from 'react-intl/locale-data/ko'
import zh from 'react-intl/locale-data/zh'

const ready = () => {

  addLocaleData(en)
  addLocaleData(ja)
  addLocaleData(ko)
  addLocaleData(zh)

  const rootReducer = combineReducers({
    comments,
    session,
    replies,
    routing: (state={},action) => { return state },
    ui,
    text
  })

  const client = new ApiClient(superagent)
  const middleware = compact([ thunk, clientMiddleware(client), createLogger() ])

  const store = createStore(
    rootReducer,
    window.__INITIAL_STATE__,
    compose(applyMiddleware(...middleware))
  )

  store.dispatch(fetch({ idx: window.__PROJECT_IDX__, pathname: `${window.__PATHNAME__}`, type: 'comments', query: { page: 0 } }))
  store.dispatch(fetchSession({ pathname: "/user/me" }))

  const locale = getLocale(store.getState())
  render((
    <Provider store={store} key="provider">
      <IntlProvider messages={ flatten(i18n) } locale={locale}>
        <comments>
          <LoginModal />
          <CommentsContainer idx={window.__PROJECT_IDX__}/>
        </comments>
      </IntlProvider>
    </Provider>),document.getElementById('comments')
  )
}

if(window.addEventListener) {
  window.addEventListener('load',ready, false)
} else if (window.attachEvent) {
  window.attachEvent('onload', ready)
} else {
  window.onload = ready
}

