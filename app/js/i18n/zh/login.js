const login = {
  login: '登录',
  email: '邮箱',
  password: '密码',
  signUp: '注册会员',
  failed: '请再次确认邮箱或密码。',
  success: '成功登录'
}

export default login
