const project = {
  title: '项目',
  subtitle: ' ',
  filterLink: '{filter, select, All {全部} Open {进行中} Closed {结束} }',
  timeRemaining: {
    day: '剩余{value}天',
    hour: '剩余{value}小时',
    min: '剩余{value}分',
    sec: '剩余{value}秒'
  },
  closed: '结束',
  update: '项目更新',
  date: '{ date }',
  attachmentUpdate: '更新'
}

export default project
