import corp from './corp'
import partnership from './partnership'
import terms from './terms'
import howto from './howto'
import enquiry from './enquiry'
import privacy from './privacy'
import faq from './faq'

const aboutpage = {
  corp,
  partnership,
  terms,
  howto,
  enquiry,
  privacy,
  faq
}

export default aboutpage
