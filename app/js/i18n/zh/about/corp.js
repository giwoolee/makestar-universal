const corp = {
  title: "公司介绍",
  html: `
  <div class="corp">
    <div><img src="/images/common/about_01_zh.jpg" class="img-responsive"></div>
    <div class="contents">
      <div class="bold">您</div>
      <div class="bold">如果喜欢K-POP,</div>
      <div class="bold">如果喜欢韩国电视剧或电影,</div>
      <div class="bold">如果喜欢韩流文化,</div>
      <div class="bold">那么我们将带给您 <span class="orange">不一样的快乐</span>, <span class="orange">全新的挑战</span>和<span class="orange">其他的机会</span>。</div>
      <br>
      <div class="bold">如果您享受视觉性的内容信息，那么现在就是玩转“参与型项目”的时间！</div>
      <div>享受麦克星达提供的各种各样的内容信息</div>
      <div>通过参与麦克星达进行的项目，直接参与制作。</div>
      <div>我们和韩国的制作公司及娱乐经纪公司紧密联合</div>
      <div>提供明星和粉丝能够一起完成目标的特别机会！</div>
      <br>
      <div>以全世界的粉丝为对象提供服务的</div>
      <div>韩流专业众筹平台“麦克星达”。</div>
      <div>通过大家的双手让麦克星达的明星项目获得成功</div>
      <div>和韩流明星一起创造特别的回忆吧！</div>
    </div>
    <div><img src="/images/common/about_02_zh.jpg" class="img-responsive"></div>
    <div class="contents">
      <div>麦克星达是为韩流明星和全世界粉丝之间</div>
      <div>提供新的交流方式而诞生的</div>
      <div>我们一直苦恼着</div>
      <div>“怎么样才能让明星和各国粉丝靠的更近”</div>
      <div>“怎么样才能让他们的交流持续下去”</div>
      <div>答案就是创造 <span class="orange bold">“粉丝能够直接参与明星作品制作的空间”。</span></div>
      <div>我们是这么觉得的。</div>
      <br>
      <div>通过麦克星达，</div>
      <div>粉丝从单纯的接受者这一定位脱离</div>
      <div>可以直接支持自己喜欢的明星项目，</div>
      <div>明星则能通过麦克星达展现</div>
      <div>在其他地方从未表现出过的属于自己的蓝图和直率的样子。</div>
      <div>将呈现明星和粉丝一起制作的</div>
      <div>各种各样项目的 <strong>麦克星达</strong>,</div>
      <div>既是他们之间促进交流的双向交流窗口</div>
      <div>也是在“项目”这一名字下将他们紧密连接在一起的媒介，</div>
      <div class="bold">更进一步，则是持续成长为将世界上所有的韩流粉丝聚集在一起</div>
      <div class="bold">实现让他们一起活动的<span class="orange">NO.1国际韩流社区</span>。</div>
    </div>
    <div><img src="/images/common/about_03_zh.jpg" class="img-responsive"></div>
  </div>
  `,
  markdown: ""
}

export default corp
