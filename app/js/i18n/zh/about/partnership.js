const partnership = {
  title: "商业合作",
  html: "",
  markdown: `
  麦克星达是开发并运营国际韩流众筹平台的企业。


  通过以全世界为对象运营的韩流文化平台， 在为制作人和明星们确切提供制作空间的同时，为全世界喜欢韩流的粉丝们呈现不一样的趣味和福利。

  麦克星达的目标是成为以国际韩流文化服务为基础，在音乐、放送、电视剧、电影、MD等所有娱乐领域里持续输出文化服务的NO.1国际专业娱乐文化企业。


  不满足于现状,总是不断向前行的企业（株）麦克星达，不但是娱乐事业新的发源地，同时为了能够作为动力促进韩流未来的发展，正在积极寻找新的挑战机会。

  如果有意愿和麦克星达合作，那么请通过以下方式随时与我们联系。


  * 地址 : 首尔市江南区三成路86-11 (大峙洞,巨峰大楼8层)(株)麦克星达
  * E-MAIL : [contact@makestar.co](mailto:contact@makestar.co)
  * 电话: 02-6959-2076


  所有的商业合作请先将提案通过邮件发送给我们， 审核后我们将会与贵社联系。
  `
}

export default partnership
