const topnav = {
  projects: "项目",
  events: "活动",
  polls: "投票",
  contents: "杂志",
  login: "登录",
  signup: "注册",
  myProjects: "我的支持信息",
  profile: "个人资料",
  logout: "退出",
  confirmAccount: "您还未进行邮箱验证，前往验证。"
}

export default topnav
