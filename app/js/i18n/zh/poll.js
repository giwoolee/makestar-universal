const poll = {
  title: "投票",
  subtitle: "和喜欢韩流的全世界的粉丝们一起，对韩国文化进行各种各样的投票。",
  filterLink: '{filter, select, All {全部} Open {进行中} Closed {已结束} Voted {我参加的投票}}',
  empty: {
    all: "没有正在进行中的投票。",
    open: "没有正在进行中的投票。",
    closed: "没有已结束的投票。",
    voted: "还未有您参与的投票。请试着参与麦克星达各种各样的投票吧！"
  },
  votes: "投票人数 {voteCount, plural, =0 {0} other {{voteCount}}}",
  vote: "{ myVoteCount, plural, =0 {投票} other {投票({myVoteCount})} }",
  on: "投票进行中 D -{ days }",
  off: "投票结束",
  voted: "{myVoteCount, plural,=0 {未投票} other {已投票} }",
  duration: "投票时间 <strong>{startDate}-{endDate}</strong>",
  voteOnce: "可投票1次",
  oneVoteOneHour: "1小时1次",
  oneVoteSixHours: "6小时1次",
  oneVoteTwelveHours: "12小时1次",
  oneVoteOneDay: "1天1次",
  snsShare: "将投票分享给朋友",
  loginPrompt: "请先登录后发表评论。",
  backNav: '前往投票页面 >',
  update: '最新投票',
  voteUnavailable: '您已投票成功！请查看其它投票项目！',
  voteLimitReached: '您已投票成功！请等到下次投票时间！',
  voteClosed: '投票结束'
}

export default poll
