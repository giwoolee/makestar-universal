const footer = {
  aboutUs: "公司介绍",
  partnership: "商业合作",
  termsOfUse: "服务条款",
  howTo: "参与方法",
  enquiries: "项目发起咨询",
  privacyPolicy: "隐私权政策",
  faq: "FAQ",
  companyInfo: "(株)麦克星达 代表 金載勉",
  companyRegistrationNumber: "营业执照号",
  salesLicense: "网络销售许可登记 第 2015-首尔江南-01180 号",
  address: "地址 : 首尔市江南区三成路86-11 (大峙洞,巨峰大楼8层)(株)麦克星达",
  email: "help_zh@makestar.co",
  phoneNumber: "电话号码"
}

export default footer
