const comment = {
  leaveComment: "评论",
  leaveReply: "回复评论",
  post: "发布",
  reply: "回复",
  edit: "修改",
  delete: "删除",
  loginPrompt: "请先登录",
  postedOn: '已发布{ date }',
  editedOn: '已修改{ date }',
  deletedOn: '已删除{ date }',
  commentDeletePrompt: '您确定要删除此评论吗？',
  deleted: '此评论已删除',
  editPost: '修改评论'
}

export default comment
