const starMessage = {
  starMessageTitle: "明星祝福",
  iFrameUrl: 'http://player.youku.com/embed/XMTM1NDA4NzMyMA==',
  artists: [
    { artistName: '金宇彬', imageUrl: '/public/images/star_message/thumb_02.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ1ODE2NA==' },
    { artistName: '防弹少年团', imageUrl: '/public/images/star_message/thumb_08.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2MzYyNA==' },
    { artistName: '金钟国&HAHA', imageUrl: '/public/images/star_message/thumb_03.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ1OTM3Ng==' },

    { artistName: '李光洙', imageUrl: '/public/images/star_message/thumb_04.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2MzEzMg==' },
    { artistName: 'T-ARA', imageUrl: '/public/images/star_message/thumb_05.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2NjExMg==' },
    { artistName: '周元', imageUrl: '/public/images/star_message/thumb_06.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2ODU1Mg==' },

    { artistName: 'AOA', imageUrl: '/public/images/star_message/thumb_07.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2MDMwNA==' },
    { artistName: '张娜拉', imageUrl: '/public/images/star_message/thumb_01.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2NDUzMg==' },
    { artistName: '徐康俊', imageUrl: '/public/images/star_message/thumb_09.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2NDE2OA==' },

    { artistName: 'GOT7', imageUrl: '/public/images/star_message/thumb_10.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2MTY3Ng==' },
    { artistName: "Girl's Day", imageUrl: '/public/images/star_message/thumb_11.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ2MTA4MA==' },
    { artistName: 'B1A4', imageUrl: '/public/images/star_message/thumb_14.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1NzQ3MDc1Ng==' },

    { artistName: '康男', imageUrl: '/public/images/star_message/thumb_13.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1ODkxNDc5Mg==' },
    { artistName: "April", imageUrl: '/public/images/star_message/thumb_24.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1ODkxNTIwOA==' },
    { artistName: 'VIXX', imageUrl: '/public/images/star_message/thumb_12.jpg', videoUrl: 'http://player.youku.com/embed/XMTM1OTYyNTQ1Mg==' },

    { artistName: 'G-Friend', imageUrl: '/public/images/star_message/thumb_19.jpg', videoUrl: 'http://player.youku.com/embed/XMTM3OTE0NDI2MA==' },
    { artistName: "BTOB", imageUrl: '/public/images/star_message/thumb_20.jpg', videoUrl: 'http://player.youku.com/embed/XMTM3OTEwNDA3Ng==' },
    { artistName: 'Lovelyz', imageUrl: '/public/images/star_message/thumb_21.jpg', videoUrl: 'http://player.youku.com/embed/XMTM3OTAyNzU0OA==' },

    { artistName: '林智妍', imageUrl: '/public/images/star_message/thumb_15.jpg', videoUrl: 'http://player.youku.com/embed/XMTM4MTQzNzg2MA==' },
    { artistName: "JUNJIN", imageUrl: '/public/images/star_message/thumb_17.jpg', videoUrl: 'http://player.youku.com/embed/XMTM3OTE0NDcyMA==' },
    { artistName: 'MAMAMOO', imageUrl: '/public/images/star_message/thumb_18.jpg', videoUrl: 'http://player.youku.com/embed/XMTM3OTAyODI2MA==' },

    { artistName: 'ULALA SESSION', imageUrl: '/public/images/star_message/thumb_22.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MzY4NDg2OA==' },
    { artistName: "BIGSTAR", imageUrl: '/public/images/star_message/thumb_23.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MTQ5MTg4OA==' },
    { artistName: 'Stellar', imageUrl: '/public/images/star_message/thumb_16.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MTUwNDMzNg==' },

    { artistName: 'Ben', imageUrl: '/public/images/star_message/thumb_25.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MzY4NTczMg==' },
    { artistName: "myB", imageUrl: '/public/images/star_message/thumb_26.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MTQ3NTUzNg==' },
    { artistName: '2EYES', imageUrl: '/public/images/star_message/thumb_27.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MzY4Njk3Ng==' },

    { artistName: 'Jjarimongttang', imageUrl: '/public/images/star_message/thumb_28.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MzY4NDM2MA==' },
    { artistName: "Unicorn", imageUrl: '/public/images/star_message/thumb_29.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MTQ3NDgzMg==' },
    { artistName: 'SONAMOO', imageUrl: '/public/images/star_message/thumb_30.jpg', videoUrl: 'http://player.youku.com/embed/XMTM5MzY4Nzg0MA==' },
  ]
}

export default starMessage
