const common = {
  loginFirst: "请先登录后发表评论。",
  share: "分享本页面",
  more: "查看更多",
  verifyFirstTitle: '需进行账号认证',
  verifyFirstMessage: '请在<a href={primary}>个人</a>信息页面进行账号认证',
  viewAll: '查看全部',
  mapTitle: '全世界的Star Makers',
  close: '关闭',
  cancel: '取消',
  confirm: '确认'
}

export default common
