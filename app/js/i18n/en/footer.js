const footer = {
  aboutUs: "About Us",
  partnership: "Partnership",
  termsOfUse: "Terms Of Use",
  howTo: "How To",
  enquiries: "Enquires",
  privacyPolicy: "Privacy Policy",
  faq: "FAQ",
  companyInfo: "Makestar Inc. CEO Jae-myun Kim",
  companyRegistrationNumber: "Registration No.",
  salesLicense: "Sales License 2015-서울강남-01180",
  address: "Address: 8th Floor, Bld 11, Samsung-Ro 86 Gil, (Geobong Bld, Daechi-Dong) Seoul, Korea",
  email: "help_en@makestar.co",
  phoneNumber: "Phone Number"
}

export default footer
