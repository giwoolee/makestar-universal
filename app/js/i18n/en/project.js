const project = {
  title: 'Projects',
  subtitle: ' ',
  filterLink: '{filter, select, All {All} Open {Open} Closed {Closed} }',
  timeRemaining: {
    day: '{value} {value, plural, zero {days left} one {day left} other { days left } }',
    hour: '{value} {value, plural, zero {hours left} one {hour left} other {hours left} }',
    min: '{value} {value, plural, zero {minutes left} one {minute left} other {minutes left} }',
    sec: '{value} {value, plural, zero {seconds left} one {second left} other {seconds left} }'
  },
  closed: 'Closed',
  update: 'Project Updates',
  date: '{ date }',
  attachmentUpdate: 'Update'
}

export default project
