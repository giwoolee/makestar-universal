const poll = {
  title: "Makestar Polls",
  subtitle: "Polls on all things K related! Vote for your favorite artists, music, food, and much much more!",
  filterLink: '{filter, select, All {All} Open {Live} Closed {Closed} Voted {My Polls} }',
  empty: {
    all: 'No Polls Here!',
    open: "No Polls Here!",
    closed: "No Polls Here!",
    voted: "You haven't voted in any polls yet!"
  },
  votes: '{ voteCount } { voteCount, plural, =0 {0 Votes} one {Vote} other{Votes}}',
  vote: "{ myVoteCount, plural, =0 {Vote} other {Voted!({myVoteCount})} }",
  on: 'Live Poll D -{ days }',
  off: "Closed",
  voted: '{ myVoteCount, plural, =0 {Not Voted} other {Voted!} }',
  duration: 'Poll Duration <strong>{ startDate }-{ endDate }</strong>',
  voteOnce: "Vote Once",
  oneVoteOneHour: "1 Vote / 1 Hr",
  oneVoteSixHours: "1 Vote / 6 Hr",
  oneVoteTwelveHours: "1 Vote / 12 Hr",
  oneVoteOneDay: "1 Vote / Day",
  snsShare: "Tell a friend to vote!",
  loginPrompt: "Log in to vote",
  backNav: 'To poll list >',
  update: 'Latest polls',
  voteUnavailable: 'You have already voted! Check out the other polls!',
  voteLimitReached: 'You have already voted! Wait to vote again on this poll!',
  voteClosed: 'Voting is closed'
}

export default poll
