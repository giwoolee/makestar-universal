const common = {
  loginFirst: "Please log in first",
  share: "Share this page!",
  more: "More",
  verifyFirstTitle: "Please confirm your account first",
  verifyFirstMessage: "Click <a href={ primary }>here</a> to visit your profile and confirm your account",
  close: 'Close',
  viewAll: 'See More',
  mapTitle: 'Worldwide Starmakers',
  cancel: 'Cancel',
  confirm: 'Confirm'
}

export default common
