const topnav = {
  projects: "Projects",
  events: "Events",
  polls: "Polls",
  contents: "Magazine",
  login: "Login",
  signup: "Sign Up",
  myProjects: "My Projects",
  profile: "Profile",
  logout: "Log Out",
  confirmAccount: "Account unconfirmed. Confirm account."
}

export default topnav
