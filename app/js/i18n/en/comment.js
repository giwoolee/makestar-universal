const comment = {
  leaveComment: "Leave a comment",
  leaveReply: "Leave a reply",
  post: "Post",
  reply: "Reply",
  edit: "Edit",
  delete: "Delete",
  loginPrompt: "Log in to comment",
  postedOn: 'Posted { date }',
  editedOn: 'Edited { date }',
  deletedOn: 'Deleted { date }',
  commentDeletePrompt: 'Are you sure you want to delete this comment?',
  deleted: 'This comment has been deleted',
  editPost: 'Edit post'
}

export default comment
