const login = {
  email: 'Email',
  password: 'Password',
  login: 'LOGIN',
  signUp: 'SIGN UP',
  failed: 'There was a problem logging in.  Check your email and password and try again',
  success: 'Login success!'
}

export default login
