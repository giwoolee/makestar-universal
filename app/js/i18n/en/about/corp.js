const corp = {
  title: "About Us",
  html: `
    <div class="corp">
      <div><img src="/images/common/about_01_en.jpg" class="img-responsive"></div>
      <div class="contents">
        <div class="bold">If you like K-POP, Korean drama/movie, or Hallyu contents,</div>
        <div class="bold">we can give you <span class="orange">special fun</span>, <span class="orange">new challenge</span>, and <span class="orange">another chance</span>.</div>
        <br>
        <div class="bold">If you have enjoyed viewing contents before, then it’s time to enjoy ‘participation style project’!</div>
        <div>Enjoy various content services that Makestar provides,</div>
        <div>and actually participate in projects that go on at Makestar.</div>
        <div>Through close cooperation with contents production companies and agencies in Korea,</div>
        <div>we provide special chance in which star and fans achieve a goal together!</div>
        <br>
        <div>Hallyu-specialized global crowdfunding platform, Makestar,</div>
        <div>that is serviced for users all over the world.</div>
        <div>Lead star’s project on Makestar through your hands and</div>
        <div>make special memories with Hallyu stars too!</div>
      </div>
      <div><img src="/images/common/about_02_en.jpg" class="img-responsive"></div>
      <div class="contents">
        <div>Makestar was born to suggest new communication method between Hallyu star and global fan.</div>
        <div>We thought hard on ‘how can we make the relationship between stars and global fans a bit closer,’ ‘how can we maintain their communication,’</div>
        <div>and we came to think that the solution was <span class="orange bold">‘in making a space where fans can actually participate in star’s contents production.’</span></div>
        <br>
        <div>Through Makestar,</div>
        <div>fans will no longer be simply a content consumer,</div>
        <div>they can actually support the project of the star that they like,</div>
        <div>and stars can appeal their vision and their frank self that they couldn’t express anywhere else.</div>
        <div><strong>Makestar</strong>, which will present the projects that stars and fans make together,</div>
        <div>is a two-way communication window that helps their communication,</div>
        <div>will do a big part in tying them together as one under the name of ‘project,’</div>
        <div class="bold">take a step further and continue to grow as <span class="orange">No. 1 global Hallyu community</span> where all the Hallyu fans everywhere can gather together and unite.</div>
      </div>
      <div><img src="/images/common/about_03_en.jpg" class="img-responsive"></div>
    </div>
  `,
  markup: ""
}

export default corp
