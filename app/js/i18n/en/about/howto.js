const howto = {
  title: "How to",
  html: `
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/z-8GAVPBYuA" allowfullscreen="allowfullscreen"></iframe>
    </div>
    <br>
    <div><img src="/images/common/howto_en.jpg" class="img-responsive"></div>
  `,
  markup: ""
}

export default howto
