const enquiry = {
  title: "Enquiries",
  html: "",
  markdown: `
  **Makestar’s ‘Crowdfunding’ projects will help make your ideas and dreams in the entertainment business come to life.**


  **Crowdfunding** is an innovative way to invest through the use of digital channels such as social media and the internet.
  This new financial technology makes it possible to raise production funds and carry out marketing in one go.


  **Makestar** is a dream team comprising an entertainment business veteran and IT experts. With unmatched knowledge in the areas of Korean entertainment business and IT servicing, Makestar has created an **Entertainment Crowdfunding Service** that is unparalleled in its uniqueness.


  **Makestar crowdfunding projects** are open globally to everyone, simple to use, and guarantees safety.
  Available in 4 different languages: English, Chinese, Japanese, and Korean.
  Makestar brings Korean contents closer to the people of the world, makes them more interactive, and launches them beyond the limitations of the Korean market.
  As a truly global service, **Makestar crowdfunding** is the perfect way to market your project to the world.


  Are you ready to take part in Makestar’s sensational global crowdfunding platform?


  * Address : 8th Floor, Bld 11, Samsung-Ro 86 Gil, (Geobong Bld, Daechi-Dong) Seoul, Korea
  * E-MAIL : [contact@makestar.co](mailto:contact@makestar.co)
  * Phone : +82-2-6959-2076


  Making your dreams a reality!
  Makestar: the door to globalizing your business.
  Make Your Own Star!
  `
}

export default enquiry
