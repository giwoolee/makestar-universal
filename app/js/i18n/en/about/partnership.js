const partnership = {
  title: "Partnership",
  html: "",
  markdown: `
  **Makestar** is..

  The team that created and operates a global Hallyu crowdfunding platform.


  Being a Hallyu contents platform that targets and operates all around the world, Makestar offers producers and artists an innovative way to bolster their contents creation process, as well as bringing the Hallyu fans the world over new entertainment and rewards.
  Makestar aims to become the forerunner in the global **Entertainment Contents** business through its global Hallyu contents service that deals with high quality contents in the area of music, broadcasting, drama, movies, and merchandising of the entertainment industry.


  With the mission to become the new mecca in the entertainment business, and breathe momentum into Hallyu’s continued expansion to the world, Makestar Inc. is always on the look out for new opportunities and challenges.
  For all partnership enquiries, do not hesitate to contact us at the email address below.


  * Address : 8th Floor, Bld 11, Samsung-Ro 86 Gil, (Geobong Bld, Daechi-Dong) Seoul, Korea
  * E-MAIL : [contact@makestar.co](mailto:contact@makestar.co)
  * Phone : +82-2-6959-2076


  We would be glad to review all business partnership proposals and get back to you at the earliest.
  `
}

export default partnership
