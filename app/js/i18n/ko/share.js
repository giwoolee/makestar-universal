const share = {
  ogDescription: "글로벌 한류 크라우드펀딩. K-POP, 드라마, 영화, 출판 등 크라우드펀딩 프로젝트 제작 및 다양한 한류 콘텐츠 제공."
}

export default share
