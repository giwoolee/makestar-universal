const starMessage = {
  starMessageTitle: "스타메시지",
  iFrameUrl: 'https://www.youtube.com/embed/tnMySuOaFBk',
  artists: [
    { artistName: '김우빈', imageUrl: '/public/images/star_message/thumb_02.jpg', videoUrl: 'https://www.youtube.com/embed/Diu3Bq4qJM0?rel=0&autoplay=1' },
    { artistName: '방탄소년단', imageUrl: '/public/images/star_message/thumb_08.jpg', videoUrl: 'https://www.youtube.com/embed/2vj-yuHUu4k?rel=0&autoplay=1' },
    { artistName: '김종국&하하', imageUrl: '/public/images/star_message/thumb_03.jpg', videoUrl: 'https://www.youtube.com/embed/SrcyHjLJXn8?rel=0&autoplay=1' },

    { artistName: '이광수', imageUrl: '/public/images/star_message/thumb_04.jpg', videoUrl: 'https://www.youtube.com/embed/EuOr8pXwi38?rel=0&autoplay=1' },
    { artistName: '티아라', imageUrl: '/public/images/star_message/thumb_05.jpg', videoUrl: 'https://www.youtube.com/embed/IIKbMtnJvtM?rel=0&autoplay=1' },
    { artistName: '주원', imageUrl: '/public/images/star_message/thumb_06.jpg', videoUrl: 'https://www.youtube.com/embed/ytkpE6B8u9k?rel=0&autoplay=1' },

    { artistName: 'AOA', imageUrl: '/public/images/star_message/thumb_07.jpg', videoUrl: 'https://www.youtube.com/embed/Yxy7ORnF5KE?rel=0&autoplay=1' },
    { artistName: '장나라', imageUrl: '/public/images/star_message/thumb_01.jpg', videoUrl: 'https://www.youtube.com/embed/mWPIQsMAv3k?rel=0&autoplay=1' },
    { artistName: '서강준', imageUrl: '/public/images/star_message/thumb_09.jpg', videoUrl: 'https://www.youtube.com/embed/WsLfngPIeVE?rel=0&autoplay=1' },

    { artistName: 'GOT7', imageUrl: '/public/images/star_message/thumb_10.jpg', videoUrl: 'https://www.youtube.com/embed/N5bIlrab9tU?rel=0&autoplay=1' },
    { artistName: "걸스데이", imageUrl: '/public/images/star_message/thumb_11.jpg', videoUrl: 'https://www.youtube.com/embed/2Frqb5A9MjM?rel=0&autoplay=1' },
    { artistName: 'B1A4', imageUrl: '/public/images/star_message/thumb_14.jpg', videoUrl: 'https://www.youtube.com/embed/5nfO3UxydxY?rel=0&autoplay=1' },

    { artistName: '강남', imageUrl: '/public/images/star_message/thumb_13.jpg', videoUrl: 'https://www.youtube.com/embed/TgERde57Q8o?rel=0&autoplay=1' },
    { artistName: "에이프릴", imageUrl: '/public/images/star_message/thumb_24.jpg', videoUrl: 'https://www.youtube.com/embed/HnH_GYSJWxs?rel=0&autoplay=1' },
    { artistName: '빅스', imageUrl: '/public/images/star_message/thumb_12.jpg', videoUrl: 'https://www.youtube.com/embed/V4ikSfNua2o?rel=0&autoplay=1' },

    { artistName: '여자친구', imageUrl: '/public/images/star_message/thumb_19.jpg', videoUrl: 'https://www.youtube.com/embed/0EybzbLuJU4?rel=0&autoplay=1' },
    { artistName: "비투비", imageUrl: '/public/images/star_message/thumb_20.jpg', videoUrl: 'https://www.youtube.com/embed/Zl6UvxHaOQU?rel=0&autoplay=1' },
    { artistName: '러블리즈', imageUrl: '/public/images/star_message/thumb_21.jpg', videoUrl: 'https://www.youtube.com/embed/2ZY7XC7IJ7M?rel=0&autoplay=1' },

    { artistName: '임지연', imageUrl: '/public/images/star_message/thumb_15.jpg', videoUrl: 'https://www.youtube.com/embed/zsJlg-frJDA?rel=0&autoplay=1' },
    { artistName: "전진", imageUrl: '/public/images/star_message/thumb_17.jpg', videoUrl: 'https://www.youtube.com/embed/cChiDxwKMvI?rel=0&autoplay=1' },
    { artistName: '마마무', imageUrl: '/public/images/star_message/thumb_18.jpg', videoUrl: 'https://www.youtube.com/embed/a8vJMCMq6AQ?rel=0&autoplay=1' },

    { artistName: '울랄라세션', imageUrl: '/public/images/star_message/thumb_22.jpg', videoUrl: 'https://www.youtube.com/embed/hfzC3S1Y9FE?rel=0&autoplay=1' },
    { artistName: "빅스타", imageUrl: '/public/images/star_message/thumb_23.jpg', videoUrl: 'https://www.youtube.com/embed/KVS8ryxt5zs?rel=0&autoplay=1' },
    { artistName: '스텔라', imageUrl: '/public/images/star_message/thumb_16.jpg', videoUrl: 'https://www.youtube.com/embed/eFzQ1helFdI?rel=0&autoplay=1' },

    { artistName: '벤', imageUrl: '/public/images/star_message/thumb_25.jpg', videoUrl: 'https://www.youtube.com/embed/4m5YHWFD-I8?rel=0&autoplay=1' },
    { artistName: "마이비", imageUrl: '/public/images/star_message/thumb_26.jpg', videoUrl: 'https://www.youtube.com/embed/VoE7j1apOxk?rel=0&autoplay=1' },
    { artistName: '투아이즈', imageUrl: '/public/images/star_message/thumb_27.jpg', videoUrl: 'https://www.youtube.com/embed/N55181lFqY8?rel=0&autoplay=1' },

    { artistName: '짜리몽딴', imageUrl: '/public/images/star_message/thumb_28.jpg', videoUrl: 'https://www.youtube.com/embed/wLgy4KrrdHk?rel=0&autoplay=1' },
    { artistName: "유니콘", imageUrl: '/public/images/star_message/thumb_29.jpg', videoUrl: 'https://www.youtube.com/embed/ZgnFbp46Wug?rel=0&autoplay=1' },
    { artistName: '소나무', imageUrl: '/public/images/star_message/thumb_30.jpg', videoUrl: 'https://www.youtube.com/embed/DVEqZgFNf4M?rel=0&autoplay=1' },
  ]
}

export default starMessage
