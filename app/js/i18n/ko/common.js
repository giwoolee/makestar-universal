const common = {
  loginFirst: "로그인 후 이용해주세요",
  share: "페이지 공유하기",
  more: "더보기",
  close: '닫기',
  verifyFirstTitle: '계정인증이 필요합니다.',
  verifyFirstMessage: '<a href={ primary }>프로필 페이지</a>에서 계정 인증을 해주세요.',
  viewAll: '모두 보기',
  mapTitle: '전 세계 스타메이커즈',
  cancel: '취소',
  confirm: '확인'
}

export default common
