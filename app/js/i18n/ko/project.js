const project = {
  title: '프로젝트',
  subtitle: ' ',
  filterLink: '{filter, select, All {전체} Open {진행중} Closed {완료} }',
  timeRemaining: {
    day: '{value}일 남음',
    hour: '{value}시간 남음',
    min: '{value}분 남음',
    sec: '{value}초 남음'
  },
  closed: '종료',
  update: '프로젝트 업데이트',
  date: '{ date }',
  attachmentUpdate: '업데이트'
}

export default project
