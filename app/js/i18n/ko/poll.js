const poll = {
  title: "투표",
  subtitle: "K-pop 을 사랑하는 전 세계 팬들과 함께 K-culture에 대한 다양한 투표를 진행합니다.",
  filterLink: '{filter, select, All {전체} Open {진행중} Closed {종료} Voted {참여한투표}}',
  empty: {
    all: "투표가 없습니다.",
    open: "진행중인 투표가 없습니다.",
    closed: "종료된 투표가 없습니다.",
    voted: "아직 참여한 투표가 없습니다. 메이크스타의 다양한 투표에 참여해보세요!"
  },
  votes: "참여 인원 {voteCount, plural, =0 {0} other {{voteCount}}}",
  vote: "{ myVoteCount, plural, =0 {투표} other {투표({myVoteCount})} }",
  on: "투표 진행중 D -{ days }",
  off: "투표 종료",
  voted: "{ myVoteCount, select, =0 {투표하지 않음} other {투표함}}",
  duration: "투표 기간 <strong>{ startDate }-{ endDate }</strong>",
  voteOnce: "1회 투표 가능",
  oneVoteOneHour: "1시간 1회",
  oneVoteSixHours: "6시간 1회",
  oneVoteTwelveHours: "12시간 1회",
  oneVoteOneDay: "1일 1회",
  snsShare: "이 투표를 친구에게 알리기",
  loginPrompt: "로그인 후 이용해주세요",
  backNav: '투표 목록으로 가기 > ',
  update: '최신 투표',
  voteUnavailable: '이미 투표하셨습니다. 다른 폴에도 참여해보세요!',
  voteLimitReached: '이미 투표하셨습니다. 다음 투표 기간까지 대기 해주세요!',
  voteClosed: '투표가 마무리 되었습니다.'
}

export default poll
