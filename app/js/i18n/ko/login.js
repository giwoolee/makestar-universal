const login = {
  login: '로그인',
  email: '이메일',
  password: '비밀번호',
  signUp: '회원가입',
  failed: '이메일 또는 비밀번호를 다시 확인하세요.',
  success: '로그인이 완료되었습니다'
}

export default login
