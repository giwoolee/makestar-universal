const footer = {
  aboutUs: "회사소개",
  partnership: "사업제휴",
  termsOfUse: "이용약관",
  howTo: "참여방법",
  enquiries: "프로젝트개설문의",
  privacyPolicy: "개인정보보호정책",
  faq: "FAQ",
  companyInfo: "(주)메이크스타 대표 김재면",
  companyRegistrationNumber: "사업자등록번호",
  salesLicense: "통신판매업신고 제 2015-서울강남-01180 호",
  address: "주소 : 서울특별시 강남구 삼성로86길 11 (대치동, 거봉빌딩 8층) (주)메이크스타",
  email: "help_ko@makestar.co",
  phoneNumber: "전화번호"
}

export default footer
