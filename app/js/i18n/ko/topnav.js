const topnav = {
  projects: "프로젝트",
  events: "이벤트",
  polls: "투표",
  contents: "매거진",
  login: "로그인",
  signup: "가입",
  myProjects: "내 후원정보",
  profile: "프로필",
  logout: "로그아웃",
  confirmAccount: "미인증 상태입니다. 인증받기"
}

export default topnav
