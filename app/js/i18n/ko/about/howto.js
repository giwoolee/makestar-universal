const howto = {
  title: "참여방법",
  html: `
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/0TTzOSrMxtQ" allowfullscreen="allowfullscreen"></iframe>
    </div>
    <br>
    <div><img src="/images/common/howto_ko.jpg" class="img-responsive"></div>
  `,
  markdown: ""
}

export default howto
