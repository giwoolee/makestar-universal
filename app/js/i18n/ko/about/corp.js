const corp = {
  title: "회사소개",
  html: `
    <div class="corp">
      <div><img src="/images/common/about_01_en.jpg" class="img-responsive"></div>
      <div class="contents">
        <div class="bold">당신이</div>
        <div class="bold">K-POP을 좋아한다면,</div>
        <div class="bold">한국 드라마나 영화를 좋아한다면,</div>
        <div class="bold">한류 콘텐츠들을 좋아한다면,</div>
        <div class="bold">우리는 당신에게 <span class="orange">특별한 즐거움</span>을, <span class="orange">새로운 도전</span>을, <span class="orange">또 다른 기회</span>를 줄 수 있습니다.</div>
        <br>
        <div class="bold">보는 콘텐츠를 즐겼다면, 이젠 '참여형 프로젝트'로 즐길 차례!</div>
        <div>메이크스타에서 제공하는 다양한 콘텐츠 서비스를 즐기고</div>
        <div>메이크스타에서 진행되는 프로젝트들을 통해 제작에 직접 참여해 보세요.</div>
        <div>우리는 한국의 콘텐츠 제작사들및 기획사들과의 긴밀한 협조를 통해</div>
        <div>스타와 팬이 함께 목표를 이룰 수 있는 특별한 기회를 제공합니다!</div>
        <br>
        <div>전세계 유저들을 대상으로 서비스되는</div>
        <div>한류 전문 글로벌 크라우드펀딩 플랫폼 '메이크스타'.</div>
        <div>여러분의 손으로 메이크스타의 스타 프로젝트를 성공시키고</div>
        <div>한류 스타들과 함께하는 특별한 추억도 만들어 보세요!</div>
      </div>
      <div><img src="/images/common/about_02_ko.jpg" class="img-responsive"></div>
      <div class="contents">
        <div>메이크스타는 한류 스타와 글로벌 팬 사이의</div>
        <div>새로운 소통 방법을 제시하기 위해 태어났습니다.</div>
        <div>우리는</div>
        <div>‘어떻게 하면 스타와 글로벌 팬의 관계를 더 가깝게 만들 수 있을까’</div>
        <div>‘어떻게 하면 그들의 커뮤니케이션을 지속시킬 수 있을까?’ 를 고민했고,</div>
        <div>그 해답이 바로 <span class="orange bold">‘팬이 스타 콘텐츠 제작에 직접 참여 할 수 있는 공간‘</span>을 만드는 데 있다고</div>
        <div>생각하게 되었습니다.</div>
        <br>
        <div>메이크스타를 통해,</div>
        <div>팬은 단순한 콘텐츠 수용자의 입장에서 벗어나</div>
        <div>자신이 좋아하는 스타의 프로젝트에 지지를 보낼 수 있게 되고,</div>
        <div>스타는 다른 곳에서 표현할 수 없었던 자신의 비전과 진솔한 모습을</div>
        <div>메이크스타를 통해 팬들에게 직접 어필할 수 있게 됩니다.</div>
        <div>스타와 팬이 함께 만드는</div>
        <div>다양한 프로젝트들을 제시할 <strong>메이크스타</strong>는,</div>
        <div>그들 사이의 커뮤니케이션을 돕는 양방향 소통 창구이자</div>
        <div>‘프로젝트’라는 이름 아래 그들을 하나로 묶는 매개로 활약하며,</div>
        <div class="bold">나아가 세계 모든 한류 팬들이 모이고 뭉칠 수 있는</div>
        <div class="bold"><span class="orange">NO.1 글로벌 한류 커뮤니티</span>로 계속 성장해 나갈 것입니다.</div>
      </div>
      <div><img src="/images/common/about_03_en.jpg" class="img-responsive"></div>
    </div>
  `,
  markdown: ""
}

export default corp
