import aboutpage from './about/index'
import main from './main/index'
import common from './common'
import poll from './poll'
import topnav from './topnav'
import comment from './comment'
import footer from './footer'
import content from './content'
import login from './login'
import project from './project'
import share from './share'

const ko = {
  aboutpage,
  main,
  common,
  poll,
  topnav,
  comment,
  footer,
  content,
  login,
  project,
  share
}

export default ko
