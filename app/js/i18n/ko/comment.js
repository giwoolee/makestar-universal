const comment = {
  leaveComment: "댓글 달기",
  leaveReply: "대댓글 달기",
  post: "등록",
  reply: "대댓글 달기",
  edit: "수정",
  delete: "삭제",
  loginPrompt: "로그인을 먼저 해주세요",
  postedOn: '{ date } 게시됨',
  editedOn: '{ date } 수정됨',
  deletedOn: '{ date } 삭제됨',
  commentDeletePrompt: '댓글을 정말 삭제하시겠습니까?',
  deleted: '삭제된 글입니다',
  editPost: '글 수정하기'
}

export default comment
