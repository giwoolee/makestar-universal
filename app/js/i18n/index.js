import ko from './ko/index'
import en from './en/index'
import ja from './ja/index'
import zh from './zh/index'

const messages = {
  ko,
  en,
  ja,
  zh
}

export default messages
