const starMessage = {
  starMessageTitle: "スターメッセージ",
  iFrameUrl: 'https://www.youtube.com/embed/tnMySuOaFBk',
  artists: [
    { artistName: 'キム・ウビン', imageUrl: '/public/images/star_message/thumb_02.jpg', videoIr: 'https://www.youtube.com/embed/Diu3Bq4qJM0?rel=0&autoplay=1' },
    { artistName: '防弾少年団', imageUrl: '/public/images/star_message/thumb_08.jpg', videoIr: 'https://www.youtube.com/embed/2vj-yuHUu4k?rel=0&autoplay=1' },
    { artistName: 'キム・ジョングク&ハハ', imageUrl: '/public/images/star_message/thumb_03.jpg', videoIr: 'https://www.youtube.com/embed/SrcyHjLJXn8?rel=0&autoplay=1' },

    { artistName: 'イ・グランス', imageUrl: '/public/images/star_message/thumb_04.jpg', videoIr: 'https://www.youtube.com/embed/EuOr8pXwi38?rel=0&autoplay=1' },
    { artistName: 'T-ARA', imageUrl: '/public/images/star_message/thumb_05.jpg', videoIr: 'https://www.youtube.com/embed/IIKbMtnJvtM?rel=0&autoplay=1' },
    { artistName: 'チュウォン', imageUrl: '/public/images/star_message/thumb_06.jpg', videoIr: 'https://www.youtube.com/embed/ytkpE6B8u9k?rel=0&autoplay=1' },

    { artistName: 'AOA', imageUrl: '/public/images/star_message/thumb_07.jpg', videoIr: 'https://www.youtube.com/embed/Yxy7ORnF5KE?rel=0&autoplay=1' },
    { artistName: 'チャン・ナラ', imageUrl: '/public/images/star_message/thumb_01.jpg', videoIr: 'https://www.youtube.com/embed/mWPIQsMAv3k?rel=0&autoplay=1' },
    { artistName: 'ソ・ガンジュン', imageUrl: '/public/images/star_message/thumb_09.jpg', videoIr: 'https://www.youtube.com/embed/WsLfngPIeVE?rel=0&autoplay=1' },

    { artistName: 'GOT7', imageUrl: '/public/images/star_message/thumb_10.jpg', videoIr: 'https://www.youtube.com/embed/N5bIlrab9tU?rel=0&autoplay=1' },
    { artistName: "Girl's Day", imageUrl: '/public/images/star_message/thumb_11.jpg', videoIr: 'https://www.youtube.com/embed/2Frqb5A9MjM?rel=0&autoplay=1' },
    { artistName: 'B1A4', imageUrl: '/public/images/star_message/thumb_14.jpg', videoIr: 'https://www.youtube.com/embed/5nfO3UxydxY?rel=0&autoplay=1' },

    { artistName: '康男', imageUrl: '/public/images/star_message/thumb_13.jpg', videoIr: 'https://www.youtube.com/embed/TgERde57Q8o?rel=0&autoplay=1' },
    { artistName: "April", imageUrl: '/public/images/star_message/thumb_24.jpg', videoIr: 'https://www.youtube.com/embed/HnH_GYSJWxs?rel=0&autoplay=1' },
    { artistName: 'VIXX', imageUrl: '/public/images/star_message/thumb_12.jpg', videoIr: 'https://www.youtube.com/embed/V4ikSfNua2o?rel=0&autoplay=1' },

    { artistName: 'G-Friend', imageUrl: '/public/images/star_message/thumb_19.jpg', videoIr: 'https://www.youtube.com/embed/0EybzbLuJU4?rel=0&autoplay=1' },
    { artistName: "BTOB", imageUrl: '/public/images/star_message/thumb_20.jpg', videoIr: 'https://www.youtube.com/embed/Zl6UvxHaOQU?rel=0&autoplay=1' },
    { artistName: 'Lovelyz', imageUrl: '/public/images/star_message/thumb_21.jpg', videoIr: 'https://www.youtube.com/embed/2ZY7XC7IJ7M?rel=0&autoplay=1' },

    { artistName: 'イム・ジヨン', imageUrl: '/public/images/star_message/thumb_15.jpg', videoIr: 'https://www.youtube.com/embed/zsJlg-frJDA?rel=0&autoplay=1' },
    { artistName: "チョン・ジン", imageUrl: '/public/images/star_message/thumb_17.jpg', videoIr: 'https://www.youtube.com/embed/cChiDxwKMvI?rel=0&autoplay=1' },
    { artistName: 'MAMAMOO', imageUrl: '/public/images/star_message/thumb_18.jpg', videoIr: 'https://www.youtube.com/embed/a8vJMCMq6AQ?rel=0&autoplay=1' },

    { artistName: 'ULALA SESSION', imageUrl: '/public/images/star_message/thumb_22.jpg', videoIr: 'https://www.youtube.com/embed/hfzC3S1Y9FE?rel=0&autoplay=1' },
    { artistName: "BIGSTAR", imageUrl: '/public/images/star_message/thumb_23.jpg', videoIr: 'https://www.youtube.com/embed/KVS8ryxt5zs?rel=0&autoplay=1' },
    { artistName: 'Stellar', imageUrl: '/public/images/star_message/thumb_16.jpg', videoIr: 'https://www.youtube.com/embed/eFzQ1helFdI?rel=0&autoplay=1' },

    { artistName: 'Ben', imageUrl: '/public/images/star_message/thumb_25.jpg', videoIr: 'https://www.youtube.com/embed/4m5YHWFD-I8?rel=0&autoplay=1' },
    { artistName: "myB", imageUrl: '/public/images/star_message/thumb_26.jpg', videoIr: 'https://www.youtube.com/embed/VoE7j1apOxk?rel=0&autoplay=1' },
    { artistName: '2EYES', imageUrl: '/public/images/star_message/thumb_27.jpg', videoIr: 'https://www.youtube.com/embed/N55181lFqY8?rel=0&autoplay=1' },

    { artistName: 'Jjarimongttang', imageUrl: '/public/images/star_message/thumb_28.jpg', videoIr: 'https://www.youtube.com/embed/wLgy4KrrdHk?rel=0&autoplay=1' },
    { artistName: "Unicorn", imageUrl: '/public/images/star_message/thumb_29.jpg', videoIr: 'https://www.youtube.com/embed/ZgnFbp46Wug?rel=0&autoplay=1' },
    { artistName: 'SONAMOO', imageUrl: '/public/images/star_message/thumb_30.jpg', videoIr: 'https://www.youtube.com/embed/DVEqZgFNf4M?rel=0&autoplay=1' },
  ]
}

export default starMessage
