const comment = {
  leaveComment: "コメント",
  leaveReply: "コメントに返書",
  post: "登録",
  reply: "コメントに返書",
  edit: "修正",
  delete: "削除",
  loginPrompt: "ログインして下さい。",
  postedOn: '{ date }に掲示される',
  editedOn: '{ date}に修正される',
  deletedOn: '{ date }削除されました',
  commentDeletePrompt: '削除しますか?',
  deleted: '削除しました。',
  editPost: 'コメント修正'
}

export default comment
