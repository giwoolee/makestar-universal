const poll = {
  title: "投票",
  subtitle: "K-popを愛する全世界のファンと一緒に K-cultureに関する多様な投票を行われてます。",
  filterLink: '{filter, select, All {全体} Open {進行中} Closed {終了} Voted {参加した投票} }',
  empty: {
    all: "進行中の投票がありません。",
    open: "進行中の投票がありません。",
    closed: "終了された投票がありません。",
    voted: "参加した投票がありません。makestarの多様な投票に参加して見て下さい!"
  },
  votes: "参加人数 {voteCount, plural, =0 {0} other {{voteCount}}}",
  vote: "投票",
  on: "投票進行中 D -{ days }",
  off: "投票終了",
  voted: "{ myVoteCount, plural, =0 {投票していない} other {投票完了} }",
  duration: "投票期間 <strong>{startDate}-{endDate}</strong>",
  voteOnce: "１回投票可能",
  oneVoteOneHour: "１時間1回",
  oneVoteSixHours: "６時間1回",
  oneVoteTwelveHours: "１２時間1回",
  oneVoteOneDay: "１日1回",
  snsShare: "この投票を友達に知らせる",
  loginPrompt: "ログイン後、利用して下さい。",
  backNav: '投票リストへ戻る >',
  update: '最新投票',
  voteUnavailable: 'すでに投票しました。他の投票にも参加して見て下さい!',
  voteLimitReached: 'すでに投票しました。投票が出来る時間まで待って下さい!',
  voteClosed: '投票終了'
}

export default poll
