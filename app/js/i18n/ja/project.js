const project = {
  title: 'プロジェクト',
  subtitle: ' ',
  filterLink: '{filter, select, All {全体} Open {進行中} Closed {完了} }',
  timeRemaining: {
    day: '残り{value}日',
    hour: '残り{value}時間',
    min: '残り{value}分',
    sec: '残り{value}秒'
  },
  closed: '終了',
  update: 'アップデート',
  date: '{ date }',
  attachmentUpdate: 'アップデート'
}

export default project
