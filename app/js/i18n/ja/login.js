const login = {
  email: 'メール',
  password: 'パスワード',
  login:'ログイン',
  signUp: '会員登録',
  failed: 'メールアドレスまた、パスワードをもう一度確認して下さい。',
  success: 'ログインできました。'
}

export default login
