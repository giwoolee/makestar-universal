const howto = {
  title: "参与方法",
  html: `
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BaLcsWXNtDo" allowfullscreen="allowfullscreen"></iframe>
    </div>
    <br>
    <div><img src="/images/common/howto_ja.jpg" class="img-responsive"></div>
  `,
  markup: ""
}

export default howto
