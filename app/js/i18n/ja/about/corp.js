const corp = {
  title: "運営会社",
  html: `
    <div class="corp">
    <div><img src="/images/common/about_01_en.jpg" class="img-responsive"></div>
      <div class="contents">
        <div class="bold">あなたが</div>
        <div class="bold">K-POPが好きなら</div>
        <div class="bold">韓国ドラマや映画が好きなら</div>
        <div class="bold">韓流コンテンツを好きだったら</div>
        <div class="bold">Makestarはあなたに <span class="orange">特別な楽しみ</span>を<span class="orange">新たな挑戦</span>を<span class="orange">別の機会</span>を与えることができます。</div>
        <br>
        <div class="bold">今まで見るコンテンツだけを楽しんだならこれからは'参加型プロジェクト'で楽しましょう!</div>
        <div>Makestarが提供する多様なコンテンツサービスを楽しんで</div>
        <div>Makestar で行われるプロジェクトを通じて製作に直接参加してみてください。</div>
        <div>Makestarは 韓国のコンテンツ制作社とエンタテインメント会社と緊密な協調を通じて</div>
        <div>スターとファンが共に目標を達成する特別な機会を提供します!</div>
        <br>
        <div>全世界のユーザーを対象にサービスされる</div>
        <div>韓流専門グローバルクラウドファンディングプラットホーム'Makestar'</div>
        <div>皆さんの手でMakestarプロジェクトを成功させ</div>
        <div>韓流スターと一緒に特別な思い出も作って見て下さい！</div>
      </div>
      <div><img src="/images/common/about_02_ja.jpg" class="img-responsive"></div>
      <div class="contents">
        <div>Ｍakestarは韓流スターとグローバルファンの間の</div>
        <div>新しいコミュニケージョン方法を提示するために生まれました。</div>
        <div>Ｍakestarは</div>

        <div>'どうすればスターとグロバールファンの関係をもっと近づけるのか？'</div>
        <div>'どうすれば彼らのコミュニケージョンを持続させることができるのか？'について考えました。</div>
        <div>その解答が <span class="orange bold">ファンがスターコンテンツ制作に直接参加できる空間'</span>を</div>
        <div>作ることにあると考えました。</div>
        <br>
        <div>Ｍakestarを通じて</div>
        <div>ファンは単純なコンテンツを見る立場から離れ</div>
        <div>自分が好きなスターのプロジェクトに支持を送ることだでき、</div>
        <div>スターは他の所で表現できなかった自分のビジョンと真率な姿を</div>
        <div>Ｍakestarを通じてファンに直接アピールすることができます。</div>
        <div>スターとファンが一緒に作る</div>
        <div>多様なプロジェクトを提示する<strong>Ｍakestar</strong>は</div>
        <div>コミュニケーションを助ける双方向疎通の窓口であり</div>
        <div>彼らを一つに束ねる媒介として活躍し</div>
        <div class="bold">ひいては世界の全ての韓流ファンが集まる</div>
        <div class="bold"><span class="orange">NO.1グローバル韓流コミュニティで成長を続けていきます。</span></div>
      </div>
      <div><img src="/images/common/about_03_en.jpg" class="img-responsive"></div>
    </div>
  `,
  markup: ""
}

export default corp
