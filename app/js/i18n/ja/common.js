const common = {
  loginFirst: "ログイン後、利用して下さい。",
  share: "ページ共有",
  more: "詳しく見る",
  verifyFirstTitle: 'アカウント認証が必要です。',
  verifyFirstMessage: '<a href={ primary }>プロフィールページ></a> でアカウント認証をして下さい! ',
  close: '閉じる',
  viewAll: '全てを見る',
  mapTitle: '全世界のStar Makers',
  cancel: '取り消し',
  confirm: '確認'
}

export default common
