const footer = {
  aboutUs: "運営会社",
  partnership: "ビジネス提携",
  termsOfUse: "利用規約",
  howTo: "参与方法",
  enquiries: "プロジェクト開設",
  privacyPolicy: "プライバシーポリジー",
  faq: "FAQ",
  companyInfo: "(株)Makestar代表取締役社長 金載勉",
  companyRegistrationNumber: "事業者登録番号",
  salesLicense: "通信販売業番号 제 2015-서울강남-01180 호",
  address: "住所：ソウル市江南区大峙洞942-16巨峰INC8F=(株) Makestar",
  email: "help_ja@makestar.co",
  phoneNumber: "電話番号"
}

export default footer
