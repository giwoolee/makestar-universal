const topnav = {
  projects: "プロジェクト",
  events: "イベント",
  polls: "投票",
  contents: "マガジン",
  login: "ログイン",
  signup: "会員登録",
  myProjects: "後援情報",
  profile: "プロフィール",
  logout: "ログアウト",
  confirmAccount: "会員認証されてない状態です。会員認証"
}

export default topnav
