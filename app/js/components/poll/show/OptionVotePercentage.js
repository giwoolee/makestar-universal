import React, { Component } from 'react'

export default class OptionVotePercentage extends Component {
  votePercentage(totalVoteCount,voteCount) {
    if(totalVoteCount == 0) {
      return 0
    } else {
      return String(Math.floor((voteCount / totalVoteCount) * 100))
    }
  }
  render() {
    const {
      voteCount,
      totalVoteCount,
      name,
      linkUrl,
      linkText,
    } = this.props

    return(
      <div className='col-xs-5 col-sm-5 col-md-5 col-lg-5 poll-show-vote-percentage'>
        <div className='row'>
          <div className='col-xs-12 col-sm-5 col-md-12 col-lg-12 poll-show-option-name'>
            <span>{ name }</span>
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-progress-bar-container'>
            <div className='progress' style={ { height: '100%' }}>
              <div className='progress-bar poll-show-progress-bar' style={ { width: `${this.votePercentage(totalVoteCount,voteCount)}%` } } aria-valuenow={this.votePercentage(totalVoteCount,voteCount)} aria-valuemin='0' aria-valuemax='100' >
              </div>
            </div>
          </div>
        </div>
        <div className='row poll-show-progress-percentage'>
          <div className='col-xs-8 col-sm-8 col-md-8 col-lg-8 poll-show-option-vote-count-container'>
            <div className='poll-show-option-vote-count-icon'/>
            <div className='poll-show-option-vote-count-text'> { voteCount } </div>
          </div>
          <div className='col-xs-4 col-sm-4 col-md-4 col-lg-4 poll-show-option-vote-percentage-text-container'>
            <p className='text-right'>{this.votePercentage(totalVoteCount,voteCount)+ '%'}</p>
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-link'>
            <a href={linkUrl} target="_blank"><span >{ linkText }</span></a>
          </div>
        </div>
      </div>
    )
  }
}
