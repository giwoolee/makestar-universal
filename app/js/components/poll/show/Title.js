import React, { Component, PropTypes } from 'react'

export default class Title extends Component {
  render() {
    const { title, idx } = this.props

    return (
      <ttl className="title-flex-container">
        <div className='poll-show-item-number'>
          <span>{ idx }</span>
        </div>
        <div className='poll-show-title-text'>
          <span>{ title }</span>
        </div>
      </ttl>
    )
  }
}
