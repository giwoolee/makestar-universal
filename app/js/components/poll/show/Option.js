import React, { Component } from 'react'
import OptionImage from './OptionImage'
import OptionVotePercentage from './OptionVotePercentage'
import OptionVote from './OptionVote'

export default class Option extends Component {
  render() {
    const {
      idx,
      rank,
      imageUrl,
      pollIdx,
      totalVoteCount,
      myVoteCount,
      voteCount,
      name,
      linkUrl,
      linkText,
      isAvailable,
      isEnd,
      actions,
    } = this.props

    return(
      <div className='col-xs-12 col-md-12 col-lg-12 poll-show-option'>
        <OptionImage { ...{ rank, imageUrl } } />
        <OptionVotePercentage { ...{ voteCount, totalVoteCount, name, linkUrl, linkText } } />
        <OptionVote { ...{ actions, pollIdx, idx, voteCount, myVoteCount,isEnd, isAvailable } } />
      </div>
    )
  }
}
