import React, { Component } from 'react'
import moment from 'moment'
import { FormattedHTMLMessage } from 'react-intl'

export default class PollDate extends Component {
  render() {
    const { startDate, endDate } = this.props

    return(
      <polldate>
        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-date-container'>
          <div className='date-icon'></div>
          <div className='date-text'>
            <FormattedHTMLMessage id="poll.duration" values={{startDate: moment(startDate).format('YYYY-MM-DD'), endDate: moment(endDate).format('YYYY-MM-DD')}}/>
          </div>
        </div>
      </polldate>
    )
  }
}
