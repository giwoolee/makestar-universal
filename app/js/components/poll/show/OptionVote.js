import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import classnames from 'classnames'

export default class OptionVote extends Component {

  render() {
    const {
      idx,
      pollIdx,
      myVoteCount,
      isEnd,
      isAvailable,
    } = this.props

    const {
      pollVote
    } = this.props.actions

    return(
      <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2 poll-show-vote-container">
        <div className={ classnames({ 'poll-show-vote-btn-off': myVoteCount == 0 ? true : false, 'poll-show-vote-btn-on': myVoteCount > 0 ? true : false }) } onClick={ () => { pollVote({ pathname: `/polls/${pollIdx}/candidates/${idx}/vote`,isEnd, isAvailable }) } }>
        </div>
        <div className={ classnames({ 'poll-show-vote-voted-text': myVoteCount > 0 ? true : false, 'poll-show-vote-not-voted-text': myVoteCount == 0 ? true : false }) }>
          <FormattedMessage id="poll.vote" values={ { myVoteCount } }/>
        </div>
      </div>
    )
  }
}
