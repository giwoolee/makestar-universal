import React, { Component } from 'react'
import LazyLoad from 'react-lazy-load'

export default class OptionImage extends Component {
  rankStyle(rank) {
    switch(rank) {
      case(1):
        return 'option-image-rank-1'
      case(2):
        return 'option-image-rank-2'
      case(3):
        return 'option-image-rank-3'
      default:
        return 'option-image-no-rank'
    }
  }
  render() {
    const {
      imageUrl,
      rank
    } = this.props

    let divStyle = {
      backgroundImage: 'url(' + imageUrl + ')',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      width: '100%',
      height: '100%',
      zIndex: 0,
      position: 'absolute',
      border: '2px solid #ffff'
    }
    return(
      <div className='col-xs-5 col-md-5 col-lg-5 poll-show-option-image'>
        <LazyLoad><div className={this.rankStyle(rank) }/></LazyLoad>
        <div style={divStyle}/>
      </div>
    )
  }
}
