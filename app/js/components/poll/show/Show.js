import React, { Component, PropTypes } from 'react'
import process from 'process'
import { chunk, findIndex } from 'lodash'
import Title from './Title'
import Header from './Header'
import StatusContainer from '../../../containers/poll/shared/StatusContainer'
import PollDate from './PollDate'
import Tag from '../../shared/Tag'
import Option from './Option'
import CommentsContainer from '../../../containers/shared/comment/CommentsContainer'
import OptionContainer from '../../../containers/poll/show/OptionContainer'
import StatusModal from '../../../containers/shared/StatusModal'
import LoginModal from '../../../containers/shared/LoginModal'
import BackNav from '../../../components/shared/BackNav'
import ShareContainer from '../../../containers/shared/sns/ShareContainer'
import HelmetContainer from '../../../containers/poll/show/HelmetContainer'

export default class Show extends Component {
  componentDidMount() {
    if(process.browser) {
      const { iframeResizer } = require('iframe-resizer')
      iframeResizer({ autoResize: true, heightCalculationMethod: 'lowestElement', checkOrigin: false },'.resizable-iframe')
    }
  }
  render() {
    const { idx, title, ranks, headerUrl, isEnd, voteCount, myVoteCount, isAvailable, endDate, startDate, tags, actions, locale, share, votingType } = this.props
    return(
      <poll>
        <HelmetContainer/>
        <StatusModal/>
        <LoginModal/>
        <div className='container-fluid poll-show-container'>
          <div className='row poll-show-title'>
            <Title title={ title } idx={idx}/>
          </div>
          <div className='row poll-show-header'>
            <Header src={ headerUrl }/>
          </div>
          <div className='row poll-show-status'>
            <StatusContainer idx={idx} />
          </div>
          <div className='row poll-show-date'>
            <PollDate startDate={ startDate } endDate={ endDate } />
          </div>
          <div className='row'>
            <Tag tags={tags} className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-tag-container'/>
          </div>
          <div className='row'>
            <ShareContainer type='polls' />
          </div>
          <div className='row poll-show-options'>
            { ranks.map((r,i) =>
              <div key={'polls-' + i} className='row'>
              { r.map( (ridx) =>
                <div className='col-xs-12 col-sm-12 col-md-12 col-lg-6'>
                  <OptionContainer idx={ridx} pollIdx={idx} />
                </div>
              ) }
              </div>
            ) }
          </div>
          <div className='row'>
            <BackNav to='/polls?status=Open' messageId="poll.backNav"/>
          </div>
          <CommentsContainer idx={idx} />
        </div>
      </poll>
    )
  }
}

Show.propTypes = {
  actions: PropTypes.object.isRequired,
}
