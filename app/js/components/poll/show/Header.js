import React, { Component } from 'react'

export default class Header extends Component {
  render () {
    const { src } = this.props

    return(
      <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-header-iframe'>
        <iframe className='resizable-iframe' src={src} width='100%' height='125px' />
      </div>
    )
  }
}

