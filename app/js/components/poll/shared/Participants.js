import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'

export default class Participants extends Component {

  render() {
    const { voteCount } = this.props
    return(
      <div className='poll-participants-icon'>
        <span className='poll-off-text'><FormattedMessage id="poll.votes" values={ { voteCount } }/></span>
      </div>
    )
  }
}
