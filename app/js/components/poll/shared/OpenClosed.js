import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import classnames from 'classnames'
import moment from 'moment'

const OpenClosed = ({ isEnd, endDate, timeRemaining }) => {
  return(
    <div className={classnames({ 'poll-on-icon': isEnd == false ? true : false, 'poll-off-icon': isEnd == true ? true : false })}>
      <span className={classnames({ 'poll-on-text': isEnd == false ? true : false, 'poll-off-text': isEnd == true ? true : false }) }><FormattedMessage id={timeRemaining.messageId} values={timeRemaining.values} /></span>
    </div>
  )
}

export default OpenClosed