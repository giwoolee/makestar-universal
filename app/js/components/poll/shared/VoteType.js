import React, { PropTypes, Component } from 'react'
import { FormattedMessage } from 'react-intl'

const VoteType = ({ className, messageId }) => {
  return(
    <div className={ className }>
      <span className="poll-off-text"><FormattedMessage id={ messageId } /></span>
    </div>
  )
}

export default VoteType
