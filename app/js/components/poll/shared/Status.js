import React, { Component } from 'react'
import OpenClosedContainer from '../../../containers/poll/shared/OpenClosedContainer'
import Participants from './Participants'
import Vote from './Vote'
import VoteTypeContainer from '../../../containers/poll/shared/VoteTypeContainer'
import VoteCountContainer from '../../../containers/poll/shared/VoteCountContainer'

const Status = ({ idx, voteCount }) => {
  return (
    <status>
      <div className='col-xs-6 col-sm-6 col-md-6 poll-status-lg'>
        <OpenClosedContainer idx={idx} />
      </div>
      <div className='col-xs-6 col-sm-6 col-md-6 poll-status-lg'>
        <VoteTypeContainer idx={idx} />
      </div>
      <div className='col-xs-6 col-sm-6 col-md-6 poll-status-lg'>
        <Participants voteCount={ voteCount } />
      </div>
      <div className='col-xs-6 col-sm-6 col-md-6 poll-status-lg'>
        <VoteCountContainer idx={ idx } />
      </div>
    </status>
  )
}

export default Status
