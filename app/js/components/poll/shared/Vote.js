import React, { PropTypes, Component } from 'react'
import { FormattedMessage } from 'react-intl'

const Vote = ({id, myVoteCount, iconClass, textClass}) => {
  return(
    <div className={ iconClass }>

      <span className={ textClass }>
        <FormattedMessage id={id} values={{ myVoteCount }}/>
      </span>
    </div>
  )
}

export default Vote