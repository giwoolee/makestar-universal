import React from 'react'
import LazyLoad from 'react-lazy-load'

const RankImage = ({ imageClass, backgroundImage, backgroundRepeat, backgroundPosition, backgroundSize }) => {
  return(
    <LazyLoad><div style={ { backgroundImage, backgroundRepeat, backgroundPosition, backgroundSize } } className={ imageClass }></div></LazyLoad>
  )
}

export default RankImage

