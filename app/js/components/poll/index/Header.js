import React, { Component, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'

export default class PollsHeader extends Component {
  render() {
    return (
      <div className='row poll-index-header'>
        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-index-header-title'>
          <FormattedMessage id="poll.title"/>
        </div>
        <div className='col-xs-11 col-sm-11 col-md-11 col-lg-11 poll-index-header-subtitle'>
          <FormattedMessage id="poll.subtitle"/>
        </div>
      </div>
    )
  }
}
