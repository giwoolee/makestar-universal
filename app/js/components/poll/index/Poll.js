import React, { PropTypes, Component } from 'react'
import { Link } from 'react-router'
import RanksContainer from '../../../containers/poll/index/RanksContainer'
import ThumbnailContainer from '../../../containers/poll/index/ThumbnailContainer'
import StatusContainer from '../../../containers/poll/shared/StatusContainer'
import Tag from '../../shared/Tag'

const Poll = (props) => {
  const {
    idx,
    title,
    rank,
    thumbnailUrl,
    tags,
    actions
  } = props

  return(
    <div className='poll-item'>
      <Link onClick={ () => { actions.gaEvent({ category: 'Poll', action: 'Click', label: `${idx} ${title}` }) } } to={`/polls/${idx}`} >
        <div className='row poll-item-header-container'>
          <div className='poll-item-number-container'>
            <span className="poll-item-number">{ idx }</span>
          </div>
          <div className='poll-item-title-container'>
            <span className='poll-item-title'>{ title }</span>
          </div>
        </div>
        <div className='row'>
          {
            thumbnailUrl ? <ThumbnailContainer idx={idx} /> : undefined
          }
          {
            !thumbnailUrl && rank.length > 0 ? <RanksContainer idx={idx} /> : undefined
          }
        </div>
        <div className='row'>
          <StatusContainer idx={idx} />
        </div>
        <div className='row'>
          <Tag tags={ tags } className='col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-index-tag-container'/>
        </div>
      </Link>
    </div>
  )
}

export default Poll
