import React from 'react'
import LazyLoad from 'react-lazy-load'

const Thumbnail = ({ thumbnailUrl }) => {
  return(
    <div className='col-xs-12 col-sm-12 col-lg-12 poll-rank-image-container'>
      <LazyLoad height={'100%'}><div className="poll-item-thumbnail-image" style={{ backgroundImage:  thumbnailUrl, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover', height: '100%' }}></div></LazyLoad>
    </div>
  )
}

export default Thumbnail
