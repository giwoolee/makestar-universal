import React from 'react'
import { FormattedMessage } from 'react-intl'

const Empty = ({ selectedStatus }) => {

  const messageId = `poll.empty.${selectedStatus.toLowerCase()}`
  return (
    <div className="row row-center poll-index-empty-container">
      <p className="poll-index-empty-text">
        <FormattedMessage id={messageId} />
      </p>
    </div>
  )
}

export default Empty
