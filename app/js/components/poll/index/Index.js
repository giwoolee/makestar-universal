import React, { Component, PropTypes } from 'react'
import PollsContainer from '../../../containers/poll/index/PollsContainer'
import Header from './Header'
import Empty from './Empty'
import PaginationContainer from '../../../containers/shared/PaginationContainer'
import PollContainer from '../../../containers/poll/index/PollContainer'
import FilterContainer from '../../../containers/shared/filter/FilterContainer'
import IndexHeaderContainer from '../../../containers/shared/index/IndexHeaderContainer'
import HelmetContainer from '../../../containers/poll/index/HelmetContainer'

export default class Index extends Component {
  renderPoll() {
    const { idxs } = this.props
    return(
      <div className='row row-centered'>
        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered'>
           { idxs.map(ch  =>
            <div className='row'>
            { ch.map( idx =>
                  <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
                    <PollContainer idx={idx} />
                  </div>
              ) }
            </div>
           ) }
        </div>
      </div>
    )
  }

  render() {
    const { numOfPolls, selectedStatus } = this.props

    return (
      <div>
        <HelmetContainer/>
        <div className='container-fluid index-header-container'>
          <IndexHeaderContainer/>
        </div>
        <div className='container poll-index-area'>
          <div className='row row-centered-mobile filter-container'>
            <FilterContainer />
          </div>
          {
            numOfPolls > 0 ? this.renderPoll() : <Empty selectedStatus={ selectedStatus }/>
          }
        </div>
        {
          numOfPolls > 0 ? <PaginationContainer { ...{ pushType: true, type: 'Polls', defaultQuery: { status: 'All' } } }/> : undefined
        }
      </div>
    )
  }
}

Index.propTypes = {
  idxs: PropTypes.array.isRequired,
  numOfPolls: PropTypes.number.isRequired
}

