import React from 'react'
import RankImageContainer from '../../../containers/poll/index/RankImageContainer'

const RankImages = ({ ranks }) => {
  return(
    <div className='col-xs-12 col-sm-12 col-lg-12 poll-rank-image-container'>
      { ranks.map((r,i,rs) => {
          return(<RankImageContainer idx={r} index={i} length={rs.length} />)
        })
      }
    </div>
  )
}

export default RankImages
