import moment from 'moment'
import React, { Component, propTypes } from 'react'

export default class Header extends Component {
  render() {
    const { subject, description, date, author } = this.props
    return(
      <header>
        <div className='content-show-header-subject-container'>
          <p itemProp='name' className='content-show-header-subject-text'>{ subject }</p>
        </div>
        <div className='content-show-header-description-container'>
          <p itemProp='articleSection' className='content-show-header-description-text'>{ description }</p>
        </div>
        <div className='content-show-header-author-date-container'>
          <div itemProp='publisher' itemType='http://schema.org/Organization' className='content-show-header-author-text'>by { author.name }</div>
          <div itemProp='datePublished' content={ moment(date).format('YYYY/MM/DD')}className='content-show-header-date-text'>{ moment(date).format('YYYY/MM/DD') }</div>
        </div>
      </header>
    )
  }
}
