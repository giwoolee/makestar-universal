import React, { Component, propTypes } from 'react'
import ShareContainer from '../../../containers/shared/sns/ShareContainer'
import Header from './Header'
import Tag from '../../shared/Tag'
import ContentIFrame from './ContentIFrame'
import AuthorBlurb from './AuthorBlurb'
import CommentsContainer from '../../../containers/shared/comment/CommentsContainer'
import LoginModal from '../../../containers/shared/LoginModal'
import BackNav from '../../shared/BackNav'
import process from 'process'
import HelmetContainer from '../../../containers/content/show/HelmetContainer'

export default class Show extends Component {

  componentDidMount() {
    if(process.browser) {
      const { iframeResizer } = require('iframe-resizer')
      iframeResizer({ autoResize: true, heightCalculationMethod: 'lowestElement', checkOrigin: false },'.resizable-iframe')
    }
  }

  render() {
    const { idx, subject, textUrl, description, date, author, tags, actions } = this.props
    return(
      <content>
        <HelmetContainer/>
        <LoginModal/>
        <div itemScope itemType='http://schema.org/Article' className='container-fluid content-show-content-container'>
          <div className='row'>
            <div className='col-xs-12 col-sm-12 col-md-8 col-lg-8 content-show-content'>
              <div className='content-show-header-container'>
                <Header { ...{ subject: subject, description: description, date: date, author: author } } />
                <div className='content-show-header-tag-container'>
                  <Tag className='content-show-content-tag' tags={ tags }/>
                </div>
                <div className='content-show-header-sns-share-container'>
                  <ShareContainer type='contents' />
                </div>
              </div>
              <div className='content-show-iframe-container'>
                <ContentIFrame { ...{ src: textUrl } } />
              </div>
              <div className='hidden-md hidden-lg content-show-authorblurb-container'>
                <AuthorBlurb { ...author }/>
              </div>
            </div>
            <div className='hidden-xs hidden-sm col-md-4 col-lg-4'>
              <div className='content-show-authorblurb-container'>
                <AuthorBlurb {...author }/>
              </div>
            </div>
            <BackNav to="/contents/articles" messageId="content.backNav"/>
          </div>
          <CommentsContainer idx={idx}/>
        </div>
      </content>)
  }
}
