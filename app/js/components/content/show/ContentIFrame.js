import React, { Component, propTypes } from 'react'

export default class ContentIFrame extends Component {

  render() {
    const { src } = this.props
    return(
      <iframe className='resizable-iframe content-show-content-iframe' src={src} />
    )
  }
}
