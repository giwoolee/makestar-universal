import React, { Component, propTypes } from 'react'

export default class AuthorBlurb extends Component {
  render() {
    const { name, description, imageUrl } = this.props

    return(
      <authorblurb>
        <div className='content-show-authorblurb-image-name-container'>
          <div className='content-show-authorblurb-image-container'>
            <img className='content-show-authorblurb-image' src={imageUrl}></img>
          </div>
          <div className='content-show-authorblurb-name-container'>
            <p className='content-show-authorblurb-name-text'>{ name }</p>
          </div>
        </div>
        <div className='content-show-authorblurb-description-container'>
          <p className='content-show-authorblurb-description-text'>{ description }</p>
        </div>
      </authorblurb>
    )
  }
}
