import React, { PropTypes, Component } from 'react'
import ContentContainer from '../../../containers/content/index/ContentContainer'
import Masonry from 'react-masonry-component'
import HelmetContainer from '../../../containers/content/index/HelmetContainer'
import { FormattedMessage } from 'react-intl'

const Index = ({ idxs, isInitialLoad, isLast, page, actions, isFetching, pathname, query, buttonMessageId }) => {
  if(isInitialLoad) {
    return(<div className='loader-lg'></div>)
  } else {
    return(
      <div className='container-fluid content-index-container'>
        <HelmetContainer/>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered'>
            <div className='row'>
              <Masonry updateOnEachImageLoad={true} disableImagesLoaded={false}>
                { idxs.map(idx => {
                  return(<div className='col-xs-12 col-sm-6 col-md-4 col-lg-4'>
                    <ContentContainer key={idx} idx={idx} />
                  </div>)
                } ) }
              </Masonry>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 content-index-more-btn-container col-centered'>
            {
              isLast || isInitialLoad || isFetching ? undefined : <button className='btn content-index-more-btn' onClick={() => { actions.fetchContents({ pathname, query }) } } ><FormattedMessage id={buttonMessageId} /></button>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default Index
