import React, { Component, PropTypes } from 'react'
import moment from 'moment'
import { Link } from 'react-router'
import Tag from '../../shared/Tag'

export default class Content extends Component {
  render() {
    const {
      idx,
      author: { name },
      subject,
      description,
      imageUrl,
      links,
      tags,
      date
    } = this.props

    const resourceUrl = `/contents/articles/${idx}`

    return(
      <Link to={resourceUrl} >
        <div className='content-index-content-container'>
          <div className='content-index-content-img-container'>
            <img className="img-responsive" src={ imageUrl } />
          </div>
          <div className='content-index-content-info-container'>
            <div className='content-index-content-info-subject-container'>
              <p className='content-index-content-info-subject-text'>{ subject }</p>
            </div>
            <div className='content-index-content-info-description-container'>
              <p className='content-index-content-info-description-text'>{ description }</p>
            </div>
            <div className='content-index-content-info-author-date-container'>
              <div className='content-index-content-info-author-text'>
                by { name }
              </div>
              <div className='content-index-content-info-date-text'>
                { moment(date).format('YYYY/MM/DD') }
              </div>
            </div>
            <div className='content-index-content-tag-container'>
              <Tag tags={tags} className='content-index-tags'/>
            </div>
          </div>
        </div>
      </Link>
    )
  }
}
