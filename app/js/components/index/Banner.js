import React, { Component } from 'react'
import { last } from 'lodash'
import classnames from 'classnames'
import BannerItem from './BannerItem'

export default class Banner extends Component {
  render() {
    const { containerWidth, items, actions, set, isInitialLoad } = this.props
    return(
      <div className='container-fluid banner-container'>
        <div className={classnames({'banner-fit-container': items.length > 1, 'center-block': items.length > 1,'banner-full-container': items.length === 1 })}>
            {
              items.map((bi,i,bnrs) => {
                return(<BannerItem key={ bi.idx } { ...{ ...bi, width: (100 / items.length), isInitialLoad, isLast: last(bnrs).idx === bi.idx, set, actions } } />)
              })
            }
          </div>
      </div>
    )
  }
}
