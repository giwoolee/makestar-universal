import React, { Component } from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import LazyLoad from 'react-lazy-load'

const ContentItem = ({ imageUrl, subject, links, isLast, actions, isInitialLoad }) => {
  const [_,web] = links
  return(
    <div className={ classnames({ 'index-content-item-container': true, 'index-item-first': isLast === false ? true : false,'index-item-last': isLast === true ? true : false })}>
      <Link to={web.href} onClick={ () => { actions.gaEvent({ category: 'Home Magazine', action: 'View', label: subject }) } }>
        <div className='index-content-item-img-container'>
          <LazyLoad height={'100%'}>
            <div style={{ backgroundImage: isInitialLoad ? "url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)" : `url(${imageUrl})`, backgroundSize: 'cover', backgroundPosition: 'top', backgroundRepeat: 'no-repeat' }} data-src={`url(${imageUrl})`} className='index-content-item-image defer-bg-img'/>
          </LazyLoad>
        </div>
        <div className='index-content-item-title-container'>
          <p className='index-content-item-title-text'>{ subject }</p>
        </div>
      </Link>
    </div>
  )
}

export default ContentItem
