import React, { Component } from 'react'
import PollItem from './PollItem'

const Polls = ({ polls, actions }) => {
  return(
    <div className='index-polls-container'>
      {
        polls.map((p,i)=> {
          return(
            <PollItem key={`polls-${i}`} {...p} actions={actions} />
          )
        })
      }
    </div>
  )
}

export default Polls
