import React, { Component } from 'react'
import classnames from 'classnames'
import LazyLoad from 'react-lazy-load'

export default class BannerItem extends Component {
  renderBanner() {
    const { mobileImageUrl, webImageUrl, videoUrl, width, isLast, isInitialLoad } = this.props
    const left = width === 50 && isLast === false
    const right = width === 50 && isLast === true
    return(
      <div className={classnames({'banner-item-container': true, 'banner-item-container-left': left, 'banner-item-container-right': right })} style={ { width: `${width}%` } }>
        <LazyLoad height={'100%'}><div style={ { backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url(${webImageUrl})`, height: width === 100 ? '380px' : '200px' } } className='banner-item-image hidden-xs'/></LazyLoad>
        <LazyLoad height={'100%'}><div style={ { backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url(${mobileImageUrl})`, height: width === 100 ? '140px' : '90px' } } className='banner-item-image hidden-sm hidden-md hidden-lg'/></LazyLoad>
      </div>
    )
  }

  render() {
    const { linkUrl, actions, position, title, set } = this.props
    if(linkUrl) {
      return(
        <a onClick={ () => { actions.gaEvent({ category: 'Main Banner', label: title, value: position, action: set }) } } href={linkUrl}>{ this.renderBanner() }</a>
      )
    } else {
      return this.renderBanner()
    }
  }
}
