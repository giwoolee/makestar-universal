import React from 'react'
import { Button } from 'react-bootstrap'

const TopNotice = (props) => {
  const { style, idx, text, iconImageUrl, buttonText, linkUrl } = props
  return(
    <topnotice id={idx}>
    {
      props.idx ?
      <div className='top-notice' style={style}>
        <div className='top-notice-button-container pull-left'>
          <a href={linkUrl}><Button className='top-notice-button'>{buttonText}</Button></a>
        </div>
        <div className='top-notice-text-container'>
          <div className='top-notice-icon' style={ { backgroundImage: `url(${iconImageUrl})`, backgroundRepeat: 'no-repeat', backgroundSize: 'contain' } }/>
          <p className='top-notice-text'>{ text }</p>
        </div>
      </div>
      : undefined
    }
    </topnotice>
  )
}

export default TopNotice
