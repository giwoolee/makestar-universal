import React from 'react'
import { range, map } from 'lodash'

const RollingBanner = () => {
  const imageRangeOne = range(2,25)
  const imageRangeTwo = range(1,16)

  const firstImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALIAAAAlAQMAAADcJyHQAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AU0BsAAAN4AAEu/rFqAAAAAElFTkSuQmCC"

  return(
    <div className='container-fluid rolling-banner-container'>
      <div className='rolling-banner'>
        <img className={`cooperation-1 first`} style={{ backgroundSize: '100%' }} src={firstImage} />
        {
          map(imageRangeOne,(v) => {
              return(<img className={`cooperation-${v}`} src={firstImage} style={{ backgroundSize: '100%' }}/>)
          })
        }
        {
          map(imageRangeTwo,(v) => {
            return(<img className={`cooperation-${v}`} src={firstImage} style={{ backgroundSize: '100%' }}/>)
          })
        }
      </div>
    </div>
  )
}

export default RollingBanner
