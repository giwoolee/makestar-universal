import React, { Component } from 'react'
import ProjectUpdateContainer from '../../containers/index/ProjectUpdateContainer'
import LazyLoad from 'react-lazy-load'

export default class ProjectUpdates extends Component {

  slide(direction) {
    /**
     * This function slides the images inside the container.
     * 'scrollingFullWidth' is the total width of the list
     * 'scrollingContainer' is the container where the images slides in
     * 'listItemWidth' is the individual width of each list item
     */
    const scrollingFullWidth = document.getElementsByClassName('project-update-scrollable')[0]
    const scrollingContainer = document.getElementsByClassName('project-update-scrollable-container')[0]
    const listItemWidth = document.getElementsByClassName('project-update-update-list-item')[0]
    const leftArrow = document.getElementsByClassName('left-arrow')[0]
    const rightArrow = document.getElementsByClassName('right-arrow')[0]

    let currentLeftValue = typeof window.getComputedStyle !== 'undefined' ? window.getComputedStyle(scrollingFullWidth, null).getPropertyValue('left') : scrollingFullWidth.currentStyle['left'];
    let currentPosition = currentLeftValue != "auto" ? parseInt(currentLeftValue) : 0
    let remainingWidth = scrollingFullWidth.scrollWidth - scrollingContainer.offsetWidth
    let scrollingWidth = remainingWidth / 2
    let safetyBuffer = scrollingWidth / 4

    if(direction == 'left' && ((currentPosition + scrollingWidth) % scrollingWidth === 0)) {
      if (currentPosition + scrollingWidth < safetyBuffer) {
        scrollingFullWidth.style.left = currentPosition + scrollingWidth + 'px'
        rightArrow.style.right = '0px'
        rightArrow.style.opacity = '.5'
        rightArrow.style.cursor = 'pointer'
      }

      if (currentPosition + scrollingWidth >= 0) {
        leftArrow.style.left = '-40px'
        leftArrow.style.opacity = '0'
        leftArrow.style.cursor = 'default'
      }
    } else if(direction == 'right' && ((currentPosition + scrollingWidth) % scrollingWidth === 0)) {
      if(currentPosition - scrollingWidth > (-1 * remainingWidth - safetyBuffer)) {
        scrollingFullWidth.style.left = currentPosition - scrollingWidth + 'px'

        try {
          window.dispatchEvent(new Event('scroll', { 'view': window, 'bubbles': true, 'cancelable': true }))
        } catch (e) {
          const event = document.createEvent('Event')
          event.initEvent('scroll',true,true)
          window.dispatchEvent(event)
        }

        leftArrow.style.left = '0px'
        leftArrow.style.opacity = '.5'
        leftArrow.style.cursor = 'pointer'
        leftArrow.style.visibility = 'visible'
        if (currentPosition - scrollingWidth <= (-1 * remainingWidth + safetyBuffer)) {
          rightArrow.style.right = '-40px'
          rightArrow.style.opacity = '0'
          rightArrow.style.cursor = 'default'
        }
      }
    }
  }

  render() {
    const { idxs } = this.props
    return(
      <div className='project-update-container-box'>
        <div className='project-update-scrollable-container'>
          <ul className='project-update-scrollable'>
            {
              idxs.map((idx) => {
                return(
                  <ProjectUpdateContainer key={idx} idx={idx}/>
                )
              })
            }
          </ul>
        </div>
        <a className='project-update-scrollable-icon left-arrow' onClick={this.slide.bind(this, 'left')}>
          <i className='fa fa-chevron-left'></i>
        </a>
        <a className='project-update-scrollable-icon right-arrow' onClick={this.slide.bind(this, 'right')}>
          <i className='fa fa-chevron-right'></i>
        </a>
      </div>
    )
  }
}
