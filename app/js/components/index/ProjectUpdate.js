import React, { Component } from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import { FormattedMessage, FormattedRelative } from 'react-intl'
import LazyLoad from 'react-lazy-load'

const ProjectUpdate = ({ dateMessageId, projectShortName, title, link, iconImageUrl, date, imageUrl, actions }) => {
  return(
    <li className='project-update-update-list-item'>
      <a onClick={ actions.onClick } className='project-update-update-link' href={link}>
        <LazyLoad offsetLeft={0} height={'100%'} width={'100%'} once>
          <div className='project-update-update-image' style={{ backgroundImage: imageUrl }} />
        </LazyLoad>
        <div className='project-update-update-image-mask'></div>
        <div className='project-update-update-icon-container'>
          <LazyLoad offsetLeft={0} height={'100%'} width={'100%'} once>
            <div style={{ backgroundImage: iconImageUrl }} className='project-update-update-icon'></div>
          </LazyLoad>
        </div>
        <div className='project-update-update-info'>
          <p className='project-update-update-short-project-name'>
            { projectShortName }
          </p>
        </div>
        <div className='project-update-update-title-container'>
          <p className='project-update-update-title-text'>{ title }</p>
        </div>
        <div className='project-update-update-type-icon-container created-date'>
          <i className='fa fa-clock-o project-update-update-type-icon'></i>
          <span className='project-update-update-time'>
            <FormattedMessage id={dateMessageId} values={{ date: <FormattedRelative value={ date } /> }} />
          </span>
        </div>
      </a>
    </li>
  )
}

export default ProjectUpdate
