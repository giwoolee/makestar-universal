import React, { Component } from 'react'
import Banner from './Banner'
import ProjectUpdatesContainer from '../../containers/index/ProjectUpdatesContainer'
import ContentsContainer from '../../containers/index/ContentsContainer'
import PollsContainer from '../../containers/index/PollsContainer'
import ProjectsContainer from '../../containers/index/ProjectsContainer'
import HelmetContainer from '../../containers/index/HelmetContainer'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'
import { Button } from 'react-bootstrap'
import Project from '../project/index/Project'
import RollingBanner from './RollingBanner'

export default class Index extends Component {
  render() {
    const {
      banners,
      projects,
      projectUpdates,
      articles,
      isInitialLoad,
      actions,
      viewAllMessageId,
      projectUpdateMessageId,
      projectTitleMessageId,
      pollUpdateMessageId,
      contentLatestMessageId
    } = this.props

    const chevron_right = () => {
      return (
        <chevrons>
          <i className="fa fa-chevron-right" aria-hidden="true"></i>
          <i className="fa fa-chevron-right" style={{ marginLeft: "-5px" }} aria-hidden="true"></i>
        </chevrons>
      )
    }
    return(
      <index>
        <HelmetContainer/>
        { banners['1'] !== undefined ? <Banner { ...banners['1'] } set="1" actions={actions} isInitialLoad={isInitialLoad} items={banners['1'].items} /> : undefined }
        <div className='container-fluid index-container project-container'>
          <div className='row'>
            <div className='index-section-title-all-container'>
              <div className='index-section-title-container'>
                <span className='index-section-title-text'><FormattedMessage id={projectUpdateMessageId}/></span>
              </div>
            </div>
            <div className='index-projects-updates-container'>
              <ProjectUpdatesContainer />
            </div>
          </div>
          <div className='row'>
            <div className='hidden-sm hidden-md hidden-lg index-section-title-all-btn-container-xs'>
              <div className='index-section-title-container'>
                <span className='index-section-title-text'><FormattedMessage  id={projectTitleMessageId}/></span>
              </div>
              <div className='index-section-all-container pull-right'>
                <Link className="index-section-all-container-link" to="/projects" onClick={ () => { actions.gaEvent({ category: 'Home Projects', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
              </div>
            </div>
            <ProjectsContainer type='mobile'/>
            <div className='hidden-xs index-section-title-all-btn-container'>
              <div className='index-section-title-container'>
                <span className='index-section-title-text'><FormattedMessage id={projectTitleMessageId}/></span>
              </div>
              <div className='index-section-all-container pull-right'>
                <Link className="index-section-all-container-link" to='/projects' onClick={ () => { actions.gaEvent({ category: 'Home Projects', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
              </div>
            </div>
            <ProjectsContainer type='web'/>
          </div>
        </div>

        <div className="index-section-contents-poll-container">
          <div className="container-fluid index-container">
            <div className='row hidden-xs'>
              <div className='hidden-xs index-all-content-row'>
                <div className='index-all-content-container'>
                  <div className='index-content-poll-container'>
                    <div className='index-content-container'>
                      <div className='index-section-all-content-title-all-btn-container'>
                        <div className='index-section-title-container'>
                          <span className='index-section-title-text'><FormattedMessage id={contentLatestMessageId}/></span>
                        </div>
                        <div className='index-section-all-container pull-right'>
                          <Link className="index-section-all-container-link" to="/contents/articles" onClick={ () => { actions.gaEvent({ category: 'Home Magazine', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
                        </div>
                      </div>
                      <ContentsContainer numToRender={4} />
                    </div>
                    <div className='index-poll-container'>
                      <div className='index-section-all-content-title-all-btn-container'>
                        <div className='index-section-title-container'>
                          <span className='index-section-title-text'><FormattedMessage id={pollUpdateMessageId}/></span>
                        </div>
                        <div className='index-section-all-container pull-right'>
                          <Link className="index-section-all-container-link" to='/polls' onClick={ () => { actions.gaEvent({ category: 'Home Polls', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
                        </div>
                      </div>
                      <PollsContainer numToRender={4} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='row hidden-sm hidden-md hidden-lg'>
              <div className='hidden-sm hidden-md hidden-lg index-section-title-all-btn-container-xs'>
                <div className='index-section-title-container'>
                  <span className='index-section-title-text'><FormattedMessage id={contentLatestMessageId}/></span>
                </div>
                <div className='index-section-all-container pull-right'>
                  <Link className="index-section-all-container-link" to="/contents/articles" onClick={ () => { actions.gaEvent({ category: 'Home Magazine', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
                </div>
              </div>
              <div className='hidden-sm hidden-md hidden-lg'>
                <ContentsContainer numToRender={2} />
              </div>
            </div>

            <div className='row hidden-sm hidden-md hidden-lg index-section-latest-poll-container'>
              <div className='hidden-sm hidden-md hidden-lg index-section-title-all-btn-container-xs'>
                <div className='index-section-title-container'>
                  <span className='index-section-title-text'><FormattedMessage id={pollUpdateMessageId}/></span>
                </div>
                <div className='index-section-all-container pull-right'>
                  <Link className="index-section-all-container-link" to="/polls" onClick={ () => { actions.gaEvent({ category: 'Home Polls', action: 'View All' }) } }><FormattedMessage id={viewAllMessageId}/> { chevron_right() } </Link>
                </div>
              </div>
              <div className='hidden-sm hidden-md hidden-lg'>
                <PollsContainer numToRender={2} />
              </div>
            </div>
          </div>
        </div>
        { banners['2'] ? <Banner { ...banners['2'] } set="2" actions={actions} isInitialLoad={isInitialLoad} items={banners['2'].items} /> : undefined }
        { banners['3'] ? <Banner { ...banners['3'] } set="3" actions={actions} isInitialLoad={isInitialLoad} items={banners['3'].items} /> : undefined }
        <RollingBanner/>
      </index>
    )
  }
}
