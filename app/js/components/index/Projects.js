import React, { Component } from 'react'
import classnames from 'classnames'
import Project from '../project/index/Project'

export default class Projects extends Component {

  renderProject(p) {
    const { type, actions, isInitialLoad } = this.props
    const id = type === 'mobile' ? `project-xs-${p._id}` : `project-${p._id}`
    return(
      <div  onClick={ () => { actions.gaEvent({ category: 'Home Projects', action: 'View' }) } }
            key={id}
            className={
              classnames({
                'index-project-xs': type === 'mobile' ? true : false,
                'index-project': type === 'web' ? true : false
              })
            }>

      <Project { ...Object.assign({},p,{ isInitialLoad, actions: actions }) } />
    </div>)
  }

  render() {
    const { type, projects } = this.props
    if(type === 'web') {
      return(
        <div  className='hidden-xs index-projects-container'>
          {
            projects.map((chunk,i) => {
              return(
                <div key={ `${type}-${i}`} className='index-project-row'>
                  { chunk.map((p) => { return(this.renderProject(p)) }) }
                </div>
              )
            })
          }
        </div>)
    } else {
      return(
        <div>
          <div className='hidden-sm hidden-md hidden-lg'></div>
            <div ref='index-projects-container' className='hidden-sm hidden-md hidden-lg index-projects-container-xs'>
              <ul className='index-projects-scrollable-xs'>
                {
                  projects.map((chunk,ci) => { return(chunk.map((p,pi) => { return(<li key={`index-project-xs-${ci}-${pi}`}>{this.renderProject(p)}</li>)})) })
                }
              </ul>
          </div>
        </div>
      )
    }
  }
}
