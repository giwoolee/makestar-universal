import React, { Component } from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import moment from 'moment'
import { FormattedMessage } from 'react-intl'
import LazyLoad from 'react-lazy-load'

const PollItem = ({ title, rank, links, isEnd, endDate, isLast, thumbnailUrl, actions, isInitialLoad }) => {
  const [image] = rank
  const [_,web] = links
  let days = moment().diff(moment(endDate),'days')
  const imageUrl = thumbnailUrl ? `url(${thumbnailUrl})` : `url(${image.imageUrl})`
  return(
    <div className={ classnames({'index-poll-item-container': true, 'index-item-first': isLast === false ? true: false, 'index-item-last': isLast === true ? true : false }) }>
      <Link to={web.href} onClick={ () => { actions.gaEvent({ category: 'Home Polls', action: 'View', label: title }) } }>
        <div className='index-poll-item-img-container'>
          <LazyLoad height={'100%'}>
            <div style={{ backgroundSize: 'cover', backgroundPosition: 'top', backgroundRepeat: 'no-repeat', backgroundImage: isInitialLoad ? 'url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)' : imageUrl }} className='index-poll-item-image defer-bg-img'>
              <p className='index-poll-item-overlay-text'>
                { isEnd === true ? <FormattedMessage id='poll.off'/> : <FormattedMessage id='poll.on' values={{ days: Math.abs(days - 1) }} /> }
              </p>
            </div>
          </LazyLoad>
        </div>
        <div className='index-poll-item-title-container'>
          <p className='index-poll-item-title-text'>{ title }</p>
        </div>
      </Link>
    </div>
  )
}

export default PollItem
