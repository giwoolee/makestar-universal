import React, { Component } from 'react'
import ContentItem from './ContentItem'

const Contents = ({ contents, actions }) => {
  return(
    <div className='index-contents-container'>
      {
        contents.map((c,i) => {
          return(
            <ContentItem key={`contents-${i}`} { ...c } actions={actions}/>
          )
        })
      }
    </div>
  )
}

export default Contents
