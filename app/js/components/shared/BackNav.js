import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'

export default class BackNav extends Component {
  render() {
    const { to, messageId } = this.props
    return(
      <div className='col-xs-12 col-sm-12 back-nav-container hidden-md hidden-lg'>
        <Link to={ to } className="back-nav-link"><FormattedMessage id={messageId }/></Link>
      </div>
    )
  }
}
