import React, { Component } from 'react'
import FilterLink from './FilterLink'

export default class Filter extends Component {
  render() {
    const { messageId, filters, actions } = this.props

    return(
      <filter className="filter-filter">
      { filters.map((f) => {
          return(<FilterLink key={ f.filter } { ...Object.assign(f,{ actions },{ messageId }) } />)
        })
      }
      </filter>
    )
  }
}
