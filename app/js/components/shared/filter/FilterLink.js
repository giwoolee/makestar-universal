import React, { PropTypes, Component } from 'react'
import { Link } from 'react-router'
import { FormattedMessage } from 'react-intl'
import classnames from 'classnames'

class FilterLink extends Component {
  render() {
    const { selected, filter, queryString, isLast, messageId, gaData, actions } = this.props
    return (
      <Link onClick={ () => { actions.gaEvent(gaData) } } to={queryString} className={classnames('filter-link', { 'filter-link-selected': selected }, { 'filter-last-link': isLast })} ><FormattedMessage id={messageId} values={{ filter }}/></Link>
    )
  }
}

FilterLink.proptypes = {
  selected: PropTypes.bool.isRequired,
  status: PropTypes.string.isRequired
}

export default FilterLink
