import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { VelocityTransitionGroup } from 'velocity-react'
import classnames from 'classnames'

class Notification extends Component {
  componentDidUpdate() {
    const { actions, show  } = this.props
    if(show === true) {
      setTimeout(actions.resetNotification,3000)
    }
  }

  render() {
    const { show, messageId, type, actions } = this.props
    return(
      <VelocityTransitionGroup enter={{ animation: { opacity: 1 }, duration: 200 }} leave={{ animation: { opacity: 0 }, duration: 200 }}>
        { show === true ?
          <notification>
            <div className={classnames('notification',{ 'notification-alert': type === 'alert' ? true : false, 'notification-info': type === 'info' ? true : false })}>
              <div className='notification-close-notification-container'>
                <button className='notification-close-notification-btn' onClick={ actions.resetNotification }>
                  <span>X</span>
                </button>
              </div>
              <div className='notification-message'>
                <FormattedMessage id={ messageId } />
              </div>
            </div>
          </notification> : undefined }
      </VelocityTransitionGroup>
    )
  }
}

export default Notification
