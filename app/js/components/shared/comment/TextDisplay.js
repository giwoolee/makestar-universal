import React from 'react'
import classnames from 'classnames'
import Linkify from 'react-linkify'
import { FormattedMessage } from 'react-intl'

const TextDisplay = ({ text, isVisible, commentDeletedMessageId }) => {
  return(<Linkify properties={{ target: '_blank' }}><p className={ classnames('comment-display-text', { 'comment-display-text-deleted' : !isVisible }) }>
    {
      isVisible === true ? text : <FormattedMessage id={commentDeletedMessageId} />
    }
  </p></Linkify>)
}

export default TextDisplay
