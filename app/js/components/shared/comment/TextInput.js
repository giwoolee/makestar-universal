import React, { Component } from 'react'
import classnames from 'classnames'
import { FormattedMessage, injectIntl } from 'react-intl'

const TextInput = ({ data, idx, pathname, isTextOverLength, textLength, isLoggedIn, leaveComment, textAreaProps, type, inputBtnProps, actions, loginPromptMessageId, postMessageId, intl: { formatMessage } }) => {
  const elTextArea = <textarea { ...textAreaProps } placeholder={ isLoggedIn === false ? formatMessage({ id: loginPromptMessageId }) : ""  } onChange={ (e)=> { actions.updateText(e.target.value); } }/>
  const elTextAreaWrapper = isLoggedIn === false ? <div onClick={ actions.promptLogin }>{ elTextArea }</div> : <div>{ elTextArea }</div>

  return(
    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 comment-text-input-container">
      <div className="comment-text-display">
        <div className="comment-text-input-top-text">
          <FormattedMessage id={leaveComment} />
        </div>
        <div className="comment-text-length-counter">
          <span className={ classnames('comment-text-length-counter-count',{ 'red' : isTextOverLength }) }> { textLength }</span> / 1000
        </div>
      </div>
      <div className="comment-text-input-controls">
        <div className="comment-text-input">
          { elTextAreaWrapper }
        </div>
        <div className="comment-text-input-btn">
           <button { ...inputBtnProps } className={ classnames({ 'disabled': inputBtnProps.disabled }) } onClick={ actions.submit }><FormattedMessage id={postMessageId}/></button>
        </div>
      </div>
    </div>
  )
}

export default injectIntl(TextInput)
