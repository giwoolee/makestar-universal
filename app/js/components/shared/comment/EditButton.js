import React from 'react'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'

const EditButton = ({ idx, isVisible, isDisabled, editMessageId, cancelMessageId, type, actions }) => {
  return(
    <div className='comment-control-btn'>
      { isVisible ? <button className='btn btn-default btn-xs comment-display-edit-btn' onClick={ actions.editOpen }><i className={classnames("fa",{ "fa-pencil": !isDisabled, "fa-times": isDisabled }) }></i><FormattedMessage id={ isDisabled ? cancelMessageId : editMessageId }/></button> : undefined }
    </div>
  )
}

export default EditButton

