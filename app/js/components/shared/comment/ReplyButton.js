import React from 'react'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'

const ReplyButton = ({ idx, replyMessageId, isReplyOpen, isLoggedIn, actions }) => {
  return(
    <div className='comment-control-btn'>
      <button className={ classnames('btn btn-default btn-xs comment-display-reply-btn',{ 'disabled' : isReplyOpen }) } onClick={() => {
        if(isLoggedIn && !isReplyOpen) {
          actions.openReply(idx)
        } else if(!isLoggedIn && !isReplyOpen) {
          actions.promptLogin()
        }
      } } ><i className="fa fa-reply"></i><FormattedMessage id={replyMessageId} /></button>
    </div>
  )
}

export default ReplyButton

