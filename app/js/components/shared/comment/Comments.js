import React from 'react'
import CommentContainer from '../../../containers/shared/comment/CommentContainer'
import TextInputContainer from '../../../containers/shared/comment/TextInputContainer'
import StatusModal from '../../../containers/shared/StatusModal'
import DeletePromptContainer from '../../../containers/shared/comment/DeletePromptContainer'
import { FormattedMessage } from 'react-intl'
import { VelocityTransitionGroup } from 'velocity-react'

const Comments = ({ idx, idxs, isLast, nextPage, actions, moreMessage }) => {
  return(
    <comments>
      <StatusModal/>
      <DeletePromptContainer type='comments' />
      <DeletePromptContainer type='replies' />
      <div className="row comments-container" >
        <TextInputContainer idx={idx} type='comments' />
        <VelocityTransitionGroup enter={{ animation: { opacity: 1, backgroundColor: '#ffffff' }, duration: 1000  }} leave={{ animation: { opacity: 0, backgroundColor: '#ffcd93' }, duration: 1000 }} >
          { idxs.map( (_idx) => {
              return(<div key={`comment-${_idx}`}className="col-xs-12 col-sm-12 col-md-12 col-lg-12 comment-container"><CommentContainer {...{ idx: _idx, type: 'comments'}} /></div>)
            })}
        </VelocityTransitionGroup>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 load-comments-btn-container">
          { isLast === false ? <button className="btn btn-default load-comments-btn" onClick={ () => actions.fetch(nextPage) } ><i className="fa fa-plus"></i><FormattedMessage id={moreMessage}/></button> : undefined }
        </div>
      </div>
    </comments>
  )
}

export default Comments
