import React from 'react'
import { FormattedMessage } from 'react-intl'
import TextInputContainer from '../../../containers/shared/comment/TextInputContainer'
import ReplyContainer from '../../../containers/shared/comment/ReplyContainer'
import DeletePromptContainer from '../../../containers/shared/comment/DeletePromptContainer'
import { VelocityTransitionGroup } from 'velocity-react'
import { map } from 'lodash'

const Replies = ({ parentIdx, idxs, isLoggedIn, isReplyOpen, moreMessage, isLast, nextPage, actions }) => {
  return(
    <replies>
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 replies-container">
        { isLast === false ? <div onClick={ () => { actions.fetch(nextPage) } } className="btn btn-default btn-sm load-replies-btn"><i className="fa fa-plus"></i><FormattedMessage id={moreMessage} /></div> : undefined }
        { isLoggedIn === true && isReplyOpen === true ? <TextInputContainer idx={parentIdx} type="replies"/> : undefined }
        <VelocityTransitionGroup enter={{ animation: { opacity: 1, backgroundColor: '#ffffff' }, duration: 1000 }} leave={{ animation: { opacity: 0, backgroundColor: '#ffcd93' }, duration: 1000 }}>
          { map(idxs,(_idx) => {
            return(<div key={`reply-${_idx}`}className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <ReplyContainer idx={_idx} /></div>)
          })}
        </VelocityTransitionGroup>
      </div>
    </replies>
  )
}

export default Replies
