import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import { FormattedMessage } from 'react-intl'

const DeletePrompt = ({ idx, show, promptMessageId, confirmMessageId, cancelMessageId, type, pathname, actions }) => {

  return(
    <Modal key={`delete-prompt-${idx}`} show={ show }>
      <Modal.Header>
        <Modal.Title>
          <FormattedMessage id={ promptMessageId } className='text-center'/>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Button className='orange-btn full comment-delete-prompt-delete-btn' style={ { marginBottom: '15px' } } onClick={ () => { actions.confirm() } }><FormattedMessage id={confirmMessageId} /></Button>
        <Button className='dark-btn full comment-delete-prompt-cancel-btn' onClick={ () => { actions.cancel() } }><FormattedMessage id={cancelMessageId}/></Button>
      </Modal.Body>
    </Modal>
  )
}

export default DeletePrompt
