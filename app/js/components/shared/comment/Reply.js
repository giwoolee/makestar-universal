import React from 'react'
import moment from 'moment'
import EditTextInputContainer from '../../../containers/shared/comment/EditTextInputContainer'
import EditButtonContainer from '../../../containers/shared/comment/EditButtonContainer'
import DeleteButtonContainer from '../../../containers/shared/comment/DeleteButtonContainer'
import TextDisplayContainer from '../../../containers/shared/comment/TextDisplayContainer'
import DateInfoContainer from '../../../containers/shared/comment/DateInfoContainer'

const Reply = ({ idx, isEditOpen, isMine, isDeleted, text, userName, profileImageUrl, badgeImageUrl, createdDate, type, action }) => {
  return(
    <reply>
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 comment-display-container">
          <div className="comment-display-profile-image-container">
            <img className="comment-display-profile-image img-circle" src={ profileImageUrl }/>
          </div>
        <div className="comment-display">
          <div className="comment-display-meta">
            <div className="comment-display-user-meta-container">
              <div className="comment-display-username">
                { userName }
              </div>
              <div className="comment-display-badge">
                { badgeImageUrl ? <img className="comment-display-profile-badge" src={ badgeImageUrl }/> : undefined }
              </div>
            </div>
            <DateInfoContainer idx={idx} type='replies' />
          </div>
          {
            isEditOpen ? <EditTextInputContainer type='replies' idx={idx} /> :
            <div className="comment-display-text-container">
              <TextDisplayContainer idx={idx} type='replies' />
            </div>
          }
          { !isDeleted && isMine ?
            <div>
              <EditButtonContainer idx={idx} type='replies' />
              <DeleteButtonContainer idx={idx} type='replies' />
            </div> : undefined
          }
        </div>
      </div>
    </reply>
  )
}

export default Reply
