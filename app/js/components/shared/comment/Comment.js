import React from 'react'
import moment from 'moment'
import { FormattedMessage } from 'react-intl'
import RepliesContainer from '../../../containers/shared/comment/RepliesContainer'
import ReplyButtonContainer from '../../../containers/shared/comment/ReplyButtonContainer'
import EditButtonContainer from '../../../containers/shared/comment/EditButtonContainer'
import DeleteButtonContainer from '../../../containers/shared/comment/DeleteButtonContainer'
import EditTextInputContainer from '../../../containers/shared/comment/EditTextInputContainer'
import DateInfoContainer from '../../../containers/shared/comment/DateInfoContainer'
import TextDisplayContainer from '../../../containers/shared/comment/TextDisplayContainer'

const Comment = ({ createdDate, isEditOpen, text, profileImageUrl, badgeImageUrl, idx, userName, closeMessage, type }) => {
  return(
    <comment>
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 comment-display-container">
        <div className="comment-display-profile-image-container">
          <img className="comment-display-profile-image img-circle" src={ profileImageUrl }/>
        </div>
        <div className="comment-display">
          <div className="comment-display-meta">
            <div className="comment-display-user-meta-container">
              <div className="comment-display-username">
                { userName }
              </div>
              <div className="comment-display-badge">
                { badgeImageUrl ? <img className="comment-display-profile-badge" src={ badgeImageUrl }/> : undefined }
              </div>
            </div>
            <DateInfoContainer idx={idx} type='comments' />
          </div>
          {
            isEditOpen ? <EditTextInputContainer type='comments' idx={idx} /> : <div className="comment-display-text-container">
              <TextDisplayContainer idx={idx} type='comments' />
            </div>
          }
          <div>
            <ReplyButtonContainer idx={idx} />
            <EditButtonContainer idx={idx} type='comments' />
            <DeleteButtonContainer idx={idx} type='comments' />
          </div>
        </div>
      </div>
      { type === 'comments' ? <RepliesContainer parentIdx={idx}/> : undefined }
    </comment>
  )
}

export default Comment
