import React from 'react'
import { FormattedMessage, FormattedRelative } from 'react-intl'

const DateInfo = ({ dateMessageId, date }) => {
  return(
    <div className="comment-display-meta-info">
      <FormattedMessage id={dateMessageId} values={{ date: <FormattedRelative value={date} /> }} />
    </div>
  )
}

export default DateInfo

