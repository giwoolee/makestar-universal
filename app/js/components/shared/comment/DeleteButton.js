import React from 'react'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'

const DeleteButton = ({ idx, isVisible, isDisabled, deleteMessageId, type, actions }) => {
  return(
    <div className='comment-control-btn'>
      { isVisible ?
        <button className={ classnames('btn btn-default btn-xs comment-display-delete-btn',{ 'disabled': isDisabled }) } onClick={() => { actions.toggleDeletePrompt({ idx, type, show: isDisabled }) } }><i className="fa fa-trash"></i><FormattedMessage id={deleteMessageId} /></button> : undefined
      }
    </div>
  )
}

export default DeleteButton
