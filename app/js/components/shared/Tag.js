import React, { Component, PropTypes } from 'react'
import { isArray } from 'lodash'

export default class Tag extends Component {
  joinTags() {
    const { tags } = this.props
    if(isArray(tags)) {
      return tags.join(',')
    } else {
      return ''
    }
  }
  render() {
    const {
      className
    } = this.props

    return (
      <div className={className}>
        <div style={{ backgroundImage: `url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAALCAYAAACQy8Z9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDUwOEU1RTBCNjgxMTFFNUE2QThGMkQ4Qzg3QjY2MjEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDUwOEU1RTFCNjgxMTFFNUE2QThGMkQ4Qzg3QjY2MjEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NTA4RTVERUI2ODExMUU1QTZBOEYyRDhDODdCNjYyMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NTA4RTVERkI2ODExMUU1QTZBOEYyRDhDODdCNjYyMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pnpi8KAAAAEWSURBVHjaYvz//z/DmjVrPjEwMPAykAGA+osYGRn7oWyG0NBQBhaoHMjA7UB8A4jZYQpAAKgBl3m/gDgAKN8HpN8A8WKYBAuSojIgvkKiQxuA+DYQLwI64hmQ3gsSZEJSIEqG7z8CsTPUR3uAWBvdUCYSDGsE4utAnA7El4HYBYh/A/F+dIP+I+sChSWe8EwGYg0gjoDyQd6+C/MtcpiykuBSRyB2A+LVUP4ikCXAcJ2HYihQ4AcelyEnIZAPQJFzG8quB7JjgXgX1AcIQ4GS4kCKC5akiDD8N1BPECgFANnngWwPmKOQvb+SGFcihzkUPAWyXZHjBGboDmggcwPxP2wGoUccUJwJyH8HZFYC8VtkPQABBgCZWFTNwKXJQAAAAABJRU5ErkJggg==)`}}className='tag-icon'/>
        <div className='tag-text'>{ this.joinTags() }</div>
      </div>
    )
  }
}
