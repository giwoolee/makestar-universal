import React, { Component } from 'react'
import { MenuItem } from 'react-bootstrap'
import NavProfileDropdown from '../../bootstrap/NavProfileDropdown'
import { FormattedMessage } from 'react-intl'

export default class ProfileDropdown extends Component {
  render() {

    const {
      nickName,
      profileImageUrl,
      email,
      actions,
      projectsMessageId,
      profileMessageId,
      logoutMessageId
    } = this.props

    return(
      <NavProfileDropdown title={ " "+nickName } img={ profileImageUrl } >
        <MenuItem id='top-nav-profile-dropdown-funding-link' href="/user/funding_list.do" onClick={ actions.fundingListClick }><FormattedMessage id={projectsMessageId}/></MenuItem>
        <MenuItem id='top-nav-profile-dropdown-profile-link' href="/user/profile.do" onClick={ actions.profileClick }><FormattedMessage  id={profileMessageId} /></MenuItem>
        <MenuItem divider />
        <MenuItem id='top-nav-profile-dropdown-logout-link' href="/logout.do" onClick={ actions.logoutClick }><FormattedMessage id={logoutMessageId}/></MenuItem>
      </NavProfileDropdown>
    )
  }
}
