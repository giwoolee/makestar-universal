import React, { Component } from 'react'
import classnames from 'classnames'
import { NavDropdown, MenuItem, Button, ButtonToolbar, ButtonGroup } from 'react-bootstrap'

export default class LocaleButton extends Component {

  render() {
    const { locale, localeChange } = this.props
    return(
      <ButtonToolbar className="nav-locale button-toolbar">
        <ButtonGroup className="button-group">
          <Button className={ classnames({ 'button-active' : locale === 'ko' })} onClick={ () => { localeChange('ko') } }>한국어</Button>
          <Button className={ classnames({ 'button-active' : locale === 'en' })} onClick={ () => { localeChange('en') } }>English</Button>
          <Button className={ classnames({ 'button-active' : locale === 'zh' })} onClick={ () => { localeChange('zh') } }>中文(简体)</Button>
          <Button className={ classnames({ 'button-active' : locale === 'ja' })} onClick={ () => { localeChange('ja') } }>日本語</Button>
        </ButtonGroup>
      </ButtonToolbar>
    )
  }
}
