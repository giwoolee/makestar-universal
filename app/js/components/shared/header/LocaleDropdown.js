import React, { Component } from 'react'
import classnames from 'classnames'
import { NavDropdown, MenuItem } from 'react-bootstrap'

export default class LocaleDropdown extends Component {

  render() {
    const { locale, localeChange, localeDisplayName } = this.props
    return(
      <NavDropdown title={ localeDisplayName } onSelect={ localeChange }>
        <MenuItem id='locale-select-ko' ref="locale-select-ko" eventKey="ko">한국어</MenuItem>
        <MenuItem id='locale-select-en' ref="locale-select-en" eventKey="en">English</MenuItem>
        <MenuItem id='locale-select-zh' ref="locale-select-zh" eventKey="zh">中文(简体)</MenuItem>
        <MenuItem id='locale-select-ja' ref="locale-select-ja" eventKey="ja">日本語</MenuItem>
      </NavDropdown>
    )
  }
}
