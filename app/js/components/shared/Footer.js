import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'

export default class Footer extends Component {

  render() {
    const { aboutUsMessageId, partnershipMessageId, termsOfUseMessageId, howToMessageId, enquiriesMessageId, privacyPolicyMessageId, faqMessageId, companyInfoMessageId, companyRegistrationNumberMessageId, emailMessageId, phoneNumberMessageId, salesLicenseMessageId, addressMessageId, link } = this.props
    return (
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="footer-menu col-xs-12 col-md-12 col-sm-12">
              <ul className="nav nav-pills first-row">
                <li role="presentation">
                  <Link to="/about/corp"><FormattedMessage id={aboutUsMessageId} /></Link>
                </li>
                <li role="presentation">
                  <Link to="/about/partnership"><FormattedMessage id={partnershipMessageId} /></Link>
                </li>
                <li role="presentation">
                  <Link to="/about/terms"><FormattedMessage id={termsOfUseMessageId} /></Link>
                </li>
                <li role="presentation">
                  <Link to="/about/howto"><FormattedMessage id={howToMessageId} /></Link>
                </li>
                <li role="presentation">
                  <Link to="/about/enquiry"><FormattedMessage id={enquiriesMessageId} /></Link>
                </li>
                <li role="presentation">
                  <Link to="/about/privacy"><FormattedMessage id={privacyPolicyMessageId} /></Link>
                </li>
                <li role="presentation">
                  <a href="/help/faq.do"><FormattedMessage id={faqMessageId} /></a>
                </li>
              </ul>
            </div>
            <div className="social-networks col-xs-12 col-sm-12 col-md-12">
              <div>
                <a href="https://www.facebook.com/makestar.co" target="_blank" className="ga-event">
                  <img className="official-sns-facebook" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
                <a href="https://www.twitter.com/Makestarcorp" target="_blank" className="ga-event">
                  <img className="official-sns-twitter" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
                <a href="https://www.instagram.com/makestar.co" target="_blank" className="ga-event">
                  <img className="official-sns-instagram" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
                <a href="https://www.youtube.com/channel/UC27NRDagT3OOkpGlkfbsheg" target="_blank" className="ga-event">
                  <img className="official-sns-youtube" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
                <a href="https://www.weibo.com/makestarco" target="_blank" className="ga-event">
                  <img className="official-sns-weibo" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
                <a href="http://blog.naver.com/makestar_nav" target="_blank" className="ga-event">
                  <img className="official-sns-naver" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApAQMAAACSrpK1AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA1JREFUeNpjYBgFeAAAAR8AAVTkCygAAAAASUVORK5CYII="/>
                </a>
              </div>
            </div>
          </div>
          <div className="row address">
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={companyInfoMessageId} />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={companyRegistrationNumberMessageId} />
              <span> : 120-88-24134</span>
              {
                link ? <a target="_blank" href={link}> [사업자등록정보 확인]</a> : undefined
              }
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={emailMessageId} />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={phoneNumberMessageId} /> : 02-6959-2076
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={salesLicenseMessageId} />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12">
              <FormattedMessage id={addressMessageId} />
            </div>
          </div>
        </div>
      </footer>
    )
  }
}
