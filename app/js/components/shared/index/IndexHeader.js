import React from 'react'
import { FormattedMessage } from 'react-intl'

const IndexHeader = ({ titleMessageId, subtitleMessageId }) => {
  return (
    <div className='row index-header'>
      <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12 index-header-title'>
        { titleMessageId ? <FormattedMessage id={titleMessageId}/> : undefined }
      </div>
      <div className='col-xs-11 col-sm-11 col-md-11 col-lg-11 index-header-subtitle'>
        { subtitleMessageId ? <FormattedMessage id={subtitleMessageId}/> : undefined }
      </div>
    </div>
  )
}

export default IndexHeader
