import React, { Component } from 'react'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import { Modal, Button } from 'react-bootstrap'

class StatusModal extends Component {

  render() {
    const { show } = this.props
    if(show) {
      const { actions, titleMessageId, links: { primary, secondary }, bodyMessageId } = this.props
      return(
        <Modal show={ show }  >
          <Modal.Header closebutton>
            <Modal.Title><FormattedMessage id={ titleMessageId }/></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormattedHTMLMessage id={ bodyMessageId } values={ { primary, secondary } }/>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={ actions }> Close </Button>
          </Modal.Footer>
        </Modal>
      )
    } else {
      return(<Modal/>)
    }
  }
}

export default StatusModal
