import React, { Component } from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import { Modal, ButtonInput, FormGroup, FormControl, ControlLabel, Button, Alert } from 'react-bootstrap'

class LoginModal extends Component {

  loginSubmit() {
    const { actions } = this.props
    const email = this.refs.email.value
    const password = this.refs.password.value
    actions.loginSubmit({ pathname: '/user/login', data: { email: email, passwd: password } })
    .then( (result) => {
      if(!result.error) {
        actions.resetStatus()
        actions.refreshContent()
      }
    })
  }

  failedMessage() {
    const { failed, failedMessageId } = this.props
    if(failed) {
      return(
        <Alert bsStyle="danger">
          <FormattedMessage id={failedMessageId}/>
        </Alert>
      )
    }
  }

  render() {
    const { show } = this.props
    const { actions, titleMessageId, bodyMessageId, emailMessageId, passwordMessageId, signUpMessageId, closeMessageId, loginMessageId, type, intl: { formatMessage, messages } } = this.props
    if(show) {
      return (
        <Modal show={ show } >
          <Modal.Header>
            <Modal.Title>
              <FormattedMessage id={ titleMessageId } type={type}/>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            { this.failedMessage() }
            <form>
              <FormGroup controlId="email">
                <ControlLabel><FormattedMessage id={emailMessageId}/></ControlLabel>
                <input className="form-control" ref="email" type="email" placeholder={ formatMessage({ id: emailMessageId, defaultMessage: messages[emailMessageId] }) }/>
              </FormGroup>
              <FormGroup controlId="password">
                <ControlLabel><FormattedMessage id={passwordMessageId}/></ControlLabel>
                <input className="form-control" ref="password" type="password" name="passwd" placeholder={ formatMessage({ id: passwordMessageId, defaultMessage: messages[emailMessageId] }) }/>
              </FormGroup>
              <FormGroup>
                <Button className="orange-btn full" bsSize="large" onClick={this.loginSubmit.bind(this)}><FormattedMessage id={loginMessageId} /></Button>
              </FormGroup>
              <FormGroup>
                <a style={ { color: 'white' } } href="/account/signup.do"><Button className="dark-btn full" bsSize="large" style={ { width: '100%', height: '40px' } }  ><FormattedMessage id={signUpMessageId} /></Button></a>
              </FormGroup>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button id="close-btn" onClick={ actions.resetStatus }><FormattedMessage id={closeMessageId}/></Button>
          </Modal.Footer>
        </Modal>
      )
    } else {
      return(<Modal />)
    }
  }
}

export default injectIntl(LoginModal)

