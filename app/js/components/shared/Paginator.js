import React, { Component } from 'react'
import Paginate from 'react-pagify'
import qs from 'qs'
import classnames from 'classnames'

export default class Paginator extends Component {
  pageSelect(selectedPage) {
    const { centerPage, query, changeLink, pathname, gaEvent, type } = this.props
    const queryObj = Object.assign({},query, { page: selectedPage - 1 })

    if(selectedPage != centerPage) {
      gaEvent({ category: type, action: 'Navigate', label: `${selectedPage - 1}`})
      return changeLink(`${pathname}?${qs.stringify(queryObj)}`)
    } else {
      return
    }
  }
  render() {
    const { centered } = this.props
    return (
      <Paginate.Context className="container" segments={this.props} onSelect={ this.pageSelect.bind(this) }>
        <div className={classnames({'row': true,  'row-centered': centered })}>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 paginator-container">
            <Paginate.Segment field='beginPages' className='paginator-text'/>
            <Paginate.Ellipsis className='ellipsis' previousField='beginPages' nextField='previousPages'>. .</Paginate.Ellipsis>
            <Paginate.Segment className='paginator-text' field="previousPages" />
            <Paginate.Segment field="centerPage" className='paginator-selected' />
            <Paginate.Segment field="nextPages" className='paginator-text' />
            <Paginate.Ellipsis className='ellipsis' previousField='nextPages' nextField='endPages'>. .</Paginate.Ellipsis>
            <Paginate.Segment field='endPages' className='paginator-text'/>
          </div>
        </div>
      </Paginate.Context>
    )
  }
}
