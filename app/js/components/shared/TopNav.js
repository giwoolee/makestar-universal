import React, { Component } from 'react'
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { Link } from 'react-router'
import ProfileDropdownContainer from '../../containers/shared/header/ProfileDropdownContainer'
import LocaleButtonContainer from '../../containers/shared/header/LocaleButtonContainer'
import LocaleDropdownContainer from '../../containers/shared/header/LocaleDropdownContainer'
import { FormattedMessage } from 'react-intl'
import classnames from 'classnames'

export default class TopNav extends Component {

  render() {
    const {
      locale,
      grade,
      isLoggedIn,
      confirmAccountMessageId,
      projectMessageId,
      contentMessageId,
      eventMessageId,
      pollMessageId,
      loginMessageId,
      signupMessageId,
      actions,
      topNavOpen
    } = this.props

    return(
      <header>
        <Navbar bsClass="navbar" expanded={topNavOpen} onToggle={actions.onToggle} className="header">
          <Navbar.Header>
            <Navbar.Toggle className={classnames({ "nav-btn": !topNavOpen, "nav-btn-opened": topNavOpen })}/>
              <Navbar.Brand>
                <Link to="/" onClick={ actions.homeClick } className="logo-container">
                  { locale !== 'zh' ? <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlQBAwMDAwMDBAQEBAUFBQUFBwcGBgcHCwgJCAkICxELDAsLDAsRDxIPDg8SDxsVExMVGx8aGRofJiIiJjAtMD4+VP/CABEIADEAsgMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABgcIBQQDCQL/2gAIAQEAAAAA/UuloRMJrPfsAAGbJZnaY31wLkBTdxf0IBPx+dOuqT9UyrnZmYYD7rfqivtJQTizT3xe1YTM+Thmdei6qhnWzcofTWv5yW3ke5512rkhscsmtPPriq8QU91rK/Q2T0B9L7xv/H301mvnWh5uv5+TOLX5mBs7crSe++4AABTnOvQAAD//xAAaAQEAAgMBAAAAAAAAAAAAAAAABAYFBwgC/9oACAECEAAAAKJzbvnagBT6TVujfQCbGxGTAEiOAP/EABoBAQADAAMAAAAAAAAAAAAAAAAGBwgBAgX/2gAIAQMQAAAAnE6rGNgGl/H7Z/4AWnojPdUACaQsAf/EADAQAAEFAQABAgQEBAcAAAAAAAQBAgMFBgcIABESExQVFiAhIhAXMUAmMzRCUWFy/9oACAEBAAESAJpoh4nyyvaxjGq5z9H1JVkePSrG1iKqKTFd2xXvMTZTyK5fdErbUlWtliMexP8Aip1U7p0hLVkjFb/mxSRysR7HI5jk/R39j1m/M0Wir8JVzpGhE0aGSP4lkftqDskKQpGIiFPBICsCK2ZE+dBO+JfQ0bB42xN9vZE9ZCio586PO8aKV07HLK/I6McDQzUjZfjDllkQd/5u5dOP5JkRbwOugPkntIBFijd8bGOX/c1F/Kdu0B6dT4x1er1sacqw+r/JJpDhdqVewuRSG2spLPV11cCirKqcqtI+sPBjKaLrqJpbH6yhneYATM6QluczyWkUp9o6QKqEajy5cttHaLXDwNRRa58MwcIj1mprD4/f4JAiP6wTsnhjlb7fC9jXJ66R1Podr0JOccxGF+7DjtntbO87N2/hBiQdDFrtEEePMtcd9x8xPsX4y+fRLCsX1X4fou0rqOMT7ukoirA8eCRstTAvmRY1CaWM2gGSRiERUPYeoM6x461Vy8RAzR9aKIeN1ju1HzSAeoBgdc6cxjGBVP476fyrCl7XptwwkwtyRVmbSXzEMpJNSh9BXxpEpTaMDyDntOK120Epo5Lg6xhqIQDH+TecEddST5e/SFvzJ6PoM23sO6YabLMAr7abHHPWO0uuwZ7FCMWnrL/TlnoO1bq78hOcVc+lup83pKoRvzrMDedSnqKrMxZUFltd6xW/ZRkz3kkrEfJr8nG5U93MNpTiNtJnWK9pE904JE7TexGb+1hHeqQVTYa+FvFN6mY0sKnlTMDejmFJ2be5/RD1lbUHfVfBM6aZ+etpKW7AOR36QExPd66eJPBtTAh0cv1s8csaiwIMLDCi+/y42sReQoi+S3ZFVPf2aJ68xY41xWT/AGov+LQk/hyrppPKuHaq1CEQuwL20wNfCHzLykuYIz7TqkNWTOiPeGjZWeN90xz/AIpU6VCivylWX40dWFP3MQ9uBpIWMZpvL2MiwI5gsFgwMWa6eiHm8B7RoRn19/2MyatnRWzxScVxC8zgwMcZENdB8EsE5FZ5D4MOQkG+qdsAKxXKJS7Cu3Xa+aaYJkkA1th7V8LOm7bU1t1ncdkIBHX+h+pfGV0fC9lg57qSrPqv1g0VMa8kTP8AsvReD/8AWBNX+A/J8tF0A7ZSjNmNnSFYG9mpaGm6XeRVMnxxzPZNO0s54k0aQ/o5qo5yz2T0GbJHM/3kT9vrn9vXz3dOTZp8waEyP6tjxs3q4giWyDmsDJjIHk9c/wCcafNdl6LqD2DJW3yDoCvkJzrTdJzVEDRMHdMFfjGTJ6x/jdaGcm0mQ1ckIZRujls64lcZ5hNB/Dq6ui+i+H5P3jZ+N9mHxKrxOUkiMNiv4bI4rYYqh3uWLz14OkwpMKNVc1w/bnYm55ruXjnZ6B6yZ+6HxHlzSgtzIGsop65jUhgtqjitxR8uCy9ftbca2CNQ+O1NznkndASVJ2oyFeMQxYpz7riFc/IZerzVlPTWeQ/fS2dzzXeaqpoLaxv6+r29BOS8GzuMB3LfBrR7DUZ8GjnRGHtO56V/MrE6EB4sNXnqY8BRvVnWh3FeSCZGskBMbo5G9I8edhmpyDs6k9zWqqvSOJny3ywHjrCREq/G1XylkJGKx7nOX2ji5n4/bXRRsJtJC6EKRzXOfQ0VZmqoasrovlDDs9mp/Z9W/wBKv/lfXIv6O/tP/8QAOBAAAgEDAgMEBwUJAQAAAAAAAQIDAAQREiETQVEiMTJhBRAUQlJxciBjgaKxI0BEUFNigoSS4f/aAAgBAQATPwBjgADmSafBJ+hTRmOn/nOKEu2flmgQCMdRzoHY/uXm/aCnyUdoijJklupXuxQ7socGvOnXJzkjAJ7sUTnSwOwH1D7c0jRgLKrktlemn7RkwEFu4TQUxvnP2W3BIc4B8sbUrDCK/wATmjvNayscssg+GnBRj92md9TUDhUjKnSSO4scV0MTf+V8xmroaorNWwQoBBHcRkkGrMCEC4jGQj4VNuvZzQt1M3Bxr0d2c45a81D23F1HgOvUoPF1001uolKHtCM5UkOehfNA5Ec8cbns+RFQZdg7jsmbTkgHkPE1W0EESpNNuiySqpYlfyingUSCMdsIcqSDjkXrWVhN/NJwxljuIveq0tZreVkG5W2mYnVJ9Qr0urskKNIhdHEB8Yqy1wWECSMdM0/EOsIg8WKsraW0uIIBvI9u7Eh9A+OpSY4tBjErzzkbiKNTk0nomRlU9AS+SKBOVzNozWe7gIM/mJoEsHRgdLMOeg1ETwx2cKD1NeSsCaAOH4+DgddzX0jHq/wf1P4OPLGpBYdAFqC1SRYc+7lQorq4tSC1FHZ7S5feTJckj+/mVojiRQSS8MxTN1AGTUFpoMqc1yGFRyYuI7lH4guQ/wDV1b1f2/sN+6JySaLKM/m1S7MrrMoePzZSKvMm3s7a1AMs7qu7nfCik9B28ImQQsWjDg5XNf60PqbwwSoCryAfEwxWviCKeQZkQE+ZzQ/StXdR3GFPeRzWonDBJE3G4/T1JLqkPD79aY7NTy8ICKJWBwcHJ39VtJx+CwQBH5eYIr+J4Xz0as1cuIRO+h1d+fkAKHjikUdmSM8nU7g1DNm6tTGewDEeVS59pjh/FC2RQdnX2gNq4ZiY72/3dWNncvdiNtmMaSnQHrAlZHIxIJUPjSb3xXo6B2tHim7JilhlOSsg8dehbeYXN3HziLznEYfuOKJbX+3VFi4exGFC+oMUJB6FcEHzFLvdwjoy++PNamQo/wCIbBqMEk/JRTZW4dBySPlnq1E6mY82ZjuzHmf5T//EACsRAAEEAQMCAwkBAAAAAAAAAAECAwQRBQASITAxBlFSExQiIzIzQUKBwf/aAAgBAgEBPwDPueIcyH4mDeSwhglLz10tSvQg/itTmssFPkTZKX03aypW8Ueebu9eGMtl8FBwbOdnty0ZJCQy8T8xClAFIWT9QN1fn0jicrh4T8hiXzJWO7V7Tdb/APNTfDuKIdyDyke2jqKlRP1eWed3e9hPNalNT50Rp9bqnHWcgolfpS+gEAeQBb40i9ovvXPRYaEy2nH0otITSuxA05gYxmsh9APu6rVxw4O4/mm8K3jZspLJYVEfVvDe34kH0k9qHTP2f4On/8QAKREAAgEDAgQFBQAAAAAAAAAAAQIDBAURABIhMDFBBgcTIlEjNkJxc//aAAgBAwEBPwC222y2ino6u9M5kqxvhgCbgsfZ36ddXKzUrxL6bIAwyu1BjbqqtsNW9aaNHD0efUXbgMFOCR8HlXnwlbr9cLPTsAyrSSSllyPpLtVF4HoS2rjRXyhusFlCSSQsnsrwpzHEPxbhjcMcDpLVBQXkU8W1IJ6LOD39FsNn5OH02NzbemTj9cny88RR0t1eW43KUMKVYIYnPsKgggbieGOw1T1lPUxRzgbkIzrzKutnqq+FqCqkaopy6YQ+xVPXiO/Li+0pP6cv/9k=" className="logo" alt="Makestar"/> : <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlQBAwMDAwMDBAQEBAUFBQUFBwcGBgcHCwgJCAkICxELDAsLDAsRDxIPDg8SDxsVExMVGx8aGRofJiIiJjAtMD4+VP/CABEIADEAsgMBIgACEQEDEQH/xAAcAAEAAwADAQEAAAAAAAAAAAAABgcIBAUJAgP/2gAIAQEAAAAA9SqWhMxms8/YAAM1y3OsyvuP3JA+BzuX9VzUN822A85df0hzZhXOz8DQLW1FzG8Ki40yjGjZKHk5O+dctTTbYvlD8bpw9KdN1RsfLdc63usKrw3UfbWZ6F97imvdKfNAIls/ItozPUYdZgPPHV6V313YAAFNcO8QAAP/xAAbAQEAAgMBAQAAAAAAAAAAAAAABAYBBQcIA//aAAgBAhAAAACiebe9dJg/fYZCoUer+jWY8kJsbUbMASI4A//EABwBAQACAwADAAAAAAAAAAAAAAAGCAQFBwECA//aAAgBAxAAAACcTrmOlzPjrwWY03tX94y8QOqWGr5ygATSFgD/xAAtEAABBQEAAQMCBQMFAAAAAAADAQIEBQYHEwARFAgSFSAhIkAWIzIwMTRRcf/aAAgBAQABEgA5gxhPKUjWDG1XPfoupOc98elUbWJ7oskV3bSv70myOVzl90SttZX2oQUwjE/6qtXIcfwTFY9ip7+UZGFYj2ORzHJ+i/wer3s3T6avwlYZBIYw0mvNxLJLXfHE+U2SjPZJSwzxZ564qJ5gHeJ3qMNgGNEn+yesjR0Zs6Az4wjOOxVK/I6MEG/NSIXyQyFJ8Z/qNqLN8kSEhxUjFdH/AHH2kx7bHwRlf8OVGRiD1cl8+pioCMRswTCkKl/KO9zWoAThTxif6XTPSTYhbHa/46taN3XI++Jc5OLR7I9CG2sVgPZqpn1QcvjntR6Kn2ECAxDTI/GevVXX8stpFB8ObGKgZ8T/AEBaabF15r8C+5/xIspvq86vX0FdVlk153TJ8EcpI2toGmY7V0J3zK6SdxJCZ7PNshGsbVSQqmIiOlEy2zdo9WKOrfjVxY5YYoivLTz0J7qx8OR7+4iIUTHtX/JqL6v/AKl+j0F7ZVUlmBEeBMLHIPI/VJ0uJXyEmTMpLI+aUn35zddHt+Uj1YsxT3dseV7wInR+99jr8lZubkIGcJ9w0fY1Hbu0WMeOkXmFdOOYA3+/SIl7ZYiruY1e9Lilm19wlbWdA4xT2tnt37mK8UkRfuqvpXwt7m83qtC+Etc7RS/LVQee9nuRH0FH1GHFz9pQ+N5p9p3qkH3SghxdpVrkiUEh05+b2mQ16SFoLyvtFjfb5/zLRzTbEeb93octwkFfXZ78U/oFyyMRfBWqOAFvEt63M6UPz5ZRwntcyUnZ95QaIdXXVE75TRkeY783bvpL2vne6/aCUJ706bEPH202CFHL8ySwg1kwPk1R4HnMDzRXBQ/WVg47WhpM7uNVfqAzR2smRkdRjqmVMNmOjVNeD3NILxbLyoPKJVkC00k4mhq/kBi4J/DKihhVe057qJumYhnSXZ0PNtH1HnR+YY2/rliX7SWcjt5dNf6mJDzlpUV5cbEDcFLmrbomk6iKfPy+FvbSopkcpKGfodLW2ca5DXVr1EoETnOMjxOwb3nGitp+nz0WkhTPFW5LH6rZdG1OTw0C5zmZrGRK2t4SfmNxjmX+Ho4VQlh7MsAflh8nysboE/YvjNLMkeJwG9ipqOm6VfhqSeQJDNKVJU58Q4/D+jmqjnepFk9IzSDK/wByJ+1efW9ee8ppVn++KGYL5bXxs5qxwZTXx5rYUph45Z0KPYwpMKQjlDJA8JUv6UubqbrmWc4TPtKV5fb8Rk8j+owVFnA6SAmpoakyFTNh6/0p+SPMj8eug2MacCGGu5ZgN7M3Fn0joCRotvLgpBrqqlB3HnOF1nOoXPbOYSxnTvi3XQLPoGdroWLuOU/1JVgrKoc2fmaPJk11bPJwXTiqPhGDNh5zUlhw1iZfldxWRADeUiann/f9lMv9yXMFii0bRwp1DkuidSwVDEo6LgdlEhREX7GfTxBtY1/uJ0rBW+SbbSY8pRfltauFc10qBMGpASROGRvR/p62OYPIm55D3Vaqq5GCZ43kDOAoZAlXyNV5ZklBRWPc5zvYYuZfT9tdCMcq1fLoYRXtc91HR1mcqo1ZXB8UeOxGsT+H1f8A4zv/ABfXIf8AFf4n/8QANRAAAgEEAQIDBQYFBQAAAAAAAQIDAAQREiETMQVBUSJCYWJxEBQgIzKBMEBQUmNygpGSsf/aAAgBAQATPwBjgKB3JNPglv8AQpozHUY+XOKEvGfpmgQCPqPOhyD/ACXzN7QU/BF5IoyZJb1KdsV5ZQ4P2OuTnJBAJ7AUT2YHgf7h9glYyBbuRo041xkFOahwzyo85iZMNjDnWlmCoBIxULGWwWZcc0X2URuSBlhwCcf81twWaTpYbzHNQ2UM2ri3lnEhaXk5MeNaNikdxHF/e8cYVtfoa236UmMgq3mjjkH+C3YlnJ1PwI4pWXEav5O9HmW1lc5ZZR6ZPenBRm/xJnnZqH6UjK5XIHGxxXoYm+wy3rFDE5GCQK8RSaKXBOQALdQpRfdNeGXRihktmYKZN5xkMOciofFbW9khO4B/IGSSai8ctd5sKDvoKWQO7SQczWyt7zFGdB6mndxcRMcYjMDNuXHI1YVchlMUEW5haVRyAd65h8PuYZn0jaN3NLNEYPve7abS+T8CrWdZenv221Jxn8WSCPzumTQPA+7oFI/7ZoZZWQg6sy+ejVGSIu2FHxNfKGGaHZ+vgjH7moG0kj2XXdG8mHcU94NVlc4EMcg4MvByTRvooIlzwXYiECvFbgTSWpMTBEi1AxvUCSoXXc4Kp1UPA4PFTh3jMKEE+3vIBqAdq8UcR2pu71mhtgzNwHRVdkzVrfxRRSG8l4uDIFm3mXokfCvDPEjdzRNIpDMzmKPRxn2a8Zm64EmySBzvwpXagDHBf3oIMsi4zthQSKhQB454e8Uh+TPH4mHEEoBWSRR22et9wk8g2lQE/Mc0P/K27UfaGFPcjzWonDhJFHHIpWKMUkGrYYYIOD5VLeiMXcgHFz1ipOy+R2oeKKr4T9CTOAOoBTzKiNG6E9VDoDohUA8VA26WNtkMQzAkbVa3GkSi7ATcUUupZb+e1hB3FzbcjpsSozQhubwCYsGjniMnTz5qytVxb2/hkZ0XOqRq7M8rYwOMZ7moLsQ3osoNCmWPqUwaW8LFmbku7dMks1XFz1LZpTtuII9EK+p/EGKEg+jLgg+hFJzdRD0dff8AqtTJo/7hsGo1LH9lWmytxIg91I/LPq1E7E+rMx5LHzJ/pP8A/8QALBEAAQMDAwIEBgMAAAAAAAAAAQIDEQQFEgAhMQZREyBBgRQVMDJSsSMzYf/aAAgBAgEBPwDqBzqG8+PS2R5LCGCUvPTC1K/BB9I1XNXaXlCtqEvp5cyVn/u8zOumbteLFQ2Ni+VzdWm5tpDD8/yIUoApC5+4Gee+qmsYpVNpcKpcmISTxzMaTfbctQAWo5RicTvKsP2NfNKOAcjBmNj6Ak/rU+Y2q62eiffYq96lY5anE+q/fjVZ07asXbi6UeNTqlVJwh1Z3y74HmNVLVfXUjT63lOOM3BRK4+1L6AQB2ALew0BIE86xT+I1Cew0/SU9StlbqMiyvNs9leanbTWEtOPhEpCYVwQNOWGmNaz46Afh1SrbZwcj203ZUW2sqkslhVK+vNLeMqQexPED6Z/p9h9P//EAC4RAAICAgECAwYGAwAAAAAAAAECAxEEBQASISIxQQYHFCAwURUjMzZzsUJhcf/aAAgBAwEBPwDW63SaiDDy90zmTLHXDCE6gkfo7/8AebLTYrxAIyAMLUKgrp5la2HLbNOGjhsP9RemgwXsSPseY+HPlLI0YBCVdsB5+VXxtJsEUlkUVdjrHalD/wBHn4ZmXQQWKvxD1IA/vlE3/r5t37I67fbPUQMAyJiSSllsflqVVF7HyJbmwwt5hbWDShJHhZbXYBTaRD/Fu1dYqgeJqoMDcfDxdKQT4d1/C1Nf3NPw+Zo9r5bfc8s/c8xs/Lw48iOCQouREY5RQPUvnXze7z2iixtpJLsdlKH+FWCGJz4CqkV4ie1V2HMfLx8iKPIA6kIvnvK22ny9hE+vy5GyIC8dIfAqnz7j19Ppx/tJ/wCQ/T//2Q==" className="logo" alt="Makestar"/> }
                </Link>
              </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem id='top-nav-project-link' href='/projects' onClick={ actions.projectClick } ><FormattedMessage id={projectMessageId} /></NavItem>
              <NavItem id='top-nav-content-link' href='/contents/articles' onClick={ actions.contentClick } ><FormattedMessage locale={locale} id={contentMessageId}/></NavItem>
              <NavItem id='top-nav-poll-link' href='/polls' onClick={ actions.pollClick } ><FormattedMessage locale={locale} id={pollMessageId}/></NavItem>
              <NavItem id='top-nav-event-link' onClick={ actions.eventClick }href="/event/event_list.do" ><FormattedMessage locale={locale} id={eventMessageId} /></NavItem>
            </Nav>
            <Nav pullRight={ true }>
              <NavItem id='top-nav-faq-link' onClick={ actions.faqClick } href="/help/faq.do" > FAQ </NavItem>
              { grade === '00' ? <NavItem href="/user/profile.do" ><span style={ { color: 'red' } }><FormattedMessage id={confirmAccountMessageId} /></span></NavItem> : undefined }
              {
                isLoggedIn ? undefined : <NavItem id='top-nav-signup-link' onClick={ actions.signUpClick } href="/account/signup.do" ><FormattedMessage id={signupMessageId}/></NavItem>
              }
              {
                isLoggedIn ? <ProfileDropdownContainer /> : <NavItem id='top-nav-login-link' onClick={ actions.loginClick } href="/login.do" ><FormattedMessage id={loginMessageId}/></NavItem>
              }
              <div className="hidden-sm hidden-md hidden-lg">
                <LocaleButtonContainer />
              </div>
              <div className="hidden-xs nav-locale">
                <LocaleDropdownContainer />
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    )
  }
}
