import React from 'react'
import { map } from 'lodash'

const Share = (props) => {
  return(
    <share>
      <div className="share-container">
          {
            map(props,(v,k) =>
              <div onClick={ v.onClick } className={ v.icon } key={ k } />
            )
          }
      </div>
    </share>
  )
}

export default Share
