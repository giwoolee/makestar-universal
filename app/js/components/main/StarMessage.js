import React, { Component } from 'react'
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl'
import Helmet from 'react-helmet'
import { map } from 'lodash'

class StarMessage extends Component {

  render() {
    const {
      starMessageTitleMessageId,
      iFrameUrlMessageId,
      artists,
      intl: { formatMessage }
    } = this.props

    return (
      <div className="star-message">
        <Helmet title={formatMessage({ id: starMessageTitleMessageId })} />
        <div className="container">
          <div className="title"><FormattedMessage id={ starMessageTitleMessageId } /></div>
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="embed-responsive embed-responsive-16by9">
              <iframe className="embed-responsive-item" src={ formatMessage({ id: iFrameUrlMessageId })} allowFullScreen="allowfullscreen"></iframe>
            </div>
          </div>
        </div>
        <div className='container star-message-thumbnails'>
        {
          map(artists, (v, k) => {
            if(!v.showVideo) {
              return (
                <div className="col-md-4 col-sm-4 col-xs-12 embed">
                  <div><img src={ formatMessage({ id: v.imageUrlMessageId }) } className="img-rounded" onClick={() => { v.playVideo() }} /></div>
                  <div><FormattedMessage id={v.artistNameMessageId} /></div>
                </div>
              )
            } else {
              return (
                <div className="col-md-4 col-sm-4 col-xs-12 embed">
                  <div>
                    <div className='embed-responsive embed-responsive-16by9'>
                      <iframe className='embed-responsive-item' src={ formatMessage({ id: v.videoUrlMessageId })} allowFullScreen="allowfullscreen"></iframe>
                    </div>
                  </div>
                  <div><FormattedMessage id={v.artistNameMessageId} /></div>
                </div>
              )
            }
          })
        }
        </div>
      </div>
    )
  }
}

export default injectIntl(StarMessage)
