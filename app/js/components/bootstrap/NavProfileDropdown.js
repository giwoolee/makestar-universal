import React, { Component } from 'react'
import { Dropdown } from 'react-bootstrap'

export default class NavProfileDropdown extends Component {
  render() {
    let { children, title, img, noCaret, ...props } = this.props

    return(
      <Dropdown {...props } componentClass="li">
        <Dropdown.Toggle
          useAnchor
          disabled={props.disabled}
          noCaret={noCaret}
        >
          <img src={ img }/>
          { title }
        </Dropdown.Toggle>
        <Dropdown.Menu>
          { children }
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}
