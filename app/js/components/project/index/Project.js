import React, { Component } from 'react'
import { Link } from 'react-router'
import Tag from '../../shared/Tag'
import { FormattedMessage, FormattedNumber } from 'react-intl'
import { Tooltip } from 'react-bootstrap'
import { pickBy, isEmpty, toPairs, last } from 'lodash'
import classnames from 'classnames'
import LazyLoad from 'react-lazy-load'

export default class Project extends Component {

  render() {
    const {
      _id,
      sumLocal,
      name,
      percent,
      sum: {currency},
      thumbnailUrl,
      remain,
      dday,
      status,
      tags,
      actions
    } = this.props

    const resourceUrl = `/project/${_id}/`
    const remainingDuration = pickBy(remain,(v) => { return v > 0 })
    let timeRemaining

    if(!isEmpty(remainingDuration)) {
      timeRemaining = last(toPairs(remainingDuration))
    }

    let toolTipPosition
    if(percent < 100) {
      toolTipPosition = { left: percent <= 49 ? `${percent.toFixed(0)}%` : null, right: percent >= 50 ? `${ 100 - percent.toFixed(0)}%` : null }
    } else {
      toolTipPosition = { right: "0%" }
    }

    // Show D - days when Status is "ComingSoon"
    //    Hide percentage (project-index-project-progress-bar-tooltip-container)
    //    Hide sum text (project-index-project-sum-text)

    // Show timeRemaining when Status is "Opened"
    //    Show percentage (project-index-project-progress-bar-tooltip-container)
    //    Show sum text (project-index-project-sum-text)
    let displayRemainingTime = ""
    let hideTooltipAndSumText = false
    let displaySumAmount = ""
    if (status === "ComingSoon") {
      displayRemainingTime = dday
      hideTooltipAndSumText = true
      displaySumAmount = "Coming Soon"
    } else {
      displayRemainingTime = isEmpty(timeRemaining) ? <FormattedMessage id={'project.closed'}/> : <FormattedMessage id={`project.timeRemaining.${timeRemaining[0]}`} values={{ value: timeRemaining[1] }} />
      displaySumAmount = <FormattedNumber value={ sumLocal } style='currency' currencyDisplay='symbol' currency={currency}/>
    }

    return(
      <a onClick={ (e) => { actions.gaEvent({ action: 'click', category: 'Projects', label: _id }) }} href={resourceUrl}>
        <div className='project-index-project-container'>
          <div className='project-index-project-thumbnail-container'>
            <LazyLoad offset={50} height={'100%'}><img className='project-index-thumbnail-img' src={thumbnailUrl} /></LazyLoad>
          </div>
          <div className={classnames({'project-index-project-progress-bar-tooltip-container': true, 'hide-container': hideTooltipAndSumText})}>
            <div className={classnames({ 'tooltip': true, 'right': percent >= 50, 'left': percent < 50 })} style={toolTipPosition} role='tooltip'>
              <div className={classnames({'tooltip-arrow': true, 'tooltip-arrow-over-target': percent > 100, 'tooltip-arrow-under-target': percent <= 100 })}/>
              <div className={classnames({'tooltip-inner': true, 'over-target': percent > 100, 'under-target': percent <= 100 })}>
                {percent.toFixed(1)}%
              </div>
            </div>
          </div>
          <div className='project-index-project-progress-bar-container'>
            <div className="progress">
              <div className={ classnames({"progress-bar": true, "progress-bar-project-index-progress-bar-over-target": percent >= 100, "progress-bar-project-index-progress-bar-under-target": percent < 100 })} role="progressbar" style={{ width: `${percent.toFixed(0)}%`}} aria-valuenow={percent.toFixed(0)} aria-valuemin="0" aria-valuemax="100"/>
            </div>
          </div>
          <div className='project-index-project-info-container'>
            <div className='project-index-project-info-sum-remaining'>
              <span className={classnames({'project-index-project-sum-text':true})}>
                { displaySumAmount }
              </span>
              <span className='project-index-project-remaining-text pull-right'>{ displayRemainingTime }</span>
            </div>
            <div className='project-index-project-info-name'>
              <p className='project-index-project-info-name-text'>
                { name }
              </p>
            </div>
            <div className='project-index-project-tag-container'>
              <Tag tags={tags} className='project-index-tags'/>
            </div>
          </div>
        </div>
      </a>
    )
  }
}
