import React, { Component } from 'react'
import PaginationContainer from '../../../containers/shared/PaginationContainer'
import IndexHeaderContainer from '../../../containers/shared/index/IndexHeaderContainer'
import FilterContainer from '../../../containers/shared/filter/FilterContainer'
import ProjectContainer from '../../../containers/project/index/ProjectContainer'
import HelmetContainer from '../../../containers/project/index/HelmetContainer'

export default class Index extends Component {
  render() {
    const { idxs } = this.props
    return(
      <projects>
        <HelmetContainer/>
        <div className='container-fluid index-header-container'>
          <IndexHeaderContainer/>
        </div>
        <div className='container project-index-container'>
          <div className='row row-centered-mobile filter-container'>
            <FilterContainer />
          </div>
          {
            idxs.map((idxChunk,i) => {
              return(<div id={i} className='row'>
                {
                  idxChunk.map((idx,i) => {
                    return(
                      <div key={idx} className='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                        <ProjectContainer idx={idx} />
                      </div>
                    )
                  })
                }
              </div>)
            })
          }
        </div>
        { idxs.length > 0 ? <PaginationContainer { ...{ pushType: true, defaultQuery: { status: 'All' } } } /> : undefined }
      </projects>
    )
  }
}
