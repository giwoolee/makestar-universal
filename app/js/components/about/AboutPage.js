import React from 'react'
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl'
import ReactMarkdown from 'react-markdown'
import Helmet from 'react-helmet'

const AboutPage = ({ titleMessageId, htmlMessageId, markdownMessageId, intl: { formatMessage }}) => {

  let renderedComponent
  if(htmlMessageId !== undefined)
    renderedComponent = <FormattedHTMLMessage id={htmlMessageId} />
  else
    renderedComponent = <ReactMarkdown source={formatMessage({ id: markdownMessageId })} className="markdown" />

  return (
    <div className="terms">
      <Helmet title={formatMessage({ id: titleMessageId })} />
      <div className="container">
        <div className="row">
          <div className="col-md-2 col-sm-3 col-xs-12">
            <div className="title"><FormattedMessage id={ titleMessageId }/></div>
          </div>
          <div className="col-md-10 col-sm-9 col-xs-12">
            { renderedComponent }
          </div>
        </div>
      </div>
    </div>
  )
}

export default injectIntl(AboutPage)
