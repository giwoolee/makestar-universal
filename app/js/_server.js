import 'babel-polyfill'
import http from 'http'
import bunyan from 'bunyan'
import bunyanTcp from 'bunyan-tcp'
import path from 'path'
import Express from 'express'
import React from 'react'
import ReactDOM from 'react-dom/server';
import favicon from 'serve-favicon'
import { renderToString } from 'react-dom/server'
import { match, RouterContext, createMemoryHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import configureStore from './store/configureStore'
import superagent from 'superagent'
import ApiClient from './helpers/ApiClient'
import { Provider } from 'react-redux'
import getRoutes from './serverRoutes'
import httpProxy from 'http-proxy'
import { trigger } from 'redial'
import fs from 'fs'
import { first } from 'lodash'
import Helmet from 'react-helmet'
import areIntlLocalesSupported from 'intl-locales-supported'
import moment from 'moment'

const supportedLocales = ['zh','ja','ko','en']
if(global.Intl) {
  if(!areIntlLocalesSupported(supportedLocales)) {
    const IntlPolyfill = require('intl')
    Intl.NumberFormat = IntlPolyfill.NumberFormat
    Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat
  }
} else {
  global.Intl = require('intl')
}

function renderFullPage(html, initialState) {
  const locale = initialState.session.session.locale
  const head = Helmet.rewind()
  return `
    <!doctype html>
    <html lang="${locale}">
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${cssPath}" rel="stylesheet">
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.ko,Intl.~locale.ja,Intl.~locale.zh"></script>
        ${head.title.toString()}
        ${head.base.toString()}
        ${head.meta.toString()}
        ${head.link.toString()}
      </head>
      <body>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
          window.GA_TRACKING_ID = ${process.env.GA_TRACKING_ID ? "'" + process.env.GA_TRACKING_ID + "'" : null}
        </script>
        <div id="root">${html}</div>
        <script src="${vendorPath}" ></script>
        <script src="${jsPath}" async></script>
      </body>
    </html>
    `
}

function hydrateOnClient(locale) {
  return `
    <!doctype html>
    <html lang="${locale}">
      <head>
        <title>Makestar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${cssPath}" rel="stylesheet">
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.ko,Intl.~locale.ja,Intl.~locale.zh"></script>
      </head>
      <body>
        <script>
          window.__HOST_NAME = ${process.env.HOST_NAME}
          window.__INITIAL_STATE__ = ${JSON.stringify({})}
          window.GA_TRACKING_ID = ${process.env.GA_TRACKING_ID ? process.env.GA_TRACKING_ID : null}
        </script>
        <div id="root"></div>
        <script src="${vendorPath}" ></script>
        <script src="${jsPath}" ></script>
      </body>
    </html>
    `
}

const app = new Express()
const server = new http.Server(app)
let log

if(process.env.FLUENTD_HOST && process.env.FLUENTD_PORT) {
  const tcpStream = bunyanTcp.createBunyanStream({
    server: process.env.FLUENTD_HOST,
    port: process.env.FLUENTD_PORT,
    transform: (evt) => {
      const ts = moment().unix()
      return ['makestar-universal',ts,evt]
    }
  })
  log = bunyan.createLogger({ name: 'makestar-universal', serializers: { req: bunyan.stdSerializers.req, res: bunyan.stdSerializers.res, err: bunyan.stdSerializers.err }, streams: [{ stream: tcpStream, type: 'raw' },{ stream: process.stdout }] })

  tcpStream.on('connect',() => {
    log.info('Connected to fluentd')
  })

  tcpStream.on('disconnect', () => {
    log.info('Disconnected from fluentd')
  })

} else {
  log = bunyan.createLogger({ name: 'makestar-universal', serializers: { req: bunyan.stdSerializers.req, res: bunyan.stdSerializers.res, err: bunyan.stdSerializers.err } })
}

const devManifest = {
  app: {
    js: '/public/js/app.js',
    css: '/public/css/app.css'
  },
  comment: {
    js: '/public/js/comment.js'
  },
  vendor:{
    js: '/public/js/vendor.js'
  }
}

const manifest = process.env.NODE_ENV && process.env.NODE_ENV !== 'development' ? JSON.parse(fs.readFileSync('./assets.json','UTF-8')) : devManifest

let jsPath
let vendorPath
let cssPath
let intlPath

if(process.env.CLOUD_FRONT_END_POINT) {
  cssPath = `${process.env.CLOUD_FRONT_END_POINT}${manifest['app']['css']}`
  jsPath = `${process.env.CLOUD_FRONT_END_POINT}${manifest['app']['js']}`
  vendorPath = `${process.env.CLOUD_FRONT_END_POINT}${manifest['vendor']['js']}`
} else if (process.env.HOST_NAME) {
  cssPath = `${manifest['app']['css']}`
  jsPath =  `${manifest['app']['js']}`
  vendorPath = `${manifest['vendor']['js']}`
} else {
  cssPath = `http://localhost:3000${manifest['app']['css']}`
  jsPath =  `http://localhost:3000${manifest['app']['js']}`
  vendorPath = `http://localhost:3000${manifest['vendor']['js']}`
}
const commentPath = manifest['comment']['js']

function handleRender(req,res,next) {
  if (process.env.NOSSR) {
    res.send(hydrateOnClient(first(first(req.headers['accept-language'].split(',')).split('-'))))
  } else {
    const client = new ApiClient(superagent,req)
    const memoryHistory = createMemoryHistory(req.originalUrl)
    const store = configureStore({},client,history)
    const history = syncHistoryWithStore(memoryHistory,store)
    const { getState, dispatch } = store

    match({ history, routes: getRoutes() }, (err,redirectLocation, renderProps) => {
      if(redirectLocation) {
        log.info("REDIRECT")
      } else if (err) {
        log.info({ err })
      } else if (renderProps) {
        const { components } = renderProps
        const locals = {
          path: renderProps.location.pathname,
          query: renderProps.location.query,
          params: renderProps.params,
          dispatch,
          getState
        }
        return trigger('fetch',components,locals)
        .then(() => {
          const state = getState()
          const html = renderToString(<Provider store={store} key="provider"><RouterContext {...renderProps}/></Provider>)

          res.status(200)
          global.navigator = { userAgent: req.headers['user-agent'] }
          res.send(renderFullPage(html,state))
        }).catch((err) => { log.info({ err }); res.send(500); })
      }
    })
  }
}

app.use((req,res,next) => {
  log.info({ req, res })
  next()
})

if(process.env.NODE_ENV && process.env.NODE_ENV !== 'development') {
  log.info("Serving assets from:" + path.join(__dirname, '../../','public'))
  app.use(favicon(path.join(__dirname,'../../public','favicon.ico')))
  app.use('/public/',Express.static(path.join(__dirname, '../../','public'),{ maxAge: process.env.NODE_ENV === 'development' || process.env.NODE_ENV === null ? 0 : 259200000 }))
  app.use('/l/public/fonts',Express.static(path.join(__dirname,'../../','public','fonts')))
  app.use('/public/js/comment.js',Express.static(path.join(__dirname,'../../',commentPath)))
  app.use('/public/js/vendor.js',Express.static(path.join(__dirname,'../../',vendorPath)))
}

app.use(/\/$/,handleRender)
app.use('/polls',handleRender)
app.use('/projects',handleRender)
app.use('/contents/articles',handleRender)
app.use('/about', handleRender)
app.use('/main', handleRender)

if((process.env.NODE_ENV == 'development' || process.env.NODE_ENV == null) && (!process.env.FAKE_API)) {

  const webpack = require('webpack')
  const webpackConfig = require('../../webpack.config.js')
  const compiler = webpack(Object.assign({},webpackConfig,{ hot: true }))
  app.use(require('webpack-dev-middleware')(compiler,{ publicPath: webpackConfig.output.publicPath }))
  app.use(require('webpack-hot-middleware')(compiler))

  log.info("Proxying requests to API...")
  const proxyApi = httpProxy.createProxyServer({
    target: `http://${process.env.API_HOST}:${process.env.API_PORT}/v1/`
  }).on('error',function(err,req,res) {
    log.info({ err, req, res })
  })

  const proxyWeb = httpProxy.createProxyServer({
    target: `http://${process.env.API_HOST}:${process.env.API_PORT}`
  }).on('error',function(err,req,res) {
    log.info({ err, req, res })
  })

  app.use('/',(req,res) => {
    proxyWeb.web(req,res)
  })

  app.use('/v1/',(req,res) => {
    proxyApi.web(req, res)
  })

} else if((process.env.NODE_ENV == 'development' || process.env.NODE_ENV == null) && (process.env.FAKE_API == 'true')) {
  log.info("Proxying to fake API...")
  const proxyFakeApi = httpProxy.createProxyServer({
    target: `http://${process.env.API_HOST}:${process.env.API_PORT}/v1/`
  }).on('error',function(err,req,res) {
    log.info({ err, req, res })
  })

  const proxyWeb = httpProxy.createProxyServer({
    target: 'http://localhost:8080'
  }).on('error',function(err,req,res) {
    log.info({ err, req, res })
  })

  app.use('/v1/',(req,res) => {
    proxyFakeApi.web(req, res)
  })
}


server.listen(3000,(err) => {
  if(err) {
    log.info({ err })
  } else {
    if(!process.env.FAKE_API || process.env.FAKE_API === 'false') {
      log.info('----\n==> ✅  %s is running, talking to API server %s on %s.', 'MakeStar Universal', process.env.API_HOST, process.env.API_PORT ? process.env.API_PORT : 8080)
    } else {
      log.info('----\n==> ✅  %s is running, talking to FAKE API server on %s.', 'MakeStar Universal', process.env.API_PORT ? process.env.API_PORT : 8081)
    }
  }
})

export default server
