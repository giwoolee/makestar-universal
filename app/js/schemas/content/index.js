import { normalize, Schema, arrayOf } from 'normalizr'

const content = new Schema('content', { idAttribute: 'idx' })

export const receiveOne = (response) => {
  return normalize(response,{ content: content })
}

export const receiveAll = (response) => {
  return normalize(response,{ content: arrayOf(content) })
}

