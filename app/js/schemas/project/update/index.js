import { normalize, arrayOf, Schema } from 'normalizr'

const projectUpdate = new Schema('projectUpdate', { idAttribute: 'idx' })

export const receiveOne = (response) => {
  return normalize(response,{ content: projectUpdate })
}

export const receiveAll = (response) => {
  return normalize(response, { content: arrayOf(projectUpdate) })
}
