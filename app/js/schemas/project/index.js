import { normalize, arrayOf, Schema } from 'normalizr'

const project = new Schema('project',{ idAttribute: 'idx' })

export const receiveOne = (response) => {
  return normalize(response,{ content: project })
}

export const receiveAll = (response) => {
  return normalize(response, { content: arrayOf(project) })
}

