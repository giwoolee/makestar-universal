import { normalize, Schema, arrayOf } from 'normalizr'

const comment = new Schema('comment',{ idAttribute: 'idx' })
const reply = new Schema('reply', { idAttribute: 'idx' })

comment.define({
  replies: { content: arrayOf(reply) }
})

export const receiveOne = (response) => {
  return normalize(response,{ content: comment })
}

export const receiveAll = (response) => {
  return normalize(response,{ content: arrayOf(comment) })
}

