import { normalize, arrayOf, Schema } from 'normalizr'

const rank = new Schema('rank', { idAttribute: 'idx' })
const poll = new Schema('poll',{ idAttribute: 'idx' })

poll.define({
  rank: arrayOf(rank)
})

export const receiveOne = (response) => {
  return normalize(response, { content: poll })
}

export const receiveAll = (response) => {
  return normalize(response, { content: arrayOf(poll) })
}

