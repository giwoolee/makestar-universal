import expect from 'expect'
import loginRefresher from '../../../middleware/loginRefresher/index'
import configureMockStore from 'redux-mock-store'
import { SESSION_LOGIN_REFRESH_CONTENT } from '../../../constants/session/index'
import * as types from '../../../constants/poll/show/index'
import clientMiddleware from '../../../middleware/clientMiddleware'
import { pollFetch } from '../../../actions/poll/show/index'
import thunk from 'redux-thunk'
import superagent from 'superagent'
import ApiClient from '../../../helpers/ApiClient'
import nock from 'nock'
import pollResponse from '../../testHelpers/pollResponse'
import contentsResponse from '../../testHelpers/contentsResponse'

function setup() {
  const client = new ApiClient(superagent)
  const middlewares = [ thunk, clientMiddleware(client), loginRefresher() ]
  const mockStore = configureMockStore(middlewares)

  return { mockStore }
}

describe('middleware/loginRefresher/index', () => {
  context('when the pathname is /polls/1', () => {
    it('creates a POLL_SHOW_FETCH_REQUEST', () => {
      const resp = pollResponse()
      nock('http://localhost:8080/')
      .get('/v1/polls/1')
      .reply(200, { content: resp })


      const { mockStore } = setup()
      const store = mockStore({ poll: { poll: { idx: 1 } }, router: { location: { pathname: '/polls/1' } },session: { session: { locale: 'en' } } })
      store.dispatch({ type: SESSION_LOGIN_REFRESH_CONTENT })
      expect(store.getActions()[0]).toEqual({ type: 'POLL_SHOW_FETCH_REQUEST' })
    })
  })

  context('when the pathname is /contents/articles/1', () => {
    it('creates a CONTENT_SHOW_FETCH_REQUEST', () => {
      const resp = contentsResponse()
      nock('http://localhost:8080/')
      .get('/v1/contents/articles/1')
      .reply(200, { content: resp })

      const { mockStore } = setup()
      const store = mockStore({ content: { content: resp }, router: { location: { pathname: '/contents/articles/1' }}, session: { session: { locale: 'en' } } })
      store.dispatch({ type: SESSION_LOGIN_REFRESH_CONTENT })
      expect(store.getActions()[0]).toEqual({ type: 'CONTENT_SHOW_FETCH_REQUEST' })
    })
  })
})
