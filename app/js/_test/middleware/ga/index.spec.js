import expect from 'expect'
import gaMiddleware from '../../../middleware/ga/index'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { GA_RENDER_COMPLETE } from '../../../constants/shared/ga/index'
import { GLOBAL_INITIAL_LOAD } from '../../../constants/global/index'

function setup () {
  const fakeGa = {
    pageview: expect.createSpy(),
    modalview: expect.createSpy(),
    event: expect.createSpy(),
    outboundLink: expect.createSpy()
  }
  const middlewares = [ thunk, gaMiddleware(fakeGa) ]
  const store = configureMockStore(middlewares)

  return { store, fakeGa }
}

describe('middleware/ga/index', () => {

  context('when the object passed is an instance of react-ga',() => {
    context('it returns a function', () => {
      const { fakeGa } = setup()
      expect(gaMiddleware(fakeGa)).toBeA(Function)
    })

    context('when the action type is in triggerActions', () => {
      it('creates an event in GA', () => {
        const { store, fakeGa } = setup()
        const action = {
          type: GA_RENDER_COMPLETE
        }
        store({ routing: { locationBeforeTransitions: { pathname: '/polls' } } },[action]).dispatch(action)
        expect(fakeGa.pageview).toHaveBeenCalledWith('/polls')
      })
    })

    context('when the action type is GLOBAL_INITIAL_LOAD', () => {
      it('creates an event in GA', () => {
        const { store, fakeGa } = setup()
        const action = {
          type: GLOBAL_INITIAL_LOAD
        }
        store({ routing: { locationBeforeTransitions: { pathname: '/polls' } } },[action]).dispatch(action)
        expect(fakeGa.pageview).toHaveBeenCalledWith('/polls')
      })
    })

    context('when the action contains gaData', () => {
      it('creates an event in ga and removes the gaData from the action', () => {
        const { store, fakeGa } = setup()
        const action = {
          type: 'POLL_FETCH_REQUEST',
          gaData: {
            category: 'POLL',
            action: 'Page Nav',
            label: 1
          }
        }
        store({ routing: { locationBeforeTransitions: { pathname: '/polls' } } },[{ type: 'POLL_FETCH_REQUEST' }]).dispatch(action)
        expect(fakeGa.event).toHaveBeenCalledWith({ category: 'POLL', action: 'Page Nav', label: 1 })
      })
    })

    context('when the action doesn\'t contain gaData or isn\'t in triggerActions type', () => {
      it('doesn\'t call any ga method', () => {
        const { store, fakeGa } = setup()
        const action = {
          type: 'COMMENT_FETCH_SUCCESS'
        }

        store({},[action]).dispatch(action)
        expect(fakeGa.event).toNotHaveBeenCalled()
        expect(fakeGa.pageview).toNotHaveBeenCalled()
      })
    })
  })

  context('when no ga object is passed', () => {
    it('throws an error', () => {
      expect(() => {
        gaMiddleware()
      }).toThrow()
    })
  })

  context('when the object passed isn\'t an instance of react-ga', () => {
    it('throws an error', () => {
      expect(() => {
        gaMiddleware({ nothing: undefined })
      }).toThrow()
    })
  })
})
