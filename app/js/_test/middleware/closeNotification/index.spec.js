import expect from 'expect'
import closeNotification from '../../../middleware/closeNotification/index'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const LOCATION_CHANGE = '@@router/UPDATE_LOCATION'

function setup() {
  const middlewares = [ thunk, closeNotification() ]
  const mockStore = configureMockStore(middlewares)

  return { mockStore }
}

describe('middleware/closeNotification', () => {
  context('when notification.show is true', () => {
    it('creates a UI_NOTIFICATION_RESET action', () => {
      const { mockStore } = setup()

      const store = mockStore({ ui: { notification: { show: true } }})
      store.dispatch({ type: '@@router/UPDATE_LOCATION' })
      expect(store.getActions()[0]).toEqual({ type: 'UI_NOTIFICATION_RESET' })
    })
  })

  context('when notification.show is false', () => {
    it('doesn\'t create a UI_NOTIFICATION_RESET',() => {
      const { mockStore } = setup()

      const store = mockStore({ ui: { notification: { show: false } }})
      store.dispatch({ type: '@@router/UPDATE_LOCATION' })
      expect(store.getActions()[0]).toEqual({ type:  '@@router/UPDATE_LOCATION' })
    })
  })
})
