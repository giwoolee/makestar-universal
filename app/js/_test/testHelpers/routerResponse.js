export default (pathname = "/contents/article", query = {}) => {
  return {
    locationBeforeTransitions: {
      pathname,
      query,
      action: "REPLACE",
      hash: "",
      key: "",
      search: ""
    }
  }
}
