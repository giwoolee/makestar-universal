import { times } from 'lodash'

export default (position = 1,level = 1, containerWidth = "Fit",numOfItems = 1) => {
  function bannerItem(idx = 1) {
    return {
      "idx": idx + 1,
      "position": idx + 1,
      "mobileImageUrl": "/file/banners/1/howto_kr_m.jpg",
      "webImageUrl": "/file/banners/1/howto_kr.jpg",
      "videoUrl": null,
      "linkUrl": 'http://makestar.co',
      "imageTypeBanner": true
    }
  }

  return {
    "level": 1,
    "containerSize": numOfItems,
    "containerWidth": containerWidth,
    "items": times(numOfItems,(i) => bannerItem(i))
  }
}

