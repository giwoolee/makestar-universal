import { take } from 'lodash'

export default (numOfGeoPoints) => {
  return take([
    {
      "code": "KR",
      "percentage": 0.37451437,
      "name": "한국"
    },
    {
      "code": "US",
      "percentage": 0.2735043,
      "name": "미국"
    },
    {
      "code": "JP",
      "percentage": 0.08003108,
      "name": "일본"
    },
    {
      "code": "CN",
      "percentage": 0.045066044,
      "name": "중국"
    },
    {
      "code": "GB",
      "percentage": 0.043512043,
      "name": "영국"
    },
    {
      "code": "CA",
      "percentage": 0.037296038,
      "name": "캐나다"
    },
    {
      "code": "AU",
      "percentage": 0.03263403,
      "name": "호주"
    },
    {
      "code": "DE",
      "percentage": 0.018648019,
      "name": "독일"
    },
    {
      "code": "SG",
      "percentage": 0.010878011,
      "name": "싱가폴"
    },
    {
      "code": "FR",
      "percentage": 0.010878011,
      "name": "프랑스"
    },
    {
      "code": "NL",
      "percentage": 0.0093240095,
      "name": "네덜란드"
    },
    {
      "code": "HK",
      "percentage": 0.008547009,
      "name": "홍콩"
    },
    {
      "code": "MY",
      "percentage": 0.006993007,
      "name": "말레이시아"
    },
    {
      "code": "SE",
      "percentage": 0.0054390053,
      "name": "스웨덴"
    },
    {
      "code": "IT",
      "percentage": 0.003885004,
      "name": "이탈리아"
    },
    {
      "code": "FI",
      "percentage": 0.003885004,
      "name": "핀란드"
    },
    {
      "code": "TH",
      "percentage": 0.003885004,
      "name": "타이"
    },
    {
      "code": "MX",
      "percentage": 0.0031080032,
      "name": "멕시코"
    },
    {
      "code": "CZ",
      "percentage": 0.0031080032,
      "name": "체코"
    },
    {
      "code": "BR",
      "percentage": 0.0023310024,
      "name": "브라질"
    },
    {
      "code": "AT",
      "percentage": 0.0023310024,
      "name": "오스트리아"
    },
    {
      "code": "ES",
      "percentage": 0.0023310024,
      "name": "스페인"
    },
    {
      "code": "DK",
      "percentage": 0.0015540016,
      "name": "덴마크"
    },
    {
      "code": "NO",
      "percentage": 0.0015540016,
      "name": "노르웨이"
    },
    {
      "code": "TW",
      "percentage": 0.0015540016,
      "name": "대만"
    },
    {
      "code": "CR",
      "percentage": 0.0015540016,
      "name": "코스타리카"
    },
    {
      "code": "NZ",
      "percentage": 0.0015540016,
      "name": "뉴질랜드"
    },
    {
      "code": "PH",
      "percentage": 0.0015540016,
      "name": "필리핀"
    },
    {
      "code": "PL",
      "percentage": 0.0015540016,
      "name": "폴란드"
    },
    {
      "code": "AR",
      "percentage": 0.0007770008,
      "name": "아르헨티나"
    },
    {
      "code": "KZ",
      "percentage": 0.0007770008,
      "name": "카자흐스탄"
    },
    {
      "code": "PT",
      "percentage": 0.0007770008,
      "name": "포르투갈"
    },
    {
      "code": "JO",
      "percentage": 0.0007770008,
      "name": "요르단"
    },
    {
      "code": "CH",
      "percentage": 0.0007770008,
      "name": "스위스"
    },
    {
      "code": "ID",
      "percentage": 0.0007770008,
      "name": "인도네시아"
    },
    {
      "code": "BN",
      "percentage": 0.0007770008,
      "name": "브루나이"
    },
    {
      "code": "CL",
      "percentage": 0.0007770008,
      "name": "칠레"
    },
    {
      "code": "IL",
      "percentage": 0.0007770008,
      "name": "이스라엘"
    }

  ],numOfGeoPoints)
}
