import loremIpsum from 'lorem-ipsum'

export default (idx = 1) => {
  const description = loremIpsum({ count: Math.floor(Math.random() * (77 - 10) + 10), units: 'words' })
  const authorDescription = loremIpsum({ count: Math.floor(Math.random() * ( 80 - 10) + 10), units: 'words' })
  const subject = loremIpsum({ count: Math.floor(Math.random() * (5 - 1) + 15),units: 'words' })
  return {
    "idx": idx,
    "date": "2016-03-01T15:00:00+0000",
    "subject": subject,
    "description": description,
    "textUrl": "/v1/contents/articles/1/text/index.html",
    "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
    "eventLinkUrl": "http://hn-hn.co.kr",
    "eventLinkText": "하늘하늘",
    "snsimageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
    "tags": [
      "tag1",
      "tag2"
    ],
    "links": [
      {
        "rel": "self",
        "href": "http://localhost:3000/v1/contents/articles/1"
      },
      {
        "rel": "web",
        "href": "http://localhost:3000/contents/articles/1"
      }
    ],
    "author": {
      "idx": 1,
      "name": "하늘",
      "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
      "linkUrl": "http://hn-hn.co.kr",
      "description": authorDescription
    }
  }
}
