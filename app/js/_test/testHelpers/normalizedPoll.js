import loremIpsum from 'lorem-ipsum'
export default (idx=1,thumbnailUrl = null,isAvailable = true, isEnd = false) => {

    const title = loremIpsum({ count: Math.floor(Math.random() * (5 - 1) + 10),units: 'words' })
    return {
      "idx": idx,
      "title": title,
      "rank": [1,2,3,4,5],
      "candidateCount": 10,
      "votingType": "ONCE",
      "myVoteCount": 1,
      "voteCount": 23051,
      "isAvailable": isAvailable,
      "isEnd": isEnd,
      "startDate": "2016-02-05",
      "endDate": "2016-02-05",
      "snsimageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
      "headerUrl": '/v1/contents/articles/1/text/index.html',
      "thumbnailUrl": thumbnailUrl,
      "links":[
        {"rel":"self","href":"http://localhost:8080/v1/polls/2"},
        {"rel":"web","href":"http://www.makestar.local/polls/2"}
      ],
      "tags": [
        "f(x)",
        "소녀시대"
      ]
    }
}

