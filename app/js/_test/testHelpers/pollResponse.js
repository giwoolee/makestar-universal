import loremIpsum from 'lorem-ipsum'
export default (idx=1,thumbnailUrl = null,isAvailable = true, isEnd = false) => {

    const title = loremIpsum({ count: Math.floor(Math.random() * (5 - 1) + 10),units: 'words' })
    return {
      "idx": idx,
      "title": title,
      "rank": [
        {
          "idx": 1,
          "imageUrl": "https://dl.dropboxusercontent.com/s/ezguqrhg4p7387d/2016-01-10%2014.35.32.jpg?dl=0",
          "name": "Option 1",
          "myVoteCount": 0,
          "voteCount": 1,
          "linkText": "text"
        },
        {
          "idx": 2,
          "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
          "name": "Option 2",
          "myVoteCount": 0,
          "voteCount": 1,
          "name": "Name",
          "linkText": "text",
          "linkUrl": "url"
        },
        {
          "idx": 3,
          "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
          "name": "Option 3",
          "myVoteCount": 0,
          "voteCount": 1,
          "name": "Name",
          "linkText": "text",
          "linkUrl": "url"
        },
        {
          "idx": 4,
          "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
          "name": "Option 4",
          "myVoteCount": 0,
          "voteCount": 1,
          "name": "Name",
          "linkText": "text",
          "linkUrl": "url"
        },
        {
          "idx": 5,
          "imageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
          "name": "Option 5",
          "myVoteCount": 0,
          "voteCount": 1,
          "name": "Name",
          "linkText": "text",
          "linkUrl": "url"
        }
      ],
      "candidateCount": 10,
      "votingType": "ONCE",
      "myVoteCount": 1,
      "voteCount": 23051,
      "isAvailable": isAvailable,
      "isEnd": isEnd,
      "startDate": "2016-02-05",
      "endDate": "2016-02-05",
      "snsimageUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
      "headerUrl": '/v1/contents/articles/1/text/index.html',
      "thumbnailUrl": thumbnailUrl,
      "links":[
        {"rel":"self","href":"http://localhost:8080/v1/polls/2"},
        {"rel":"web","href":"http://www.makestar.local/polls/2"}
      ],
      "tags": [
        "f(x)",
        "소녀시대"
      ]
    }
}

