export default (page = 1, isLast = true) => {
  return {
    isLast,
    currentPage: page,
    totalElements: 1,
    totalPages: 1,
  }
}
