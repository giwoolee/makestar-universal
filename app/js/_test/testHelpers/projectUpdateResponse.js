import loremIpsum from 'lorem-ipsum'

export default (idx = 1,contentType = 'Photo') => {
  const description = loremIpsum({ count: Math.floor(Math.random() * (30 - 10) + 10), units: 'words' })
  //const title = loremIpsum({ count: Math.floor(Math.random() * (20 - 10) + 10), units: 'words' })
  return {
    "idx": idx,
    "projectId": "diea",
    "projectIdx": 14,
    "date": "2016-02-29T08:17:54+0000",
    "projectShortName": "TAHITI",
    "title": "Seoul day trip prep!",
    "description": description,
    "iconImageUrl": null,
    "contentType": contentType,
    "imageUrl": "/file/project/diea/update/1/TAHITI_update03_thumb.jpg",
    "snsImageUrl": "/file/project/diea/update/1/TAHITI_update03_thumb.jpg",
    "links": [
      {
        "rel": "web",
        "href": "http://www.makestar.co/project/diea/update/1/#tab"
      }
    ]
  }
}
