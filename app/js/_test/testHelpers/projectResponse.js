import loremIpsum from 'lorem-ipsum'

export default (_id = 'singer_nca',idx = 1) => {
  const description = loremIpsum({ count: Math.floor(Math.random() * (20 - 1) + 10), units: 'words' })
  const name = loremIpsum({ count: Math.floor(Math.random() * (5 - 1) + 5),units: 'words' })
  const backers = Math.floor(Math.random() * (1000 - 1) + 10)
  const goalDollar = Math.floor(Math.random() * (10000 - 1) + 10)
  const goalLocal = Math.floor(Math.random() * (10000 - 1) + 10)
  const sumDollar = Math.floor(Math.random() * (goalDollar - 1) + 10)
  const sumLocal = Math.floor(Math.random() * (goalLocal - 1) + 10)
  const percent = Math.random() * (0 - 100) + 0

  return {
    "idx": idx,
     "_id": _id,
     "startDate": "2016-03-24T07:00:00+0000",
     "endDate": "2016-05-24T14:59:59+0000",
     "backers": backers,
     "percent": percent,
     "goalDollar": goalDollar,
     "goalLocal": goalLocal,
     "sumDollar": sumDollar,
     "sumLocal": sumLocal,
     "sum": {
       "currency":"USD"
     },
     "name": name,
     "description": description,
     "imageUrl":"https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
     "thumbnailUrl": "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0",
     "videoUrl": "https://www.youtube.com/embed/gyI1eu13QN8",
     "tags": [
       "음반"
     ],
     "dday": "D - 1",
     "remain": {
       "sec": 37,
       "min": 22,
       "hour": 12,
       "day": 40
     },
     "status": "Opened",
     "fundingType": "AllOrNothing",
     "links": [
       {
         "rel": "web",
         "href": "http://localhost:8080/project/singer_nca/"
       }
     ]
   }
}
