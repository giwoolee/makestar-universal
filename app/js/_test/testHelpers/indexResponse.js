import { times, map, zipObject, fill } from 'lodash'
import projectResponse from './projectResponse'
import pollResponse from './pollResponse'
import contentsResponse from './contentsResponse'
import bannerResponse from './bannerResponse'
import mapResponse from './mapResponse'
import topNoticeResponse from './topNoticeResponse'

export default function(bannerOptions = [{ position: 1, level: 1, numOfItems: 1, containerWidth: "Fit" },{ position: 2, level: 2, numOfItems: 2, containerWidth: "Fit"},{ position: 3, level: 3, numOfItems: 1, containerWidth: "Full"} ],numOfGeoPoints = 10) {

  const polls = times(3,(i) => { return pollResponse(i+1) })
  const projects = [projectResponse('tahiti_album'),projectResponse('singer_nca'),projectResponse('stellar_mini_album'),projectResponse('laboum_mini_album'),projectResponse('god_of_noodles_stack_the_bowls'),projectResponse('heyne_single_album')]
  const banners = zipObject(times(bannerOptions.length,(i) => { return i + 1 }),map(bannerOptions,(bOpts,i) => { return bannerResponse(bOpts.position,bOpts.level,bOpts.containerWidth,bOpts.numOfItems) }))
  const maps = mapResponse(numOfGeoPoints)
  const articles = times(3,(i) => { return contentsResponse(i+1) })
  const topNotice = topNoticeResponse()

  return {
    "projects": projects,
    "polls": polls,
    "banners": banners,
    "articles": articles,
    "map": maps,
    "topNotice": topNotice
  }
}
