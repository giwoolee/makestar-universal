import expect from 'expect'
import React from 'react'
import Index from '../../../../components/project/index/Index'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'
import i18n from '../../../../i18n/index'
import projectResponse from '../../../testHelpers/projectResponse'
import PaginationContainer from '../../../../containers/shared/PaginationContainer'
import ProjectContainer from '../../../../containers/project/index/ProjectContainer'
import { fill } from 'lodash'

function setup(fullRendering = false) {
  const props = {
    idxs: [[1]],
  }
  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<Index {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Index { ...props} /></IntlProvider>)
  }

  return {
    output,
    props
  }
}

describe('components',() => {
  describe('project/index/Index',() => {
    it('renders the correct number of Project components', () => {
      const { output } = setup()
      expect(output.find(ProjectContainer).length).toEqual(1)
    })

    it('renders ProjectContainer with the correct props', () => {
      const { output } = setup()
      expect(output.find(ProjectContainer).first().props()).toEqual({ idx: 1 })
    })

    it('renders the Pagination container',() => {
      const { output } = setup()
      expect(output.find(PaginationContainer).length).toEqual(1)
    })
  })
})
