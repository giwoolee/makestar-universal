import expect from 'expect'
import React from 'react'
import Project from '../../../../components/project/index/Project'
import { Link } from 'react-router'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'
import i18n from '../../../../i18n/index'

function setup(fullRendering = false,locale='en',remain={ sec: 30, min: 22, hour: 22, day: 10 },isInitialLoad = false) {
  let currency
  switch(locale) {
    case 'en':
      currency = 'USD'
      break
    case 'ko':
      currency = 'KRW'
      break
    case 'zh':
      currency = 'CNY'
      break
    case 'ja':
      currency = 'JPY'
      break
  }
  const props = {
    thumbnailUrl: 'https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0',
    sumLocal: 10000,
    remain,
    sum: { currency },
    percent: 66.5,
    name: 'Project Name',
    tags: ['Musical','Production'],
    isInitialLoad,
    actions: {
      gaEvent: expect.createSpy()
    }
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<Project { ...props } />)
  } else {
    output = mount(<IntlProvider locale={locale} messages={messages}><Project { ...props } /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('components/project/index/Project',() => {
    context('when isInitialLoad is false', () => {
      it('renders the image with the correct src', () => {
        const { output, props } = setup()
        expect(output.find('img').props().src).toEqual(props.thumbnailUrl)
      })
    })

    it('renders the progress bar with the correct properties', () => {
      const { output, props } = setup()
      expect(output.find('.progress-bar').props().style).toEqual({ width: `${props.percent.toFixed(0)}%` })
    })

    it('renders the sum', () => {
      const { output, props } = setup(true)
      expect(output.find('.project-index-project-sum-text').text()).toEqual('$10,000.00')
    })

    context('when the currency is KRW', () => {
      it('renders the sum with the correct currency', () => {
        const { output, props } = setup(true,'ko')
        expect(output.find('.project-index-project-sum-text').text()).toEqual('₩10,000')
      })
    })

    context('when the currency is JPY', () => {
      it('renders the sum with the correct currency', () => {
        const { output, props } = setup(true,'ja')
        expect(output.find('.project-index-project-sum-text').text()).toEqual('￥10,000')
      })
    })

    context('when the currency is CNY', () => {
      it('renders the sum with the correct currency', () => {
        const { output, props } = setup(true,'zh')
        expect(output.find('.project-index-project-sum-text').text()).toEqual('￥10,000.00')
      })
    })

    it('renders the project name', () => {
      const { output, props } = setup(true)
      expect(output.find('.project-index-project-info-name-text').text()).toEqual(props.name)
    })

    it('renders the tag component', () => {
      const { output, props } = setup(true)
      expect(output.find('.project-index-tags').length).toEqual(1)
    })

    context('when the remaining time is days', () => {
      it('renders the time remaining in days',() => {
        const { output, props } = setup(true)
        expect(output.find('.project-index-project-remaining-text').text()).toEqual('10 days left ')
      })
    })

    context('when the remaining time is hours', () => {
      it('renders the time remaining in hours',() => {
        const { output, props } = setup(true,'en-US',{ sec: 10, min: 10, hour: 10, days: 0 })
        expect(output.find('.project-index-project-remaining-text').text()).toEqual('10 hours left')
      })
    })

    context('when the remaining time is minutes', () => {
      it('renders the time remaining in hours',() => {
        const { output, props } = setup(true,'en-US',{ sec: 10, min: 10, hour: 0, day: 0 })
        expect(output.find('.project-index-project-remaining-text').text()).toEqual('10 minutes left')
      })
    })

    context('when the remaining time is seconds', () => {
      it('renders the time remaining in hours',() => {
        const { output, props } = setup(true,'en-US',{ sec: 10, min: 0, hour: 0, day: 0 })
        expect(output.find('.project-index-project-remaining-text').text()).toEqual('10 seconds left')
      })
    })

    it('calls gaEvent when the link is clicked', () => {
      const { output, props } = setup()
      output.find('a').simulate('click')
      expect(props.actions.gaEvent).toHaveBeenCalledWith({ action: 'click', category: 'Projects', label: props._id })
    })
  })
})
