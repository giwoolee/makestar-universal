import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Contents from '../../../components/index/Contents'
import ContentItem from '../../../components/index/ContentItem'
import contentResponse from '../../testHelpers/contentsResponse'
import { times } from 'lodash'

function setup(fullRendering = false) {
  const props = {
    contents: times(3,(i) => contentResponse(i))
  }

  let output
  if(!fullRendering) {
    output = shallow(<Contents {...props} />)
  } else {
    output = mount(<Contents {...props} />)
  }

  return {
    output,
    props
  }
}

describe('components/index/Contents', () => {
  it('renders the correct number of ContentItems', () => {
    const { output, props } = setup()
    expect(output.find(ContentItem).length).toEqual(props.contents.length)
  })
})
