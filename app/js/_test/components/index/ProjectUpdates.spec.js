import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import ProjectUpdates from '../../../components/index/ProjectUpdates'
import projectUpdateResponse from '../../testHelpers/projectUpdateResponse'
import ProjectUpdateContainer from '../../../containers/index/ProjectUpdateContainer'
import { times } from 'lodash'
import i18n from '../../../i18n'
import flatten from 'flat'

function setup(numOfItems=1) {
  const idxs = times(numOfItems, (i) => i + 1)

  const props = {
    idxs
  }

  let output = shallow(<ProjectUpdates {...props} />)

  return {
    output,
    props
  }
}

describe('components/index/ProjectUpdates', () => {
  context('when there are no updates',() => {
    it('renders no updates', () => {
      const { output } = setup(0)
      expect(output.find(ProjectUpdateContainer).length).toEqual(0)
    })
  })

  context('when there are updates', () => {
    it('renders ProjectUpdateContainer with the corect props', () => {
      const { output } = setup()
      expect(output.find(ProjectUpdateContainer).props()).toEqual({ idx: 1 })
    })
  })
})
