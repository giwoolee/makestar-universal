import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import ContentItem from '../../../components/index/ContentItem'
import { IntlProvider } from 'react-intl'
import contentResponse from '../../testHelpers/contentsResponse'
import i18n from '../../../i18n'
import flatten from 'flat'

function setup(isLast = false, fullRendering = false) {
  const props = Object.assign({},{ isLast },contentResponse(1))

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<ContentItem {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><ContentItem {...props } /></IntlProvider>)
  }

  return {
    output,
    props
  }
}

describe('components/index/ContentItem', () => {
  it('renders the image', () => {
    const { output, props } = setup()
    expect(output.find('.index-content-item-image').props()['data-src']).toEqual(`url(${props.imageUrl})`)
  })

  it('renders the content title', () => {
    const { output, props } = setup()
    expect(output.find('.index-content-item-title-text').text()).toEqual(props.subject)
  })

  context('when isLast is false', () => {
    it('it has the class index-content-item-first', () => {
      const { output } = setup()
      expect(output.find('.index-item-first').length).toEqual(1)
    })
  })

  context('when isLast is true', () => {
    it('it has the class index-content-item-last', () => {
      const { output } = setup(true)
      expect(output.find('.index-item-last').length).toEqual(1)
    })
  })
})
