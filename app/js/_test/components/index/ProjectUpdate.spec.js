import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import ProjectUpdate from '../../../components/index/ProjectUpdate'
import projectUpdateResponse from '../../testHelpers/projectUpdateResponse'
import { times } from 'lodash'
import i18n from '../../../i18n'
import flatten from 'flat'
import moment from 'moment'

function setup(contentType = 'Video', fullRendering = false) {
  const projectUpdate = projectUpdateResponse(1,contentType)

  const props = {
    date: moment().toDate(),
    title: 'Update Title',
    link: 'http://link',
    projectShortName: 'Short Name',
    iconImageUrl: 'url(http://iconimage.jpg)',
    imageUrl: 'url(http://image.jpg)',
    dateMessageId: 'en.project.date',
    actions: {
      onClick: expect.createSpy()
    }
  }

  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<ProjectUpdate {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><ProjectUpdate {...props} /></IntlProvider>)
  }

  return {
    output,
    props
  }
}

describe('components/index/ProjectUpdate', () => {
  it('renders the project update title', () => {
    const { output, props } = setup()
    expect(output.find('.project-update-update-title-text').text()).toEqual(props.title)
  })

  it('renders the project update short name', () => {
    const { output, props } = setup()
    expect(output.find('.project-update-update-short-project-name').text()).toEqual(props.projectShortName)
  })

  it('calls onClick when the link is clicked', () => {
    const { output, props } = setup()
    output.find('.project-update-update-link').simulate('click')
    expect(props.actions.onClick).toHaveBeenCalled()
  })

  it('sets the iconImageUrl to the backgroundImage of the project-update-icon class', () => {
    const { output, props } = setup()
    expect(output.find('.project-update-update-icon').props().style.backgroundImage).toEqual(props.iconImageUrl)
  })

  it('sets the imageUrl to the backgroundImage of the project-update-update-image class', () => {
    const { output, props } = setup()
    expect(output.find('.project-update-update-image').props().style.backgroundImage).toEqual(props.imageUrl)
  })
})

