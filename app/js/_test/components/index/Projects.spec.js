import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Projects from '../../../components/index/Projects'
import Project from '../../../components/project/index/Project'
import { chunk, times } from 'lodash'
import projectResponse from '../../testHelpers/projectResponse'

function setup(type = 'web', fullRendering = false) {
  const props = {
    projects: chunk(times(3,(i) => projectResponse(i+1)),3),
    type
  }

  let output
  if(!fullRendering) {
    output = shallow(<Projects {...props} />)
  } else {
    output = mount(<Projects {...props} />)
  }

  return {
    output,
    props
  }
}

describe('components/index/Projects', () => {

  context('when type is web', () => {
    it('renders the correct number of Projects', () => {
      const { output, props } = setup('web')

      expect(output.find(Project).length).toEqual(3)
    })

    it('renders the the web layout', () => {
      const { output, props } = setup()

      expect(output.find('.hidden-xs .index-projects-container').length).toEqual(1)
    })
  })

  context('when type is mobile', () => {
    it('renders the correct number of Projects', () => {
      const { output, props } = setup('mobile')

      expect(output.find(Project).length).toEqual(3)
    })
    it('renders the the web layout', () => {
      const { output, props } = setup('mobile')

      expect(output.find('.hidden-sm .hidden-md .hidden-lg .index-projects-container-xs').length).toEqual(1)
    })
  })
})

