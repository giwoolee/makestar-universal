import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import PollItem from '../../../components/index/PollItem'
import pollResponse from '../../testHelpers/pollResponse'

function setup(isLast = false,isInitialLoad=false,thumbnailUrl=null,fullRendering = false) {
  const props = Object.assign({},{ isLast, isInitialLoad },pollResponse(1,thumbnailUrl))

  let output
  if(!fullRendering) {
    output = shallow(<PollItem {...props } />)
  } else {
    output = mount(<PollItem {...props } />)
  }

  return {
    output,
    props
  }
}

describe('components/index/PollItem', () => {
  context('when initialLoad is false', () => {
    context('when the poll doesn\'t  have a thumbnailUrl',() => {
      it('renders the top image', () => {
        const { output, props } = setup()

        expect(output.find('.index-poll-item-image').props().style.backgroundImage).toEqual(`url(${props.rank[0].imageUrl})`)
      })
    })

    context('when the poll has a thumbnailUrl', () => {
      it('renders the thumbnailUrl',() => {
        const { output, props } = setup(false,false,'http://thumbnail.com/image.jpg')
        expect(output.find('.index-poll-item-image').props().style.backgroundImage).toEqual(`url(${props.thumbnailUrl})`)
      })
    })
  })

  context('when isInitialLoad is true', () => {
    it('renders the placeholder', () => {
      const { output, props } = setup(false,true)

      expect(output.find('.index-poll-item-image').props().style.backgroundImage).toEqual("url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)")
    })
  })

  it('renders the title', () => {
    const { output, props } = setup()

    expect(output.find('.index-poll-item-title-text').text()).toEqual(props.title)
  })

  context('when isLast is false', () => {
    it('has the class index-poll-item-first', () => {
      const { output } = setup()
      expect(output.find('.index-item-first').length).toEqual(1)
    })
  })

  context('when isLast is true', () => {
    it('has the class index-poll-item-last', () => {
      const { output } = setup(true)
      expect(output.find('.index-item-last').length).toEqual(1)
    })
  })

})
