import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import BannerItem from '../../../components/index/BannerItem'
import bannerResponse from '../../testHelpers/bannerResponse'

function setup(isLast=false, width: 100, isInitialLoad = false, fullRendering = false) {
  const { items: [banner] } = bannerResponse(1,1,'Fit',1)
  const props = Object.assign({},banner,{ isLast, width, isInitialLoad })
  let output
  if(!fullRendering) {
    output = shallow(<BannerItem { ...props } />)
  } else {
    output = mount(<BannerItem { ...props } />)
  }

  return {
    output,
    props
  }
}

describe('components/index/BannerItem', () => {
  context('when the item is an image item', () => {
    it('renders the webImageUrl image', () => {
      const { output, props } = setup()
      expect(output.find('.banner-item-image').first().props().style.backgroundImage).toEqual(`url(${props.webImageUrl})`)
    })

    it('renders the mobileImageUrl', () => {
      const { output, props } = setup()
      expect(output.find('.banner-item-image').last().props().style.backgroundImage).toEqual(`url(${props.mobileImageUrl})`)
    })

    it('renders the link', () => {
      const { output, props } = setup()
      expect(output.find('a').props().href).toEqual(props.linkUrl)
    })
  })

  context('when the banner isLast is false and width is 50%', () => {
    it('renders the banner-item-container with the correct class', () => {
      const { output, props } = setup(false,50)
      expect(output.find('.banner-item-container-left').length).toEqual(1)
    })
  })

  context('when the banner isLast is true and width is 50%', () => {
    it('renders the banner-item-container with the correct class', () => {
      const { output, props } = setup(true,50)
      expect(output.find('.banner-item-container-right').length).toEqual(1)
    })
  })
})

