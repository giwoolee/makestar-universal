import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import RollingBanner from '../../../components/index/RollingBanner'

function setup() {
  const props = {
  }

  const output = shallow(<RollingBanner/>)

  return {
    output,
    props
  }
}

describe('components/index/RollingBanner', () => {
  it('should render the first img with the class first',() => {
    const { output } = setup()
    expect(output.find('img').first().hasClass('first')).toEqual(true)
  })

  it('should render all images', () => {
    const { output } = setup()
    expect(output.find('img').length).toEqual(39)
  })
})
