import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import TopNotice from '../../../components/index/TopNotice'

function setup() {
  const props = {
    style: {
      background: 'red',
      color: 'white'
    },
    text: 'Text',
    iconImageUrl: 'http://image.com/image.jpeg',
    buttonText: 'Text',
    idx: 1,
    linkUrl: 'http://www.makestar.co'
  }

  const output = shallow(<TopNotice {...props} />)

  return {
    output,
    props
  }
}

describe('components/index/TopNotice', () => {
  it('renders the correct text',() => {
    const { output, props } = setup()
    expect(output.find('.top-notice-text').text()).toEqual(props.text)
  })

  it('applies the styles', () => {
    const { output, props } = setup()
    expect(output.find('.top-notice').props().style).toEqual(props.style)
  })

  it('renders the icon', () => {
    const { output, props } = setup()
    expect(output.find('.top-notice-icon').props().style.backgroundImage).toEqual(`url(${props.iconImageUrl})`)
  })

  it('renders a button', () => {
    const { output, props } = setup()
    expect(output.find('.top-notice-button').length).toEqual(1)
  })

  it('renders a link', () => {
    const { output, props } = setup()
    expect(output.find('a').props().href).toEqual(props.linkUrl)
  })
})
