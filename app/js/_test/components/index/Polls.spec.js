import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import PollItem from '../../../components/index/PollItem'
import Polls from '../../../components/index/Polls'
import pollResponse from '../../testHelpers/pollResponse'
import { times } from 'lodash'

function setup(numToRender = 1, fullRendering = false) {
  const props = {
    polls: times(numToRender,(i) => pollResponse(i+1))
  }

  let output
  if(!fullRendering) {
    output = shallow(<Polls {...props } />)
  } else {
    output = mount(<Polls {...props} />)
  }

  return {
    props,
    output
  }
}

describe('components/index/Polls',() => {
  it('renders the correct number of PollItems', () => {
    const { output, props } = setup()
    expect(output.find(PollItem).length).toEqual(props.polls.length)
  })
})
