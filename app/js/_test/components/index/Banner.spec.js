import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import bannerResponse from '../../testHelpers/bannerResponse'
import Banner from '../../../components/index/Banner'
import BannerItem from '../../../components/index/BannerItem'

function setup(containerWidth = "Fit", numOfItems = 1,isInitialLoad,fullRendering = false) {
  const props = Object.assign({},bannerResponse(1,1,containerWidth,numOfItems),{ isInitialLoad: false })

  let output
  if(!fullRendering) {
    output = shallow(<Banner {...props } />)
  } else {
    output = mount(<Banner {...props } />)
  }

  return {
    output,
    props
  }
}

describe('components/index/Banner',() => {
  context('when the banner has one item', () => {
    it('renders the correct number of BannerItems', () => {
      const { output } = setup()
      expect(output.find(BannerItem).length).toEqual(1)
    })

    it('renders the banner with the fit class', () => {
      const { output } = setup()
      expect(output.find('.banner-full-container').length).toEqual(1)
    })
  })

  context('when the banner has two items', () => {
    it('renders the correct number of BannerItems', () => {
      const { output } = setup("Fit", 2)
      expect(output.find(BannerItem).length).toEqual(2)
    })

    it('renders BannerItem with the correct props', () => {
      const { output } = setup("Fit",2)
      expect(output.find(BannerItem).first().props().isLast).toEqual(false)
    })

    it('renders the second BannerItem with the correct props', () => {
      const { output } = setup("Fit",2)
      expect(output.find(BannerItem).last().props().isLast).toEqual(true)
    })

    it('renders the banner with the banner-fit-container class', () => {
      const { output } = setup("Fit",2)
      expect(output.find('.banner-fit-container').length).toEqual(1)
    })
  })

})
