import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import LocaleDropdownContainer from '../../../containers/shared/header/LocaleDropdownContainer'
import ProfileDropdownContainer from '../../../containers/shared/header/ProfileDropdownContainer'
import TopNav from '../../../components/shared/TopNav'
import i18n from '../../../i18n/index'
import flatten from 'flat'

function setup(loggedIn = false,locale='en',fullRendering = false) {
  let props
  const messages = flatten(i18n)
  if(loggedIn) {
    props = {
      isLoggedIn: true,
      projectMessageId: 'en.topnav.projects',
      contentMessageId: 'en.topnav.contents',
      eventMessageId: 'en.topnav.events',
      loginMessageId: 'en.topnav.login',
      signupMessageId: 'en.topnav.signup',
      pollMessageId: 'en.topnav.polls',
      topNavOpen: false,
      actions: {
        homeClick: expect.createSpy(),
        onToggle: expect.createSpy(),
        signUpClick: expect.createSpy(),
        loginClick: expect.createSpy(),
        projectClick: expect.createSpy(),
        contentClick: expect.createSpy(),
        pollClick: expect.createSpy(),
        eventClick: expect.createSpy(),
        faqClick: expect.createSpy()
      }
    }
  } else {
    props = {
      isLoggedIn: false,
      locale: locale,
      projectMessageId: 'en.topnav.projects',
      contentMessageId: 'en.topnav.contents',
      eventMessageId: 'en.topnav.events',
      loginMessageId: 'en.topnav.login',
      signupMessageId: 'en.topnav.signup',
      pollMessageId: 'en.topnav.polls',
      topNavOpen: false,
      actions: {
        homeClick: expect.createSpy(),
        onToggle: expect.createSpy(),
        signUpClick: expect.createSpy(),
        loginClick: expect.createSpy(),
        projectClick: expect.createSpy(),
        contentClick: expect.createSpy(),
        pollClick: expect.createSpy(),
        eventClick: expect.createSpy(),
        faqClick: expect.createSpy()
      }
    }
  }
  let output
  if(!fullRendering) {
    output = shallow(<TopNav {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><TopNav {...props} /></IntlProvider>)
  }
  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('shared/TopNav', () => {
    it('should call homeClick when the logo is clicked', () => {
      const { output, props } = setup()
      output.find('.logo-container').simulate('click')

      expect(props.actions.homeClick).toHaveBeenCalled()
    })

    it('should call signUpClick when the sign up link is clicked', () => {
      const { output, props } = setup()
      output.find('#top-nav-signup-link').simulate('click')

      expect(props.actions.signUpClick).toHaveBeenCalled()
    })

    it('should call projectClick when the project link is clicked', () => {
      const { output, props } = setup()
      output.find('#top-nav-project-link').simulate('click')

      expect(props.actions.projectClick).toHaveBeenCalled()
    })

    it('should call contentClick when the content link is clicked', () => {
      const { output, props } = setup()
      output.find('#top-nav-content-link').simulate('click')

      expect(props.actions.contentClick).toHaveBeenCalled()
    })

    it('should call faqClick when the faq link is clicked', () => {
      const { output, props } = setup()
      output.find('#top-nav-faq-link').simulate('click')

      expect(props.actions.faqClick).toHaveBeenCalled()
    })

    it('should call loginClick when the login link is clicked', () => {
      const { output, props } = setup()
      output.find('#top-nav-login-link').simulate('click')

      expect(props.actions.loginClick).toHaveBeenCalled()
    })

    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('header')
    })

    describe('children', () => {
      it('should render LocaleDropdownContainer', () => {
        const { output } = setup()
        expect(output.find(LocaleDropdownContainer).length).toEqual(1)
      })

      it('should render logo-container', () => {
        const { output } = setup()
        expect(output.find('.logo-container').length).toEqual(1)
      })

      context('when the user is logged in', () => {
        it('should render ProfileDropdownContainer', () => {
          const { output } = setup(true)
          expect(output.find(ProfileDropdownContainer).length).toEqual(1)
        })
      })

      context('when the user isn\'t logged in', () => {
        it('should render a Login link', () => {
          const { output } = setup(false)
          expect(output.find('NavItem').at(6).props().href).toEqual('/login.do')
        })
        it('should render a Signup link', () => {
          const { output } = setup(false)
          expect(output.find('NavItem').at(5).props().href).toEqual('/account/signup.do')
        })
      })
    })
  })
})

