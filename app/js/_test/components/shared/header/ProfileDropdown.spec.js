import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import ProfileDropdown from '../../../../components/shared/header/ProfileDropdown'
import i18n from '../../../../i18n/index'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'

function setup(user = true,fullRendering = false) {
  let props = {
    nickName: 'IAN',
    profileImageUrl: '/file/user_pic/145441057410493.jpg',
    email: 'mxion9522@gmail.com',
    actions: {
      profileClick: expect.createSpy(),
      fundingListClick: expect.createSpy(),
      logoutClick: expect.createSpy()
    },
    projectsMessageId: 'en.topnav.projects',
    profileMessageId: 'en.topnav.profile',
    logoutMessageId: 'en.topnav.logout'
  }

  const messages = flatten(i18n)
  let output
  if(!fullRendering) {
    output = shallow(<ProfileDropdown {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={ messages }><ProfileDropdown {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('shared/header/ProfileDropdown', () => {
    context('when a user is logged in', () => {
      it('should render NavProfileDropdown', () => {
        const { output } = setup(true)
        expect(output.find('NavProfileDropdown').length).toEqual(1)
      })

      it('calls fundingListClick when the funding list link is clicked',() => {
        const { output, props } = setup(true)
        output.find('#top-nav-profile-dropdown-funding-link').simulate('click')
        expect(props.actions.fundingListClick).toHaveBeenCalled()
      })

      it('calls profileClick when the profile link is clicked',() => {
        const { output, props } = setup(true)
        output.find('#top-nav-profile-dropdown-profile-link').simulate('click')
        expect(props.actions.profileClick).toHaveBeenCalled()
      })

      it('calls logoutClick when the log out link is clicked',() => {
        const { props, output } = setup(true)
        output.find('#top-nav-profile-dropdown-logout-link').simulate('click')
        expect(props.actions.logoutClick).toHaveBeenCalled()
      })

      it('should render the user\'s nickname', () => {
        const { output, props } = setup(true)
        expect(output.props().title).toBe(' '+props.nickName)
      })

      it('should render the correct message for my projects', () => {
        const { output, props } = setup(true,true)
        expect(output.find('MenuItem').first().text()).toEqual('Projects')
      })

      it('should render the correct message for profile', () => {
        const { output, props } = setup(true,true)
        expect(output.find('MenuItem').at(1).text()).toEqual('Profile')
      })

      it('should render the correct message for Logout', () => {
        const { output, props } = setup(true,true)
        expect(output.find('MenuItem').last().text()).toEqual('Log Out')
      })
    })
  })
})
