import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import LocaleDropdown from '../../../../components/shared/header/LocaleDropdown'
import { IntlProvider } from 'react-intl'
import { MenuItem } from 'react-bootstrap'

function setup(locale='ko',fullRendering = false) {
  let props = {
    locale,
    localeDisplayName: '한국어',
    localeChange: expect.createSpy()
  }

  let output
  if(!fullRendering) {
    output = shallow(<LocaleDropdown {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={{}} ><LocaleDropdown {...props} /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/header/LocaleDropdown', () => {
    it('should render NavDropdown', () => {
      const { output } = setup()
      expect(output.find('NavDropdown').length).toEqual(1)
    })

    it('should set the locale to the one matching the session', () => {
      const { output, props } = setup()
      expect(output.find('NavDropdown').props().title).toBe('한국어')
    })

    context('when the locale is changed',() => {
      it('should call localeChange', () => {
        const { output, props } = setup('ko',true)
        output.find('#locale-select-ko').simulate('click')
        expect(props.localeChange).toHaveBeenCalled()
      })
    })
  })
})
