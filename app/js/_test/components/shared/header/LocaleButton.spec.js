import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import LocaleButton from '../../../../components/shared/header/LocaleButton'
import { IntlProvider } from 'react-intl'

function setup(locale='ko',fullRendering = false) {
  let props = {
    locale,
    localeChange: expect.createSpy()
  }

  let output
  if(!fullRendering) {
    output = shallow(<LocaleButton {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={{}} ><LocaleButton {...props} /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/header/LocaleButton', () => {
    it('should render ButtonToolbar', () => {
      const { output } = setup()
      expect(output.find('ButtonToolbar').length).toEqual(1)
    })

    it('should render ButtonGroup', () => {
      const { output } = setup()
      expect(output.find('ButtonGroup').length).toEqual(1)
    })

    context('when the locale is changed',() => {
      it('should call localeChange', () => {
        const { output, props } = setup('ko',true)
        output.find('Button').first().simulate('click')
        expect(props.localeChange).toHaveBeenCalled()
      })
    })
  })
})
