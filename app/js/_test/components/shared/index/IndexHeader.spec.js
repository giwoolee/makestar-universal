import expect from 'expect'
import React from 'react'
import { IntlProvider } from 'react-intl'
import Header from '../../../../components/shared/index/IndexHeader'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import i18n from '../../../../i18n/index'

function setup(fullRendering=false,section='poll') {

  const messages = flatten(i18n)
  let output
  const props = {
    titleMessageId: 'en.poll.title',
    subtitleMessageId: 'en.poll.subtitle'
  }
  if(!fullRendering) {
    output = shallow(<Header { ...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Header { ...props } /></IntlProvider>)
  }

  return {
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/index/Header', () => {
    it('should have the correct type', () => {
      const { output } = setup()

      expect(output.type()).toBe('div')
    })

    describe('children', () => {
      it('should render the title', () => {
        const { output, messages } = setup(true)

        expect(output.find('.index-header-title').text()).toBe(messages['en.poll.title'])
      })

      it('should the subtitle', () => {
        const { output, messages } = setup(true)

        expect(output.find('.index-header-subtitle').text()).toEqual(messages['en.poll.subtitle'])
      })
    })
  })
})
