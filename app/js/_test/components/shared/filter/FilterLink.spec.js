import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { Link } from 'react-router'
import FilterLink from '../../../../components/shared/filter/FilterLink'
import i18n from '../../../../i18n/index'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'

function setup(selected = true, isLast = false, fullRendering = false) {
  let props = {
    queryString: '/polls?status=All',
    selected: selected,
    filter: 'All',
    isLast,
    messageId: 'poll.filterLink',
    gaData: {
      category: 'Poll',
      action: 'Navigate',
      label: 'All'
    },
    actions: {
      gaEvent: expect.createSpy()
    }
  }

  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<FilterLink {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><FilterLink {...props} /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/index/FilterLink', () =>{
    it('should render correctly', () => {
      const { output } = setup()
      expect(output.type()).toBe(Link)
    })

    it('calls gaEvent with the correct arguments', () => {
      const { output, props } = setup()
      output.find('Link').simulate('click')
      expect(props.actions.gaEvent).toHaveBeenCalledWith(props.gaData)
    })

    it('should have the correct classname when selected is true', () => {
      const { output, renderer } = setup()
      expect(output.hasClass('filter-link filter-link-selected')).toBe(true)
    })

    it('should have the correct classname when selected is false', () => {
      const { output } = setup(false)
      expect(output.hasClass('filter-link')).toBe(true)
    })

    it('should have the correct border style when isLast is false', () => {
      const { output } = setup(false,false)
      expect(output.hasClass('filter-link')).toBe(true)
    })

    it('should render the correct link text', () => {
      const { output } = setup(true,false,true)
      expect(output.text()).toBe('All')
    })

    it('should have the correct border style when isLast is true', () => {
      const { output } = setup(false,true)
      expect(output.hasClass('filter-link filter-last-link')).toBe(true)
    })
  })
})
