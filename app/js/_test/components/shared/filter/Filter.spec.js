import expect from 'expect'
import React from 'react'
import Filter from '../../../../components/shared/filter/Filter'
import flatten from 'flat'
import { shallow, mount } from 'enzyme'
import i18n from '../../../../i18n/index'
import { IntlProvider } from 'react-intl'
import { last } from 'lodash'

function setup(fullRendering = false) {
  const props = {
    messageId: 'poll.filterLink',
    filters: [
      {
        key: 'All',
        queryString: '/polls?status=All',
        selected: false
      },
      {
        key: 'Open',
        queryString: '/polls?status=Open',
        selected: true
      },
      {
        key: 'Closed',
        queryString: '/polls?status=Closed',
        selected: false
      }
    ]
  }

  const messages = flatten(i18n.en)
  let output

  if(!fullRendering) {
    output = shallow(<Filter { ...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Filter {...props } /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/filter/Filter', () => {
    it('should render the correct number of filters', () => {
      const { output, props } = setup()

      expect(output.find('FilterLink').length).toEqual(3)
    })
  })
})
