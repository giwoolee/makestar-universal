import expect from 'expect'
import React from 'react'
import { shallow, render } from 'enzyme'
import StatusModal from '../../../components/shared/StatusModal'
import { Modal } from 'react-bootstrap'
import { IntlProvider } from 'react-intl'
import i18n from '../../../i18n/index'
import flatten from 'flat'

function setup(show,fullRendering = false) {
  let props = {
    show,
    messages: {
      titleMessageId: 'common.loginFirst',
      bodyMessageId: 'poll.loginPrompt'
    },
    links: {
      primary: 'link'
    },
    actions: expect.createSpy()
  }

  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<StatusModal {...props } />)
  } else {
    output = render(<IntlProvider locale="en" messages={messages}><StatusModal {...props} /></IntlProvider>)
  }
  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/StatusModal', () => {

    context('when show is true', () => {
      it('should have show set to true', () => {
        const { output, props } = setup(true)
        expect(output.find('Modal').props().show).toEqual(true)
      })
    })

    context('when show is false', () => {
      it('should have show set to false', () => {
        const { output, props } = setup(false)
        expect(output.find('Modal').props().show).toEqual(false)
      })
    })

    it('calls actions when the close button is clicked', () => {
      const { output, props } = setup(true)
      output.find('Button').simulate('click')
      expect(props.actions).toHaveBeenCalled()
    })
  })
})
