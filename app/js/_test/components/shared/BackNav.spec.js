import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import BackNav from '../../../components/shared/BackNav'
import i18n from '../../../i18n/index'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'

function setup (messageId ='poll.backNav', to = '/polls?status=Open', fullRendering = false) {
  const props = {
    messageId,
    to
  }

  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<BackNav {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><BackNav {...props} /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components/shared/BackNav', () => {
  it('renders the correct message', () => {
    const { output } = setup('poll.backNav','/polls?status=Open',true)
    expect(output.text()).toBe('To poll list >')
  })

  it('renders the correct link', () => {
    const { output, props } = setup()

    expect(output.find('Link').props().to).toEqual(props.to)
  })
})
