import expect from 'expect'
import moment from 'moment'
import React from 'react'
import { shallow } from 'enzyme'
import { FormattedMessage, FormattedRelative } from 'react-intl'
import DateInfo from '../../../../components/shared/comment/DateInfo'

function setup(dateMessageId,date) {
  const props = {
    dateMessageId,
    date
  }

  const output = shallow(<DateInfo { ...props } />)

  return {
    props,
    output
  }
}

describe('containers/shared/comment/DateInfo', () => {
  it('renders FormattedMessage with the correct props', () => {
    const { output, props } = setup('en.comment.postedOn',moment().subtract(1,'days').toDate())

    expect(output.find(FormattedMessage).props().id).toEqual(props.dateMessageId)
  })
})
