import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import Comments from '../../../../components/shared/comment/Comments'
import CommentContainer from '../../../../containers/shared/comment/CommentContainer'
import TextInputContainer from '../../../../containers/shared/comment/TextInputContainer'
import DeletePromptContainer from '../../../../containers/shared/comment/DeletePromptContainer'
import flatten from 'flat'
import { times } from 'lodash'
import i18n from '../../../../i18n/index'

function setup(numberOfComments = 1, isLast = true, nextPage = null, fullRendering = false) {

  const props = {
    idx: 1,
    idxs: times(numberOfComments,(i) => { return i + 1 }),
    isLast,
    nextPage,
    actions: {
      fetch: expect.createSpy()
    },
    moreMessage: 'en.common.more'
  }

  const messages = flatten(i18n)
  let output
  if(!fullRendering) {
    output = shallow(<Comments {...props }/>)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Comments {...props}/></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/comment/Comments', () => {
    it('renders DeletePromptContainer with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(DeletePromptContainer).first().props()).toEqual({ type: 'comments' })
      expect(output.find(DeletePromptContainer).last().props()).toEqual({ type: 'replies' })
    })

    context('when there are more comments to paginate through', () => {
      it('renders the more button', () => {
        const { output, props } = setup(1,false,{ pathname: '/polls/1/comments', query: { page: 2 } })

        expect(output.find('button').length).toEqual(1)
      })

      it('calls fetch with the correct arguments', () => {
        const { output, props } = setup(1,false,{ pathname: '/polls/1/comments', query: { page: 2 } })

        output.find('button').simulate('click')
        expect(props.actions.fetch).toHaveBeenCalledWith(props.nextPage)
      })
    })
    context('when there are no comments to paginate through', () => {
      it('renders Comment with the correct props', () => {
        const { output, props } = setup()

        expect(output.find(CommentContainer).first().props()).toEqual({
          idx: 1,
          type: 'comments'
        })
      })

      it('renders Comment the correct number of times', () => {
        const { output, props } = setup(2)
        expect(output.find(CommentContainer).length).toBe(2)
      })

      it('renders TextInput with the correct props', () => {
        const { output, props } = setup()

        expect(output.find(TextInputContainer).props()).toEqual({
          idx: 1,
          type: 'comments'
        })
      })
    })
  })
})
