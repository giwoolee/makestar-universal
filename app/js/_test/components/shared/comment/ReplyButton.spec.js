import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import ReplyButton from '../../../../components/shared/comment/ReplyButton'

function setup(isReplyOpen,isLoggedIn) {
  const props = {
    idx: 1,
    replyMessageId: 'en.comment.reply',
    isReplyOpen,
    isLoggedIn,
    actions: {
      promptLogin: expect.createSpy(),
      openReply: expect.createSpy()
    }
  }

  const output = shallow(<ReplyButton {...props }/>)
  return {
    output,
    props
  }
}

describe('components/shared/comment/ReplyButton', () => {
  context('when isLoggedIn is true', () => {
    context('when isReplyOpen is false', () => {
      it('calls openReply', () => {
        const { output, props } = setup(false,true)
        output.find('.comment-display-reply-btn').simulate('click')
        expect(props.actions.openReply).toHaveBeenCalledWith(props.idx)
      })
    })

    context('when isReplyOpen is true', () => {
      it('doesn\'t call openReply', () => {
        const { output, props } = setup(true,true)
        output.find('.comment-display-reply-btn').simulate('click')
        expect(props.actions.openReply).toNotHaveBeenCalled()
      })

      it('adds the disabled class', () => {
        const { output, props } = setup(true,true)
        expect(output.find('.comment-display-reply-btn').props().className).toEqual('btn btn-default btn-xs comment-display-reply-btn disabled')
      })
    })
  })

  context('when isLoggedIn is false', () => {
    it('calls promptLogin', () => {
      const { output, props } = setup(false,false)
      output.find('.comment-display-reply-btn').simulate('click')
      expect(props.actions.promptLogin).toHaveBeenCalled()
    })
  })
})

