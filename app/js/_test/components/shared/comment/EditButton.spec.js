import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import EditButton from '../../../../components/shared/comment/EditButton'
import { FormattedMessage } from 'react-intl'

function setup(isDisabled, isVisible) {
  const props = {
    idx: 1,
    isDisabled,
    isVisible,
    editMessageId: 'en.comment.edit',
    cancelMessageId: 'en.common.cancel',
    type: 'comments',
    actions: {
      editOpen: expect.createSpy()
    }
  }

  const output = shallow(<EditButton { ...props } />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/comment/EditButton', () => {
    context('when isVisble is true', () => {
      context('when isDisabled is false', () => {
        it('calls editOpen with the correct props', () => {
          const { output, props } = setup(false, true)
          output.find('.comment-display-edit-btn').simulate('click')
          expect(props.actions.editOpen).toHaveBeenCalledWith()
        })

        it('renders FormattedMessage with the correct props', () => {
          const { output, props } = setup(false, true)
          expect(output.find(FormattedMessage).props()).toEqual({ id: props.editMessageId, values: {}, tagName: 'span' })
        })

        it('renders the pencil icon', () => {
          const { output, props } = setup(false, true)
          expect(output.find('.fa-pencil').length).toEqual(1)
        })
      })

      context('when isDisabled is true', () => {
        it('renders the cancel button', () => {
          const { output, props } = setup(true, true)
          expect(output.find('.comment-display-edit-btn').length).toEqual(1)
        })

        it('renders the times icon', () => {
          const { output, props } = setup(true, true)
          expect(output.find('.fa-times').length).toEqual(1)
        })

        it('renders FormattedMessage with the correct props', () => {
          const { output, props } = setup(true, true)
          expect(output.find(FormattedMessage).props()).toEqual({ id: props.cancelMessageId, values: {}, tagName: 'span' })
        })
      })
    })

    context('when isVisble is false', () => {
      it('doens\'t render the button', () => {
        const { output, props } = setup(false, false)
        expect(output.find('button').length).toEqual(0)
      })
    })
  })
})
