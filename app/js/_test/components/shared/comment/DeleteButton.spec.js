import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import DeleteButton from '../../../../components/shared/comment/DeleteButton'

function setup(isDisabled,isPromptShow,isVisible) {
  const props = {
    idx: 1,
    isDisabled,
    deleteMessageId: 'en.comment.delete',
    type: 'comments',
    isVisible,
    actions: {
      toggleDeletePrompt: expect.createSpy()
    }
  }

  const output = shallow(<DeleteButton { ...props } />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/comment/DeleteButton', () => {
    context('when isVisible is false', () => {
      it('doesn\'t render a button', () => {
        const { output, props } = setup(false,false,false)
        expect(output.find('button').length).toEqual(0)
      })
    })
    context('when isDisabled is false', () => {
      it('calls del with the correct props', () => {
        const { output, props } = setup(false,false,true)
        output.find('.comment-display-delete-btn').simulate('click')
        expect(props.actions.toggleDeletePrompt).toHaveBeenCalledWith({ idx: props.idx, show: props.isDisabled, type: props.type })
      })
    })

    context('when isDisabled is true', () => {
      it('renders the button with the disabled class', () => {
        const { output, props } = setup(true,true,true)
        expect(output.find('.disabled').length).toEqual(1)
      })
    })
  })
})

