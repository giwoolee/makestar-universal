import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Comment from '../../../../components/shared/comment/Comment'
import { fill } from 'lodash'
import flatten from 'flat'
import { IntlProvider, FormattedMessage } from 'react-intl'
import RepliesContainer from '../../../../containers/shared/comment/RepliesContainer'
import DeleteButtonContainer from '../../../../containers/shared/comment/DeleteButtonContainer'
import ReplyButtonContainer from '../../../../containers/shared/comment/ReplyButtonContainer'
import EditButtonContainer from '../../../../containers/shared/comment/EditButtonContainer'
import EditTextInputContainer from '../../../../containers/shared/comment/EditTextInputContainer'
import TextDisplayContainer from '../../../../containers/shared/comment/TextDisplayContainer'
import DateInfoContainer from '../../../../containers/shared/comment/DateInfoContainer'
import i18n from '../../../../i18n/index'

function setup (badgeImageUrl = null, isEditOpen = false, fullRendering = false) {
  let props = {
    idx: 1,
    userIdx: 2,
    createdDate: "2016-02-24T12:53:51+0000",
    updatedDate: "2016-02-24T12:53:51+0000",
    userName: "IAN",
    profileImageUrl: "/file/profile/145441057410493.jpg",
    badgeImageUrl,
    status: "Visible",
    parentIdx: null,
    replies: [],
    text: "댓글2",
    isEditOpen,
    commentDeletedMessageId: 'en.comment.deleted',
    actions: {
      openReply: expect.createSpy(),
      closeReply: expect.createSpy(),
      promptLogin: expect.createSpy()
    },
    type: 'comments'
  }

  const messages = flatten(i18n)
  let output
  if(!fullRendering) {
    output = shallow(<Comment {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Comment {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('shared/comment/Comment', () => {

    it('renders TextDisplayContainer with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(TextDisplayContainer).props()).toEqual({
        idx: props.idx,
        type: props.type
      })
    })

    it('renders RepliesContainer', () => {
      const { output, props } = setup()
      expect(output.find(RepliesContainer).length).toEqual(1)
    })

    context('when isEditOpen is false', () => {
      it('renders the text', () => {
        const { output, props } = setup()
        expect(output.find(TextDisplayContainer).length).toEqual(1)
      })
    })

    context('when isEditOpen is true', () => {
      it('renders the EditTextInputContainer with the correct props', () => {
        const { output, props } = setup(null,true)
        expect(output.find(EditTextInputContainer).props()).toEqual({ type: 'comments', idx: props.idx  })
      })
    })

    it('renders DeleteButtonContainer with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(DeleteButtonContainer).props()).toEqual({ idx: props.idx, type: 'comments' })
    })

    it('renders ReplyButtonContainer with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(ReplyButtonContainer).props()).toEqual({ idx: props.idx })
    })

    it('renders the user\'s name', () => {
      const { output, props } = setup()
      expect(output.find('.comment-display-username').text()).toBe(props.userName)
    })

    it('renders EditButtonContainer with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(EditButtonContainer).props()).toEqual({ idx: props.idx, type: props.type })
    })

    it('renders the user\'s profileImage', () => {
      const { output, props } = setup()
      expect(output.find('.comment-display-profile-image').props().src).toEqual(props.profileImageUrl)
    })

    context('when the user has a badge', () => {
      it('renders the user\'s badge', () => {
        const { output, props } = setup("badge.jpg")
        expect(output.find('.comment-display-profile-badge').props().src).toEqual(props.badgeImageUrl)
      })
    })

    context('when the user doesn\'t have a badge', () => {
      it('doesn\'t render the user\'s badge', () => {
        const { output, props } = setup(false,false)
        expect(output.find('.comment-display-profile-badge').length).toEqual(0)
      })
    })

    it('renders DateInfo with the correct props', () => {
      const { output, props } = setup()
      expect(output.find(DateInfoContainer).props()).toEqual({ idx: props.idx, type: props.type })
    })
  })
})

