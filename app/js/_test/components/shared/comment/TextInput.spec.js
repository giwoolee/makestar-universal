import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import TextInput from '../../../../components/shared/comment/TextInput'
import { fill } from 'lodash'
import i18n from '../../../../i18n/index'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'
import Promise from 'bluebird'

function setup (textLength, isTextOverLength, inputBtnProps, textAreaProps, fullRendering = false) {
  const props = {
    data: { text: 'text' },
    idx: 1,
    pathname: `/polls/1/comments`,
    isTextOverLength,
    textLength,
    leaveComment: "en.comment.leaveComment",
    textAreaProps,
    inputBtnProps,
    loginPromptMessageId: 'en.comment.loginPromptMessageId',
    postMessageId: 'en.comment.post',
    actions: {
      submit: expect.createSpy(),
      promptLogin: expect.createSpy(),
      updateText: expect.createSpy()
    }
  }

  const messages = flatten(i18n)
  let output
  if(!fullRendering) {
    output = shallow(<TextInput {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><TextInput {...props}/></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/comment/TextInput', () => {
    it('renders the length of text', () => {
      const inputBtnProps = { disabled: true }
      const textAreaProps = { disabled : false, value: "" }
      const { output, props } = setup(10,false,inputBtnProps,textAreaProps,true)
      expect(output.find('.comment-text-length-counter-count').text()).toEqual(' 10')
    })
    context('when text length is 0', () => {
      it('should disable the post button',() => {
        const inputBtnProps = { disabled: true }
        const textAreaProps = { disabled : false, value: "" }
        const { output, props } = setup(0,false,inputBtnProps,textAreaProps,true)
        expect(output.find('button').props().disabled).toBe(true)
      })
    })
    context('when text is shorter than 1000', () => {
      it('should not add red to the counter', () => {
        const inputBtnProps = { disabled: true }
        const textAreaProps = { disabled : false, value: "" }
        const { output, props } = setup(999,false,inputBtnProps,textAreaProps,true)
        expect(output.find('.comment-text-length-counter-count').hasClass('red')).toBe(false)
      })
    })
    context('when text is longer than 1000', () => {
      it('should disable the post button', () => {
        const inputBtnProps = { disabled: true  }
        const textAreaProps = { disabled : false, value: "" }
        const { output, props } = setup(1001,true,inputBtnProps,textAreaProps,true)
        expect(output.find('button').props().disabled).toBe(true)
      })
      it('should add .red to the counter', () => {
        const inputBtnProps = { disabled: true }
        const textAreaProps = { disabled : false, value: "" }
        const { output, props } = setup(1001,true,inputBtnProps,textAreaProps,true)
        expect(output.find('.comment-text-length-counter-count').hasClass('red')).toBe(true)
      })
    })
  })

  it('renders the correct text for the button', () => {
    const inputBtnProps = { disabled: true, message: "en.comment.post" }
    const textAreaProps = { disabled : false, value: "" }
    const { output } = setup(0,true,inputBtnProps,textAreaProps,true)
    expect(output.find('button').text()).toEqual('Post')
  })

  it('renders the correct text for the text box', () => {
    const inputBtnProps = { disabled: true }
    const textAreaProps = { disabled : false, value: "" }
    const { output } = setup(1,false,inputBtnProps,textAreaProps,true)
    expect(output.find('.comment-text-input-top-text').text()).toEqual('Leave a comment')
  })

  it('calls submit when the button is clicked', () => {
    const inputBtnProps = { disabled: false }
    const textAreaProps = { disabled : false, value: "" }
    const { output, props } = setup(1,false,inputBtnProps,textAreaProps,true)
    output.find('button').simulate('click')
    expect(props.actions.submit).toHaveBeenCalled()
  })

  it('calls updateText when the text value is changed', () => {
    const inputBtnProps = { disabled: false }
    const textAreaProps = { disabled : false, value: "" }
    const { output,props } = setup(1,false,inputBtnProps,textAreaProps,true)
    output.find('textarea').simulate('change')
    expect(props.actions.updateText).toHaveBeenCalled()
  })
})
