import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import { IntlProvider } from 'react-intl'
import Replies from '../../../../components/shared/comment/Replies'
import TextInputContainer from '../../../../containers/shared/comment/TextInputContainer'
import ReplyContainer from '../../../../containers/shared/comment/ReplyContainer'
import DeletePromptContainer from '../../../../containers/shared/comment/DeletePromptContainer'
import flatten from 'flat'
import { times } from 'lodash'
import i18n from '../../../../i18n/index'

function setup(numberOfReplies = 1, isLast = true, nextPage = null, isReplyOpen = false, isLoggedIn = false,fullRendering = false) {

  const props = {
    idx: 1,
    parentIdx: 1,
    idxs: times(numberOfReplies,(i) => { return i + 1 }),
    isLast,
    nextPage,
    isLoggedIn,
    isReplyOpen,
    actions: {
      fetch: expect.createSpy()
    },
    moreMessage: 'en.common.more'
  }

  const messages = flatten(i18n)
  let output
  if(!fullRendering) {
    output = shallow(<Replies {...props }/>)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages} ><Replies {...props } /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/comment/Replies', () => {

    context('when there are more replies to paginate through', () => {
      it('renders the load-relies-btn', () => {
        const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/replies', idx: 1, type: 'replies', query: { page: 2 } })

        expect(output.find('.load-replies-btn').length).toEqual(1)
      })

      it('calls fetch with the correct arguments', () => {
        const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/replies', idx: 1, type: 'replies', query: { page: 2 } })

        output.find('.load-replies-btn').simulate('click')
        expect(props.actions.fetch).toHaveBeenCalledWith(props.nextPage)
      })
    })
    context('when there are no replies to paginate through', () => {
      it('renders Comment with the correct props', () => {
        const { output, props } = setup()

        expect(output.find(ReplyContainer).first().props()).toEqual({
          idx: 1,
        })
      })

      it('renders Comment the correct number of times', () => {
        const { output, props } = setup(2)
        expect(output.find(ReplyContainer).length).toBe(2)
      })

      context('when isReplyOpen is true and isLoggedIn is true', () => {
        it('renders TextInputContainer', () => {
          const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/reply', query: { page: 2 } },true,true)
          expect(output.find(TextInputContainer).length).toEqual(1)
        })

        it('renders TextInputContainer with the correct props', () => {
          const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/reply', query: { page: 2 } },true,true)
          expect(output.find(TextInputContainer).props()).toEqual({
            idx: props.idx,
            type: 'replies'
          })
        })
      })

      context('when isReplyOpen is true and isLoggedIn is false', () => {
        it('doesn\'t render TextInputContainer', () => {
          const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/reply', query: { page: 2 } },true,false)
          expect(output.find(TextInputContainer).length).toEqual(0)
        })
      })

      context('when isReplyOpen is false, and isLogged in is false', () => {
        it('doesn\'t render TextInputContainer', () => {
          const { output, props } = setup(1,false,{ pathname: '/polls/1/comments/1/reply', query: { page: 2 } },false,false)
          expect(output.find(TextInputContainer).length).toEqual(0)
        })
      })
    })
  })
})
