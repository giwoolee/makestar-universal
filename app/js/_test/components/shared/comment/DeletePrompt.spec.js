import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import DeletePrompt from '../../../../components/shared/comment/DeletePrompt'
import { Modal } from 'react-bootstrap'

function setup(show = true, type = 'comments') {
  const props = {
    idx: 1,
    show,
    promptMessageId: 'en.comments.deletePrompt',
    confirmMessageId: 'en.common.confirm',
    cancelMessageId: 'en.common.cancel',
    type,
    pathname: '/polls/1/comments/1',
    actions: {
      confirm: expect.createSpy(),
      cancel: expect.createSpy()
    }
  }

  const output = shallow(<DeletePrompt { ...props } />)

  return {
    output,
    props
  }

}

describe('components/shared/comment/DeletePrompt', () => {
  context('when visible is true', () => {
    it('renders Modal with show = true', () => {
      const { output, props } = setup(true,'comments')

      expect(output.find(Modal).props().show).toEqual( true )
    })
  })

  context('when visible is false', () => {
    it('renders Modal with show = false', () => {
      const { output, props } = setup(false,'comments')

      expect(output.find(Modal).props().show).toEqual(false)
    })
  })

  it('calls del when the confirm button is clicked', () => {
    const { output, props } = setup(true,'comments')

    output.find('.comment-delete-prompt-delete-btn').simulate('click')
    expect(props.actions.confirm).toHaveBeenCalledWith()
  })

  it('calls cancel when the cancel button is clicked', () => {
    const { output, props } = setup(true,'comments')

    output.find('.comment-delete-prompt-cancel-btn').simulate('click')
    expect(props.actions.cancel).toHaveBeenCalled()
  })
})

