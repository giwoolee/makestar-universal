import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import { FormattedMessage } from 'react-intl'
import TextDisplay from '../../../../components/shared/comment/TextDisplay'

function setup(isVisible = true) {
  const props = {
    text: 'Text',
    isVisible,
    commentDeletedMessageId: 'en.comment.deleted'
  }

  const output = shallow(<TextDisplay { ...props } />)

  return {
    props,
    output
  }
}

describe('components/shared/comment/TextDisplay', () => {
  context('when isVisible is true', () => {
    it('renders comment-display-text', () => {
      const { output, props } = setup()
      expect(output.find('.comment-display-text').length).toEqual(1)
      expect(output.find('.comment-display-text').text()).toEqual(props.text)
    })
  })

  context('when isVisible is false', () => {
    it('renders FormattedMessage with the correct props', () => {
      const { output, props } = setup(false)
      expect(output.find(FormattedMessage).props()).toEqual({
        id: props.commentDeletedMessageId,
        values: {},
        tagName: 'span'
      })
    })
  })
})
