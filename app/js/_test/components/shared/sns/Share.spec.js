import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import Share from '../../../../components/shared/sns/Share'

function setup(fullRendering = false) {
  const props = {
    facebook: {
      onClick: expect.createSpy(),
      icon: 'share-facebook-icon',
      key: 'facebook'
    },
    google: {
      onClick: expect.createSpy(),
      icon: 'share-google-icon',
      key: 'google'
    }
  }

  const output = shallow(<Share {...props }/>)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/sns/Share', () => {
    it('should have the correct number of sns share links', () => {
      const { output } = setup()
      expect(output.find('.share-container').children().length).toEqual(2)
    })

    it('calls snsShare when an icon is clicked', () => {
      const { output, props } = setup('en')
      output.find('.share-facebook-icon').simulate('click')
      expect(props.facebook.onClick).toHaveBeenCalled()
    })
  })
})

