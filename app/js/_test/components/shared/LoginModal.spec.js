import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import LoginModal from '../../../components/shared/LoginModal'
import { Modal, Button } from 'react-bootstrap'
import { IntlProvider } from 'react-intl'
import i18n from '../../../i18n/index'
import flatten from 'flat'

function setup(show,fullRendering = true) {
  let props = {
    show,
    titleMessageId: 'en.common.loginFirst',
    bodyMessageId: 'en.login.prompt',
    signUpMessageId: 'en.login.signUp',
    loginMessageId: 'en.login.login',
    failedMessageId: 'en.login.failed',
    closeMessageId: 'en.common.close',
    emailMessageId: 'en.login.email',
    passwordMessageId: 'en.login.password',
    type: 'session',
    failed: true,
    links: {
      primary: '/login.do',
      secondary: '/signup.do'
    },
    actions: {
      login: expect.createSpy(),
      resetStatus: expect.createSpy()
    }
  }

  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<LoginModal {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><LoginModal {...props } /></IntlProvider>)
  }
  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/LoginModal',() => {

    context('when show is true', () => {
      it('should have show set to true', () => {
        const { output, props } = setup(true,true)
        expect(output.find(Modal).props().show).toEqual(true)
      })
    })
  })
})
