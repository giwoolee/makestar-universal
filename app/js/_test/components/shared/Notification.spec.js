import React from 'react'
import { IntlProvider } from 'react-intl'
import { shallow, mount } from 'enzyme'
import expect from 'expect'
import Notification from '../../../components/shared/Notification'
import i18n from '../../../i18n/index'
import flatten from 'flat'

function setup(show = true,messageId='login.success',type='info',fullRendering = true) {
  let props = {
    show,
    type,
    messageId: 'login.success',
    actions: {
      resetNotification: expect.createSpy()
    }
  }

  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<Notification {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Notification { ...props } /></IntlProvider>)
  }

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/Notification', () => {
    context('when show is true', () => {
      context('when type is alert', () => {
        it('sets the class to alert', () => {
          const { output } = setup(true,'poll.voteClosed','alert')
          expect(output.find('.notification').hasClass('notification-alert')).toEqual(true)
        })
      })

      context('when type is info', () => {
        it('sets the class the info', () => {
          const { output } = setup(true,'login.success','info')
          expect(output.find('.notification').hasClass('notification-info')).toEqual(true)
        })
      })

      it('should have show set to true', () => {
        const { output } = setup()
        expect(output.find(Notification).props().show).toEqual(true)
      })

      it('should display the correct message', () => {
        const { output } = setup()
        expect(output.find('.notification-message').text()).toEqual('Login success!')
      })

      it('should call resetNotification', () => {
        const { output, props } = setup()
        output.find('.notification-close-notification-btn').simulate('click')
        expect(props.actions.resetNotification).toHaveBeenCalled()
      })
    })
  })
})

