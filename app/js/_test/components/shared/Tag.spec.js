import expect from 'expect'
import React from 'react'
import * as _ from 'lodash'
import Tag from '../../../components/shared/Tag'
import { shallow } from 'enzyme'

function setup(numOfTags,className = 'poll-index-container') {
  let tags = _.fill(Array(numOfTags),'tag')

  let props = {
    tags,
    className
  }

  const output = shallow(<Tag {...props } />)
  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/index/Tag', () => {
    it('has the correct classname', () => {
      const { output } = setup(3,'content-index-content-tags-container')
      expect(output.find('.content-index-content-tags-container').length).toEqual(1)
    })
    context('when props.tags\'s length is 3', () => {
      it('should render tags as comma seperated text', () => {
        const { output } = setup(3)
        expect(output.text()).toBe('tag,tag,tag')
      })
    })
  })
})
