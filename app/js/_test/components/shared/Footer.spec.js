import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Footer from '../../../components/shared/Footer'
import i18n from '../../../i18n/index'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'

function setup(fullRendering = false) {
  let props = {
    locale: 'en'
  }
  const messages = flatten(i18n.en)
  let output
  if(!fullRendering) {
    output = shallow(<Footer {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Footer {...props} /></IntlProvider>)
  }
  return {
    props,
    output
  }
}

describe('components', () => {
  describe('shared/Footer', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('footer')
    })

    describe('children', () => {
      it('should render address', () => {
        const { output } = setup()
        expect(output.find('.address').length).toEqual(1)
      })
    })
  })
})
