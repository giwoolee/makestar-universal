import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import NavProfileDropdown from '../../../components/bootstrap/NavProfileDropdown'
import { Dropdown } from 'react-bootstrap'

function setup () {
  let props = {
    img: '/path/to/img',
    title: 'Title',
    noCaret: false
  }

  const output = shallow(<NavProfileDropdown {...props } />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('bootstrap/NavProfileDropdown', () => {
    it('should render img',() => {
      const { output } = setup()
      expect(output.find('img')).toExist()
    })

    it('should render Dropdown.Toggle',() => {
      const { output } = setup()
      expect(output.find(Dropdown.Toggle).length).toEqual(1)
    })
  })
})
