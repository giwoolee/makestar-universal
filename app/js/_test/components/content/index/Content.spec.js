import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Content from '../../../../components/content/index/Content'
import { IntlProvider } from 'react-intl'
import contentResponse from '../../../testHelpers/contentsResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(isInitialLoad = false,fullRendering = false) {
  let props = {
    ...contentResponse(),
    isInitialLoad,
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<Content {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Content {...props} /></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/index/Content', () => {
  it('renders the main image', () => {
    const { output, props } = setup()
    expect(output.find('img').props().src).toEqual(props.imageUrl)
  })

  it('renders the subject', () => {
    const { output, props } = setup()
    expect(output.find('.content-index-content-info-subject-text').text()).toEqual(props.subject)
  })

  it('renders the description', () => {
    const { output, props } = setup()
    expect(output.find('.content-index-content-info-description-text').text()).toEqual(props.description)
  })

  it('renders the author name', () => {
    const { output, props: { author } } = setup()
    expect(output.find('.content-index-content-info-author-text').text()).toEqual(`by ${author.name}`)
  })

  it('renders the tags', () => {
    const { output } = setup()
    expect(output.find('Tag').length).toEqual(1)
  })

  it('renders the date in the correct format', () => {
    const { output } = setup()
    expect(output.find('.content-index-content-info-date-text').text()).toEqual('2016/03/01')
  })

  it('renders Link with the correct props', () => {
    const { output, props } = setup()
    expect(output.find('Link').props().to).toEqual('/contents/articles/1')
  })
})

