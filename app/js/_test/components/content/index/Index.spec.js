import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Index from '../../../../components/content/index/Index'
import { IntlProvider } from 'react-intl'
import { fill } from 'lodash'
import contentResponse from '../../../testHelpers/contentsResponse'
import ContentContainer from '../../../../containers/content/index/ContentContainer'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(isLast = false, isFetching = false, isInitialLoad = false, fullRendering = false) {

  let props = {
    idxs: [1,2],
    isLast,
    pathname: '/contents/articles',
    query: { page: 2 },
    isFetching,
    isInitialLoad,
    actions: {
      fetchContents: expect.createSpy()
    }
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<Index {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Index {...props} /></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/index/Index', () => {
  it('renders the correct number of ContentContainer components', () => {
    const { output } = setup(false)
    expect(output.find(ContentContainer).length).toEqual(2)
  })

  it('renders ContentContainer with the correct props', () => {
    const { output } = setup(false)
    expect(output.find(ContentContainer).first().props()).toEqual({ idx: 1 })
    expect(output.find(ContentContainer).last().props()).toEqual({ idx: 2 })
  })

  it('calls fetchContents when then more button is clicked', () => {
    const { output, props } = setup(false)
    output.find('button').simulate('click')
    expect(props.actions.fetchContents).toHaveBeenCalledWith({ pathname: '/contents/articles', query: { page: 2 } })
  })

  it('renders the button if isLast is false and isInitialLoad is false', () => {
    const { output, props } = setup(false,false,false)
    expect(output.find('button').length).toEqual(1)
  })

  it('doesn\'t render the button if isLast is false and isInitialLoad is true', () => {
    const { output, props } = setup(false,false,true)
    expect(output.find('button').length).toEqual(0)
  })

  it('doesn\'t render the button if isLast is true and isInitialLoad is false', () => {
    const { output, props } = setup(true,true,false)

    expect(output.find('button').length).toEqual(0)
  })

  context('when isInitialLoad is true', () => {
    it('renders loader', () => {
      const { output, props } = setup(true,true,true)

      expect(output.find('.loader-lg').length).toEqual(1)
    })
  })
})
