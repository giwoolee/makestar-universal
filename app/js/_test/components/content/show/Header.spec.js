import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Header from '../../../../components/content/show/Header'
import { IntlProvider } from 'react-intl'
import contentResponse from '../../../testHelpers/contentsResponse'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(fullRendering = false) {
  let props = {
    subject: 'Bongos bring back bongo bongo',
    description: 'This is bongos indeed',
    author: { name: 'Bongos' },
    date: "2016-03-01T15:00:00+0000"
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<Header {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Header {...props}/></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/show/Header', () => {
  it('renders the subject with the correct text', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-header-subject-text').text()).toEqual(props.subject)
  })

  it('renders the description with the correct text', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-header-description-text').text()).toEqual(props.description)
  })

  it('renders the author with the correct text', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-header-author-text').text()).toEqual(`by ${props.author.name}`)
  })

  it('renders the date', () => {
    const { output, pros } = setup()
    expect(output.find('.content-show-header-date-text').text()).toEqual('2016/03/01')
  })
})
