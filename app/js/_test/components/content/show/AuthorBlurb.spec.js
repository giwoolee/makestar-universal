import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import AuthorBlurb from '../../../../components/content/show/AuthorBlurb'
import { IntlProvider } from 'react-intl'
import contentResponse from '../../../testHelpers/contentsResponse'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(fullRendering = false) {
  let props = {
    name: 'Bongos',
    description: 'Bongos mmmmm bongos',
    imageUrl: "https://image.com/test.jpg"
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<AuthorBlurb {...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><AuthorBlurb {...props} /></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/show/AuthorBlurb', () => {
  it('renders the author image with the correct props', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-authorblurb-image').props().src).toEqual(props.imageUrl)
  })

  it('renders the author name with the correct text', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-authorblurb-name-text').text()).toEqual(props.name)
  })

  it('renders the author description with the correct text', () => {
    const { output, props } = setup()
    expect(output.find('.content-show-authorblurb-description-text').text()).toEqual(props.description)
  })

})
