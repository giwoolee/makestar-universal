import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import ContentIFrame from '../../../../components/content/show/ContentIFrame'
import { IntlProvider } from 'react-intl'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(fullRendering = false) {
  let props = {
    src: 'http://makestar.co/contents/articles/1/text'
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<ContentIFrame {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><ContentIFrame {...props} /></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/show/ContentIFrame', () => {
  it('has the correct classname', () => {
    const { output, props } = setup()
    expect(output.find('iframe').hasClass('content-show-content-iframe')).toEqual(true)
  })

  it('has the class resizable-iframe', () => {
    const { output, props } = setup()
    expect(output.find('iframe').hasClass('resizable-iframe')).toEqual(true)
  })
})
