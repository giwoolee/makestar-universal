import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Show from '../../../../components/content/show/Show'
import { IntlProvider } from 'react-intl'
import contentResponse from '../../../testHelpers/contentsResponse'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(fullRendering = false) {
  const content = contentResponse()
  let props = {
    ...content
  }

  let output
  const messages = flatten(i18n.en)
  if(!fullRendering) {
    output = shallow(<Show {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Show {...props}/></IntlProvider>)
  }

  return {
    output,
    props,
    messages
  }
}

describe('content/show/Show',() => {
  it('renders the Header', () => {
    const { output, props } = setup()
    expect(output.find('Header').length).toEqual(1)
  })

  it('renders Header with the correct props',() => {
    const { output, props } = setup()
    expect(output.find('Header').props()).toEqual({ subject: props.subject, description: props.description, author: props.author, date: props.date })
  })

  it('renders ContentIFrame', () => {
    const { output } = setup()
    expect(output.find('ContentIFrame').length).toEqual(1)
  })

  it('renders ContentIFrame with the correct props', () => {
    const { output, props } = setup()
    expect(output.find('ContentIFrame').props()).toEqual({ src: props.textUrl })
  })

  it('renders Tag', () => {
    const { output } = setup()
    expect(output.find('Tag').length).toEqual(1)
  })

  it('renders AuthorBlurb', () => {
    const { output } = setup()
    expect(output.find('AuthorBlurb').length).toEqual(2)
  })

  it('renders AuthorBlurb with the correct props', () => {
    const { output, props } = setup()
    expect(output.find('AuthorBlurb').first().props()).toEqual(props.author)
    expect(output.find('AuthorBlurb').last().props()).toEqual(props.author)
  })

  it('renders BackNav with the correct props', () => {
    const { output, props } = setup()
    expect(output.find('BackNav').props()).toEqual({ to: '/contents/articles', messageId: 'content.backNav' })
  })
})
