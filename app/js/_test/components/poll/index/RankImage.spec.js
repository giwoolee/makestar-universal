import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import RankImage from '../../../../components/poll/index/RankImage'
import { IntlProvider } from 'react-intl'

function setup(numOfRanks,isInitialLoad=false,fullRendering = false) {
  const rank = {
    imageClass: 'col-xs-6 col-sm-6 col-md-6 col-lg-6 poll-rank-image-split-large',
    backgroundImage: "url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)",
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover'
  }

  let output
  if(!fullRendering) {
    output = shallow(<RankImage {...rank} />)
  } else {
    output = mount(<IntlProvider locale='en' messages={{}}><RankImage {...rank}/></IntlProvider>)
  }
  return {
    rank,
    output
  }
}

describe('components', () => {
  describe('poll/index/RankImage', () => {
    it('renders with the correct props', () => {
      const { output, rank } = setup()
      expect(output.find('div').props().style).toEqual({ backgroundImage: rank.backgroundImage, backgroundRepeat: rank.backgroundRepeat, backgroundPosition: rank.backgroundPosition, backgroundSize: rank.backgroundSize })
    })
  })
})
