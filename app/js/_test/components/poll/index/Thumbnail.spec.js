import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Thumbnail from '../../../../components/poll/index/Thumbnail'

function setup(isInitialLoad = false ) {

  const props  = {
    thumbnailUrl: 'url',
    isInitialLoad
  }

  const output = shallow(<Thumbnail {...props } />)
  return {
    output,
    props
  }
}

describe('components/poll/index/Thumbnail', () => {
  context('when isInitialLoad is false', () => {
    it('renders the placeholder', () => {
      const { output, props } = setup()

      expect(output.find('.poll-item-thumbnail-image').props().style.backgroundImage).toEqual(props.thumbnailUrl)
    })
  })
})
