import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Poll from '../../../../components/poll/index/Poll'
import { IntlProvider } from 'react-intl'
import normalizedPoll from '../../../testHelpers/normalizedPoll'
import RanksContainer from '../../../../containers/poll/index/RanksContainer'
import ThumbnailContainer from '../../../../containers/poll/index/ThumbnailContainer'
import StatusContainer from '../../../../containers/poll/shared/StatusContainer'

function setup(numOfRanks = 5,thumbnailUrl=null,isInitialLoad,fullRendering = false) {
  const poll = normalizedPoll(1,thumbnailUrl)
  let props = {
    ...poll,
    actions: {
      gaEvent: expect.createSpy()
    }
  }
  let output
  if(!fullRendering) {
    output = shallow(<Poll { ...props } />)
  } else {
    output = mount(<IntlProvider locale="en" messages={{}}><Poll {...props} /></IntlProvider>)
  }
  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/index/Poll', () => {

    it('calls gaEvent when the link is clicked', () => {
      const { output, props } = setup()
      output.find('Link').simulate('click')
      expect(props.actions.gaEvent).toHaveBeenCalledWith({ category: 'Poll', action: 'Click', label: `${props.idx} ${props.title}` })
    })

    it('renders RanksContainer', () => {
      const { output, props } = setup()
      expect(output.find(RanksContainer).length).toBe(1)
      expect(output.find(RanksContainer).props()).toEqual({ idx: props.idx })
    })

    it('renders StatusContainer', () => {
      const { output } = setup()
      expect(output.find(StatusContainer).length).toBe(1)
    })

    it('passes the correct props to Status', () => {
      const { output, props } = setup()
      expect(output.find(StatusContainer).props()).toEqual({
        idx: props.idx
      })
    })

    it('renders Tag', () => {
      const { output } = setup()
      expect(output.find('Tag').length).toBe(1)
    })

    it('passes the correct props to Tag', () => {
      const { output, props } = setup()

      expect(output.find('Tag').props()).toEqual({ tags: props.tags, className: 'col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-index-tag-container' })
    })
  })


  context('when there is a thumbnail', () => {
    it('doesn\'t render RanksContainer', () => {
      const { output, props } = setup(5,'http://makestar.co/file/image.jpeg')
      expect(output.find(RanksContainer).length).toEqual(0)
    })

    it('renders ThumbnailContainer', () => {
      const { output, props } = setup(5,'http://makestar.co/file/image.jpeg')
      expect(output.find(ThumbnailContainer).props()).toEqual({ idx : props.idx })
    })
  })
})
