import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import Index from '../../../../components/poll/index/Index'
import PollContainer from '../../../../containers/poll/index/PollContainer'
import PollsContainer from '../../../../containers/poll/index/PollsContainer'
import FilterContainer from '../../../../containers/shared/filter/FilterContainer'
import PaginationContainer from '../../../../containers/shared/PaginationContainer'
import IndexHeaderContainer from '../../../../containers/shared/index/IndexHeaderContainer'
import Empty from '../../../../components/poll/index/Empty'
import { chunk, times } from 'lodash'

function setup(numOfPolls) {

  const props = {
    numOfPolls,
    idxs: chunk(times(numOfPolls, i => i + 1),2),
    selectedStatus: 'All'
  }

  const output = shallow(<Index { ...props } />)
  return {
    props,
    output
  }
}

describe('components/poll/index/Index', () => {
  it('renders IndexHeaderContainer', () => {
    const { output } = setup(2)
    expect(output.find(IndexHeaderContainer).length).toEqual(1)
  })

  it('renders FilterContainer', () => {
    const { output } = setup(2)
    expect(output.find(FilterContainer).length).toEqual(1)
  })

  context('when numOfPolls is greater than 0', () => {
    it('renders PaginationContainer',() => {
      const { output } = setup(2)
      expect(output.find(PaginationContainer).length).toEqual(1)
    })

    it('doesn\'t render Empty', () =>{
      const { output } = setup(2)
      expect(output.find(Empty).length).toEqual(0)
    })

    it('renders PollContainer', () => {
      const { output } = setup(2)
      expect(output.find(PollContainer).length).toEqual(2)
      expect(output.find(PollContainer).first().props()).toEqual({ idx: 1 })
      expect(output.find(PollContainer).last().props()).toEqual({ idx: 2 })
    })
  })

  context('when numOfPolls is 0', () => {
    it('doesn\'t render PaginationContainer',() => {
      const { output } = setup(0)
      expect(output.find(PaginationContainer).length).toEqual(0)
    })

    it('renders Empty', () =>{
      const { output, props } = setup(0)
      expect(output.find(Empty).length).toEqual(1)
      expect(output.find(Empty).props()).toEqual({ selectedStatus: props.selectedStatus })
    })
  })
})
