import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import RankImages from '../../../../components/poll/index/RankImages'
import RankImageContainer from '../../../../containers/poll/index/RankImageContainer'
import { times } from 'lodash'

function setup(numOfRanks) {
  const props = {
    ranks: times(numOfRanks,i => i+1)
  }

  const output = shallow(<RankImages { ...props } />)
  return { props, output }
}

describe('components/poll/index/RankImages', () => {
  context('when the numOfRanks is 5',() => {
    it('calls RankImageContainer with the correct props', () => {
      const { output } = setup(5)

      expect(output.find(RankImageContainer).first().props()).toEqual({ idx: 1, index: 0, length: 5 })
      expect(output.find(RankImageContainer).last().props()).toEqual({ idx: 5, index: 4, length: 5 })
      expect(output.find(RankImageContainer).length).toEqual(5)
    })
  })

  context('when the numOfRanks is 3', () => {
    it('calls RankImageContainer with the correct props', () => {
      const { output } = setup(3)

      expect(output.find(RankImageContainer).first().props()).toEqual({ idx: 1, index: 0, length: 3 })
      expect(output.find(RankImageContainer).length).toEqual(3)
    })
  })
})
