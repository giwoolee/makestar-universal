//import expect from 'expect'
//import React from 'react'
//import Filter from '../../../../components/poll/index/Filter'
//import FilterLink from '../../../../components/poll/index/FilterLink'
//import flatten from 'flat'
//import { shallow, mount } from 'enzyme'
//import i18n from '../../../../i18n/index'
//import { IntlProvider } from 'react-intl'
//import { last } from 'lodash'
//
//function setup (isLoggedIn = true,fullRendering = false) {
//  const props = {
//    actions: { fetchPolls: expect.createSpy() },
//    selectedStatus: 'all',
//    isLoggedIn
//  }
//
//  const messages = flatten(i18n.en)
//  let output
//  if(!fullRendering) {
//    output = shallow(<Filter {...props }/>)
//  } else {
//    output = mount(<IntlProvider locale="en" messages={messages}><Filter {...props} /></IntlProvider>)
//  }
//
//  return {
//    props,
//    output
//  }
//}
//
//describe('components', () => {
//  describe('poll/index/Filter', () => {
//
//    it('should have the type filter', () => {
//      const { output } = setup()
//
//      expect(output.type()).toBe('filter')
//    })
//
//    it('should have the class name filter', () => {
//      const { output } = setup()
//
//      expect(output.hasClass('poll-filter')).toBe(true)
//    })
//
//    describe('children', () => {
//      it('should have children of type filter', () => {
//        const { output } = setup()
//        expect(output.find('FilterLink').length).toBe(4)
//      })
//    })
//
//    context('when isLoggedIn is false', () => {
//      it('should have children of length 3', () => {
//        const { output } = setup(false)
//        expect(output.find('FilterLink').length).toBe(3)
//      })
//    })
//  })
//})
