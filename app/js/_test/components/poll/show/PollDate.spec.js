import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import flatten from 'flat'
import PollDate from '../../../../components/poll/show/PollDate'
import i18n from '../../../../i18n/index'

function setup(endDate='2016-02-23T00:00:00+0000', startDate='2016-02-1T00:00:00+0000',fullRendering=false) {
  let props = {
    endDate,
    startDate,
  }
  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<PollDate {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><PollDate {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/show/Date', () => {
    it('should have the correct type', () => {
      const { output } = setup()

      expect(output.type()).toBe('polldate')
    })

    describe('children', () => {
      it('should render the date-icon', () => {
        const { output } = setup()
        expect(output.find('.date-icon').length).toEqual(1)
      })

      it('should render the correct date', () => {
        const { output, messages } = setup('2016-02-23T00:00:00+0000','2016-02-1T00:00:00+0000',true)
        expect(output.find('.date-text').text()).toBe('Poll Duration 2016-02-01-2016-02-23')
      })
    })
  })
})
