import expect from 'expect'
import React from 'react'
import OptionVotePercentage from '../../../../components/poll/show/OptionVotePercentage'
import { shallow } from 'enzyme'

function setup(voteCount=10,totalVoteCount=100,linkUrl='http://www.makestar.co') {
  let props = {
    voteCount,
    totalVoteCount,
    name: 'Option Name',
    linkUrl: 'linkUrl',
    linkText: 'linkText'
  }

  const output = shallow(<OptionVotePercentage {...props} />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/show/OptionVotePercentage', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    describe('children', () => {
      context('when the percentage is 0', () => {
        it('should render 0%', () => {
          const { output } = setup(0,0)
          expect(output.find('.poll-show-option-vote-percentage-text-container').text()).toBe('0%')
        })
      })
      context('when the percentage is 10%', () => {
        it('should render the progress-bar correctly', () => {
          const { output } = setup()
          let props = output.find('.poll-show-progress-bar').props()
          expect(props).toEqual({ className: 'progress-bar poll-show-progress-bar', style: { width: '10%' }, 'aria-valuenow': '10', 'aria-valuemin': '0', 'aria-valuemax': '100' })
        })
      })
      context('when the linkUrl is isn\'t undefined', () => {
        it('should render the link', () => {
          const { output, props } = setup()
          expect(output.find('.poll-show-link').text()).toBe(props.linkText)
        })
      })
      it('should render the name', () => {
        const { output, props } = setup()
        expect(output.find('.poll-show-option-name').text()).toBe(props.name)
      })
    })
  })
})
