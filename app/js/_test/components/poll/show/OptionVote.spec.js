import expect from 'expect'
import React from 'react'
import OptionVote from '../../../../components/poll/show/OptionVote'
import TestUtils from 'react-addons-test-utils'
import { shallow, mount } from 'enzyme'
import { IntlProvider } from 'react-intl'
import flatten from 'flat'
import i18n from '../../../../i18n/index'

function setup(myVoteCount = 0,isAvailable=true,fullRendering = false) {
  let props = {
    pollIdx: 1,
    idx: 1,
    actions: { pollVote: expect.createSpy() },
    voteCount: 100,
    myVoteCount,
    isEnd: false,
    isAvailable
  }

  const messages = flatten(i18n.en)
  let output

  if(!fullRendering) {
    output = shallow(<OptionVote {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><OptionVote {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/show/OptionVote', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })
    context('when isAvailable is true', () => {
      it('should call pollVote when clicked', () => {
        const { props, output } = setup(0,true,true)
        output.find('.poll-show-vote-btn-off').simulate('click')
        expect(props.actions.pollVote).toHaveBeenCalled()
      })
    })
    context('when isAvailable is false', () => {
      it('should call pollVote when clicked', () => {
        const { props, output } = setup(0,false,true)
        output.find('.poll-show-vote-btn-off').simulate('click')
        expect(props.actions.pollVote).toHaveBeenCalledWith({ pathname: `/polls/${props.pollIdx}/candidates/${props.idx}/vote`, isEnd: props.isEnd, isAvailable: props.isAvailable })
      })
    })
    context('when the option hasn\'t been voted on', () => {
      it('should render the button with the correct classname', () => {
        const { output } = setup()
        expect(output.find('.poll-show-vote-btn-off').length).toEqual(1)
      })
      it('should render the correct not voted text', () => {
        const { output, messages } = setup(0,false,true)
        expect(output.find('.poll-show-vote-not-voted-text').text()).toEqual('Vote')
      })
    })
    context('when the option has been voted on', () => {
      it('should render the button with the correct classname', () => {
        const { output } = setup(1)
        expect(output.find('.poll-show-vote-btn-on').length).toEqual(1)
      })
      it('should render the correct voted text', () => {
        const { output, messages, props } = setup(4,false,true)
        expect(output.find('.poll-show-vote-voted-text').text()).toBe('Voted!(4)')
      })
    })
  })
})
