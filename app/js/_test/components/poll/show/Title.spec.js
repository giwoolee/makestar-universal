import expect from 'expect'
import React from 'react'
import Title from '../../../../components/poll/show/Title'
import { shallow } from 'enzyme'

function setup() {
  let props = {
    title: 'Im the title ok',
    idx: 1
  }

  const output = shallow(<Title {...props} />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/show/Title', () => {
    it('should have the correct type', () => {
      const { output } = setup()

      expect(output.type()).toBe('ttl')
    })
  })

  describe('children', () => {
    it('should render the title', () => {
      const { output } = setup()

      expect(output.find('.poll-show-title-text').text()).toBe('Im the title ok')
    })

    it('should render the item number', () => {
      const { output } = setup()
      expect(output.find('.poll-show-item-number').text()).toBe('1')
    })
  })
})
