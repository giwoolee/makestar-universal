import expect from 'expect'
import React from 'react'
import OptionImage from '../../../../components/poll/show/OptionImage'
import { shallow } from 'enzyme'

function setup(rank=1) {
  let props = {
    rank,
    imageUrl: "https://dl.dropboxusercontent.com/s/qf8thjao2nfg5qw/TestImage.jpg?dl=0"
  }

  const output = shallow(<OptionImage {...props} />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/show/OptionImage', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    describe('children', () => {
      context('when rank is 1', () => {
        it('should render option-image-rank-1', () => {
          const { output } = setup()
          expect(output.find('.option-image-rank-1').length).toEqual(1)
        })
      })

      context('when rank is 2', () => {
        it('should render option-image-rank-2', () => {
          const { output } = setup(2)
          expect(output.find('.option-image-rank-2').length).toEqual(1)
        })
      })

      context('when rank is 3', () => {
        it('should render option-image-rank-3', () => {
          const { output } = setup(3)
          expect(output.find('.option-image-rank-3').length).toEqual(1)
        })
      })

      context('when rank is > 3', () => {
        it('should render option-image-rank-3', () => {
          const { output } = setup(4)
          expect(output.find('.option-image-no-rank').length).toEqual(1)
        })
      })
    })
  })
})
