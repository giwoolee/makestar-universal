import expect from 'expect'
import React from 'react'
import Header from '../../../../components/poll/show/Header'
import { shallow } from 'enzyme'

function setup() {
  let props = {
    src: '/v1/polls/header'
  }

  const output = shallow(<Header {...props} />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/show/Header', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    it('should have the correct className', () => {
      const { output } = setup()
      expect(output.hasClass('col-xs-12 col-sm-12 col-md-12 col-lg-12 poll-show-header-iframe')).toBe(true)
    })
  })

  describe('children',() => {
    it('should render the iframe', () => {
      const { output } = setup()

      expect(output.find('iframe').hasClass('resizable-iframe')).toBe(true)
    })
  })
})
