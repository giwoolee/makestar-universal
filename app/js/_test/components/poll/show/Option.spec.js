import expect from 'expect'
import React from 'react'
import Option from '../../../../components/poll/show/Option'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import { IntlProvider } from 'react-intl'
import i18n from '../../../../i18n/index'

function setup(fullRendering = false) {
  let props = {
    idx: 1,
    rank: 1,
    imageUrl: 'image/poll/1.jpeg',
    pollIdx: 1,
    voteCount: 1,
    name: 'Option Name',
    linkUrl: '/polls/link.html',
    linkText: 'link text',
    isAvailable: false,
    isEnd: false,
    actions: {
      pollVote: expect.createSpy()
    },
  }
  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<Option {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={ messages }><Option {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/show/Option', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    describe('children', () => {
      it('should render OptionImage', () => {
        const { output, props } = setup()
        expect(output.find('OptionImage',{ rank: props.idx, imageUrl: props.imageUrl }).length).toEqual(1)
      })
      it('should render OptionVote', () => {
        const { output, props } = setup()
        expect(output.find('OptionVote').props()).toEqual({ actions: { pollVote: props.actions.pollVote }, idx: props.idx, pollIdx: props.pollIdx, voteCount: props.voteCount, isAvailable: props.isAvailable, isEnd: props.isEnd, myVoteCount: props.myVoteCount })
      })
      it('should render OptionVotePercentage', () => {
        const { output, props } = setup()
        expect(output.find('OptionVotePercentage').props()).toEqual({ voteCount: props.voteCount, totalVoteCount: props.totalVoteCount, name: props.name, linkUrl: props.linkUrl, linkText: props.linkText })
      })
    })
  })
})
