import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import VoteType from '../../../../components/poll/shared/VoteType'
import i18n from '../../../../i18n/index'
import { IntlProvider, FormattedMessage } from 'react-intl'

function setup(className, messageId,fullRendering = false) {
  let props = {
    className,
    messageId
  }
  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<VoteType {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><VoteType {...props} /></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/shared/VoteType', () => {
    it('calls <FormattedMessage /> with the correct props', () => {
      const { output } = setup('poll-possible-votes-once', 'en.poll.voteOnce')
      expect(output.find(FormattedMessage).props()).toEqual({tagName: 'span', values: {}, id: 'en.poll.voteOnce'})
    })

    it('renders poll-possible-votes-once', () => {
      const { output } = setup('poll-possible-votes-once', 'en.poll.voteOnce')
      expect(output.find('.poll-possible-votes-once').length).toEqual(1)
    })     
  })
})
