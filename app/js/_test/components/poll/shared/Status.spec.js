import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import Status from '../../../../components/poll/shared/Status'
import OpenClosedContainer from '../../../../containers/poll/shared/OpenClosedContainer'
import VoteTypeContainer from '../../../../containers/poll/shared/VoteTypeContainer'
import VoteCountContainer from '../../../../containers/poll/shared/VoteCountContainer'
import i18n from '../../../../i18n/index'

function setup(isEnd = true) {
  let props = {
    idx: 1,
    isEnd,
    endDate: '2016-01-01',
    votingType: 'ONCE',
    voteCount: 0,
    myVoteCount: 0
  }

  const output = shallow(<Status {...props} />)

  return {
    props,
    output
  }
}

describe('components', () => {
  describe('poll/shared/status', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('status')
    })

    describe('children',() => {
      it('should render OpenClosedContainer', () => {
        const { output } = setup()
        expect(output.find(OpenClosedContainer).props()).toEqual({ idx: 1 })
      })

      it('should render VoteType', () => {
        const { output } = setup()
        expect(output.find(VoteTypeContainer).props()).toEqual({ idx: 1 })
      })

      it('should render Participants', () => {
        const { output } = setup()
        expect(output.find('Participants').props()).toEqual({ voteCount: 0 })
      })

      it('should render VoteCountContainer', () => {
        const { output } = setup()
        expect(output.find(VoteCountContainer).props()).toEqual({ idx: 1 }).toExist()
      })
    })
  })
})
