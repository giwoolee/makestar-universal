import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import { IntlProvider, FormattedMessage } from 'react-intl'
import flatten from 'flat'
import Vote from '../../../../components/poll/shared/Vote'
import i18n from '../../../../i18n/index'

function setup(fullRendering = false) {
  let props = {
    id: 'en.poll.voted',
    myVoteCount: 0,
    iconClass: 'poll-vote-count-on',
    textClass: 'poll-off-text'
  }

  const messages = flatten(i18n.en)

  let output
  if(!fullRendering) {
    output = shallow(<Vote {...props }/>)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Vote {...props }/></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/share/Vote', () => {
    it('should render correctly', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    context('when props.myVoteCount is 0', () => {
      it('should render with the correct classname', () => {
        const { output } = setup()
        expect(output.find(FormattedMessage).props()).toEqual({id: 'en.poll.voted', tagName: 'span', values: { myVoteCount: 0 }})
      })

      it('should render poll-off-text', () => {
        const { output, messages } = setup()
        expect(output.find('.poll-off-text').length).toEqual(1)
      })

      it('should render poll-vote-count-on', () => {
        const { output } = setup()
        expect(output.find('.poll-vote-count-on').length).toEqual(1)
      })
    })
  })
})
