import expect from 'expect'
import React from 'react'
import { useFakeTimers } from 'sinon'
import moment from 'moment'
import { shallow, mount } from 'enzyme'
import OpenClosed from '../../../../components/poll/shared/OpenClosed'
import { IntlProvider, FormattedMessage } from 'react-intl'
import i18n from '../../../../i18n/index'
import flatten from 'flat'

function setup(isEnd=false, timeRemaining,fullRendering = false) {
  let props = {
    isEnd,
    timeRemaining
  }

  const messages = flatten(i18n.en)
  let output

  if(!fullRendering) {
    output = shallow(<OpenClosed {...props}/>)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><OpenClosed {...props}/></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/shared/OpenClosed', () => {

    context('when isEnd is false', () => {
      it('should render the poll-on-icon', () => {
        const { output } = setup(false,{ messageId: 'en.poll.off', values: { days: undefined } })
        expect(output.find('.poll-on-icon').length).toEqual(1)
      })
      it('passes the correct props to FormattedMessage',() => {
        const { output } = setup(false,{ messageId: 'en.poll.off', values: { days: undefined } })
        expect(output.find(FormattedMessage).props()).toEqual({ id: 'en.poll.off', tagName: 'span', values: { days: undefined } })
      })
    })

    context('when isEnd is true', () => {
      it('should render the poll-off-icon', () => {
        const { output } = setup(true,{ messageId: 'en.poll.off', values: { days: 3 } })
        expect(output.find('.poll-off-icon').length).toEqual(1)
      })

      it('passes the correct props to FormattedMessage',() => {
        const { output } = setup(true,{ messageId: 'en.poll.off', values: { days: 3 } })
        expect(output.find(FormattedMessage).props()).toEqual({ id: 'en.poll.off', tagName: 'span', values: { days: 3 } })
      })
    })
  })
})
