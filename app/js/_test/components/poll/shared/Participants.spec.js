import expect from 'expect'
import React from 'react'
import { shallow, mount } from 'enzyme'
import flatten from 'flat'
import Participants from '../../../../components/poll/shared/Participants'
import { IntlProvider } from 'react-intl'
import i18n from '../../../../i18n/index'

function setup(fullRendering = false) {
  let props = {
    voteCount: 100,
  }

  const messages = flatten(i18n.en)
  let output

  if(!fullRendering) {
    output = shallow(<Participants {...props} />)
  } else {
    output = mount(<IntlProvider locale="en" messages={messages}><Participants { ...props}/></IntlProvider>)
  }

  return {
    props,
    output,
    messages
  }
}

describe('components', () => {
  describe('poll/share/participants', () => {
    it('should have the correct type', () => {
      const { output } = setup()
      expect(output.type()).toBe('div')
    })

    it('should have the correct classname', () => {
      const { output } = setup()
      expect(output.find('.poll-participants-icon').length).toEqual(1)
    })

    describe('children', () => {
      it('should have the correct text', () => {
        const { output, props } = setup(true)
        expect(output.text()).toBe('100 Votes')
      })
    })
  })
})
