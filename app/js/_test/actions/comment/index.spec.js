import configureMockStore from 'redux-mock-store'
import expect from 'expect'
import thunk from 'redux-thunk'
import * as types from '../../../constants/comment/index'
import * as actions from '../../../actions/comment/index'
import nock from 'nock'
import commentResponse from '../../testHelpers/commentResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import ApiClient from '../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../middleware/clientMiddleware'
import { receiveOne, receiveAll } from '../../../schemas/comment'
import ApiError from '../../../helpers/api/ApiError'

const client = new ApiClient(superagent)
const middlewares = [ thunk, clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('comment actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  describe('toggleDeletePrompt', () => {
    context('when show is false', () => {
      it('creates an action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_DELETE_PROMPT_OPEN, payload: { type: 'comments', idx: 1 } }]
        const store = mockStore({})
        store.dispatch(actions.toggleDeletePrompt({ idx: 1, show: false, type: 'comments' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })
    context('when show is true', () => {
      it('creates an action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_DELETE_PROMPT_CLOSE }]
        const store = mockStore({})
        store.dispatch(actions.toggleDeletePrompt({ idx: 1, show: true, type: 'comments' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })
  })

  describe('editOpen', () => {
    context('when type is comments', () => {
      it('creates the action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_EDIT_OPEN, payload: { type: 'comments', idx: 1 } }]
        const store = mockStore({})
        store.dispatch(actions.editOpen({ idx: 1, type: 'comments' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })

    context('when type is replies', () => {
      it('creates the action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_EDIT_OPEN, payload: { type: 'replies', idx: 1 } }]
        const store = mockStore({})
        store.dispatch(actions.editOpen({ idx: 1, type: 'replies' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })
  })

  describe('editClose', () => {
    context('when type is comments', () => {
      it('creates the action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_EDIT_CLOSE, payload: { type: 'comments', idx: 1 } }]
        const store = mockStore({})
        store.dispatch(actions.editClose({ idx: 1, type: 'comments' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })

    context('when type is replies', () => {
      it('creates the action', () => {
        const expectedAction = [{ type: types.COMMENT_UI_EDIT_CLOSE, payload: { type: 'replies', idx: 1 } }]
        const store = mockStore({})
        store.dispatch(actions.editClose({ idx: 1, type: 'replies' }))
        expect(store.getActions()).toEqual(expectedAction)
      })
    })
  })


  describe('update', () => {
    context('when type is comments', () => {
      it('should create a COMMENT_UPDATE_SUCCESS when the response is ok', () => {

        const comment = commentResponse()
        nock('http://localhost:8080/')
        .intercept('/v1/polls/1/comments','PUT')
        .reply(200,{ content: comment })

        const expectedAction = [{ type: types.COMMENT_UPDATE_SUCCESS, ...receiveOne({ content: comment }), idx: 1 } ]

        const store = mockStore({})
        return store.dispatch(actions.update({ idx: 1, pathname: '/polls/1/comments', data: { text: 'text' }, type: 'comments' })).then(() => {
          expect(store.getActions()[0]).toEqual(expectedAction[0])
        })
      })
    })

    context('when type is replies', () => {
      it('should create a REPLY_UPDATE_SUCCESS when the response is ok', () => {

        const comment = commentResponse()
        nock('http://localhost:8080/')
        .intercept('/v1/polls/1/comments/1/reply/1','PUT')
        .reply(200,{ content: comment })

        const expectedAction = [{ type: types.REPLY_UPDATE_SUCCESS, ...receiveOne({ content: comment }), idx: 1 } ]

        const store = mockStore({})
        return store.dispatch(actions.update({ idx: 1, pathname: '/polls/1/comments/1/reply/1', data: { text: 'text' }, type: 'replies' })).then(() => {
          expect(store.getActions()[0]).toEqual(expectedAction[0])
        })
      })
    })
  })

  describe('del', () => {
    context('when type is comments', () => {
      it('delete should create a COMMENT_DELETE_SUCCESS when the response is ok', () => {

        const comment = commentResponse()
        nock('http://localhost:8080/')
        .intercept('/v1/polls/1/comments','DELETE')
        .reply(200,{ content: comment })

        const expectedAction = [{ type: types.COMMENT_DELETE_SUCCESS, ...receiveOne({ content: comment }), idx: 1 } ]

        const store = mockStore({})
        return store.dispatch(actions.del({ idx: 1, pathname: '/polls/1/comments', type: 'comments' })).then(() => {
          expect(store.getActions()[0]).toEqual(expectedAction[0])
        })
      })
    })

    context('when type is replies', () => {
      it('delete should create a REPLY_DELETE_SUCCESS when the response is ok', () => {

        const comment = commentResponse()
        nock('http://localhost:8080/')
        .intercept('/v1/polls/1/comments/1/reply/1','DELETE')
        .reply(200,{ content: comment })

        const expectedAction = [{ type: types.REPLY_DELETE_SUCCESS, ...receiveOne({ content: comment }),idx: 1 } ]

        const store = mockStore({})
        return store.dispatch(actions.del({ idx: 1, pathname: '/polls/1/comments/1/reply/1', type: 'replies' })).then(() => {
          expect(store.getActions()[0]).toEqual(expectedAction[0])
        })
      })
    })
  })

  it('openReply should create an REPLY_OPEN action', () => {
    const expectedActions = [
      { type: types.COMMENT_UI_REPLY_OPEN, payload: { idx: 1 } }
    ]

    const store = mockStore({})
    store.dispatch(actions.openReply(1))
    expect(store.getActions()).toEqual(expectedActions)
  })

  it('closeReply should create a REPLY_CLOSE action', () => {
    const expectedActions = [
      { type: types.COMMENT_UI_REPLY_CLOSE, payload: { idx: 1 } }
    ]

    const store = mockStore({})
    store.dispatch(actions.closeReply(1))
    expect(store.getActions()).toEqual(expectedActions)
  })

  it('fetch should create a COMMENT_FETCH_SUCCESS when the response is ok', () => {
    const comment = commentResponse()
    const pagination = paginationResponse(1)
    nock('http://localhost:8080/')
    .get('/v1/polls/1/comments')
    .query({ page: 0 })
    .reply(200,{ content: [comment], pagination })

    const expectedActions = [
      { type: types.COMMENT_FETCH_REQUEST, query: { page: 0 } },
      { type: types.COMMENT_FETCH_SUCCESS, ...receiveAll({ content: [comment], pagination }), idx: 1, query: { page: 0 } }
    ]
    const store = mockStore({ comments: [], pagination: {} })
    return store.dispatch(actions.fetch({ pathname: '/polls/1/comments', idx: 1, type: 'comments', query: { page: 0 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchComments should create a COMMENT_FETCH_FAILURE when the COMMENT_FETCH_REQUEST action fails', () => {
    const comment = commentResponse()
    const pagination = paginationResponse(1)
    const resp = { error: { msg: "Error Processing Request" } }
    nock('http://localhost:8080/')
    .get('/v1/polls/1/comments')
    .query({ page: 0 })
    .reply(400,resp)

    const error = new ApiError({ pathname: '/v1/polls/1/comments', status: 400, payload: resp })
    const expectedActions = [
      { type: types.COMMENT_FETCH_REQUEST, query: { page: 0 } },
      { type: types.COMMENT_FETCH_FAILURE, error, query: { page: 0 }, idx: 1 }
    ]
    const store = mockStore({ comments: [], pagination: {} })
    return store.dispatch(actions.fetch({ pathname: '/polls/1/comments', idx: 1, type: 'comments', query: { page: 0 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  context('when the type is replies', () => {
    it('create should create a REPLY_CREATE_SUCCESS when the response is ok', () => {
      const comment = commentResponse()
      nock('http://localhost:8080/')
      .post('/v1/polls/1/comments/1/replies')
      .reply(200,{ content: comment })

      const expectedActions = [
        { type: types.REPLY_CREATE_REQUEST, data: { text: "댓글" }, gaData: { category: 'Poll Detail', action: 'Reply', label: "1" } },
        { type: types.REPLY_CREATE_SUCCESS, idx: 1, ...receiveOne({ content: comment }), data: { text: '댓글' }, gaData: { category: 'Poll Detail', action: 'Reply', label: "1" } }
      ]
      const store = mockStore({ comments: [], pagination: {} })
      return store.dispatch(actions.create({ idx: 1, pathname: '/polls/1/comments/1/replies', type: 'replies', data: { text: "댓글" } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('create should create a REPLY_CREATE_FAILURE when the REPLY_CREATE_REQUEST action fails', () => {
      const comment = commentResponse()
      const resp = { error: { msg: "Error Processing Request" } }
      nock('http://localhost:8080/')
      .post('/v1/polls/1/comments/1/replies')
      .query({ page: 0 })
      .reply(400,resp)

      const error = new ApiError({ pathname: 'http://localhost:8080/v1/polls/1/comments/1/reply', status: 400, payload: resp, method: 'post' })
      const expectedActions = [
        { type: types.REPLY_CREATE_REQUEST, data: { text: "댓글" }, gaData: { category: 'Poll Detail', action: 'Reply', label: "1" } },
        { type: types.REPLY_CREATE_FAILURE, idx: 1, error, data: { text: '댓글' }, gaData: { category: 'Poll Detail', action: 'Reply', label: "1" } }
      ]
      const store = mockStore({ comments: [], pagination: {} })
      return store.dispatch(actions.create({ idx: 1, pathname: '/polls/1/comments/1/replies', type: 'replies', data: { text: "댓글" } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })

  context('when the type is comments', () => {
    it('create should create a COMMENT_CREATE_SUCCESS when the response is ok', () => {
      const comment = commentResponse()
      nock('http://localhost:8080/')
      .post('/v1/polls/1/comments')
      .reply(200,{ content: comment })

      const expectedActions = [
        { type: types.COMMENT_CREATE_REQUEST, data: { text: "댓글" }, gaData: { category: 'Poll Detail', action: 'Comment', label: "1" } },
        { type: types.COMMENT_CREATE_SUCCESS, idx: 1, ...receiveOne({ content: comment }), data: { text: '댓글' }, gaData: { category: 'Poll Detail', action: 'Comment', label: "1" } }
      ]
      const store = mockStore({ comments: [], pagination: {} })
      return store.dispatch(actions.create({ idx: 1, pathname: '/polls/1/comments', type: 'comments', data: { text: "댓글" } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('create should create a COMMENT_CREATE_FAILURE when the COMMENT_CREATE_REQUEST action fails', () => {
      const comment = commentResponse()
      const resp = { error: { msg: "Error Processing Request" } }
      nock('http://localhost:8080/')
      .post('/v1/polls/1/comments')
      .query({ page: 0 })
      .reply(400,resp)

      const error = new ApiError({ pathname: '/v1/polls/1/comments', status: 400, payload: resp, method: 'post' })
      const expectedActions = [
        { type: types.COMMENT_CREATE_REQUEST, data: { text: "댓글" }, gaData: { category: 'Poll Detail', action: 'Comment', label: "1" } },
        { type: types.COMMENT_CREATE_FAILURE, idx: 1, error, data: { text: '댓글' }, gaData: { category: 'Poll Detail', action: 'Comment', label: "1" } }
      ]
      const store = mockStore({ comments: [], pagination: {} })
      return store.dispatch(actions.create({ idx: 1, pathname: '/polls/1/comments', type: 'comments', data: { text: "댓글" } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })

  describe('updateText', () => {
    context('when the type is comments', () => {
      it('should create a COMMENT_TEXT_UPDATE action', () => {
        const expectedActions = [
          { type: types.COMMENT_TEXT_UPDATE, idx: 1, text: "TEXT" }
        ]

        const store = mockStore({ comments: { comments: [], pagination: {}, text: {} } })
        store.dispatch(actions.updateText({ idx: 1, type: 'comments', text: "TEXT" }))
        expect(store.getActions()).toEqual(expectedActions)
      })
    })

    context('when the type is replies', () => {
      it('should create a REPLY_TEXT_UPDATE', () => {
        const expectedActions = [
          { type: types.REPLY_TEXT_UPDATE, idx: 1, text: "TEXT" }
        ]

        const store = mockStore({ replies: { 1: { pagination: {} } } })
        store.dispatch(actions.updateText({ idx: 1, type: 'replies', text: "TEXT" }))
        expect(store.getActions()).toEqual(expectedActions)
      })
    })
  })

  context('when the type is comments', () => {
    it('clear should create a COMMENT_CLEAR action', () => {
      const expectedActions = [
        { type: types.COMMENT_CLEAR, idx: 1 }
      ]

      const store = mockStore({ comments: [], pagination: {}, text: {} })
      store.dispatch(actions.clear('comments',1))
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  context('when the type is replies', () => {
    it('clear should create a COMMENT_CLEAR action', () => {
      const expectedActions = [
        { type: types.REPLY_CLEAR, idx: 1 }
      ]

      const store = mockStore({ comments: [], pagination: {}, text: {} })
      store.dispatch(actions.clear('replies',1))
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('promptLogin should create COMMENT_CREATE_FAILURE', () => {
    const expectedActions = [
      { type: types.COMMENT_CREATE_FAILURE, error: { status: 401 } }
    ]
    const store = mockStore({ comments: [], pagination: {}, text: "" })
    store.dispatch(actions.promptLogin())
    expect(store.getActions()).toEqual(expectedActions)
  })

})
