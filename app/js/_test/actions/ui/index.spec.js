import expect from 'expect'
import { resetStatus, resetNotification, topNavToggle, delPrompt } from '../../../actions/ui/index'
import * as types from '../../../constants/ui/index'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [ thunk ]

const mockStore = configureMockStore(middlewares)

describe('ui actions', () => {

  it('resetStatus should create UI_ERROR_STATUS_RESET', () => {
    const expectedActions = [
      { type: types.UI_ERROR_MODAL_RESET }
    ]
    const store = mockStore({ ui: { error: true, status: 401, type: 'AN_ERROR_TYPE' } })
    store.dispatch(resetStatus())
    expect(store.getActions()).toEqual(expectedActions)
  })

  it('resetNotification should create UI_NOTIFICATION_RESET', () => {
    const expectedActions = [
      { type: types.UI_NOTIFICATION_RESET }
    ]
    const store = mockStore({ ui: { error: undefined, status: 200, type: 'A_TYPE', notification: true } })
    store.dispatch(resetNotification())
    expect(store.getActions()).toEqual(expectedActions)
  })

  describe('topNavToggle', () => {
    context('when the argument is true', () => {
      it('closeTopNav should create UI_TOP_NAV_CLOSE', () => {
        const expectedActions = [
          { type: types.UI_CLOSE_TOP_NAV }
        ]
        const store = mockStore({ ui: { error: undefined, status: undefined, topNavOpen: true } })
        store.dispatch(topNavToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
      })
    })

    context('when the argument is false', () => {
      it('closeTopNav should create UI_TOP_NAV_CLOSE', () => {
        const expectedActions = [
          { type: types.UI_OPEN_TOP_NAV }
        ]
        const store = mockStore({ ui: { error: undefined, status: undefined, topNavOpen: true } })
        store.dispatch(topNavToggle(false))
        expect(store.getActions()).toEqual(expectedActions)
      })
    })
  })
})
