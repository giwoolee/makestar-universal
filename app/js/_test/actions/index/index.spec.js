import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../constants/index/index'
import * as actions from '../../../actions/index/index'
import nock from 'nock'
import indexResponse from '../../testHelpers/indexResponse'
import ApiClient from '../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../middleware/clientMiddleware'
import ApiError from '../../../helpers/api/ApiError'

const client = new ApiClient(superagent)
const middlewares = [ thunk,clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)


describe('index (main page) actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('fetchIndexPage should create an INDEX_FETCH_SUCCESS action when the response is ok', () => {
    const resp = indexResponse()
    nock('http://localhost:8080/')
    .get('/v1/index')
    .reply(200,{ content: resp })

    const expectedActions = [
      { type: types.INDEX_FETCH_REQUEST, query: undefined },
      { type: types.INDEX_FETCH_SUCCESS, query: undefined, content: resp }
    ]
    const store = mockStore({ projects: { projects: [] },polls: { polls: [] }, contents: { contents: [] } })
    return store.dispatch(actions.fetchIndex({ pathname: '/index' }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchIndexPage should create an INDEX_FETCH_FAILURE action when the response is not ok', () => {
    const resp = { error: { msg: 'Error Processing Request' } }
    nock('http://localhost:8080/')
    .get('/v1/index')
    .reply(400,resp)

    const error = new ApiError({ pathname: '/v1/index', status: 400, method: 'get', payload: resp })
    const expectedActions = [
      { type: types.INDEX_FETCH_REQUEST, query: undefined },
      { type: types.INDEX_FETCH_FAILURE, error, query: undefined }
    ]

    const store = mockStore({ projects: { projects: [] },polls: { polls: [] }, contents: { contents: [] } })
    return store.dispatch(actions.fetchIndex({ pathname: '/index' }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('closeTopNotice should create an INDEX_CLOSE_TOP_NOTICE action', () => {
    const expectedActions = [
      { type: types.INDEX_CLOSE_TOP_NOTICE }
    ]

    const store = mockStore({ projects: { projects: [] }, polls: { polls: [] }, contents: { contents: [] } },expectedActions)

    store.dispatch(actions.closeTopNotice())
    expect(store.getActions()).toEqual(expectedActions)
  })
})
