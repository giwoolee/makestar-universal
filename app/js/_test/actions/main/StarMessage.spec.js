import expect from 'expect'
import { showVideo } from '../../../actions/main/starMessage'
import * as types from '../../../constants/main/starMessage'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [ thunk ]

const mockStore = configureMockStore(middlewares)

describe('main/starMessage actions', () => {
  describe('showVideo', () => {
    context('when the argument is true', () => {
      it('showVideo should create MAIN_STAR_MESSAGE_SHOW_VIDEO', () => {
        const expectedActions = [
          {
            type: types.MAIN_STAR_MESSAGE_SHOW_VIDEO,
            artist: 'kim_woo_bin'
          }
        ]
        const store = mockStore([{ id: 0, artist: 'kim_woo_bin', showVideo: false }])
        store.dispatch(showVideo('kim_woo_bin'))
        expect(store.getActions()).toEqual(expectedActions)
      })
    })
  })
})
