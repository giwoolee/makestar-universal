import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/poll/show/'
import * as actions from '../../../../actions/poll/show/'
import nock from 'nock'
import pollResponse from '../../../testHelpers/pollResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveOne } from '../../../../schemas/poll/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk,clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('poll actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('votePoll should create POLL_VOTE_FAILURE action when the POLL_VOTE_REQUEST action fails', () => {
    const resp = { error: { msg: 'You have already voted on this poll' } }
    nock('http://localhost:8080/')
    .post('/v1/polls/1/candidates/1/vote')
    .reply(412,resp)

    const error = new ApiError({ method: 'post', status: 412, path: '/v1/polls/1/candidates/1/vote', payload: resp })
    const expectedActions = [
      { type: types.POLL_VOTE_REQUEST },
      { type: types.POLL_VOTE_FAILURE, error, gaData: { action: "Vote", category: "Poll Detail", label: "1" } }
    ]

    const store = mockStore({ poll: pollResponse() })
    return store.dispatch(actions.pollVote({ pathname: '/polls/1/candidates/1/vote', isEnd: false, isAvailable: true }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('votePoll should create POLL_VOTE_SUCCESS action when the POLL_VOTE_REQUEST action succeeds', () => {
    const resp = pollResponse()
    nock('http://localhost:8080')
    .post('/v1/polls/1/candidates/1/vote')
    .reply(200, { content: resp })

    const expectedActions = [
      { type: types.POLL_VOTE_REQUEST },
      { type: types.POLL_VOTE_SUCCESS, ...receiveOne({ content: resp }), gaData: { action: "Vote", category: "Poll Detail", label: "1" } }
    ]

    const store = mockStore({ poll: resp })
    return store.dispatch(actions.pollVote({ pathname: '/polls/1/candidates/1/vote',isEnd: false, isAvailable: true }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  context('when isAvailable is false and isEnd is false', () => {
    it('pollVoteUnavailable should create POLL_VOTE_ALREADY_VOTED action', () => {
      const resp = pollResponse(1,null,false,false)
      const expectedActions = [
        { type: types.POLL_VOTE_UNAVAILABLE }
      ]
      const store = mockStore({ poll: resp })
      store.dispatch(actions.pollVote({ pathname: '/v1/polls/1/candidates/1/vote', isEnd: false, isAvailable: false }))
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  context('when isAvailable is false and isEnd is true', () => {
    it('pollVoteUnavailable should create POLL_VOTE_CLOSED action', () => {
      const resp = pollResponse(1,null,false,true)
      const expectedActions = [
        { type: types.POLL_VOTE_CLOSED }
      ]
      const store = mockStore({ poll: resp })
      store.dispatch(actions.pollVote({ pathname: '/v1/polls/1/candidates/1/vote', isAvailable: false, isEnd: true }))
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  context('when the locale is set in the query string', () => {
    it('pollFetch should create POLL_FETCH_REQUEST_SUCCESS action when the POLL_FETCH_REQUEST action succeeds', () => {
      const resp = pollResponse()
      nock('http://localhost:8080/')
      .get('/v1/polls/1?locale=ko')
      .reply(200, { content: resp })

      const expectedActions = [
        { type: types.POLL_SHOW_FETCH_REQUEST },
        { type: types.POLL_SHOW_FETCH_SUCCESS, ...receiveOne({ content: resp }) }
      ]

      const store = mockStore({ poll: resp })
      return store.dispatch(actions.pollFetch({ pathname: '/polls/1', query: { locale: 'ko' } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('pollFetch should create POLL_FETCH_REQUEST_FAILURE action when the POLL_FETCH_REQUEST action fails', () => {
      const resp = { error: { msg: 'Error' } }
      nock('http://localhost:8080/')
      .get('/v1/polls/1?locale=ko')
      .reply(500, resp)

      const error = new ApiError({ status: 500, method: 'get', path: '/v1/polls/1', payload: resp })
      const expectedActions = [
        { type: types.POLL_SHOW_FETCH_REQUEST },
        { type: types.POLL_SHOW_FETCH_FAILURE, error }
      ]
      const store = mockStore({ poll: pollResponse() })
      return store.dispatch(actions.pollFetch({ pathname: '/polls/1', query: { locale: 'ko' } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })

  context('when the locale is not set in the query string', () => {
    it('pollFetch should create POLL_SHOW_FETCH_SUCCESS action when the POLL_FETCH_REQUEST action succeeds', () => {
      const resp = pollResponse()
      nock('http://localhost:8080/')
      .get('/v1/polls/1')
      .reply(200, { content: resp })

      const expectedActions = [
        { type: types.POLL_SHOW_FETCH_REQUEST },
        { type: types.POLL_SHOW_FETCH_SUCCESS, ...receiveOne({ content: resp }) }
      ]

      const store = mockStore({ poll: pollResponse() } )
      return store.dispatch(actions.pollFetch({ pathname: '/polls/1' }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('pollFetch should create POLL_FETCH_REQUEST_FAILURE action when the POLL_FETCH_REQUEST action fails', () => {
      const resp = { error: { msg: 'Error' } }
      nock('http://localhost:8080/')
      .get('/v1/polls/1')
      .reply(500, resp)

      const error = new ApiError({ method: 'get', status: 500, path: '/v1/polls/1', payload: resp })
      const expectedActions = [
        { type: types.POLL_SHOW_FETCH_REQUEST },
        { type: types.POLL_SHOW_FETCH_FAILURE, error }
      ]
      const store = mockStore({ poll: pollResponse() })
      return store.dispatch(actions.pollFetch({ pathname: '/polls/1' }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })
})
