import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/poll/index'
import * as actions from '../../../../actions/poll/index'
import nock from 'nock'
import pollResponse from '../../../testHelpers/pollResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveAll } from '../../../../schemas/poll/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk,clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('poll index actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('fetchPolls should create a POLL_INDEX_FETCH_SUCCESS action when the response is ok', () => {
    const resp = pollResponse()
    nock('http://localhost:8080/')
    .get('/v1/polls')
    .query({ status: 'All', page: 1 })
    .reply(200, {
      pagination: paginationResponse(1),
      content: [resp]
    })

    const expectedActions = [
      { type: types.POLL_INDEX_FETCH_REQUEST, query: { status: 'All', page: 1 } },
      { type: types.POLL_INDEX_FETCH_SUCCESS, ...receiveAll({ content: [resp], pagination: paginationResponse(1) }), query: { status: 'All', page: 1  } }
    ]
    const store = mockStore({ polls: [] })
    return store.dispatch(actions.fetchPolls({ pathname: '/polls', query: { status: 'All', page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchPolls should request the endpoint with the correct filters', () => {
    const resp = pollResponse()
    nock('http://localhost:8080/')
    .get('/v1/polls')
    .query({ status: 'Open', page: 1 })
    .reply(200, {
      pagination: paginationResponse(1),
      content: [resp]
    })

    const expectedActions = [
      { type: types.POLL_INDEX_FETCH_REQUEST, query: { status: 'Open', page: 1  } },
      { type: types.POLL_INDEX_FETCH_SUCCESS, ...receiveAll({ content: [resp], pagination: paginationResponse(1) }), query: { status: 'Open', page: 1  } }
    ]

    const store = mockStore({ polls: [] })
    return store.dispatch(actions.fetchPolls({ pathname: '/polls', query: { status: 'Open', page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchPolls should create a POLL_INDEX_FETCH_FAILURE action when the POLL_INDEX_FETCH_REQUEST action fails', () => {
    const resp = { error: { msg: 'Error Processing Request' } }
    nock('http://localhost:8080/')
    .get('/v1/polls')
    .query({ status: 'All', page: 1 })
    .reply(400,resp)

    const error = new ApiError({ pathname: '/v1/polls', status: 400, method: 'get', payload: resp })
    const expectedActions = [
      { type: types.POLL_INDEX_FETCH_REQUEST, query: { status: 'All', page: 1 } },
      { type: types.POLL_INDEX_FETCH_FAILURE, error, query: { status: 'All', page: 1  } }

    ]

    const store = mockStore({ polls: [] })
    return store.dispatch(actions.fetchPolls({ pathname: '/polls', query: { status: 'All', page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })
})
