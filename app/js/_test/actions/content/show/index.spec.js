import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/content/show/'
import * as actions from '../../../../actions/content/show/'
import nock from 'nock'
import contentResponse from '../../../testHelpers/contentsResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveOne } from '../../../../schemas/content/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk, clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('content show actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('contentFetch should create CONTENT_SHOW_FETCH_SUCCESS action when the CONTENT_SHOW_FETCH_REQUEST succeeds', () => {
    const _content = contentResponse()

    nock('http://localhost:8080/')
    .get('/v1/contents/articles/1')
    .reply(200, { content: _content })

    const expectedActions = [
      { type: types.CONTENT_SHOW_FETCH_REQUEST, },
      { type: types.CONTENT_SHOW_FETCH_SUCCESS,  ...receiveOne({ content: _content  }) }
    ]

    const store = mockStore({ content: _content })
    return store.dispatch(actions.contentFetch({ pathname: '/contents/articles/1' }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('contentFetch should create CONTENT_SHOW_FETCH_FAILURE action when the CONTENT_SHOW_FETCH_REQUEST fails', () => {

    const resp = { err: { msg: 'Error Processing Request' } }
    nock('http://localhost:8080')
    .get('/v1/contents/articles/1')
    .reply(400,resp)

    const error = new ApiError({ status: 400, pathname: '/v1/contents/articles/1', method: 'get', payload: resp })
    const expectedActions = [
      { type: types.CONTENT_SHOW_FETCH_REQUEST  },
      { type: types.CONTENT_SHOW_FETCH_FAILURE, error }
    ]

    const store = mockStore({})
    return store.dispatch(actions.contentFetch({ pathname: '/contents/articles/1' }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })
})
