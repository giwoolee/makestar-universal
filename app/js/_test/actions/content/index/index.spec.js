import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/content/index'
import * as actions from '../../../../actions/content/index'
import nock from 'nock'
import contentsResponse from '../../../testHelpers/contentsResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveAll } from '../../../../schemas/content/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk, clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('content index actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('fetchContents should create a CONTENT_INDEX_FETCH_SUCCESS action when the response is ok', () => {
    const content = contentsResponse()
    const pagination = paginationResponse()
    nock('http://localhost:8080/')
    .get('/v1/contents/articles')
    .query({ page: 1 })
    .reply(200, {
      pagination,
      content: [content]
    })

    const expectedActions = [
      { type: types.CONTENT_INDEX_FETCH_REQUEST, query: { page: 1 } },
      { type: types.CONTENT_INDEX_FETCH_SUCCESS, ...receiveAll({ content: [content], pagination }), query: { page: 1 } }
    ]
    const store = mockStore({ contents: [] })
    return store.dispatch(actions.fetchContents({ pathname: '/contents/articles', query: { page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchContents should create a CONTENT_INDEX_FETCH_FAILURE action when the CONTENT_INDEX_FETCH_REQUEST action fails', () => {
    const resp = { error: { msg: 'Error Processing Request' } }
    nock('http://localhost:8080/')
    .get('/v1/contents/articles')
    .query({ page: 1 })
    .reply(400,resp)

    const error = new ApiError({ status: 400, method: 'get', payload: resp, pathname: 'http://localhost:8080/v1/contents/articles' })
    const expectedActions = [
      { type: types.CONTENT_INDEX_FETCH_REQUEST, query: { page: 1 } },
      { type: types.CONTENT_INDEX_FETCH_FAILURE, error, query: { page: 1 } }
    ]

    const store = mockStore({ contents: [] })
    return store.dispatch(actions.fetchContents({ pathname: '/contents/articles', query: { page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })
})

