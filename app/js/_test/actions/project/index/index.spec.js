import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/project/index'
import * as actions from '../../../../actions/project/index'
import nock from 'nock'
import projectResponse from '../../../testHelpers/projectResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveAll } from '../../../../schemas/project/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk,clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('projects index actions', () => {

  afterEach(() => {
    nock.cleanAll()
  })

  it('fetchProjects should create a PROJECT_INDEX_FETCH_SUCCESS action when the response is ok', () => {
    const resp = projectResponse()
    nock('http://localhost:8080/')
    .get('/v1/projects')
    .query({ status: 'Open', page: 1 })
    .reply(200, {
      pagination: paginationResponse(1),
      content: [resp]
    })

    const expectedActions = [
      { type: types.PROJECT_INDEX_FETCH_REQUEST, query: { status: 'Open', page: 1 }  },
      { type: types.PROJECT_INDEX_FETCH_SUCCESS, ...receiveAll({ content: [resp], pagination: paginationResponse(1) }), query: { status: 'Open', page: 1 } }
    ]

    const store = mockStore({ projects: [] })
    return store.dispatch(actions.fetchProjects({ pathname: '/projects', query: { status: 'Open', page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchProjects should create a PROJECT_INDEX_FETCH_FAILURE action when the response is not ok', () => {
    const resp = { error: { msg: 'Error processing request' } }
    nock('http://localhost:8080/')
    .get('/v1/projects')
    .query({ status: 'All', page: 1 })
    .reply(400,resp)

    const error = new ApiError({ pathname: 'http://localhost:8080/v1/projects', status: 400, method: 'get', payload: resp })
    const expectedActions = [
      { type: types.PROJECT_INDEX_FETCH_REQUEST, query: { status: 'All', page: 1 } },
      { type: types.PROJECT_INDEX_FETCH_FAILURE, error, query: { status: 'All', page: 1 } }
    ]

    const store = mockStore({ projects: [] })
    return store.dispatch(actions.fetchProjects({ pathname: '/projects', query: { status: 'All', page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })
})
