import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/project/update/index'
import * as actions from '../../../../actions/project/update/index'
import nock from 'nock'
import projectUpdateResponse from '../../../testHelpers/projectUpdateResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import ApiClient from '../../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../../middleware/clientMiddleware'
import ApiError from '../../../../helpers/api/ApiError'
import { receiveAll } from '../../../../schemas/project/update/index'

const client = new ApiClient(superagent)
const middlewares = [ thunk,clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)


describe('project update actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('fetchProjectUpdates should create a PROJECT_UPDATE_INDEX_FETCH_SUCCESS action when the response is ok', () => {
    const resp = projectUpdateResponse()
    nock('http://localhost:8080/')
    .get('/v1/projects/updates')
    .query({ page: 1 })
    .reply(200, {
      pagination: paginationResponse(1),
      content: [resp]
    })

    const expectedActions = [
      { type: types.PROJECT_UPDATE_INDEX_FETCH_REQUEST, query: { page: 1 } },
      { type: types.PROJECT_UPDATE_INDEX_FETCH_SUCCESS, ...receiveAll({content: [resp], pagination: paginationResponse(1) }), query: { page: 1 } }
    ]

    const store = mockStore({ projectUpdates: [] })
    return store.dispatch(actions.fetchProjectUpdates({ pathname: '/projects/updates', query: { page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('fetchProjectUpdates should create a PROJECT_UPDATE_INDEX_FETCH_FAILURE action when the response is not ok', () => {
    const resp = { error: { msg: 'Error processing request' } }
    nock('http://localhost:8080/')
    .get('/v1/projects/updates')
    .query({ page: 1 })
    .reply(400,resp)

    const error = new ApiError({ pathname: '/v1/projects/updates', status: 400, method: 'get', payload: resp })
    const expectedActions = [
      { type: types.PROJECT_UPDATE_INDEX_FETCH_REQUEST, query: { page: 1 } },
      { type: types.PROJECT_UPDATE_INDEX_FETCH_FAILURE, error, query: { page: 1 } }
    ]

    const store = mockStore({ projectUpdates: [] })
    return store.dispatch(actions.fetchProjectUpdates({ pathname: '/projects/updates', query: { page: 1 } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })
})
