import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/shared/sns/'
import * as actions from '../../../../actions/shared/sns/'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('actions/shared/sns', () => {
  it('creates an SNS_SHARE', () => {
    const expectedActions = [
      { type: types.SNS_SHARE, gaData: { category: 'Poll Detail', action: 'SNS', label: '1 Facebook' } }
    ]

    const store = mockStore({})
    store.dispatch(actions.snsShare({ category: 'Poll Detail', idx: 1, sns: 'Facebook' }))
    expect(store.getActions()).toEqual(expectedActions)
  })

  it('creates an SNS_REFRESH action', () => {
    const expectedActions = [
      { type: types.SNS_REFRESH, key: 'content', content: { a: 'b' } }
    ]

    const state = {
      content: {
        content: { a: 'b' }
      }
    }
    const fakeGetState = expect.createSpy().andReturn(state)
    const store = mockStore(state)
    store.dispatch(actions.snsRefresh({ key: 'content', getState: fakeGetState }))
    expect(store.getActions()).toEqual(expectedActions)
  })
})
