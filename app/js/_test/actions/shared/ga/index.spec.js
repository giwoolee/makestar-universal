import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../../constants/shared/ga/'
import * as actions from '../../../../actions/shared/ga/'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('actions/shared/ga', () => {
  it('create a GA_EVENT', () => {
    const gaData = { category: 'Home Polls', action: 'View', label: 'Poll' }
    const expectedActions = [
      { type: types.GA_EVENT, gaData }
    ]

    const store = mockStore({})
    store.dispatch(actions.gaEvent(gaData))
    expect(store.getActions()).toEqual(expectedActions)
  })
})
