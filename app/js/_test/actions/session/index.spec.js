import expect from 'expect'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as types from '../../../constants/session/index'
import * as actions from '../../../actions/session/index'
import nock from 'nock'
import sessionResponse from '../../testHelpers/sessionResponse'
import ApiClient from '../../../helpers/ApiClient'
import superagent from 'superagent'
import clientMiddleware from '../../../middleware/clientMiddleware'
import ApiError from '../../../helpers/api/ApiError'

const client = new ApiClient(superagent)
const middlewares = [ thunk, clientMiddleware(client) ]
const mockStore = configureMockStore(middlewares)

describe('session actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('refreshContent should create a SESSION_LOGIN_REFRESH_CONTENT action', () => {
    const store = mockStore({ session: { } })
    store.dispatch(actions.refreshContent())
    expect(store.getActions()[0]).toEqual({ type: types.SESSION_LOGIN_REFRESH_CONTENT })
  })

  it('login should create a SESSION_LOGIN_SUCCESS action when the response is ok', () => {
    nock('http://localhost:8080')
    .post('/v1/user/me')
    .reply(200,{ content: sessionResponse() })

    const expectedActions = [
      { type: types.SESSION_LOGIN_REQUEST },
      { type: types.SESSION_LOGIN_SUCCESS, content: sessionResponse()  }
    ]
    const store = mockStore({ session: { } })
    return store.dispatch(actions.login({ pathname: '/user/me', data: { email: 'email@email.com', passwd: 'passwd' } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  it('login should create a SESSION_LOGIN_FAILURE action when the response is no ok', () => {
    const resp = { error: { msg: 'Error Processing Request' } }
    nock('http://localhost:8080')
    .post('/v1/user/me')
    .reply(402,resp)

    const error = new ApiError({ method: 'post', status: 402, path: '/v1/user/me', payload: resp })
    const expectedActions = [
      { type: types.SESSION_LOGIN_REQUEST },
      { type: types.SESSION_LOGIN_FAILURE, error }
    ]

    const store = mockStore({ session: {} })
    return store.dispatch(actions.login({ pathname: '/user/me', data: { email: 'email@email.com', passwd: 'passwd' } }))
    .then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions[1])
    })
  })

  context('when locale is present in the query string', () => {
    it('fetchSession should create a SESSION_FETCH_SUCCESS action when the response is ok', () => {

      nock('http://localhost:8080')
      .get('/v1/user/me?locale=ko')
      .reply(200,{ content: sessionResponse() })

      const expectedActions = [
        { type: types.SESSION_FETCH_REQUEST },
        { type: types.SESSION_FETCH_SUCCESS, content: sessionResponse()  }
      ]
      const store = mockStore({ session: {} })
      return store.dispatch(actions.fetchSession({ pathname: '/user/me', query: { locale: 'ko' } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('fetchSession should create a SESSION_FETCH_FAILURE action when the response is not ok', () => {

      const resp = { error: { msg: 'Error Processing Request' } }
      nock('http://localhost:8080')
      .get('/v1/user/me?locale=ko')
      .reply(400,resp)

      const error = new ApiError({ method: 'get', status: 400, path: '/v1/user/me', payload: resp })
      const expectedActions = [
        { type: types.SESSION_FETCH_REQUEST },
        { type: types.SESSION_FETCH_FAILURE, error }
      ]
      const store = mockStore({ session: {} })
      return store.dispatch(actions.fetchSession({ pathname: '/user/me', query: { locale: 'ko' } }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })

  context('when locale is not present in the query string', () => {
    it('fetchSession should create a SESSION_FETCH_SUCCESS action when the response is ok', () => {

      nock('http://localhost:8080')
      .get('/v1/user/me')
      .reply(200,{ content: sessionResponse() })

      const expectedActions = [
        { type: types.SESSION_FETCH_REQUEST },
        { type: types.SESSION_FETCH_SUCCESS, content: sessionResponse() }
      ]
      const store = mockStore({ session: {} })
      return store.dispatch(actions.fetchSession({ pathname: '/user/me' }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })

    it('fetchSession should create a SESSION_FETCH_FAILURE action when the response is not ok', () => {

      const resp = { error: { msg: 'Error Processing Request' } }
      nock('http://localhost:8080')
      .get('/v1/user/me')
      .reply(400,resp)

      const error = new ApiError({ method: 'get', status: 400, path: '/v1/user/me', payload: resp })
      const expectedActions = [
        { type: types.SESSION_FETCH_REQUEST },
        { type: types.SESSION_FETCH_FAILURE, error: error }
      ]
      const store = mockStore({ session: {} })
      return store.dispatch(actions.fetchSession({ pathname: '/user/me' }))
      .then(() => {
        expect(store.getActions()[1]).toEqual(expectedActions[1])
      })
    })
  })
})
