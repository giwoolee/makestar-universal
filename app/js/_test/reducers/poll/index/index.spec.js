import expect from 'expect'
import polls from '../../../../reducers/poll/index'
import * as types from '../../../../constants/poll/index'
import { POLL_SHOW_FETCH_SUCCESS, POLL_VOTE_SUCCESS } from '../../../../constants/poll/show'
import { INDEX_FETCH_SUCCESS } from '../../../../constants/index/index'
import pollResponse from '../../../testHelpers/pollResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import indexResponse from '../../../testHelpers/indexResponse'
import { receiveAll, receiveOne } from '../../../../schemas/poll/index'
import ApiError from '../../../../helpers/api/ApiError'

function setup () {
  const poll = pollResponse()
  return {
    poll
  }
}

describe('reducers', () => {
  describe('polls', () => {
    it('should handle the initial state', () => {
      expect(polls(undefined,{ type: 'NOT_A_TYPE' })).toEqual({ polls: {}, ranks: {}, idxs: [], isFetching: false, selectedStatus: 'All' })
    })

    it('should handle POLL_INDEX_FETCH_REQUEST', () => {

      expect(polls({ polls: {}, ranks: {}, idxs: [], selectedStatus: 'All', isFetching: true }, {
        type: types.POLL_INDEX_FETCH_REQUEST,
        query: { page: 1, status: 'All' }
        })
      ).toEqual({
        polls: {},
        idxs: [],
        ranks: {},
        isFetching: true,
        selectedStatus: 'All'
      })
    })

    it('should handle POLL_INDEX_FETCH_SUCCESS', () => {
      const { poll } = setup()
      expect(polls({ polls: [], isFetching: true, selectedStatus: 'All' }, {
        type: types.POLL_INDEX_FETCH_SUCCESS,
        ...receiveAll({ content: [poll] }),
        query: { page: 1, status: 'All' }
        })
      ).toEqual({
        polls:{ [poll.idx]: Object.assign({},poll,{ rank: [1,2,3,4,5]  }) },
        ranks: { 1: poll.rank[0], 2: poll.rank[1], 3: poll.rank[2], 4: poll.rank[3], 5: poll.rank[4] },
        idxs: [poll.idx],
        isFetching: false,
        selectedStatus: 'All'
      })
    })

    it('should handle POLL_INDEX_FETCH_FAILURE', () => {
      const { poll } = setup()
      const error = new ApiError({ payload: { } })
      expect(polls({ polls: { [poll.idx]: poll }, idxs: [poll.idx], isFetching: true }, {
        type: types.POLL_INDEX_FETCH_FAILURE,
        error,
        query: { page: 1, status: 'All' }
        })
      ).toEqual({
        isFetching: false,
        polls: { [poll.idx]: poll },
        idxs: [poll.idx],
        error
      })
    })

    it('should handle POLL_SHOW_FETCH_SUCCESS', () => {
      const { poll } = setup()
      expect(polls({ polls: {}, idxs: [], isFetching:false }, {
        type: POLL_SHOW_FETCH_SUCCESS,
        ...receiveOne({ content: poll })
      })).toEqual({
        isFetching: false,
        polls: { [poll.idx]: Object.assign({},poll,{ rank: [1,2,3,4,5] }) },
        ranks: { 1: poll.rank[0], 2: poll.rank[1], 3: poll.rank[2], 4: poll.rank[3], 5: poll.rank[4] },
        idxs: []
      })
    })

    it('should handle POLL_VOTE_SUCCESS', () => {
      const { poll } = setup()
      const pollBefore = Object.assign({},poll, { rank: [{ idx: 1, imageUrl: 'image', myVoteCount: 1 }] })
      const pollAfter = Object.assign({},poll, { rank: [{ idx: 1, imageUrl: 'image', myVoteCount: 2 }] })

      expect(polls({ ranks: { 1: { idx: 1, imageUrl: 'image', myVoteCount: 1 }, 2: { idx: 2, imageUrl: 'image', myVoteCount: 1 } }, isFetching: true },{
        type: POLL_VOTE_SUCCESS,
        ...receiveOne({ content: pollAfter })
      })).toEqual({
        isFetching: false,
        ranks: {
          1: { idx: 1, imageUrl: 'image', myVoteCount: 2 },
          2: { idx: 2, imageUrl: 'image', myVoteCount: 1 }
        }
      })
    })
  })
})
