import expect from 'expect'
import poll from '../../../../reducers/poll/show'
import * as types from '../../../../constants/poll/show'
import pollResponse from '../../../testHelpers/pollResponse'
import { receiveOne } from '../../../../schemas/poll/index'
import ApiError from '../../../../helpers/api/ApiError'

function setup() {
  const pollRes = pollResponse()

  return {
    pollRes
  }
}

describe('reducers', () => {
  describe('poll', () => {

    it('should handle the initial state', () => {
      expect(poll({ idx: undefined, isFetching: true }, { type: types.POLL_SHOW_FETCH_REQUEST, id: 1})).toEqual({ idx: undefined, isFetching: true })
    })

    it('should handle POLL_SHOW_FETCH_REQUEST', () => {
      const { pollRes } = setup()
      expect(poll({
        isFetching: false
      },{
        type: types.POLL_SHOW_FETCH_REQUEST,
        idx:pollRes.idx
      })).toEqual({
        isFetching: true,
      })
    })

    it('should handle POLL_SHOW_FETCH_SUCCESS', () => {
      const { pollRes } = setup()
      expect(poll({
        idx: undefined, isFetching: true
      },{
        type: types.POLL_SHOW_FETCH_SUCCESS,
        ...receiveOne({ content: pollRes })
      })).toEqual({
        idx: pollRes.idx,
        isFetching: false
      })
    })

    it('should handle POLL_SHOW_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: 'Error while fetching poll' })
      expect(poll({
        idx: undefined,
        isFetching: true
      },{
        type: types.POLL_SHOW_FETCH_FAILURE,
        error
      })).toEqual({
        isFetching: false,
        idx: undefined,
        error,
      })
    })
  })
})
