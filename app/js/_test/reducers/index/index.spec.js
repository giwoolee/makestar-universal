import expect from 'expect'
import index from '../../../reducers/index/index'
import { INDEX_FETCH_REQUEST, INDEX_FETCH_SUCCESS, INDEX_FETCH_FAILURE } from '../../../constants/index/index'
import indexResponse from '../../testHelpers/indexResponse'
import ApiError from '../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('index/index', () => {
    it('should handle the initial state', () => {
      expect(index(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        contents: [],
        polls: [],
        projects: [],
        error: null,
        isFetching: false
      })
    })

    it('should handle INDEX_FETCH_REQUEST', () => {
      expect(index({ contents: [], polls: [], projects: [], isFetching: false, error: undefined },{
        type: INDEX_FETCH_REQUEST
      })).toEqual({
        contents: [],
        polls: [],
        projects: [],
        isFetching: true,
        error: null
      })
    })

    it('should handle INDEX_FETCH_SUCCESS', () => {
      const indexRes = indexResponse()
      expect(index({ contents: [], polls: [], projects: [], isFetching: true, error: undefined },{
        type: INDEX_FETCH_SUCCESS,
        content: indexRes,
      })).toEqual({
        contents: { contents: indexRes.articles },
        polls: { polls: indexRes.polls },
        projects: { projects: indexRes.projects },
        isFetching: false,
        error: null
      })
    })

    it('should handle INDEX_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: {} })
      expect(index({ contents: [], polls: [], projects: [], isFetching: true, error: undefined },{
        type: INDEX_FETCH_FAILURE,
        error
      })).toEqual({
        contents: [],
        polls: [],
        projects: [],
        error,
        isFetching: false
      })
    })
  })
})
