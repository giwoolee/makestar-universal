import expect from 'expect'
import session from '../../../reducers/session/index'
import * as types from '../../../constants/session/index'
import sessionResponse from '../../testHelpers/sessionResponse'
import ApiError from '../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('session', () => {
    it('should handle the initial state', () => {
      expect(session(undefined,{ session: { isLoggedIn: false } })).toEqual({ session: { isLoggedIn: false, locale: 'ko' } })
    })

    it('should handle SESSION_FETCH_SUCCESS', () => {
      const res = sessionResponse()
      expect(session({ session: { isLoggedIn: false, locale: 'en' } },{ content: res, type: types.SESSION_FETCH_SUCCESS })).toEqual({
        session: res
      })
    })

    it('should handle SESSION_LOGIN_SUCCESS', () => {
      const res = sessionResponse()
      expect(session({ session: { isLoggedIn: false, locale: 'en' } }, { content: res, type: types.SESSION_LOGIN_SUCCESS })).toEqual({
        session: res
      })
    })

    it('should handle SESSION_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: { content: { isLoggedIn: false, locale: 'ko' } } })
      expect(session({ session: { isLoggedIn: false, locale: 'ko' } },{ type: types.SESSION_FETCH_FAILURE, error })).toEqual({
        session: { isLoggedIn: false, locale: 'ko' }
      })
    })
  })
})
