//import expect from 'expect'
//import meta from '../../../reducers/meta/index'
//import { POLL_SHOW_FETCH_SUCCESS } from '../../../constants/poll/show/index'
//import { POLL_INDEX_FETCH_SUCCESS } from '../../../constants/poll/index/index'
//import { SESSION_FETCH_SUCCESS, SESSION_FETCH_FAILURE } from '../../../constants/session/index'
//import { CONTENT_SHOW_FETCH_SUCCESS } from '../../../constants/content/show/index'
//import { SNS_REFRESH } from '../../../constants/metad/sns/index'
//import pollResponse from '../../testHelpers/pollResponse'
//import sessionResponse from '../../testHelpers/sessionResponse'
//import contentResponse from '../../testHelpers/contentsResponse'
//import ApiError from '../../../helpers/api/ApiError'
//
//describe('reducers', () => {
//  describe('meta', () => {
//    it('should handle the initial state', () => {
//      expect(meta(undefined, { meta: {} })).toEqual({ meta: {} })
//    })
//
//    it('should handle POLL_INDEX_FETCH_SUCCESS', () => {
//      let pollRes = pollResponse()
//      expect(meta({ meta: { locale: 'ko', title: 'Poll title' } }, { content: [pollRes], type: POLL_INDEX_FETCH_SUCCESS }))
//      .toEqual({
//        meta: {
//          defaultTitle: 'Makestar',
//          title: `${i18n.poll.title}`,
//          titleTemplate: 'Makestar - %s',
//          description: i18n.poll.subtitle,
//          locale: 'ko',
//          htmlAttributes: {{lang: "ko" }},
//
//        }
//      })
//    })
//
//    it('should handle SESSION_FETCH_SUCCESS', () => {
//      let sessionRes = sessionResponse()
//
//      expect(meta({ meta: {} },{ content: sessionRes, type: SESSION_FETCH_SUCCESS }))
//      .toEqual({
//        meta: {
//          locale: sessionRes.locale
//        }
//      })
//    })
//
//    context('when the SNS_REFRESH action\'s key is content', () => {
//      it('should refresh the meta data', () => {
//        let contentRes = contentResponse()
//        expect(meta({ meta: {locale: 'ko' } }, { content: contentRes, key: 'content', type: SNS_REFRESH }))
//        .toEqual({
//          meta: {
//            title: contentRes.subject,
//            description: contentRes.description,
//            canonical: `${contentRes.links[1].href}?locale=ko`,
//            snsimageUrl: contentRes.snsimageUrl,
//            locale: 'ko',
//            meta: {
//              property: {
//                'og:title': contentRes.subject,
//                'og:image': contentRes.snsimageUrl,
//                'twitter:title': contentRes.subject,
//                'twitter:site': '@Makestarcorp'
//              }
//            },
//            auto: {
//              ograph: true
//            }
//          }
//        })
//      })
//    })
//
//    context('when the SNS_REFRESH action\'s key is poll', () => {
//      const pollRes = pollResponse()
//      it('should refresh the meta data', () => {
//        expect(meta({ meta: { locale: 'ko' } }, { content: pollRes, key: 'poll', type: SNS_REFRESH }))
//        .toEqual({
//          meta: {
//            title: pollRes.title,
//            description: pollRes.title,
//            canonical: `${pollRes.links[1].href}?locale=ko`,
//            snsimageUrl: pollRes.snsimageUrl,
//            locale: 'ko',
//            meta: {
//              property: {
//                'og:title': pollRes.title,
//                'og:image': pollRes.snsimageUrl,
//                'twitter:title': pollRes.title,
//                'twitter:site': '@Makestarcorp'
//              }
//            },
//            auto: {
//              ograph: true
//            }
//          }
//        })
//      })
//    })
//
//    context('when a meta state doesn\'t exist',() => {
//      it('should handle SESSION_FETCH_FAILURE', () => {
//        let sessionRes = sessionResponse()
//        const error = new ApiError({ status: 401, pathname: '/v1/user/me', payload: { content: sessionRes }, method: 'get' })
//        expect(meta({ meta: {} },{ error, type: SESSION_FETCH_FAILURE }))
//        .toEqual({
//          meta: {
//            locale: sessionRes.locale
//          }
//        })
//      })
//    })
//
//    context('when a meta state exists', () => {
//      it('should handle a SESSION_FETCH_FAILURE', () => {
//        let sessionRes = sessionResponse()
//        const error = new ApiError({ status: 401, pathname: '/v1/user/me', payload: { content: sessionRes }, method: 'get' })
//        expect(meta({ meta: { title: 'Makestar', snsimageUrl: 'url', canonical: 'canonical', meta:{ property: {} } } },{ error, type: SESSION_FETCH_FAILURE }))
//        .toEqual({
//          meta: {
//            locale: 'ko',
//            title: 'Makestar',
//            snsimageUrl: 'url',
//            canonical: 'canonical',
//            meta: { property: {} }
//          }
//        })
//      })
//    })
//
//    it('should handle POLL_SHOW_FETCH_SUCCESS', () => {
//      let pollRes = pollResponse()
//      expect(meta({ meta: { locale: 'ko' } },{ content: pollRes, type: POLL_SHOW_FETCH_SUCCESS }))
//      .toEqual({
//        meta: {
//          title: pollRes.title,
//          description: pollRes.title,
//          canonical: `${pollRes.links[1].href}?locale=ko`,
//          snsimageUrl: pollRes.snsimageUrl,
//          locale: 'ko',
//          meta: {
//            property: {
//              'og:title': pollRes.title,
//              'og:image': pollRes.snsimageUrl,
//              'twitter:title': pollRes.title,
//              'twitter:site': '@Makestarcorp'
//            }
//          },
//          auto: {
//            ograph: true
//          }
//        }
//      })
//    })
//
//    it('should habdle CONTENT_SHOW_FETCH_SUCCESS', () => {
//      let contentRes = contentResponse()
//      expect(meta({ meta: { locale: 'ko' } },{ content: contentRes, type: CONTENT_SHOW_FETCH_SUCCESS }))
//      .toEqual({
//        meta: {
//          title: contentRes.subject,
//          description: contentRes.description,
//          canonical: `${contentRes.links[1].href}?locale=ko`,
//          snsimageUrl: contentRes.snsimageUrl,
//          locale: 'ko',
//          meta: {
//            property: {
//              'og:title': contentRes.subject,
//              'og:image': contentRes.snsimageUrl,
//              'twitter:title': contentRes.subject,
//              'twitter:site': '@Makestarcorp'
//            }
//          },
//          auto: {
//            ograph: true
//          }
//        }
//      })
//    })
//  })
//})
