import expect from 'expect'
import pagination from '../../../reducers/pagination'
import paginationResponse from '../../testHelpers/paginationResponse'
import { PROJECT_INDEX_FETCH_SUCCESS } from '../../../constants/project/index'
import { POLL_INDEX_FETCH_SUCCESS } from '../../../constants/poll/index'
import { PROJECT_UPDATE_INDEX_FETCH_SUCCESS } from '../../../constants/project/update/index'

describe('reducers',() => {
  describe('pagination', () => {
    it('should handle the initial state', () => {
      expect(pagination(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        isLast: undefined,
        currentPage: undefined,
        totalElements: undefined,
        totalPages: undefined
      })
    })

    it('should handle POLL_INDEX_FETCH_SUCCESS', () => {
      const paginationRes = paginationResponse()
      expect(pagination(undefined,{ type: POLL_INDEX_FETCH_SUCCESS, result: { pagination: paginationRes } })).toEqual(paginationRes)
    })

    it('should handle PROJECT_INDEX_FETCH_SUCCESS', () => {
      const paginationRes = paginationResponse()
      expect(pagination(undefined,{ type: POLL_INDEX_FETCH_SUCCESS, result: { pagination: paginationRes } })).toEqual(paginationRes)
    })

    it('should handle PROJECT_UPDATE_INDEX_FETCH_SUCCESS', () => {
      const paginationRes = paginationResponse()
      expect(pagination(undefined,{ type: PROJECT_UPDATE_INDEX_FETCH_SUCCESS, pagination: paginationRes })).toEqual(paginationRes)
    })
  })
})
