import expect from 'expect'
import contents from '../../../../reducers/content/index'
import * as types from '../../../../constants/content/index'
import { CONTENT_SHOW_FETCH_SUCCESS } from '../../../../constants/content/show/index'
import { receiveOne, receiveAll } from '../../../../schemas/content/index'
import contentResponse from '../../../testHelpers/contentsResponse'
import ApiError from '../../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('content/index', () => {
    it('should handle the initial state', () => {
      expect(contents(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        contents: {},
        idxs: [],
        isFetching: false
      })
    })

    it('should handle CONTENT_INDEX_FETCH_REQUEST', () => {
      expect(contents({ contents: {}, idxs: [], isFetching: false },{
        type: types.CONTENT_INDEX_FETCH_REQUEST,
        isFetching: true,
        query: { page: 1 }
      })).toEqual({
        contents: {},
        idxs: [],
        isFetching: true
      })
    })

    context('when there are no contents in the store', () => {
      it('should handle CONTENT_INDEX_FETCH_SUCCESS', () => {
        const content = contentResponse()
        expect(contents({ contents: {}, idxs: [] },{
          type: types.CONTENT_INDEX_FETCH_SUCCESS,
          isFetching: false,
          query: { page: 1 },
          ...receiveAll({ content: [content] })
        })).toEqual({
          contents: { [content.idx]: content },
          idxs: [content.idx],
          isFetching: false
        })
      })
    })

    context('when there are already contents in the store', () => {
      it('should handle CONTENT_INDEX_FETCH_SUCCESS', () => {
        const contentOne = contentResponse(1)
        const contentTwo = contentResponse(2)

        expect(contents({ contents: { [contentOne.idx]: contentOne }, idxs: [contentOne.idx] },{
          type: types.CONTENT_INDEX_FETCH_SUCCESS,
          isFetching: false,
          query: { page: 2 },
          ...receiveAll({ content: [contentTwo] })
        })).toEqual({
          contents: { [contentOne.idx]: contentOne, [contentTwo.idx]: contentTwo },
          idxs: [contentOne.idx,contentTwo.idx],
          isFetching: false,
        })
      })
    })

    it('should handle CONTENT_INDEX_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: {} })
      expect(contents({ contents: {}, idxs: [], isFetching: true }, {
        type: types.CONTENT_INDEX_FETCH_FAILURE,
        error
      })).toEqual({
        contents: {},
        idxs: [],
        isFetching: false,
        error
      })
    })

    it('should handle CONTENT_SHOW_FETCH_SUCCESS', () => {
      const content = contentResponse()
      expect(contents({ contents: {}, idxs: [], isFetching: false }, {
        type: CONTENT_SHOW_FETCH_SUCCESS,
        ...receiveOne({ content: content })
      })).toEqual({
        idxs: [],
        isFetching: false,
        contents: { [content.idx]: content },
      })
    })
  })
})
