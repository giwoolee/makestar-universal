import expect from 'expect'
import content from '../../../../reducers/content/show'
import * as types from '../../../../constants/content/show/'
import contentResponse from '../../../testHelpers/contentsResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import { receiveOne } from '../../../../schemas/content/index'
import ApiError from '../../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('content/show', () => {
    it('should handle the initial state', () => {
      expect(content(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        idx: undefined, isFetching: false, error: undefined
      })
    })

    it('should handle CONTENT_SHOW_FETCH_REQUEST', () => {
      expect(content({ idx: undefined, isFetching: false }, {
        type: types.CONTENT_SHOW_FETCH_REQUEST,
        id: 1
      })).toEqual({
        idx: undefined,
        isFetching: true
      })
    })

    it('should handle CONTENT_SHOW_FETCH_SUCCESS', () => {
      const _content = contentResponse()
      expect(content({ idx: undefined, isFetching: false }, {
        type: types.CONTENT_SHOW_FETCH_SUCCESS,
        ...receiveOne({ content: _content })
      })).toEqual({
        idx: _content.idx,
        isFetching: false
      })
    })

    it('should handle CONTENT_SHOW_FETCH_FAILURE', () => {

      const error = new ApiError({ status: 400, payload: {} })
      expect(content({ idx: undefined, isFetching: false }, {
         type: types.CONTENT_SHOW_FETCH_FAILURE,
         error
      })).toEqual({
        idx: undefined,
        error,
        isFetching: false
      })
    })
  })
})
