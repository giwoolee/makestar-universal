import expect from 'expect'
import { text } from '../../../reducers/comment/text'
import * as types from '../../../constants/comment/index'

describe('reducers',() => {
  describe('comments/text',() => {
    describe('when the argument to text is comment', () => {
      it('returns a function', () => {
        expect(text('comment',[types.COMMENT_TEXT_UPDATE,types.COMMENT_TEXT_CLEAR,types.COMMENT_CREATE_SUCCESS])).toBeA(Function)
      })

      it('handles the initial state', () => {
        const commentText = text('comment',[types.COMMENT_TEXT_UPDATE,types.COMMENT_TEXT_CLEAR,types.COMMENT_CREATE_SUCCESS])
        expect(commentText(undefined,{ type: 'NOT_A_TYPE' })).toEqual({})
      })

      it('handles COMMENT_TEXT_UPDATE', () => {
        const commentText = text('comment',[types.COMMENT_TEXT_UPDATE,types.COMMENT_TEXT_CLEAR,types.COMMENT_CREATE_SUCCESS])
        expect(commentText({},{ type: types.COMMENT_TEXT_UPDATE, idx: '1', text: 'Text' })).toEqual(
          {
            '1': 'Text'
          }
        )
      })

      it('handles COMMENT_TEXT_CLEAR', () => {
        const commentText = text('comment',[types.COMMENT_TEXT_UPDATE,types.COMMENT_TEXT_CLEAR,types.COMMENT_CREATE_SUCCESS])
        expect(commentText({ '1': 'text', '2': 'text' },{ type: types.COMMENT_TEXT_CLEAR, idx: '2' })).toEqual(
          {
            '1': 'text'
          }
        )
      })

      it('handles COMMENT_CREATE_SUCCESS', () => {
        const commentText = text('comment',[types.COMMENT_TEXT_UPDATE,types.COMMENT_TEXT_CLEAR,types.COMMENT_CREATE_SUCCESS])
        expect(commentText({ '1': 'text' },{ type: types.COMMENT_CREATE_SUCCESS, idx: '1' })).toEqual({})
      })
    })

    describe('when the arguments to text is reply', () => {
      it('returns a function', () => {
        expect(text('reply',[types.REPLY_TEXT_UPDATE,types.REPLY_TEXT_CLEAR,types.REPLY_CREATE_SUCCESS])).toBeA(Function)
      })

      it('handles the initial state',() => {
        const replyText = text('reply',[types.REPLY_TEXT_UPDATE,types.REPLY_TEXT_CLEAR,types.REPLY_CREATE_SUCCESS])
        expect(replyText(undefined,{ type: 'NOT_A_TYPE'})).toEqual({})
      })

      it('handles REPLY_TEXT_UPDATE', () => {
        const replyText = text('reply',[types.REPLY_TEXT_UPDATE,types.REPLY_TEXT_CLEAR,types.REPLY_CREATE_SUCCESS])
        expect(replyText({},{ type: types.REPLY_TEXT_UPDATE, idx: '1', text: 'Text' })).toEqual(
          {
            '1': 'Text'
          }
        )
      })

      it('handles REPLY_TEXT_CLEAR', () => {
        const replyText = text('reply',[types.REPLY_TEXT_UPDATE,types.REPLY_TEXT_CLEAR,types.REPLY_CREATE_SUCCESS])
        expect(replyText({ '1': 'text', '2': 'text' },{ type: types.REPLY_TEXT_CLEAR, idx: '1' })).toEqual(
          {
            '2': 'text'
          }
        )
      })

      it('handles REPLY_CREATE_SUCCESS', () => {
        const replyText = text('reply',[types.REPLY_TEXT_UPDATE,types.REPLY_TEXT_CLEAR,types.REPLY_CREATE_SUCCESS])
        expect(replyText({ '1': 'text' },{ type: types.REPLY_CREATE_SUCCESS, idx: '1' })).toEqual({})
      })
    })
  })
})
