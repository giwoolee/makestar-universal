import expect from 'expect'
import map from '../../../reducers/map/index'
import { INDEX_FETCH_SUCCESS } from '../../../constants/index/index'
import indexResponse from '../../testHelpers/indexResponse'

describe('reducers', () => {
  describe('map/index', () => {
    it('should handle the initial state', () => {
      expect(map(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        countries: []
      })
    })

    it('should handle INDEX_FETCH_SUCCESS', () => {
      const index = indexResponse()
      expect(map({ countries: [] },{ type: INDEX_FETCH_SUCCESS, content: index })).toEqual({
        countries: index.map
      })
    })
  })
})
