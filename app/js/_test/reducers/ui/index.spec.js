import expect from 'expect'
import ui from '../../../reducers/ui/index'
import { COMMENT_CREATE_FAILURE } from '../../../constants/comment/index'
import { POLL_VOTE_FAILURE, POLL_VOTE_CLOSED, POLL_VOTE_UNAVAILABLE } from '../../../constants/poll/show/index'
import { UI_ERROR_MODAL_RESET, UI_NOTIFICATION_RESET, UI_OPEN_TOP_NAV, UI_CLOSE_TOP_NAV } from '../../../constants/ui/index'
import { SESSION_LOGIN_SUCCESS } from '../../../constants/session/index'
import { GLOBAL_INITIAL_LOAD } from '../../../constants/global/index'
import sessionResponse from '../../testHelpers/sessionResponse'

describe('reducers', () => {
  describe('ui', () => {
    it('should handle the initial state', () => {
      expect(ui(undefined, { type: 'NOT_A_TYPE' })).toEqual({
        modal: { error: undefined, type: undefined, status: undefined },
        notification: { type: undefined, show: undefined },
        comments: {
          deletePromptShow: undefined
        },
        topNavOpen: false,
        isInitialLoad: true
      })
    })

    it('should handle GLOBAL_INITIAL_LOAD', () => {
      expect(ui({ modal: { error: undefined, type: undefined, status: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false, isInitialLoad: true }, { type: GLOBAL_INITIAL_LOAD })).toEqual({
        modal: { error: undefined, type: undefined, status: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false, isInitialLoad: false })
    })

    it('should handle UI_OPEN_TOP_NAV', () => {
      expect(ui({ modal: { error: undefined, status: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: UI_OPEN_TOP_NAV })).toEqual({
        modal: { error: undefined, status: undefined },
        notification: { type: undefined, show: undefined },
        topNavOpen: true
      })
    })

    it('should handle UI_CLOSE_TOP_NAV', () => {
      expect(ui({ modal: { error: undefined, status: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: true },{ type: UI_CLOSE_TOP_NAV })).toEqual({
        modal: { error: undefined, status: undefined },
        notification: { type: undefined, show: undefined },
        topNavOpen: false
      })
    })

    it('should handle COMMENT_CREATE_FAILURE', () => {
      expect(ui({ modal: { error: undefined, status: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: COMMENT_CREATE_FAILURE, error: { status: 401 } })).toEqual(
        {
          modal: {
            error: true,
            status: 401,
            type: COMMENT_CREATE_FAILURE,
          },
          notification: {
            type: undefined,
            show: undefined
          },
          topNavOpen: false
        }
      )
    })

    it('should handle POLL_VOTE_FAILURE', () => {
      expect(ui({ modal: { error: undefined, status: undefined, type: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: POLL_VOTE_FAILURE, error: { status: 401 } })).toEqual(
        {
          modal: {
            error: true,
            status: 401,
            type: POLL_VOTE_FAILURE,
          },
          notification: {
            type: undefined,
            show: undefined
          },
          topNavOpen: false
        }
      )
    })

    it('should handle POLL_VOTE_CLOSED', () => {
      expect(ui({ modal: { error: undefined, status: undefined, type: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: POLL_VOTE_CLOSED })).toEqual({
        modal: {
          error: undefined,
          status: undefined,
          type: undefined
        },
        notification: {
          type: POLL_VOTE_CLOSED,
          show: true
        },
        topNavOpen: false
      })
    })

    it('should handle POLL_VOTE_UNAVAILABLE', () => {
      expect(ui({ modal: { error: undefined, status: undefined, type: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false }, { type: POLL_VOTE_UNAVAILABLE })).toEqual({
        modal: {
          error: undefined,
          status: undefined,
          type: undefined
        },
        notification: {
          type: POLL_VOTE_UNAVAILABLE,
          show: true
        },
        topNavOpen: false
      })
    })

    it('should handle UI_ERROR_MODAL_RESET', () => {
      expect(ui({ modal: { error: true , status: 401, type: 'A_TYPE' }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: UI_ERROR_MODAL_RESET, error: undefined, status: undefined })).toEqual(
        {
          modal: {
            error: undefined,
            status: undefined,
            type: undefined,
          },
          notification: {
            type: undefined,
            show: undefined
          },
          topNavOpen: false
       })
    })
  })

  it('should handle SESSION_LOGIN_SUCCESS', () => {
    expect(ui({ modal: { error: undefined, status: undefined, type: undefined }, notification: { type: undefined, show: undefined }, topNavOpen: false },{ type: SESSION_LOGIN_SUCCESS })).toEqual({
      modal: {
        error: undefined,
        status: undefined,
        type: SESSION_LOGIN_SUCCESS,
      },
      notification: {
        type: SESSION_LOGIN_SUCCESS,
        show: true
      },
      topNavOpen: false
    })
  })

  it('should handle UI_NOTIFICATION_RESET', () => {
    expect(ui({ modal: { error: undefined, status: undefined, type: undefined }, notification: { type: SESSION_LOGIN_SUCCESS, show: true }, topNavOpen: false }, { type: UI_NOTIFICATION_RESET })).toEqual(
      {
        modal: {
          error: undefined,
          status: undefined,
          type: undefined
        },
        notification: {
          type: undefined,
          show: undefined
        },
        topNavOpen: false
      }
    )
  })
})
