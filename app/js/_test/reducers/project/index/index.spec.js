import expect from 'expect'
import projects from '../../../../reducers/project/index'
import * as types from '../../../../constants/project/index'
import { INDEX_FETCH_SUCCESS } from '../../../../constants/index/index'
import projectResponse from '../../../testHelpers/projectResponse'
import indexResponse from '../../../testHelpers/indexResponse'
import { receiveAll } from '../../../../schemas/project/index'
import ApiError from '../../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('projects', () => {
    it('should handle the initial state', () => {
      expect(projects(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        projects: {},
        idxs: [],
        isFetching: false, selectedStatus: 'All'
      })
    })

    it('should handle PROJECT_INDEX_FETCH_REQUEST', () => {
      expect(projects({ projects: {}, idxs: [], isFetching: false }, {
        type: types.PROJECT_INDEX_FETCH_REQUEST,
        query: { page: 1, status: 'All' }
      })).toEqual({
        projects: {},
        idxs: [],
        isFetching: true,
        selectedStatus: 'All'
      })
    })

    it('should handle PROJECT_INDEX_FETCH_SUCCESS', () => {
      const project = projectResponse()
      expect(projects({ projects: {}, idxs: [], isFetching: true, selectedStatus: 'All' }, {
        type: types.PROJECT_INDEX_FETCH_SUCCESS,
        query: { page: 1, status: 'All' },
        ...receiveAll({ content: [project] })
      })).toEqual({
        projects: { [project.idx]: project },
        idxs: [project.idx],
        isFetching: false,
        selectedStatus: 'All'
      })
    })

    it('should handle PROJECT_INDEX_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: {}})
      expect(projects({ projects: {}, idxs: [], isFetching: true }, {
        type: types.PROJECT_INDEX_FETCH_FAILURE,
        error,
        query: { page: 1, status: 'All' }
      })).toEqual({
        isFetching: false,
        projects: {},
        idxs: [],
        error
      })
    })
  })
})
