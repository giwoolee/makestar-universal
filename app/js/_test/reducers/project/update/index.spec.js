import expect from 'expect'
import projectUpdates from '../../../../reducers/project/update/index'
import * as types from '../../../../constants/project/update/index'
import projectUpdateResponse from '../../../testHelpers/projectUpdateResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import { receiveAll } from '../../../../schemas/project/update/index'
import ApiError from '../../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('projectUpdates', () => {
    it('should handle the initial state', () => {
      expect(projectUpdates(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        projectUpdates: {},
        idxs: [],
        isFetching: true
      })
    })

    it('should handle PROJECT_UPDATE_INDEX_FETCH_REQUEST', () => {
      expect(projectUpdates({ projectUpdates: {}, idxs: [], isFetching: false }, {
        type: types.PROJECT_UPDATE_INDEX_FETCH_REQUEST,
        query: { page: 1 }
      })).toEqual({
        projectUpdates: {},
        idxs: [],
        isFetching: true
      })
    })

    it('should handle PROJECT_UPDATE_INDEX_FETCH_SUCCESS', () => {
      const update = projectUpdateResponse()
      const pagination = paginationResponse()
      expect(projectUpdates({ projectUpdates: {}, idxs: [], isFetching: true }, {
        type: types.PROJECT_UPDATE_INDEX_FETCH_SUCCESS,
        query: { page: 1 },
        ...receiveAll({content: [update],
        pagination})
      })).toEqual({
        projectUpdates: { [update.idx]: update },
        idxs: [update.idx],
        isFetching: false,
      })
    })

    it('should handle PROJECT_UPDATE_INDEX_FETCH_FAILURE', () => {
      const error = new ApiError({ payload: {}})
      expect(projectUpdates({ projectUpdates: {}, idxs: [], isFetching: true },{
        type: types.PROJECT_UPDATE_INDEX_FETCH_FAILURE,
        error,
        query: { page: 1 }
      })).toEqual({
        isFetching: false,
        projectUpdates: {},
        idxs: [],
        error
      })
    })
  })
})
