import expect from 'expect'
import main from '../../../reducers/main/starMessage'
import { MAIN_STAR_MESSAGE_SHOW_VIDEO } from '../../../constants/main/starMessage'
import { LOCATION_CHANGE } from 'react-router-redux'

const setup = () => {
  return [
    {
      id: 0,
      artist: 'kim_woo_bin',
      showVideo: false,
    },
    {
      id: 1,
      artist: 'bangtan_boys',
      showVideo: false
    }
  ]
}

describe('reducers', () => {
  describe('main', () => {
    it('should handle the initial state', () => {
      const reducer = main(['kim_woo_bin','bangtan_boys'])
      const initialState = setup()
      expect(reducer(undefined, { type: 'NOT_A_TYPE' })).toEqual(initialState)
    })

    it('should handle MAIN_STAR_MESSAGE_SHOW_VIDEO', () => {
      const reducer = main(['kim_woo_bin'])
      expect(reducer([{ id: 0, artist: 'kim_woo_bin', showVideo: false }], { type: MAIN_STAR_MESSAGE_SHOW_VIDEO, artist: 'kim_woo_bin' })).toEqual([
        {
          id: 0,
          artist: 'kim_woo_bin',
          showVideo: true
        }
      ])
    })

    it('should handle LOCATION_CHANGE', () => {
      const reducer = main(['kim_woo_bin'])

        expect(reducer([{ id: 0, artist: 'kim_woo_bin', show: true }],{ type: LOCATION_CHANGE })).toEqual([{ id: 0, artist: 'kim_woo_bin', showVideo: false }])
    })
  })
})
