import expect from 'expect'
import topNotice from '../../../reducers/topNotice/index'
import { INDEX_FETCH_SUCCESS, INDEX_TOP_NOTICE_CLOSE } from '../../../constants/index/index'
import indexResponse from '../../testHelpers/indexResponse'

describe('reducers', () => {
  describe('topNotice', () => {
    it('should handle the initial state', () => {
      expect(topNotice(undefined,{ type: 'NOT_A_TYPE' })).toEqual(null)
    })

    it('should handle INDEX_FETCH_SUCCESS', () => {
      const res = indexResponse()
      expect(topNotice({ topNotice: null },{ type: INDEX_FETCH_SUCCESS, content: res })).toEqual({
        ...Object.assign({},res.topNotice, { closed: false })
      })
    })

    it('should handle INDEX_TOP_NOTICE_CLOSE', () => {
      const res = indexResponse()
      expect(topNotice({ topNotice: res.topNotice },{ type: INDEX_TOP_NOTICE_CLOSE })).toEqual({
        ...Object.assign({},res.topNotice, { closed: true })
      })
    })
  })
})
