import expect from 'expect'
import banners from '../../../reducers/banner/index'
import { INDEX_FETCH_SUCCESS } from '../../../constants/index/index'
import indexResponse from '../../testHelpers/indexResponse'

describe('reducers', () => {
  describe('banner/index', () => {
    it('should handle the initial state', () => {
      expect(banners(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        banners: {}
      })
    })

    it('should handle INDEX_FETCH_SUCCESS', () => {
      const index = indexResponse()
      expect(banners({ banners: {} }, { type: INDEX_FETCH_SUCCESS, content: index })).toEqual({
        banners: index.banners
      })
    })
  })
})
