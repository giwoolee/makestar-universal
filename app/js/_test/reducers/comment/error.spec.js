import expect from 'expect'
import error from '../../../reducers/comment/error'
import ApiError from '../../../helpers/api/ApiError'
import { COMMENT_FETCH_FAILURE, COMMENT_CREATE_FAILURE } from '../../../constants/comment/index'

describe('reducers',() => {
  describe('comments/error', () => {
    it('should handle the initial state', () => {
      expect(error(undefined,{ type: 'NOT_A_TYPE' })).toEqual(null)
    })

    it('should handle COMMENT_FETCH_FAILURE', () => {
      const err = new ApiError({ payload: {} })
      expect(error(undefined,{ type: COMMENT_FETCH_FAILURE, error: err })).toEqual(err)
    })

    it('should handle COMMENT_CREATE_FAILURE', () => {
      const err = new ApiError({ payload: {} })
      expect(error(undefined,{ type: COMMENT_CREATE_FAILURE, error: err })).toEqual(err)
    })
  })
})
