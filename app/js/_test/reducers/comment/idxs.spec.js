import expect from 'expect'
import idxs from '../../../reducers/comment/idxs'
import { COMMENT_FETCH_SUCCESS, COMMENT_CREATE_SUCCESS, COMMENT_CLEAR } from '../../../constants/comment/index'

describe('reducers', () => {
  describe('comments/idxs', () => {
    it('handles the initial state', () => {
      expect(idxs(undefined,{ type: 'NOT_A_TYPE' })).toEqual([])
    })

    context('when there are no idxs in the store', () => {
      it('handles COMMENT_FETCH_SUCCESS',() => {
        expect(idxs([],{ type: COMMENT_FETCH_SUCCESS, result: { content: [1] } })).toEqual([1])
      })
    })

    context('when there are idxs in the store', () => {
      it('handles COMMENT_FETCH_SUCCESS',() => {
        expect(idxs([1],{ type: COMMENT_FETCH_SUCCESS, result: { content: [2] } })).toEqual([1,2])
      })
    })

    it('handles COMMENT_CREATE_SUCCESS', () => {
      expect(idxs([1], { type: COMMENT_CREATE_SUCCESS, result: { content: 2 } })).toEqual([2,1])
    })

    it('handles COMMENT_CLEAR', () => {
      expect(idxs([1], { type: COMMENT_CLEAR })).toEqual([])
    })
  })
})
