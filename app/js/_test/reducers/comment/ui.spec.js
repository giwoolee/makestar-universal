import expect from 'expect'
import ui from '../../../reducers/comment/ui'
import {
  COMMENT_UI_DELETE_PROMPT_OPEN,
  COMMENT_UI_DELETE_PROMPT_CLOSE,
  COMMENT_UI_REPLY_OPEN,
  COMMENT_UI_REPLY_CLOSE,
  COMMENT_UI_EDIT_OPEN,
  COMMENT_UI_EDIT_CLOSE
} from '../../../constants/comment/index'

describe('reducers',() => {
  describe('comments/ui', () => {
    it('should handle the initial state', () => {
      expect(ui(undefined,{ type: 'NOT_A_TYPE' })).toEqual(
        {
          deletePrompt: {
            show: false,
            type: undefined,
            idx: undefined
          },
          replyOpen: [],
          editOpen: {
            comments: [],
            replies: []
          }
        }
      )
    })

    it('should handle COMMENT_UI_DELETE_PROMPT_OPEN', () => {
      expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_DELETE_PROMPT_OPEN, payload: { idx: 1, type: 'comments' }})).toEqual({
        deletePrompt: {
          show: true,
          idx: 1,
          type: 'comments'
        },
        replyOpen: [],
        editOpen: {
          comments: [],
          replies: []
        }
      })
    })

    it('should handle COMMENT_UI_DELETE_PROMPT_CLOSE', () => {
      expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_DELETE_PROMPT_CLOSE, payload: { idx: 1, type: 'commments' } })).toEqual({
        deletePrompt: {
          show: false,
          idx: undefined,
          type: undefined
        },
        replyOpen: [],
        editOpen: {
          comments: [],
          replies: []
        }
      })
    })

    it('should handle COMMENT_UI_REPLY_OPEN', () => {
      expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [2], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_REPLY_OPEN, payload: { idx: 1 } })).toEqual({
        deletePrompt: {
          show: false,
          idx: undefined,
          type: undefined
        },
        replyOpen: [2,1],
        editOpen: {
          comments: [],
          replies: []
        }
      })
    })

    it('should handle COMMENT_UI_REPLY_CLOSE', () => {
      expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_REPLY_CLOSE, payload: { idx: 1 } })).toEqual({
        deletePrompt: {
          show: false,
          idx: undefined,
          type: undefined
        },
        replyOpen: [],
        editOpen: {
          comments: [],
          replies: []
        }
      })
    })

    context('when payload.type is comments', () => {
      it('should handle COMMENT_UI_EDIT_OPEN', () => {
        expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_EDIT_OPEN, payload: { idx: 1, type: 'comments' } })).toEqual({
          deletePrompt: {
            show: false,
            idx: undefined,
            type: undefined
          },
          replyOpen: [],
          editOpen: {
            comments: [1],
            replies: []
          }
        })
      })

      it('should handle COMMENT_UI_EDIT_CLOSE', () => {
        expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_EDIT_CLOSE, payload: { idx: 1, type: 'comments' } })).toEqual({
          deletePrompt: {
            show: false,
            idx: undefined,
            type: undefined
          },
          replyOpen: [],
          editOpen: {
            comments: [],
            replies: []
          }
        })
      })
    })

    context('when payload.type is replies', () => {
      it('should handle COMMENT_UI_EDIT_OPEN', () => {
        expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [] } },{ type: COMMENT_UI_EDIT_OPEN, payload: { idx: 1, type: 'replies' } })).toEqual({
          deletePrompt: {
            show: false,
            idx: undefined,
            type: undefined
          },
          replyOpen: [],
          editOpen: {
            comments: [],
            replies: [1]
          }
        })
      })

      it('should handle COMMENT_UI_EDIT_CLOSE', () => {
        expect(ui({ deletePrompt: { show: false, idx: undefined, type: undefined }, replyOpen: [], editOpen: { comments: [], replies: [1] } },{ type: COMMENT_UI_EDIT_CLOSE, payload: { idx: 1, type: 'replies' } })).toEqual({
          deletePrompt: {
            show: false,
            idx: undefined,
            type: undefined
          },
          replyOpen: [],
          editOpen: {
            comments: [],
            replies: []
          }
        })
      })
    })
  })
})
