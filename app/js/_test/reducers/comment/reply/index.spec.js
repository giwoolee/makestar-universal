import expect from 'expect'
import replies from '../../../../reducers/comment/reply/index'
import * as types from '../../../../constants/comment/index'
import commentResponse from '../../../testHelpers/commentResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import { concat, uniqBy } from 'lodash'
import { receiveAll, receiveOne } from '../../../../schemas/comment'
import ApiError from '../../../../helpers/api/ApiError'

describe('reducers', () => {
  describe('comments/replies', () => {
    it('should handle the initial state', () => {
      expect(replies(undefined,{ type: 'NOT_A_TYPE' })).toEqual({
        error: undefined
      })
    })


    context('when there are no replies in the store', () => {
      it('should handle COMMENT_FETCH_SUCCESS', () => {
        const pagination = paginationResponse()
        const replyOne = Object.assign({},commentResponse(1),{ parentIdx: 1 })
        const replyTwo = Object.assign({},commentResponse(2),{ parentIdx: 2 })
        const commentOne = Object.assign({},commentResponse(1),{ replies: { pagination, content: [replyOne] } })
        const commentTwo = Object.assign({}, commentResponse(2), { replies: { pagination, content: [replyTwo] } })
        expect(replies({ error: undefined },{
          type: types.COMMENT_FETCH_SUCCESS,
          ...receiveAll({ content: [commentOne,commentTwo] })
        })).toEqual({
            [replyOne.idx]: replyOne,
            [replyTwo.idx]: replyTwo,
            error: undefined
          })
      })

      context('when the comment replies value is `null`',() => {
        it('should handle COMMENT_FETCH_SUCCESS', () => {
          const pagination = paginationResponse()
          const comment = Object.assign({}, commentResponse(1), { replies: null })

          expect(replies({ error: undefined }, {
            type: types.COMMENT_FETCH_SUCCESS,
            ...receiveAll({ content: [comment] }),
            error: undefined
          })).toEqual({
            error: undefined
          })
        })
      })
    })

    context('when there are replies in the store', () => {

      it('should handle REPLY_FETCH_SUCCESS', () => {
        const pagination = paginationResponse()
        const replyOne = Object.assign({},commentResponse(1),{ parentIdx: 1})
        const replyTwo = Object.assign({},commentResponse(2),{ parentIdx: 1})
        const comment = Object.assign({},commentResponse(1),{ replies: { pagination, content: [replyOne,replyTwo] } })
        expect(replies({
          [replyOne.idx]: replyOne,
          error: undefined
        },{
          type: types.REPLY_FETCH_SUCCESS,
          ...receiveAll({ content: [replyTwo], pagination }),
          idx: comment.idx
        })).toEqual({
          [replyOne.idx]: replyOne,
          [replyTwo.idx]: replyTwo,
          error: undefined
        })
      })

      it('should handle REPLY_CREATE_SUCCESS', () => {
        const pagination = paginationResponse()
        const replyOne = Object.assign({}, commentResponse(1), { parentIdx: 1 })
        const replyTwo = Object.assign({}, commentResponse(2), { parentIdx: 1 })
        const comment = Object.assign({}, commentResponse(1), { replies: { pagination, content: [replyOne] } })

        expect(replies({
          [replyOne.idx]: replyOne,
          error: undefined
        },{
          type: types.REPLY_CREATE_SUCCESS,
          idx: comment.idx,
          ...receiveOne({ content: replyTwo })
        })).toEqual({
          [replyOne.idx]: replyOne,
          [replyTwo.idx]: replyTwo,
          error: undefined
        })
      })

      it('should handle REPLY_DELETE_SUCCESS', () => {
        const pagination = paginationResponse()
        const replyOne = Object.assign({}, commentResponse(1), { parentIdx: 1 })
        const replyOneAfter = Object.assign({},replyOne,{ text: '', status: 'Deleted' })

        expect(replies({
          [replyOne.idx]: replyOne,
          error: undefined
        },{
          type: types.REPLY_DELETE_SUCCESS,
          idx: replyOne.idx,
          ...receiveOne({ content: replyOneAfter })
        })).toEqual({
          [replyOne.idx]: replyOneAfter,
          error: undefined
        })
      })

      it('should handle REPLY_UPDATE_SUCCESS', () => {
        const replyOne = commentResponse(1)
        const replyOneAfter = Object.assign({},replyOne, { text: 'Updated text' })
        expect(replies({ 1: replyOne , error: undefined },{ type: types.REPLY_UPDATE_SUCCESS, idx: replyOne.idx, ...receiveOne({ content: replyOneAfter }) })).toEqual({
          [replyOne.idx]: replyOneAfter,
          error: undefined
        })
      })
    })
  })
})

