import expect from 'expect'
import { COMMENT_FETCH_REQUEST, COMMENT_FETCH_SUCCESS, COMMENT_FETCH_FAILURE, COMMENT_CREATE_REQUEST, COMMENT_CREATE_SUCCESS, COMMENT_CREATE_FAILURE } from '../../../constants/comment/index'
import isFetching from '../../../reducers/comment/isFetching'

describe('reducers', () => {
  describe('comment/isFetching', () => {
    it('handles the initial state', () => {
      expect(isFetching(undefined,{ type: 'NOT_A_TYPE' })).toEqual(false)
    })

    it('handles the COMMENT_FETCH_REQUEST', () => {
      expect(isFetching(false,{ type: COMMENT_FETCH_REQUEST })).toEqual(true)
    })

    it('handles the COMMENT_FETCH_FAILURE', () => {
      expect(isFetching(true,{ type: COMMENT_FETCH_FAILURE })).toEqual(false)
    })

    it('handles the COMMENT_FETCH_SUCCESS', () => {
      expect(isFetching(true,{ type: COMMENT_FETCH_SUCCESS })).toEqual(false)
    })

    it('handles the COMMENT_CREATE_REQUEST', () => {
      expect(isFetching(false,{ type: COMMENT_CREATE_REQUEST })).toEqual(true)
    })

    it('handles the COMMENT_CREATE_FAILURE', () => {
      expect(isFetching(true,{ type: COMMENT_CREATE_FAILURE })).toEqual(false)
    })

    it('handles the COMMENT_CREATE_SUCCESS', () => {
      expect(isFetching(true,{ type: COMMENT_CREATE_SUCCESS })).toEqual(false)
    })
  })
})

