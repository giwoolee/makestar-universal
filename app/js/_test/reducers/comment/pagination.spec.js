import expect from 'expect'
import paginationResponse from '../../testHelpers/paginationResponse'
import { COMMENT_FETCH_SUCCESS } from '../../../constants/comment/index'
import pagination from '../../../reducers/comment/pagination'

describe('reducers', () => {
  describe('comment/pagination', () => {
    it('handles the initial state', () => {
      expect(pagination(undefined,{ type: 'NOT_A_TYPE' })).toEqual({})
    })

    it('handles the COMMENT_FETCH_SUCCESS', () => {
      const paginationRes = paginationResponse()
      expect(pagination({},{ type: COMMENT_FETCH_SUCCESS, result: { pagination: paginationRes } })).toEqual(paginationRes)
    })
  })
})
