import expect from 'expect'
import comments from '../../../reducers/comment/comments'
import { COMMENT_FETCH_SUCCESS, COMMENT_CREATE_SUCCESS, COMMENT_UPDATE_SUCCESS, COMMENT_DELETE_SUCCESS } from '../../../constants/comment/index'
import { REPLY_FETCH_SUCCESS, REPLY_CREATE_SUCCESS } from '../../../constants/comment/index'
import commentResponse from '../../testHelpers/commentResponse'
import paginationResponse from '../../testHelpers/paginationResponse'

describe('reducers', () => {
  describe('comments/comments', () => {
    it('handles the initial state', () => {
      expect(comments(undefined,{ type: 'NOT_A_TYPE' })).toEqual({ })
    })

    context('when there are no comments in the store', () => {
      it('handles COMMENT_FETCH_SUCCESS', () => {
        const comment = commentResponse()
        expect(comments({},{ type: COMMENT_FETCH_SUCCESS, entities: { comment: { 1: comment  } } })).toEqual({
          1: comment
        })
      })

      it('handles COMMENT_CREATE_SUCCESS', () => {
        const comment = commentResponse()
        expect(comments({}, { type: COMMENT_CREATE_SUCCESS, entities: { comment: { 1: comment } } })).toEqual({
          1: comment
        })
      })
    })

    context('when there are comments in the store', () => {
      it('handles COMMENT_FETCH_SUCCESS', () => {
        const comment = commentResponse()
        expect(comments({ 1: comment },{ type: COMMENT_FETCH_SUCCESS, entities: { comment: { 2: comment  } } })).toEqual({
          1: comment,
          2: comment
        })
      })

      it('handles COMMENT_CREATE_SUCCESS', () => {
        const comment = commentResponse()
        expect(comments({ 1: comment }, { type: COMMENT_CREATE_SUCCESS, entities: { comment: { 2: comment } } })).toEqual({
          1: comment,
          2: comment
        })
      })

      it('handles COMMENT_UPDATE_SUCCESS', () => {
        const comment = commentResponse()
        const updatedComment = Object.assign({},comment,{ text: 'New Updated text' })
        expect(comments({ 1: comment }, { type: COMMENT_UPDATE_SUCCESS, entities: { comment: { 1: updatedComment } } })).toEqual({
          1: updatedComment
        })
      })

      it('handles COMMENT_DELETE_SUCCESS', () => {
        const comment = commentResponse()
        const deletedComment = Object.assign({}, comment,{ text: "", status: "Deleted" })
        expect(comments({ 1: comment }, { type: COMMENT_DELETE_SUCCESS, entities: { comment: { 1: deletedComment } } })).toEqual({
          1: deletedComment
        })
      })

      it('handles REPLY_FETCH_SUCCESS', () => {
        const comment = Object.assign({},commentResponse(1),{ replies: {} })
        const pagination = paginationResponse()
        expect(comments({ 1: comment }, { type: REPLY_FETCH_SUCCESS, idx: 1, result: { pagination, content: [2] } })).toEqual({
          1: Object.assign({},comment,{ replies: { content: [2] }, pagination })
        })
      })

      it('handles REPLY_CREATE_SUCCESS', () => {
        const comment = commentResponse(1)
        expect(comments({ 1: comment }, { type: REPLY_CREATE_SUCCESS, idx: 1, result: { content: 2 } })).toEqual({
          1: Object.assign({},comment,{ replies: { content : [2] } })
        })
      })
    })
  })
})

