import expect from 'expect'
import paginationResponse from '../../testHelpers/paginationResponse'

import {
  getPagination,
  getPaginationIsLast,
  getPaginationNextPage,
  getPaginationCurrentPage,
  getPaginationTotalPages
} from '../../../selectors/pagination/index'

function setup(currentPage,isLast) {
  const pagination = paginationResponse(currentPage,isLast)
  const state = {
    pagination
  }

  return {
    pagination,
    state
  }
}

describe('selectors/pagination/index', () => {
  describe('getPagination', () => {
    it('returns the contents from the store', () => {
      const { state } = setup()
      expect(getPagination(state)).toEqual(state.pagination)
    })
  })

  describe('getPaginationIsLast', () => {
    it('returns the contents from the store', () => {
      const { state } = setup(true)
      expect(getPaginationIsLast(state)).toEqual(true)
    })
  })

  describe('getPaginationCurrentPage', () => {
    it('returns the contents from the store', () => {
      const { state } = setup(3)
      expect(getPaginationCurrentPage(state)).toEqual(3)
    })
  })

  describe('getPaginationNextPage', () => {
    it('returns the contents from the store', () => {
      const { state } = setup(3)
      expect(getPaginationNextPage(state)).toEqual(4)
    })
  })

  describe('getPaginationTotalPages', () => {
    it('returns the contents from the store', () => {
      const { state } = setup(1)
      expect(getPaginationTotalPages(state)).toEqual(1)
    })
  })
})
