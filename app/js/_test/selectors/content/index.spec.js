import expect from 'expect'
import contentResponse from '../../testHelpers/contentsResponse'
import paginationResponse from '../../testHelpers/paginationResponse'

import {
  getContents,
  getContentsIdxs,
  getContent,
  getContentIsFetching,
  getContentSns
} from '../../../selectors/content/index'

function setup() {
  const content = contentResponse()
  const pagination = paginationResponse(0)
  const state = {
    contents: {
      contents: {[content.idx]: content },
      idxs: [content.idx],
      isFetching: false,
      pagination
    }
  }

  return {
    content,
    pagination,
    state
  }
}

describe('selectors/content/index', () => {
  describe('getContents', () => {
    it('returns the contents from the store', () => {
      const { state } = setup()
      expect(getContents(state)).toEqual(state.contents.contents)
    })
  })

  describe('getContentsIdxs', () => {
    it('returns the idxs from the store', () => {
      const { state, content } = setup()
      expect(getContentsIdxs(state)).toEqual([content.idx])
    })
  })

  describe('getContent', () => {
    it('returns the content from the store', () => {
      const { state, content } = setup()
      expect(getContent(state,content.idx)).toEqual(content)
    })
  })

  describe('getContentIsFetching', () => {
    it('returns the isLast value from the store', () => {
      const { state } = setup()
      expect(getContentIsFetching(state)).toEqual(false)
    })
  })

  describe('getContentSns', () => {
    it('returns the correct data', () => {
      const { state, content } = setup()
      expect(getContentSns(state,content.idx)).toEqual(
        {
          idx: content.idx,
          title: content.subject,
          description: content.description,
          snsimageUrl: content.snsimageUrl,
          canonical: content.links[1].href
        })
    })
  })
})
