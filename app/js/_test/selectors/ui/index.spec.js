import expect from 'expect'
import { getIsInitialLoad } from '../../../selectors/ui/index'

describe('selectors/ui/index', () => {
  describe('getIsInitialLoad', () => {
    it('returns the value from the store', () => {
      const state = {
        ui: {
          isInitialLoad: false
        }
      }
      expect(getIsInitialLoad(state)).toEqual(state.ui.isInitialLoad)
    })
  })
})
