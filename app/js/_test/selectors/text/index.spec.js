import expect from 'expect'
import loremIpsum from 'lorem-ipsum'

import {
  getText,
  getTextLength,
  getTextIsOverLength
} from '../../../selectors/text/index'

function setup(textLength = 10) {
  let text
  if(textLength > 0) {
    text = loremIpsum({ count: textLength, units: 'words' })
  } else {
    text = ''
  }

  const state = {
    text: {
      comments: {
        1 : text
      },
      replies: {
        2: text
      }
    }
  }
  return { text, state }
}

describe('selectors/text/index', () => {
  describe('getText', () => {
    context('when type is comments', () => {
      it('returns the text',() => {
        const { state, text } = setup()
        expect(getText(state,1,'comments')).toEqual(text)
      })
    })

    context('when type is replies', () => {
      it('returns the text', () => {
        const { state, text } = setup()
        expect(getText(state,2,'replies')).toEqual(text)
      })
    })

    context('when there is no match', () => {
      it('returns and empty string', () => {
        const { state, text } = setup()
        expect(getText(state,1,'replies')).toEqual('')
      })
    })
  })

  describe('getTextLength', () => {
    it('returns the length of the text', () => {
      const { state, text } = setup()
      expect(getTextLength(state,1,'comments')).toEqual(text.length)
      expect(getTextLength(state,2,'replies')).toEqual(text.length)
      expect(getTextLength(state,2,'comments')).toEqual(0)
    })
  })

  describe('getTextIsOverLength', () => {
    context('when the text length is under 1000', () => {
      it('returns the false', () => {
        const { state } = setup(2)
        expect(getTextIsOverLength(state,1,'comments')).toEqual(false)
      })
    })
    context('when the text length is over 1000', () => {
      it('returns true', () => {
        const { state } = setup(250)
        expect(getTextIsOverLength(state,1,'comments')).toEqual(true)
      })
    })
  })
})

