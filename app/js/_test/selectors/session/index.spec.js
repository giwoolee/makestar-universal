import expect from 'expect'
import sessionResponse from '../../testHelpers/sessionResponse'
import {
  getSession,
  getIsLoggedIn,
  getLocale,
  getUserIdx,
  getUserEmail,
  getUserNickname,
  getUserProfileImageUrl,
  getLocaleDisplayName,
  getUserGrade
} from '../../../selectors/session/index'


function setup() {
  const session = sessionResponse()

  const state = {
    session: {
      session
    }
  }

  return {
    state,
    session
  }
}

describe('selectors/session/index', () => {
  describe('getSession', () => {
    it('returns the session from the store', () => {
      const { state, session } = setup()
      expect(getSession(state)).toEqual(session)
    })

    describe('getIsLoggedIn', () => {
      it('returns isLoggedIn from the store', () => {
        const { state, session } = setup()
        expect(getIsLoggedIn(state)).toEqual(session.isLoggedIn)
      })
    })

    describe('getLocale', () => {
      it('returns locale from the store', () => {
        const { state, session } = setup()
        expect(getLocale(state)).toEqual(session.locale)
      })
    })

    describe('getUserIdx', () => {
      it('returns the user\'s idx', () => {
        const { state, session } = setup()
        expect(getUserIdx(state)).toEqual(session.userIdx)
      })
    })

    describe('getUserProfileImageUrl', () => {
      it('returns the user\'s profileImageUrl', () => {
        const { state, session } = setup()
        expect(getUserProfileImageUrl(state)).toEqual(session.profileImageUrl)
      })
    })

    describe('getUserEmail', () => {
      it('returns the user\'s email', () => {
        const { state, session } = setup()
        expect(getUserEmail(state)).toEqual(session.email)
      })
    })

    describe('getUserNickname', () => {
      it('returns the user\'s nickname', () => {
        const { state, session } = setup()
        expect(getUserNickname(state)).toEqual(session.nickName)
      })
    })

    describe('getUserGrade', () => {
      it('returns the user\'s grade', () => {
        const { state, session } = setup()
        expect(getUserGrade(state)).toEqual(session.grade)
      })
    })

    describe('getLocaleDisplayName', () => {
      context('when locale is en', () => {
        it('returns the display name', () => {
          const { state, session } = setup()
          expect(getLocaleDisplayName({ session: { session: { locale: 'en' }}})).toEqual('English')
        })
      })
      context('when locale is ja', () => {
        it('returns the display name', () => {
          const { state, session } = setup()
          expect(getLocaleDisplayName({ session: { session: { locale: 'ja' }}})).toEqual('日本語')
        })
      })
      context('when locale is ko', () => {
        it('returns the display name', () => {
          const { state, session } = setup()
          expect(getLocaleDisplayName({ session: { session: { locale: 'ko' }}})).toEqual('한국어')
        })
      })
      context('when locale is zh', () => {
        it('returns the display name', () => {
          const { state, session } = setup()
          expect(getLocaleDisplayName({ session: { session: { locale: 'zh' }}})).toEqual('中文(简体)')
        })
      })
    })
  })
})

