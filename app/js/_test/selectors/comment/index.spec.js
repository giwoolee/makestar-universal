import expect from 'expect'
import moment from 'moment'
import commentResponse from '../../testHelpers/commentResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import {
  getComments,
  getCommentsIdxs,
  getComment,
  getCommentPagination,
  getCommentNextPage,
  getCommentIsLast,
  getCommentCreatedDate,
  getCommentUpdatedDate,
  getReplyCreatedDate,
  getReplyUpdatedDate,
  getCommentIsDeleted,
  getReplyIsDeleted
} from '../../../selectors/comment/index'

function setup(status="Visible") {
  const comment = Object.assign({}, commentResponse(), {status})
  const pagination = paginationResponse(0)
  const state = {
    comments: {
      comments: { [comment.idx]: comment },
      idxs: [comment.idx],
      isFetching: false,
      pagination
    },
    replies: {
      [comment.idx]: comment
    }
  }

  return {
    comment,
    pagination,
    state
  }
}

describe('selectors/comment/index', () => {
  describe('getComments', () => {
    it('returns the comments from the store', () => {
      const { state, comment } = setup()
      expect(getComments(state)).toEqual(state.comments.comments)
    })
  })

  describe('getCommentsIdxs', () => {
    it('returns the idxs from the store', () => {
      const { state, comment } = setup()
      expect(getCommentsIdxs(state)).toEqual([comment.idx])
    })
  })

  describe('getComment', () => {
    it('returns the comment from the store', () => {
      const { state, comment } = setup()
      expect(getComment(state,comment.idx)).toEqual(comment)
    })
  })

  describe('getCommentPagination', () => {
    it('returns the pagination from the store', () => {
      const { state, pagination } = setup()
      expect(getCommentPagination(state)).toEqual(pagination)
    })
  })

  describe('getCommentNextPage', () => {
    it('returns the nextPage from the store', () => {
      const { state } = setup()
      expect(getCommentNextPage(state)).toEqual(1)
    })
  })

  describe('getCommentIsLast', () => {
    it('returns the isLast value from the store', () => {
      const { state } = setup()
      expect(getCommentIsLast(state)).toEqual(true)
    })
  })

  describe('getCommentCreatedDate', () => {
    it('returns the date as a date object', () => {
      const { state, comment } = setup()
      const expected = moment(comment.createdDate).toDate()

      expect(getCommentCreatedDate(state,comment.idx)).toEqual(expected)
    })
  })

  describe('getReplyCreatedDate', () => {
    it('returns the reply createdDate as a date object', () => {
      const { state, comment } = setup()
      const expected = moment(comment.createdDate).toDate()

      expect(getReplyCreatedDate(state,comment.idx)).toEqual(expected)
    })
  })

  describe('getCommentUpdatedDate', () => {
    it('returns the comment updatedDate as a date object', () => {
      const { state, comment } = setup()
      const expected = moment(comment.updatedDate).toDate()

      expect(getReplyCreatedDate(state,comment.idx)).toEqual(expected)
    })
  })

  describe('getReplyUpdatedDate', () => {
    it('returns the reply updatedDate as a date object', () => {
      const { state, comment } = setup()
      const expected = moment(comment.updatedDate).toDate()

      expect(getReplyCreatedDate(state,comment.idx)).toEqual(expected)
    })
  })

  describe('getCommentIsDeleted', () => {
    it('returns true is comment is deleted', () => {
      const { state, comment } = setup("Deleted")

      expect(getCommentIsDeleted(state,comment.idx)).toEqual(true)
    })
  })

  describe('getReplyIsDeleted', () => {
    it('returns true is reply is deleted', () => {
      const { state, comment } = setup("Deleted")

      expect(getReplyIsDeleted(state,comment.idx)).toEqual(true)
    })
  })
})
