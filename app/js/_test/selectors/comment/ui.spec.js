import expect from 'expect'
import {
  getCommentUi,
  getIsCommentEditOpen,
  getIsCommentOpen,
  getIsReplyOpen,
  getIsReplyEditOpen,
  getCommentUiDeletePrompt,
  getCommentUiDeletePromptIdx
} from '../../../selectors/comment/ui'

function setup(show=true,replyOpen=[], editOpen={ comments: [], replies: [] }) {
  const state = {
    comments: {
      ui: {
        deletePrompt: { show, idx: 1 },
        editOpen,
        replyOpen,
      }
    }
  }

  return state
}

describe('selector/comment/ui', () => {
  describe('getCommentUi', () => {
    it('returns the comment ui state', () => {
      const state = setup()
      expect(getCommentUi(state)).toEqual(state.comments.ui)
    })
  })

  describe('getIsReplyOpen', () => {
    it('returns true if the reply is open for the comment', () => {
      const replyOpen = [1]
      const state = setup(true,replyOpen)

      expect(getIsReplyOpen(state,1)).toEqual(true)
      expect(getIsReplyOpen(state,2)).toEqual(false)
    })
  })

  describe('getIsCommentEdit', () => {
    it('returns true if the reply is being edited', () => {
      const editOpen =  {
        comments: [1],
        replies: []
      }

      const state = setup(true,[],editOpen)

      expect(getIsCommentEditOpen(state,1)).toEqual(true)
      expect(getIsCommentEditOpen(state,2)).toEqual(false)
    })
  })

  describe('getIsReplyEdit', () => {
    it('returns true if the reply is being edited', () => {
      const editOpen =  {
        comments: [],
        replies: [1]
      }

      const state = setup(true,[],editOpen)

      expect(getIsReplyEditOpen(state,1)).toEqual(true)
      expect(getIsReplyEditOpen(state,2)).toEqual(false)
    })
  })

  describe('getCommentUiDeletePrompt', () => {
    it('returns the deletePrompt props', () => {
      const show = true
      const state = setup(show)

      expect(getCommentUiDeletePrompt(state)).toEqual({ show: true, idx: 1 })
    })
  })

  describe('getCommentUiDeletePromptIdx', () => {
    it('returns the deletePrompt props', () => {
      const show = true
      const state = setup(show)

      expect(getCommentUiDeletePromptIdx(state)).toEqual(1)
    })
  })
})
