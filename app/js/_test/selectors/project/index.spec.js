import expect from 'expect'
import projectResponse from '../../testHelpers/projectResponse'
import moment from 'moment'

import {
  getProjects,
  getProjectIdxs,
  getProject,
  getProjectPercentage,
  getProjectUpdateDate,
  getChunkedProjectIdxs
} from '../../../selectors/project/index'

function setup() {
  const project = projectResponse()
  const state = {
    projects: {
      projects: { [project.idx]: project },
      idxs: [project.idx],
      isFetching: false
    }
  }

  return {
    project,
    state
  }
}

describe('selectors/project/index', () => {
  describe('getProjects', () => {
    it('returns projects from the store', () => {
      const { state } = setup()
      expect(getProjects(state)).toEqual(state.projects.projects)
    })
  })

  describe('getProjectIdxs', () => {
    it('returns idxs from the store', () => {
      const { state, project } = setup()
      expect(getProjectIdxs(state)).toEqual([project.idx])
    })
  })

  describe('getChunkedProjectIdxs', () => {
    it('returns chunked idxs', () => {
      const { state, project } = setup()

      expect(getChunkedProjectIdxs(state)).toEqual([[project.idx]])
    })
  })

  describe('getProject', () => {
    it('returns the project from store', () => {
      const { state, project } = setup()
      expect(getProject(state,project.idx)).toEqual(project)
    })
  })

  describe('getProjectPercentage', () => {
    it('returns the percentage', () => {
      const { state, project } = setup()
      expect(getProjectPercentage(state,project.idx)).toEqual(project.percent.toFixed(0))
    })
  })

  describe('getProjectTimeRemaining', () => {
    context('when the project is open', () => {
      it('returns the time remaining', () => {
      })
    })

    context('when the project is closed', () => {
      it('returns closed', () => {
      })
    })
  })
})
