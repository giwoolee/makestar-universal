import expect from 'expect'
import projectUpdateResponse from '../../../testHelpers/projectUpdateResponse'
import moment from 'moment'
import {
  getProjectUpdateIdxs,
  getProjectUpdate,
  getProjectUpdateDate,
  getProjectUpdateWebLink,
  getProjectUpdateTitle,
  getProjectUpdateIconImageUrl,
  getProjectUpdateShortName,
  getProjectUpdateImageUrl
} from '../../../../selectors/project/update/index'

function setup(longTitle = null) {
  let projectUpdate = projectUpdateResponse(1)

  if(longTitle) {
    projectUpdate = Object.assign({}, projectUpdate, { title: 'A long title with more than 25 characters' })
  }

  const state = {
    projectUpdates: {
      idxs: [projectUpdate.idx],
      projectUpdates: {
        [projectUpdate.idx]: projectUpdate
      },
      isFetching: false
    }
  }

  return {
    projectUpdate,
    state
  }
}

describe('selectors/project/update/index',() => {
  describe('getIdxs', () => {
    it('returns idxs from the store', () => {
      const { state } = setup()
      expect(getProjectUpdateIdxs(state)).toEqual(state.projectUpdates.idxs)
    })
  })

  describe('getProjectUpdate', () => {
    it('returns the projectUpdate', () => {
      const { state, projectUpdate } = setup()
      expect(getProjectUpdate(state,projectUpdate.idx)).toEqual(projectUpdate)
    })
  })

  describe('getProjectUpdateTitle', () => {
    context('when the length is greater than eqt 25', () => {
      it('returns the projectUpdateTitle', () => {
        const { state, projectUpdate } = setup(true)
        expect(getProjectUpdateTitle(state,projectUpdate.idx)).toEqual('A long title with more...')
      })
    })

    context('when the length is less than 25', () => {
      it('returns the projectUpdateTitle', () => {
        const { state, projectUpdate } = setup()
        expect(getProjectUpdateTitle(state,projectUpdate.idx)).toEqual(projectUpdate.title)
      })
    })
  })

  describe('getProjectUpdateDate', () => {
    it('returns the projectUpdateDate', () => {
      const { state, projectUpdate } = setup()
      const expected = moment(projectUpdate.date).toDate()
      expect(getProjectUpdateDate(state,projectUpdate.idx)).toEqual(expected)
    })
  })

  describe('getProjectUpdateWebLink', () => {
    it('returns the projectUpdate web link', () => {
      const { state, projectUpdate } = setup()
      expect(getProjectUpdateWebLink(state,projectUpdate.idx)).toEqual("http://www.makestar.co/project/diea/update/1/#tab")
    })
  })

  describe('getProjectUpdateShortName', () => {
    it('returns the projectUpdate shortTitle', () => {
      const { state, projectUpdate } = setup()
      expect(getProjectUpdateShortName(state,projectUpdate.idx)).toEqual(projectUpdate.projectShortName)
    })
  })

  describe('getProjectUpdateIconImageUrl', () => {
    it('returns the projectUpdate iconImageUrl', () => {
      const { state, projectUpdate } = setup()
      expect(getProjectUpdateIconImageUrl(state,projectUpdate.idx)).toEqual('url(/public/images/icon/index/update-placeholder.png)')
    })
  })

  describe('getProjectUpdateImageUrl', () => {
    it('returns the projectUpdate imageUrl', () => {
      const { state, projectUpdate } = setup()
      expect(getProjectUpdateImageUrl(state,projectUpdate.idx)).toEqual(`url(${projectUpdate.imageUrl})`)
    })
  })
})

