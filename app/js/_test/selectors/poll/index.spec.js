import expect from 'expect'
import normalizedPoll from '../../testHelpers/normalizedPoll'
import { take, chunk } from 'lodash'

import {
  getPolls,
  getPollIdxs,
  getPoll,
  getSelectedStatus,
  getChunkedPollIdxs,
  getPollRanks,
  getPollTopFiveRanks,
  getPollTopThreeRanks,
  getPollTopRank,
  getRankImageByIdx,
  getPollRankTwoChunk,
  getPollSns
} from '../../../selectors/poll/index'

function setup() {
  const poll = normalizedPoll()
  const state = {
    polls: {
      polls: { [poll.idx]: poll },
      idxs: [poll.idx],
      selectedStatus: 'All',
      isFetching: false,
      ranks: {
        1: { idx: 1, imageUrl: 'url' },2: { idx: 2, imageUrl: 'url' }
      }
    },
    ui: {
      isInitialLoad: false
    }
  }

  return {
    poll,
    state
  }
}

describe('selectors/poll/index', () => {
  describe('getPolls', () => {
    it('returns the polls from the store', () => {
      const { state } = setup()
      expect(getPolls(state)).toEqual(state.polls.polls)
    })
  })

  describe('getPollIdxs', () => {
    it('returns the idxs from the store', () => {
      const { state, poll } = setup()
      expect(getPollIdxs(state)).toEqual([poll.idx])
    })
  })

  describe('getPoll', () => {
    it('returns the poll from the store', () => {
      const { state, poll } = setup()

      expect(getPoll(state,poll.idx)).toEqual(poll)
    })
  })

  describe('getSelectedStatus', () => {
    it('returns the selectedStatus from the store', () => {
      const { state, poll } = setup()

      expect(getSelectedStatus(state)).toEqual('All')
    })
  })

  describe('getChunkedPollIdxs', () => {
    it('returns the chunkedIdxs', () => {
      const { state, poll } = setup()

      expect(getChunkedPollIdxs(state)).toEqual([[poll.idx]])
    })
  })

  describe('getPollRanks', () => {
    it('returns all the poll ranks', () => {
      const { state, poll } = setup()

      expect(getPollRanks(state,poll.idx)).toEqual(poll.rank)
    })
  })

  describe('getPollTopFiveRanks', () => {
    it('returns at most the top 5 ranks', () => {
      const { state, poll } = setup()

      expect(getPollTopFiveRanks(state,poll.idx)).toEqual(poll.rank)
    })
  })

  describe('getPollTopThreeRanks', () => {
    it('returns at most the top 3 ranks', () => {
      const { state, poll } = setup()

      expect(getPollTopThreeRanks(state,poll.idx)).toEqual(take(poll.rank,3))
    })
  })

  describe('getPollRankTwoChunk', () => {
    it('returns the ranks chunked by 2', () => {
      const { state, poll } = setup()

      expect(getPollRankTwoChunk(state,poll.idx)).toEqual(chunk(poll.rank,2))
    })
  })

  describe('getPollTopRank', () => {
    it('returns the top rank', () => {
      const { state, poll } = setup()

      expect(getPollTopRank(state,poll.idx)).toEqual(take(poll.rank,1))
    })
  })

  describe('getRankImageByIdx', () => {
    it('returns a rankImage', () => {
      const { state } = setup()

      expect(getRankImageByIdx(state,1)).toEqual(state.polls.ranks[1])
    })
  })

  describe('getPollSns', () => {
    it('returns the correct data', () => {
      const { state, poll } = setup()
      expect(getPollSns(state,poll.idx)).toEqual(
        {
          idx: poll.idx,
          title: poll.title,
          description: poll.title,
          snsimageUrl: poll.snsimageUrl,
          canonical: poll.links[1].href
        }
      )
    })
  })

})
