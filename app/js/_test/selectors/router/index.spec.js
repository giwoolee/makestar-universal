import expect from 'expect'
import routerResponse from '../../testHelpers/routerResponse'

import {
  getRouter,
  getRouterPathname,
  getRouterQuery
} from '../../../selectors/router/index'

function setup(pathname, query) {
  const router = routerResponse(pathname, query)
  const state = {
    routing: router
  }

  return {
    router,
    state
  }
}

describe('selectors/router/index', () => {
  describe('getRouter', () => {
    it('returns the correct props', () => {
      const { state } = setup()
      expect(getRouter(state)).toEqual(state.routing)
    })
  })

  describe('getRouterPathname', () => {
    it('returns the correct props', () => {
      const { state } = setup("/contents/article")
      expect(getRouterPathname(state)).toEqual("/contents/article")
    })
  })

  describe('getRouterQuery', () => {
    it('returns the correct props', () => {
      const { state } = setup({})
      expect(getRouterQuery(state)).toEqual({})
    })
  })
})
