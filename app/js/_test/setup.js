import { jsdom } from 'jsdom'
import { addLocaleData } from 'react-intl'

global.document = jsdom('<!doctype html><html><body></body></html>')
global.window = document.defaultView
global.navigator = global.window.navigator
global.Element = global.window.Element
global.HTMLElement = global.window.HTMLElement
global.getComputedStyle = global.window.getComputedStyle
//if(!global.Intl) {
global.Intl = require('intl')
addLocaleData(require('react-intl/locale-data/zh'))
addLocaleData(require('react-intl/locale-data/en'))
addLocaleData(require('react-intl/locale-data/ja'))
addLocaleData(require('react-intl/locale-data/ko'))
//}

