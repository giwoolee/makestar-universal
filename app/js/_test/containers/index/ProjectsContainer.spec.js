import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/ProjectsContainer'
import projectResponse from '../../testHelpers/projectResponse'
import { times } from 'lodash'

function setup(numToRender = 3,type = 'web') {
  const state = {
    index: {
      projects: {
        projects: times(numToRender,(i) => projectResponse(i+1)),
      }
    },
    ui: {
      isInitialLoad: false
    }
  }

  const ownProps = {
    type
  }

  return { state, ownProps }
}

describe('containers', () => {
  describe('index/ProjectsContainer', () => {
    context('when the number of projects is 3', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup()

        const expected = [
          [
            state.index.projects.projects[0],
            state.index.projects.projects[1],
            state.index.projects.projects[2]
          ]
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ projects: expected, type: 'web', isInitialLoad: state.ui.isInitialLoad })
      })
    })

    context('when the number of projects is 6', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup(6)

        const expected = [
          [
            state.index.projects.projects[0],
            state.index.projects.projects[1],
            state.index.projects.projects[2],
          ],
          [
            state.index.projects.projects[3],
            state.index.projects.projects[4],
            state.index.projects.projects[5],
          ]
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ projects: expected, type: 'web', isInitialLoad: state.ui.isInitialLoad })
      })
    })

    context('when the type is mobile', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup(3,'mobile')

        const expected = [
          [
            state.index.projects.projects[0],
            state.index.projects.projects[1],
            state.index.projects.projects[2]
          ]
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ projects: expected, type: 'mobile', isInitialLoad: state.ui.isInitialLoad })
      })
    })

    context('when the type is web', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup(3,'web')

        const expected = [
          [
            state.index.projects.projects[0],
            state.index.projects.projects[1],
            state.index.projects.projects[2]
          ]
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ projects: expected, type: 'web', isInitialLoad: state.ui.isInitialLoad })
      })
    })
  })
})
