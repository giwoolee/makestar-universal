import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/HelmetContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    }
  }

  return state
}

describe('containers',() => {
  describe('index/HelmetContainer',() => {
    it('returns the correct props', () => {
      const state = setup()

      const expected = {
        title: 'Make Your Own Star!',
        meta:[
          {
            'name': 'description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': 'http://'
          },
          {
            'rel': 'alternate', 'href': 'http://?locale=ko', 'hreflang':'ko'
          },
          {
            'rel': 'alternate', 'href': 'http://?locale=ja', 'hreflang':'ja'
          },
          {
            'rel': 'alternate', 'href': 'http://?locale=zh', 'hreflang':'zh'
          }
        ]
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
