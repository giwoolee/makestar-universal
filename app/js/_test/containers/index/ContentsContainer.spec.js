import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/ContentsContainer'
import contentsResponse from '../../testHelpers/contentsResponse'
import { times, slice, head, nth } from 'lodash'

function setup(numToRender = 3) {
  const state = {
    index: {
      contents: {
        contents: times(3,(i) => contentsResponse(i + 1))
      }
    },
    ui: {
      isInitialLoad: false
    }
  }

  const ownProps = {
    numToRender
  }

  return { state, ownProps }
}

describe('containers', () => {
  describe('index/ContentsContainer', () => {
    context('when the numToRender is 3', () => {
      it('sets the correct props',() => {
        const { state, ownProps } = setup()

        const expected = [
          Object.assign({},{ isLast: false },head(state.index.contents.contents)),
          Object.assign({},{ isLast: false },nth(state.index.contents.contents,1)),
          Object.assign({},{ isLast: true },nth(state.index.contents.contents,2))
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ contents: expected, isInitialLoad: state.ui.isInitialLoad })
      })
    })

    context('when the numToRender is 2', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup(2)

        const expected = [
          Object.assign({},{ isLast: false },head(state.index.contents.contents)),
          Object.assign({},{ isLast: true },nth(state.index.contents.contents,1))
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ contents: expected, isInitialLoad: state.ui.isInitialLoad })
      })
    })
  })
})
