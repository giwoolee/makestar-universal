import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/TopNoticeContainer'
import topNoticeResponse from '../../testHelpers/topNoticeResponse'

function setup() {
  const state = {
    topNotice: { ...topNoticeResponse() }
  }

  return state
}

describe('containers', () => {
  describe('index/TopNoticeContainer', () => {
    it('sets the props correctly', () => {
      const state = setup()
      expect(mapStateToProps(state)).toEqual({
        style: {
          background: 'red',
          color: 'white'
        },
        text: state.topNotice.text,
        iconImageUrl: state.topNotice.iconImageUrl,
        idx: state.topNotice.idx,
        linkUrl: state.topNotice.linkUrl,
        buttonText: state.topNotice.buttonText
      })
    })
  })
})
