import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/PollsContainer'
import pollResponse from '../../testHelpers/pollResponse'
import { times, slice, nth, head } from 'lodash'

function setup(numToRender = 3) {
  const state = {
    index: {
      polls: {
        polls: times(3,(i) => pollResponse(i+1)),
      }
    },
    ui: {
      isInitialLoad: false
    }
  }

  const ownProps = {
    numToRender
  }

  return { state, ownProps }
}

describe('containers', () => {
  describe('index/PollsContainer', () => {
    context('when the numToRender is 3', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup()

        const expected = [
          Object.assign({},{ isLast: false },head(state.index.polls.polls)),
          Object.assign({},{ isLast: false },nth(state.index.polls.polls,1)),
          Object.assign({},{ isLast: true },nth(state.index.polls.polls,2))
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ polls: expected, isInitialLoad: state.ui.isInitialLoad })
      })
    })

    context('when the numToRender is 2', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup(2)

        const expected = [
          Object.assign({}, { isLast: false },head(state.index.polls.polls)),
          Object.assign({}, { isLast: true },nth(state.index.polls.polls,1)),
        ]

        expect(mapStateToProps(state,ownProps)).toEqual({ polls: expected, isInitialLoad: state.ui.isInitialLoad })
      })
    })
  })
})
