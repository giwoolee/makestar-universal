import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/ProjectUpdatesContainer'
import projectUpdateResponse from '../../testHelpers/projectUpdateResponse'
import moment from 'moment'

function setup() {
  const projectUpdate = projectUpdateResponse(1)
  const state = {
    projectUpdates: {
      idxs: [1]
    },
    session: {
      session: {
        locale: 'en'
      }
    }
  }

  return state
}

describe('ProjectUpdatesContainer',() => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const state = setup()

      expect(mapStateToProps(state)).toEqual({
        idxs: state.projectUpdates.idxs
      })
    })
  })
})
