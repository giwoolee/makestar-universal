import expect from 'expect'
import { mapStateToProps } from '../../../containers/index/IndexContainer'
import indexResponse from '../../testHelpers/indexResponse'
import projectUpdateResponse from '../../testHelpers/projectUpdateResponse'

function setup() {
  const { projects, polls, articles, banners, map } = indexResponse()

  const state = {
    banners: { banners: banners },
    ui: { isInitialLoad: false },
    index: {
      projects: { projects: projects },
      polls: { polls: polls },
      contents: { contents: articles },
    },
    session: {
      session: { locale: 'en' },
    },
    map: { countries: map },
    projectUpdates: { projectUpdates: [projectUpdateResponse()] }
  }

  return state
}

describe('containers', () => {
  describe('IndexContainer', () => {
    it('sets the correct props', () => {
      const state = setup()

      expect(mapStateToProps(state)).toEqual({
        projects: state.index.projects.projects,
        isInitialLoad: state.ui.isInitialLoad,
        countries: state.map.countries,
        banners: state.banners.banners,
        projectUpdates: state.projectUpdates.projectUpdates,
        viewAllMessageId: 'en.common.viewAll',
        projectUpdateMessageId: 'en.project.update',
        projectTitleMessageId: 'en.project.title',
        pollUpdateMessageId: 'en.poll.update',
        contentLatestMessageId: 'en.content.latest'
      })
    })
  })
})
