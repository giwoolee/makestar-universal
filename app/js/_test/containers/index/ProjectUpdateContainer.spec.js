import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../containers/index/ProjectUpdateContainer'
import projectUpdateResponse from '../../testHelpers/projectUpdateResponse'
import moment from 'moment'

function setup() {
  const projectUpdate = projectUpdateResponse(1)
  const state = {
    projectUpdates: {
      idxs: [projectUpdate.idx],
      projectUpdates: {
        [projectUpdate.idx]: projectUpdate
      }
    },
    session: {
      session: {
        locale: 'en'
      }
    }
  }

  return {
    state,
    projectUpdate
  }
}

describe('ProjectUpdateContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, projectUpdate } = setup()

      expect(mapStateToProps(state, { idx: projectUpdate.idx })).toEqual({
        title: projectUpdate.title,
        link: projectUpdate.links[0].href,
        date: moment(projectUpdate.date).toDate(),
        iconImageUrl: 'url(/public/images/icon/index/update-placeholder.png)',
        imageUrl: `url(${projectUpdate.imageUrl})`,
        dateMessageId: 'en.project.date',
        projectShortName: projectUpdate.projectShortName
      })
    })
  })

  describe('mergeProps', () => {
    it('returns the merged props', () => {
      const { state, projectUpdate } = setup()
      const fakeDispatchProps = {
        actions: {
          gaEvent: expect.createSpy()
        }
      }
      const stateProps = mapStateToProps(state,{ idx: projectUpdate.idx })
      const mergedProps = mergeProps(stateProps,fakeDispatchProps)
      mergedProps.actions.onClick()
      expect(fakeDispatchProps.actions.gaEvent).toHaveBeenCalledWith({
        category: 'Home Updates',
        action: 'View',
        label: stateProps.title
      })
    })
  })
})
