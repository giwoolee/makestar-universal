import expect from 'expect'
import { mapStateToProps } from '../../../../containers/content/index/HelmetContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    }
  }

  return state
}


describe('containers',() => {
  describe('content/index/HelmetContainer',() => {
    it('returns the correct props', () => {
      const state = setup()

      const expected = {
        title: 'Magazine',
        meta:[
          {
            'name': 'description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': 'http:///contents/articles'
          },
          {
            'rel': 'alternate', 'href': 'http:///contents/articles?locale=ko', 'hreflang':'ko'
          },
          {
            'rel': 'alternate', 'href': 'http:///contents/articles?locale=ja', 'hreflang':'ja'
          },
          {
            'rel': 'alternate', 'href': 'http:///contents/articles?locale=zh', 'hreflang':'zh'
          }
        ]
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
