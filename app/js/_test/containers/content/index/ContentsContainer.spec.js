import expect from 'expect'
import { mapStateToProps } from '../../../../containers/content/index/ContentsContainer'

function setup() {
  const state = {
    routing: {
      locationBeforeTransitions: {
        pathname: '/contents'
      }
    },
    pagination: {
      isLast: true,
      currentPage: 0
    },
    contents: {
      idxs: [1,2],
      isFetching: false
    },
    session: {
      session: {
        locale: 'en'
      }
    },
    ui: {
      isInitialLoad: true
    }
  }
  return state
}

describe('containers/content/ContentsContainer', () => {
  it('returns the correct props', () => {
    const state = setup()
    const expected = {
      isFetching: false,
      idxs: [1,2],
      isLast: true,
      isInitialLoad: true,
      buttonMessageId: 'en.common.more',
      query: {
        page: 1,
      },
      pathname: '/contents'
    }

    expect(mapStateToProps(state)).toEqual(expected)
  })
})
