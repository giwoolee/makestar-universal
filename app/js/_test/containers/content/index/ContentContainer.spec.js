import expect from 'expect'
import contentResponse from '../../../testHelpers/contentsResponse'
import { mapStateToProps } from '../../../../containers/content/index/ContentContainer'

function setup() {
  const content = contentResponse()
  const state = {
    contents: {
      contents: {
        1: content
      }
    },
    ui: {
      isInitialLoad: false
    }
  }
  return {
    content,
    state
  }
}

describe('containers/content/index/ContentContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, content } = setup()
      const expected = {
        ...content,
        isInitialLoad: state.ui.isInitialLoad
      }

      expect(mapStateToProps(state,{ idx: 1 })).toEqual(expected)
    })
  })
})
