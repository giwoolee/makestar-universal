import expect from 'expect'
import contentResponse from '../../../testHelpers/contentsResponse'
import { mapStateToProps } from '../../../../containers/content/show/ContentContainer'

function setup(idx = 1) {
  const content = contentResponse()
  const state = {
    content: {
      idx,
    },
    contents: {
      contents: {
        1: content
      }
    }
  }

  return {
    content,
    state
  }
}

describe('containers/content/show/ContentContainer', () => {
  describe('mapStateToProps', () => {
    context('when idx exists in the store', () => {
      it('returns the correct props', () => {
        const { state, content } = setup()

        expect(mapStateToProps(state)).toEqual(content)
      })
    })

    context('when idx doesn\'t exist in the store', () => {
      it('returns the correct props', () => {
        const { state, content } = setup(2)

        expect(mapStateToProps(state)).toEqual({
          idx: undefined,
          subject: undefined,
          description: undefined,
          date: undefined,
          author: {
            name: undefined,
            description: undefined,
            imageUrl: undefined
          },
          textUrl: undefined
        })
      })
    })
  })
})
