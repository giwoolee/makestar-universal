import expect from 'expect'
import contentsResponse from '../../../testHelpers/contentsResponse'
import { mapStateToProps } from '../../../../containers/content/show/HelmetContainer'

function setup() {
  const content = contentsResponse()
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    },
    contents: {
      contents: {
        1: content
      }
    },
    content: {
      idx: 1
    }
  }

  return {
    state,
    content
  }
}

describe('containers',() => {
  describe('content/show/HelmetContainer',() => {
    it('returns the correct props', () => {
      const { state, content } = setup()

      const expected = {
        title: content.subject,
        meta: [
          {
            'name': 'description', 'content': content.description
          },
          {
            'name': 'og:title', 'content': content.subject
          },
          {
            'name': 'og:description', 'content': content.description
          },
          {
            'name': 'og:image', 'content': content.snsimageUrl
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': content.links[1].href
          },
          {
            'rel': 'alternate', 'href': `${content.links[1].href}?locale=ko`, 'hreflang': 'ko'
          },
          {
            'rel': 'alternate', 'href': `${content.links[1].href}?locale=ja`, 'hreflang': 'ja'
          },
          {
            'rel': 'alternate', 'href': `${content.links[1].href}?locale=zh`, 'hreflang': 'zh'
          }
        ]
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})

