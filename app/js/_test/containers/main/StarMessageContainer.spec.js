import expect from 'expect'
import { mapStateToProps, mergeProps }from '../../../containers/main/StarMessageContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en'
      }
    },
    main: [
      {
        id: 0,
        artist: 'kim_woo_bin',
        showVideo: false
      }
    ]
  }

  return state
}

describe('containers',() => {
  describe('main/StarMessageContainer',() => {
    describe('mapStateToProps', () => {
      it('returns the correct props', () => {
        const state = setup()
        expect(mapStateToProps(state)).toEqual(
          {
            artists: [
              {
                id: 0,
                artist: 'kim_woo_bin',
                showVideo: false,
                artistNameMessageId: 'en.main.starMessage.artists.0.artistName',
                imageUrlMessageId: 'en.main.starMessage.artists.0.imageUrl',
                videoUrlMessageId: 'en.main.starMessage.artists.0.videoUrl'
              }
            ],
            starMessageTitleMessageId: 'en.main.starMessage.starMessageTitle',
            iFrameUrlMessageId: 'en.main.starMessage.iFrameUrl'
          }
        )
      })
    })

    describe('mergeProps', () => {
      it('returns the correct props', () => {
        const state = setup()
        const fakeDispatchProps = {
          actions: {
            showVideo: expect.createSpy()
          }
        }
        const mergedProps = mergeProps(mapStateToProps(state),fakeDispatchProps)
        mergedProps.artists[0].playVideo()
        expect(fakeDispatchProps.actions.showVideo).toHaveBeenCalledWith('kim_woo_bin')

        expect(mergedProps.artists[0].artist).toEqual('kim_woo_bin')
        expect(mergedProps.artists[0].showVideo).toEqual(false)
      })
    })
  })
})
