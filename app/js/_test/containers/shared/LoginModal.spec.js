import expect from 'expect'
import { mapStateToProps } from '../../../containers/shared/LoginModal'
import { COMMENT_CREATE_FAILURE } from '../../../constants/comment/index'
import { POLL_VOTE_FAILURE } from '../../../constants/poll/show/index'
import { SESSION_LOGIN_FAILURE } from '../../../constants/session/index'

function setup(status = 401, type = POLL_VOTE_FAILURE) {
  let ui

  if(status !== 200) {
    ui = {
      modal: {
        error: true,
        status,
        type
      }
    }
  } else {
    ui = {
      modal: {
        error: undefined,
        status: undefined,
        type: undefined
      }
    }
  }

  const state = {
    ui,
    session: {
      session: { locale: 'en' }
    }
  }
  return state
}

describe('containers/shared/LoginModal', () => {
  context('when there are errors and the type is COMMENT_CREATE_FAILURE', () => {
    it('sets the correct props', () => {
      const state = setup(401,COMMENT_CREATE_FAILURE)
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.common.loginFirst',
          bodyMessageId: 'en.login.prompt',
          signUpMessageId: 'en.login.signUp',
          loginMessageId: 'en.login.login',
          failedMessageId: 'en.login.failed',
          closeMessageId: 'en.common.close',
          emailMessageId: 'en.login.email',
          passwordMessageId: 'en.login.password',
          failed: false,
          type: 'comment',
          links: {
            primary: '/login.do',
            secondary: '/signup.do'
          },
          show: true
        }
      )
    })
  })

  context('when there are errors and the type is SESSION_LOGIN_FAILURE with status 401', () => {
    it('should map the correct state', () => {
      const state = setup(400,SESSION_LOGIN_FAILURE)
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.common.loginFirst',
          bodyMessageId: 'en.login.prompt',
          signUpMessageId: 'en.login.signUp',
          loginMessageId: 'en.login.login',
          failedMessageId: 'en.login.failed',
          closeMessageId: 'en.common.close',
          emailMessageId: 'en.login.email',
          passwordMessageId: 'en.login.password',
          type: 'session',
          failed: true,
          links: {
            primary: '/login.do',
            secondary: '/signup.do'
          },
          show: true
        }
      )
    })
  })

  context('when there are errors and the type is POLL_VOTE_FAILURE with status 401', () => {
    it('should map the correct state', () => {
      const state = setup(401,POLL_VOTE_FAILURE)
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.common.loginFirst',
          bodyMessageId: 'en.login.prompt',
          signUpMessageId: 'en.login.signUp',
          loginMessageId: 'en.login.login',
          failedMessageId: 'en.login.failed',
          closeMessageId: 'en.common.close',
          emailMessageId: 'en.login.email',
          passwordMessageId: 'en.login.password',
          failed: false,
          type: 'poll',
          links: {
            primary: '/login.do',
            secondary: '/signup.do'
          },
          show: true
        }
      )
    })
  })
})
