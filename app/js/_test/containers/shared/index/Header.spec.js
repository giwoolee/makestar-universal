import expect from 'expect'
import { mapStateToProps} from '../../../../containers/shared/index/IndexHeaderContainer'

function setup(pathname='/polls') {
  const state = {
    routing: {
      locationBeforeTransitions: {
        pathname
      }
    },
    session: {
      session: { locale: 'en' }
    }
  }

  return state
}

describe('containers', () => {
  context('when the location is /polls', () => {
    it('sets the correct props', () => {
      const state = setup()
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.poll.title',
          subtitleMessageId: 'en.poll.subtitle'
        }
      )
    })
  })

  context('when the location is /projects', () => {
    it('sets the correct props', () => {
      const state = setup('/projects')
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.project.title',
          subtitleMessageId: 'en.project.subtitle'
        }
      )
    })
  })
})

