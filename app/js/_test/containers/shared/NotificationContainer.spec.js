import expect from 'expect'
import { mapStateToProps } from '../../../containers/shared/NotificationContainer'
import { SESSION_LOGIN_SUCCESS } from '../../../constants/session/index'
import { POLL_VOTE_CLOSED, POLL_VOTE_UNAVAILABLE } from '../../../constants/poll/show'
import { resetNotification } from '../../../actions/ui/index'

function setup(show = true,type,votingType='ONCE') {
  let ui
  let polls

  if(show) {
    ui = {
      notification:{
        type,
        show
      }
    }
    polls = {
      polls: {
        1: { votingType },
      }
    }
  } else {
    ui = {
      notification: {
        type: undefined,
        show: undefined,
      }
    }
    polls = {
      polls: {
        1: { votingType },
      }
    }
  }

  const state = {
    ui,
    polls,
    poll: { idx: 1 },
    session: {
      session: {
        locale: 'en'
      }
    }
  }

  return state
}

describe('containers/shared/NotificationContainer', () => {
  context('when the type is SESSION_LOGIN_SUCCESS', () => {
    it('sets the correct props', () => {
      const state = setup(true,SESSION_LOGIN_SUCCESS)
      expect(mapStateToProps(state)).toEqual(
        {
          messageId: 'en.login.success',
          show: true,
          type: 'info'
        }
      )
    })
  })

  context('when the type is POLL_VOTE_CLOSED', () => {
    it('sets the correct props', () => {
      const state = setup(true,POLL_VOTE_CLOSED)
      expect(mapStateToProps(state)).toEqual(
        {
          messageId: 'en.poll.voteClosed',
          show: true,
          type: 'alert'
        }
      )
    })
  })

  context('when the type is POLL_VOTE_UNAVAILABLE', () => {
    context('when the poll votingType is \'once\'',() => {
      it('sets the correct props', () => {
        const state = setup(true,POLL_VOTE_UNAVAILABLE,'ONCE')
        expect(mapStateToProps(state)).toEqual(
          {
            messageId: 'en.poll.voteUnavailable',
            show: true,
            type: 'alert'
          }
        )
      })
    })

    context('when the poll votingType isn\'t \'once\'',() => {
      it('sets the correct props', () => {
        const state = setup(true,POLL_VOTE_UNAVAILABLE,'RE6')
        expect(mapStateToProps(state)).toEqual(
          {
            messageId: 'en.poll.voteLimitReached',
            show: true,
            type: 'alert'
          }
        )
      })
    })
  })
})
