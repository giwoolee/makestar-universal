import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/filter/FilterContainer'

function setup(isLoggedIn = false, pathname='/polls',status = 'Open') {
  const state = {
    session: { session: { isLoggedIn,locale: 'en' } },
    routing: {
      locationBeforeTransitions: {
        pathname,
        query: {
          page: 0,
          status
        }
      }
    }
  }
  return state
}

describe('containers',() => {
  describe('FilterContainer', () => {
    context('when the location is /polls and the user is logged in',() => {
      it('sets the correct props', () => {
        const state = setup(true)
        expect(mapStateToProps(state)).toEqual(
          {
            messageId: 'en.poll.filterLink',
            filters: [
              {
                filter: 'All',
                queryString: '/polls?status=All',
                selected: false,
                isLast: false,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'All'
                }
              },
              {
                filter: 'Open',
                queryString: '/polls?status=Open',
                selected: true,
                isLast: false,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'Open'
                }
              },
              {
                filter: 'Closed',
                queryString: '/polls?status=Closed',
                selected: false,
                isLast: false,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'Closed'
                }
              },
              {
                filter: 'Voted',
                queryString: '/polls?status=Voted',
                selected: false,
                isLast: true,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'Voted'
                }
              }
            ]
          }
        )
      })
    })

    context('when the location is /polls and the user is not logged in',() => {
      it('sets the correct props', () => {
        const state = setup(false)
        expect(mapStateToProps(state)).toEqual(
          {
            messageId: 'en.poll.filterLink',
            filters: [
              {
                filter: 'All',
                queryString: '/polls?status=All',
                selected: false,
                isLast: false,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'All'
                }
              },
              {
                filter: 'Open',
                queryString: '/polls?status=Open',
                selected: true,
                isLast: false,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'Open'
                }
              },
              {
                filter: 'Closed',
                queryString: '/polls?status=Closed',
                selected: false,
                isLast: true,
                gaData: {
                  category: 'Poll',
                  action: 'Navigate',
                  label: 'Closed'
                }
              }
            ]
          }
        )
      })
    })

    context('when the location is /projects',() => {
      it('sets the correct props', () => {
        const state = setup(true,'/projects','Opened')
        expect(mapStateToProps(state)).toEqual(
          {
            messageId: 'en.project.filterLink',
            filters: [
              {
                filter: 'All',
                queryString: '/projects?status=All',
                selected: false,
                isLast: false,
                gaData: {
                  category: 'Project',
                  action: 'Navigate',
                  label: 'All'
                }
              },
              {
                filter: 'Open',
                queryString: '/projects?status=Opened',
                selected: true,
                isLast: false,
                gaData: {
                  category: 'Project',
                  action: 'Navigate',
                  label: 'Open'
                }
              },
              {
                filter: 'Closed',
                queryString: '/projects?status=Closed',
                selected: false,
                isLast: true,
                gaData: {
                  category: 'Project',
                  action: 'Navigate',
                  label: 'Closed'
                }
              }
            ]
          }
        )
      })
    })
  })
})
