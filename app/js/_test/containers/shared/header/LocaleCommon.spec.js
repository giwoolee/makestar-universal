import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../../containers/shared/header/LocaleCommon'

function setup() {
  const state = {
    session: {
      session: { locale: 'en' }
    }
  }

  return state
}

describe('containers/shared/header/LocaleCommon', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props',() => {
      const state = setup()
      const expected = {
        locale: 'en',
        localeDisplayName: 'English',
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })

  describe('mergeProps', () => {
    let originalWindow = global.window
    let originalDocument = global.document

    beforeEach(() => {
      global.window = {
        location: null
      }
    })

    afterEach(() => {
      global.window = originalWindow
      global.document = originalDocument
    })

    it('returns a localeChange function', () => {
      const state = setup()
      const mergedProps = mergeProps(mapStateToProps(state))

      mergedProps.localeChange('en')

      expect(window.location).toEqual(`/change_locale.do?lang=en&redirect_url=${encodeURIComponent(document.location)}`)
    })
  })
})
