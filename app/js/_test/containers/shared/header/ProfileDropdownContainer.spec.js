import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../../containers/shared/header/ProfileDropdownContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        email: 'mxion9522@gmail.com',
        nickName: 'IAN',
        profileImageUrl: '/file/user_pic/145441057410493.jpg'
      }
    }
  }

  const actions = {
    gaEvent: expect.createSpy()
  }

  return {
    state,
    actions
  }
}

describe('containers/shared/header/ProfileDropdownContainer',() => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state } = setup()
      const expected = {
        email: 'mxion9522@gmail.com',
        nickName: ' IAN',
        profileImageUrl: '/file/user_pic/145441057410493.jpg',
        projectsMessageId: 'en.topnav.projects',
        profileMessageId: 'en.topnav.profile',
        logoutMessageId: 'en.topnav.logout'
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })

  describe('mergeProps', () => {

    it('returns a fundingListClick function', () => {
      const { state, actions } = setup()
      const mergedProps = mergeProps(mapStateToProps(state),{ actions })

      mergedProps.actions.fundingListClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'My Pledges' })
    })

    it('returns a profileClick function', () => {
      const { state, actions } = setup()
      const mergedProps = mergeProps(mapStateToProps(state),{ actions })
      mergedProps.actions.profileClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Profile' })
    })

    it('returns a logoutClick function', () => {
      const { state, actions } = setup()
      const mergedProps = mergeProps(mapStateToProps(state),{ actions })

      mergedProps.actions.logoutClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Account Activity', action: 'LogOut' })
    })
  })
})
