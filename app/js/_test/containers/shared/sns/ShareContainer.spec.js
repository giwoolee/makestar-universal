import expect from 'expect'
import pollResponse from '../../../testHelpers/pollResponse'
import contentResponse from '../../../testHelpers/contentsResponse'
import { mapStateToProps, mergeProps } from '../../../../containers/shared/sns/ShareContainer'

function setup(idx = 1) {
  const poll = pollResponse()
  const content = contentResponse()

  const state = {
    session: {
      session: {
        locale: 'en',
      }
    },
    poll: {
      idx,
    },
    polls: {
      polls: { 1: poll }
    },
    content: {
      idx,
    },
    contents: {
      contents: { 1: content }
    }
  }
  return {
    poll,
    content,
    state
  }
}

describe('containers/shared/sns/ShareContainer', () => {
  describe('mapStateToProps', () => {
    context('when the type is polls', () => {
      it('returns the correct props', () => {
        const { state, poll } = setup()
        const expected = {
          idx: 1,
          locale: 'en',
          title: poll.title,
          description: poll.title,
          snsimageUrl: poll.snsimageUrl,
          canonical: poll.links[1].href
        }

        expect(mapStateToProps(state,{ type: 'polls' })).toEqual(expected)
      })
    })

    context('when type is contents', () => {
      it('returns the correct props', () => {
        const { state, content } = setup()
        const expected = {
          idx: 1,
          locale: 'en',
          title: content.subject,
          description: content.description,
          snsimageUrl: content.snsimageUrl,
          canonical: content.links[1].href
        }
        expect(mapStateToProps(state,{ type: 'contents' })).toEqual(expected)
      })
    })

    context('when the idx doesn\'t match', () => {
      it('returns the correct props', () => {
        const { state, content } = setup(2)
        const expected = {
          idx: undefined,
          locale: 'en',
          title: undefined,
          description: undefined,
          snsimageUrl: undefined,
          canonical: undefined
        }
        expect(mapStateToProps(state,{ type: 'polls' })).toEqual(expected)
      })
    })
  })

  describe('mergeProps', () => {
    it('returns the merged props', () => {
      const { state, content } = setup()
      const mappedProps = mapStateToProps(state,{ type: 'polls' })
      const fakeSnsShare = expect.createSpy()

      const mergedProps = mergeProps(mappedProps,{ actions: { snsShare: fakeSnsShare } },{ type: 'polls' })

      mergedProps.facebook.onClick()
      expect(fakeSnsShare).toHaveBeenCalledWith({ idx: 1, category: 'Poll Detail',  action: 'SNS', sns: 'facebook' })
      expect(mergedProps.facebook.icon).toEqual('share-facebook-icon')
      mergedProps.google.onClick()
      expect(mergedProps.google.icon).toEqual('share-google-icon')
      expect(fakeSnsShare).toHaveBeenCalledWith({ category: 'Poll Detail', idx: 1, action: 'SNS', sns: 'google' })
      mergedProps.pinterest.onClick()
      expect(mergedProps.pinterest.icon).toEqual('share-pinterest-icon')
      expect(fakeSnsShare).toHaveBeenCalledWith({ category: 'Poll Detail', idx: 1, action: 'SNS', sns: 'pinterest' })
      mergedProps.reddit.onClick()
      expect(mergedProps.reddit.icon).toEqual('share-reddit-icon')
      expect(fakeSnsShare).toHaveBeenCalledWith({ category: 'Poll Detail', idx: 1, action: 'SNS', sns: 'reddit' })
      mergedProps.twitter.onClick()
      expect(mergedProps.twitter.icon).toEqual('share-twitter-icon')
      expect(fakeSnsShare).toHaveBeenCalledWith({ category: 'Poll Detail', idx: 1, action: 'SNS', sns: 'twitter' })
    })
  })
})

