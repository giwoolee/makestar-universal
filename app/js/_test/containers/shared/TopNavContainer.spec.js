import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../containers/shared/TopNavContainer'

function setup(isLoggedIn = false,topNavOpen = false) {
  const session = {
    isLoggedIn,
    email: isLoggedIn ? 'email@email.com' : null,
    nickName: isLoggedIn ? 'username' : null,
    profileImageUrl: isLoggedIn ? 'imageurl' : null,
    locale: 'en'
  }

  const ui = {
    topNavOpen
  }

  const actions = {
    gaEvent: expect.createSpy(),
    topNavToggle: expect.createSpy(),
    push: expect.createSpy()
  }

  const state = {
    session: {
      session
    },
    ui
  }

  return {
    ui,
    session,
    state,
    actions
  }
}

describe('containers/shared/TopNavContainer', () => {
  describe('mapStateToProps', () => {
    context('when isLoggedIn is false', () => {
      it('returns the correct props', () => {
        const { state, session, ui } = setup()

        const expected = {
          confirmAccountMessageId: 'en.topnav.confirmAccount',
          projectMessageId: 'en.topnav.projects',
          contentMessageId: 'en.topnav.contents',
          eventMessageId: 'en.topnav.events',
          loginMessageId: 'en.topnav.login',
          signupMessageId: 'en.topnav.signup',
          pollMessageId: 'en.topnav.polls',
          topNavOpen: ui.topNavOpen,
          isLoggedIn: session.isLoggedIn,
          locale: 'en',
          grade: session.grade
        }

        expect(mapStateToProps(state)).toEqual(expected)
      })
    })

    context('when isLoggedIn is true', () => {
      it('returns the correct props', () => {
        const { state, ui, session } = setup(true)

        const expected = {
          confirmAccountMessageId: 'en.topnav.confirmAccount',
          projectMessageId: 'en.topnav.projects',
          contentMessageId: 'en.topnav.contents',
          eventMessageId: 'en.topnav.events',
          loginMessageId: 'en.topnav.login',
          signupMessageId: 'en.topnav.signup',
          pollMessageId: 'en.topnav.polls',
          topNavOpen: ui.topNavOpen,
          isLoggedIn: session.isLoggedIn,
          locale: 'en',
          grade: session.grade
        }

        expect(mapStateToProps(state)).toEqual(expected)
      })
    })
  })

  describe('mergeProps', () => {

    it('returns a signUpClick function', () => {
      const { state, actions } = setup()

      const mergedProps = mergeProps(mapStateToProps(state),{ actions })
      mergedProps.actions.signUpClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Sign Up' })
    })

    it('returns a loginClick function', () => {
      const { state, actions } = setup()

      const mergedProps = mergeProps(mapStateToProps(state),{ actions })
      mergedProps.actions.loginClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Login' })
    })

    it('returns a eventClick function', () => {
      const { state, actions } = setup()

      const mergedProps = mergeProps(mapStateToProps(state),{ actions })
      mergedProps.actions.eventClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Events' })
    })

    it('returns a faqClick function', () => {
      const { state, actions } = setup()

      const mergedProps = mergeProps(mapStateToProps(state),{ actions })
      mergedProps.actions.faqClick()
      expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Faqs' })
    })
    context('when topNavOpen is false', () => {
      it('returns a homeClick function', () => {
        const { state, actions } = setup()

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.homeClick(fakeEvent)
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Home' })
        expect(actions.push).toHaveBeenCalledWith('/')
        expect(actions.topNavToggle).toNotHaveBeenCalled()
      })

      it('returns a onToggle function', () => {
        const { state, actions } = setup()
        const mergedProps = mergeProps(mapStateToProps(state),{ actions })

        mergedProps.actions.onToggle()
        expect(actions.topNavToggle).toHaveBeenCalledWith(false)
      })

      it('returns a projectClick function', () => {
        const { state, actions } = setup()

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.projectClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Projects' })
        expect(actions.push).toHaveBeenCalledWith('/projects')
        expect(actions.topNavToggle).toNotHaveBeenCalled()
      })

      it('returns a contentClick function', () => {
        const { state, actions } = setup()

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.contentClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Magazine' })
        expect(actions.push).toHaveBeenCalledWith('/contents/articles')
        expect(actions.topNavToggle).toNotHaveBeenCalled()
      })

      it('returns a pollClick function', () => {
        const { state, actions } = setup()

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.pollClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Polls' })
        expect(actions.push).toHaveBeenCalledWith('/polls')
        expect(actions.topNavToggle).toNotHaveBeenCalled()
      })

    })

    context('when topNavOpen is true', () => {
      it('returns a onToggle function', () => {
        const { state, actions } = setup(false,true)
        const mergedProps = mergeProps(mapStateToProps(state),{ actions })

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        mergedProps.actions.onToggle(fakeEvent)
        expect(actions.topNavToggle).toHaveBeenCalledWith(true)
      })

      it('returns a homeClick function', () => {
        const { state, actions } = setup(false,true)

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.homeClick(fakeEvent)
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Home' })
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.push).toHaveBeenCalledWith('/')
        expect(actions.topNavToggle).toHaveBeenCalledWith(true)
      })

      it('returns a projectClick function', () => {
        const { state, actions } = setup(false,true)

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.projectClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Projects' })
        expect(actions.push).toHaveBeenCalledWith('/projects')
        expect(actions.topNavToggle).toHaveBeenCalledWith(true)
      })

      it('returns a contentClick function', () => {
        const { state, actions } = setup(false,true)

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.contentClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Magazine' })
        expect(actions.push).toHaveBeenCalledWith('/contents/articles')
        expect(actions.topNavToggle).toHaveBeenCalledWith(true)
      })

      it('returns a pollClick function', () => {
        const { state, actions } = setup(false,true)

        const fakeEvent = {
          preventDefault: expect.createSpy()
        }

        const mergedProps = mergeProps(mapStateToProps(state),{ actions })
        mergedProps.actions.pollClick(fakeEvent)
        expect(fakeEvent.preventDefault).toHaveBeenCalled()
        expect(actions.gaEvent).toHaveBeenCalledWith({ category: 'Header', action: 'Polls' })
        expect(actions.push).toHaveBeenCalledWith('/polls')
        expect(actions.topNavToggle).toHaveBeenCalledWith(true)
      })
    })
  })
})
