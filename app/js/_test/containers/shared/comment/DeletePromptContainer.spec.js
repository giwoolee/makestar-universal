import Promise from 'bluebird'
import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../../containers/shared/comment/DeletePromptContainer'

function setup(show,idx,type) {
  const state = {
    comments: {
      comments: {
        1: { idx: 1 }
      },
      ui: {
        deletePrompt: { idx, type, show }
      }
    },
    replies: {
      2: { idx: 2, parentIdx: 1 }
    },
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1'
      }
    },
    session: {
      session: {
        locale: 'en'
      }
    }
  }

  return state
}

describe('containers/shared/comment/DeletePromptContainer', () => {
  describe('mapStateToProps', () => {
    context('when the idx doesn\'t match', () => {
      it('returns the correct props', () => {
        const show = true
        const type = 'comments'
        const state = setup(show,2,type)
        const expected = {
          idx: undefined,
          show,
          promptMessageId: 'en.comment.commentDeletePrompt',
          confirmMessageId: 'en.common.confirm',
          cancelMessageId: 'en.common.cancel',
          type,
          pathname: undefined
        }

        expect(mapStateToProps(state,{ type: 'comments' })).toEqual(expected)
      })
    })

    context('when the type is comments', () => {
      it('returns the correct props', () => {
        const show = true
        const type = 'comments'
        const state = setup(show,1,type)
        const expected = {
          idx: 1,
          show,
          promptMessageId: 'en.comment.commentDeletePrompt',
          confirmMessageId: 'en.common.confirm',
          cancelMessageId: 'en.common.cancel',
          type,
          pathname: '/polls/1/comments/1'
        }

        expect(mapStateToProps(state,{ type })).toEqual(expected)
      })
    })

    context('when the type is replies', () => {
      it('retuns the correct props', () => {
        const show = true
        const type = 'replies'
        const state = setup(show,2,type)
        const expected = {
          idx: 2,
          show,
          promptMessageId: 'en.comment.commentDeletePrompt',
          confirmMessageId: 'en.common.confirm',
          cancelMessageId: 'en.common.cancel',
          type,
          pathname: '/polls/1/comments/1/replies/2'
        }

        expect(mapStateToProps(state,{ type })).toEqual(expected)
      })
    })
  })

  describe('mergeProps', () => {
    it('returns a bound toggleDeletePrompt function', () => {
      const fakeDispatchProps = {
        actions: {
          toggleDeletePrompt: expect.createSpy(),
          del: expect.createSpy().andReturn(Promise.resolve()),
          editClose: expect.createSpy()
        }
      }
      const show = true
      const type = 'comments'
      const state = setup(show,1,type)
      const mergedProps = mergeProps(mapStateToProps(state,{ type }),fakeDispatchProps)
      mergedProps.actions.cancel()
      expect(fakeDispatchProps.actions.toggleDeletePrompt).toHaveBeenCalledWith({ show })
    })

    it('returns a bound del function', () => {
      const fakeDispatchProps = {
        actions: {
          toggleDeletePrompt: expect.createSpy(),
          del: expect.createSpy().andReturn(Promise.resolve()),
          editClose: expect.createSpy()
        }
      }
      const show = true
      const type = 'comments'
      const state = setup(show,1,type)
      const stateProps = mapStateToProps(state,{ type })
      const mergedProps = mergeProps(stateProps,fakeDispatchProps)
      return mergedProps.actions.confirm().then(() => {
        expect(fakeDispatchProps.actions.del).toHaveBeenCalledWith({ type, pathname: stateProps.pathname, idx: stateProps.idx })
        expect(fakeDispatchProps.actions.toggleDeletePrompt).toHaveBeenCalledWith({ show: true })
        expect(fakeDispatchProps.actions.editClose).toHaveBeenCalledWith({ idx: stateProps.idx, type })
      })
    })
  })
})

