import expect from 'expect'
import { mapStateToProps, mergeProps } from '../../../../containers/shared/comment/EditButtonContainer'

function setup (idx, comments = [], replies = [], status = 'Visible') {
  const state = {
    session: {
      session: {
        idx,
        locale: 'en'
      }
    },
    comments: {
      ui: {
        editOpen: {
          comments,
          replies
        }
      },
      comments: {
        1: {
          userIdx: idx, text: 'text', status
        }
      }
    },
    replies: {
      2: {
        userIdx: idx, text: 'text', status
      }
    }
  }

  return state
}

describe('containers/shared/comment/EditButtonContainer', () => {
  describe('mapStateToProps', () => {
    context('when type is comments', () => {
      context('when the status is Visible', () => {
        it('returns the correct props', () => {
          const state = setup(1)
          const expected = {
            isDisabled: false,
            isVisible: true,
            type: 'comments',
            text: 'text',
            editMessageId: 'en.comment.edit',
            cancelMessageId: 'en.common.cancel',
            idx: 1
          }
          expect(mapStateToProps(state, {idx: 1, type: 'comments'})).toEqual(expected)
        })
      })
      context('when the status is Deleted', () => {
        it('returns the correct props', () => {
          const state = setup(1,[],[],'Deleted')
          const expected = {
            isDisabled: false,
            isVisible: false,
            type: 'comments',
            text: 'text',
            editMessageId: 'en.comment.edit',
            cancelMessageId: 'en.common.cancel',
            idx: 1
          }
          expect(mapStateToProps(state, {idx: 1, type: 'comments'})).toEqual(expected)
        })
      })
    })

    context('when type is replies', () => {
      context('when the status is Visible', () => {
        it('returns the correct props', () => {
          const state = setup(1)
          const expected = {
            isDisabled: false,
            isVisible: true,
            type: 'replies',
            text: 'text',
            editMessageId: 'en.comment.edit',
            cancelMessageId: 'en.common.cancel',
            idx: 2
          }
          expect(mapStateToProps(state, {idx: 2, type: 'replies'})).toEqual(expected)
        })
      })

      context('when the status is Deleted', () => {
        it('returns the correct props', () => {
          const state = setup(1,[],[],'Deleted')
          const expected = {
            isDisabled: false,
            isVisible: false,
            type: 'replies',
            text: 'text',
            editMessageId: 'en.comment.edit',
            cancelMessageId: 'en.common.cancel',
            idx: 2
          }
          expect(mapStateToProps(state, {idx: 2, type: 'replies'})).toEqual(expected)
        })
      })
    })
  })

  describe('mergeProps', () => {
    context('when isDisabled is true', () => {
      it('returns an editOpen', () => {
        const state = setup(1,[1])
        const dispatchProps = {
          actions: {
            editOpen: expect.createSpy(),
            updateText: expect.createSpy(),
            editClose: expect.createSpy()
          }
        }
        const stateProps = mapStateToProps(state,{ idx: 1, type: 'comments' })
        const mergedProps = mergeProps(stateProps,dispatchProps)

        mergedProps.actions.editOpen()
        expect(dispatchProps.actions.editOpen).toNotHaveBeenCalled()
        expect(dispatchProps.actions.updateText).toNotHaveBeenCalled()
        expect(dispatchProps.actions.editClose).toHaveBeenCalledWith({ idx: stateProps.idx, type: stateProps.type })
      })
    })

    context('when status is Visible', () => {
      it('maps updateText and editOpen to editOpen function', () => {
        const state = setup(1)
        const stateProps = mapStateToProps(state,{ idx: 1, type: 'comments' })

        const dispatchProps = {
          actions: {
            editOpen: expect.createSpy(),
            updateText: expect.createSpy(),
            editClose: expect.createSpy()
          }
        }

        const mergedProps = mergeProps(stateProps,dispatchProps)
        mergedProps.actions.editOpen()
        expect(dispatchProps.actions.editOpen).toHaveBeenCalledWith({ idx: stateProps.idx, type: stateProps.type })
        expect(dispatchProps.actions.editClose).toNotHaveBeenCalled()
        expect(dispatchProps.actions.updateText).toHaveBeenCalledWith({ text: stateProps.text, idx: stateProps.idx, type: stateProps.type })
      })
    })
  })
})
