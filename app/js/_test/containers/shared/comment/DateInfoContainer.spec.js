import moment from 'moment'
import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/DateInfoContainer'

function setup (createdDate,updatedDate) {
  const state = {
    comments: {
      comments: {
        1: { createdDate, updatedDate }
      }
    },
    replies: {
      2: { createdDate, updatedDate }
    },
    session: {
      session: {
        locale: 'en'
      }
    }
  }

  return state
}

describe('containers/shared/comment/DateInfoContainer', () => {
  context('when the type is comments', () => {
    context('when the two dates match', () => {
      it('returns the correct props', () => {
        const date = moment().toISOString()
        const state = setup(date,date)
        const expected = {
          dateMessageId: 'en.comment.postedOn',
          date: moment(date).toDate()
        }

        expect(mapStateToProps(state,{ idx: 1, type: 'comments' })).toEqual(expected)
      })
    })

    context('when two dates don\'t match', () => {
      it('returns the correct props', () => {
        const createdDate = moment().subtract(1,'day').toISOString()
        const updatedDate = moment().toISOString()
        const state = setup(createdDate,updatedDate)
        const expected = {
          dateMessageId: 'en.comment.editedOn',
          date: moment(updatedDate).toDate()
        }

        expect(mapStateToProps(state,{ idx: 1, type: 'comments' })).toEqual(expected)
      })
    })
  })

  context('when the type is replies', () => {
    context('when the two dates match', () => {
      it('returns the correct props', () => {
        const date = moment().toISOString()
        const state = setup(date,date)
        const expected = {
          dateMessageId: 'en.comment.postedOn',
          date: moment(date).toDate()
        }

        expect(mapStateToProps(state,{ idx: 2, type: 'replies' })).toEqual(expected)
      })
    })

    context('when two dates don\'t match', () => {
      it('returns the correct props', () => {
        const createdDate = moment().subtract(1,'day').toISOString()
        const updatedDate = moment().toISOString()
        const state = setup(createdDate,updatedDate)
        const expected = {
          dateMessageId: 'en.comment.editedOn',
          date: moment(updatedDate).toDate()
        }

        expect(mapStateToProps(state,{ idx: 2, type: 'replies' })).toEqual(expected)
      })
    })
  })
})

