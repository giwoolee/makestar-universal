import { mapStateToProps, mergeProps } from '../../../../containers/shared/comment/EditTextInputContainer'
import expect from 'expect'
import Promise from 'bluebird'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: true
      }
    },
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1'
      }
    },
    replies: {
      2: { text: 'text 2', parentIdx: 1 }
    },
    text: {
      comments: { 1: 'text 1' },
      replies: { 2: 'text 2' }
    }
  }

  return state
}

describe('containers/shared/comment/EditTextInputContainer', () => {
  describe('mapStateToProps',() => {
    context('when the type is comments', () => {
      it('returns the correct props', () => {
        const state = setup()
        const expected = {
          data: { text: 'text 1' },
          idx: 1,
          pathname: '/polls/1/comments/1',
          isTextOverLength: false,
          postMessageId: 'en.comment.post',
          textLength: state.text.comments[1].length,
          textAreaProps: { disabled: false, value: 'text 1', message: undefined },
          inputBtnProps: { disabled: false, message: "en.comment.edit" },
          type: 'comments',
          isLoggedIn: true,
          leaveComment: 'en.comment.editPost'
        }

        expect(mapStateToProps(state,{ idx: 1, type: 'comments' })).toEqual(expected)
      })
    })

    context('when the type is replies', () => {
      it('returns the correct props', () => {
        const state = setup()
        const expected = {
          data: { text: 'text 2' },
          idx: 2,
          pathname: '/polls/1/comments/1/replies/2',
          isTextOverLength: false,
          postMessageId: 'en.comment.post',
          textLength: state.text.replies[2].length,
          textAreaProps: { disabled: false, value: 'text 2', message: undefined },
          inputBtnProps: { disabled: false, message: "en.comment.edit" },
          type: 'replies',
          isLoggedIn: state.session.session.isLoggedIn,
          leaveComment: 'en.comment.editPost',
        }

        expect(mapStateToProps(state,{ idx: 2, type: 'replies' })).toEqual(expected)
      })
    })
  })

  describe('mergeProps', () => {
    context('when the type is comments', () => {
      it('returns a bound submit function', () => {
        const state = setup()
        const fakeDispatchProps = {
          actions: {
            update: expect.createSpy().andReturn(Promise.resolve()),
            updateText: expect.createSpy(),
            editClose: expect.createSpy(),
            promptLogin: expect.createSpy()
          }
        }

        const stateProps = mapStateToProps(state,{ idx: 1, type: 'comments' })
        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        return mergedProps.actions.submit().then(() => {
          expect(fakeDispatchProps.actions.update).toHaveBeenCalledWith({ pathname: stateProps.pathname, idx: stateProps.idx, data: stateProps.data, type: stateProps.type })
          expect(fakeDispatchProps.actions.editClose).toHaveBeenCalledWith({idx: stateProps.idx, type: stateProps.type })
        })
      })

      it('returns a bound updateText function', () => {
        const state = setup()
        const fakeDispatchProps = {
          actions: {
            update: expect.createSpy().andReturn(Promise.resolve()),
            updateText: expect.createSpy(),
            editClose: expect.createSpy(),
            promptLogin: expect.createSpy()
          }
        }
        const stateProps = mapStateToProps(state,{ idx: 1, type: 'comments' })
        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        mergedProps.actions.updateText('text')
        expect(fakeDispatchProps.actions.updateText).toHaveBeenCalledWith({ idx: 1, text: 'text', type: 'comments' })
      })
    })

    context('when the type is comments', () => {
      it('returns a bound submit function', () => {
        const state = setup()
        const fakeDispatchProps = {
          actions: {
            update: expect.createSpy().andReturn(Promise.resolve()),
            updateText: expect.createSpy(),
            editClose: expect.createSpy(),
            promptLogin: expect.createSpy()
          }
        }

        const stateProps = mapStateToProps(state,{ idx: 2, type: 'replies' })
        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        mergedProps.actions.submit().then(() => {
          expect(fakeDispatchProps.actions.update).toHaveBeenCalledWith({ pathname: stateProps.pathname, idx: stateProps.idx, data: stateProps.data, type: stateProps.type })
          expect(fakeDispatchProps.actions.editClose).toHaveBeenCalledWith({idx: stateProps.idx, type: stateProps.type })
        })
      })

      it('returns a bound updateText function', () => {
        const state = setup()
        const fakeDispatchProps = {
          actions: {
            update: expect.createSpy().andReturn(Promise.resolve()),
            updateText: expect.createSpy(),
            editClose: expect.createSpy(),
            promptLogin: expect.createSpy()
          }
        }
        const stateProps = mapStateToProps(state,{ idx: 2, type: 'replies' })
        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        mergedProps.actions.updateText('text')
        expect(fakeDispatchProps.actions.updateText).toHaveBeenCalledWith({ idx: 2, text: 'text', type: 'replies' })
      })
    })
  })
})
