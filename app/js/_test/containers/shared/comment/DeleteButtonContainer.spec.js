import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/DeleteButtonContainer'

function setup (idx = 1, userIdx = 1, status = 'Visible') {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: true,
        idx: 1
      }
    },
    replies: {
      2: { idx: 2, status, userIdx }
    },
    comments: {
      comments: {
        1: { idx: 1, status, userIdx  }
      },
      ui: {
        deletePrompt: {
          idx
        }
      }
    }
  }

  return state
}

describe('containers/shared/comment/DeleteButtonContainer', () => {
  describe('mapStateToProps', () => {
    context('when the ownProps.idx matches the deletePrompt idx', () => {
      context('when the comment status is Visible', () => {
        it('returns the correct props', () => {
          const type = 'comments'
          const state = setup(1)
          const expected = {
            isDisabled: true,
            isVisible: true,
            type,
            deleteMessageId: 'en.comment.delete',
            idx: 1
          }
          expect(mapStateToProps(state,{ idx: 1, type })).toEqual(expected)
        })
      })

      context('when the user doesn\'t own the comment', () => {
        it('returns the correct props', () => {
          const type = 'comments'
          const state = setup(1,2)
          const expected = {
            isDisabled: true,
            isVisible: false,
            type,
            deleteMessageId: 'en.comment.delete',
            idx: 1
          }
          expect(mapStateToProps(state,{ idx: 1, type })).toEqual(expected)
        })
      })

      context('when the comment status is Deleted', () => {
        it('returns the correct props', () => {
          const type = 'comments'
          const state = setup(1,1,'Deleted')
          const expected = {
            isDisabled: true,
            isVisible: false,
            type,
            deleteMessageId: 'en.comment.delete',
            idx: 1
          }
          expect(mapStateToProps(state,{ idx: 1, type })).toEqual(expected)
        })
      })
    })

    context('when the ownProps.idx doesn\'t match the deletePrompt idx', () => {
      it('returns the correct props', () => {
        const state = setup(1)
        const type = 'replies'
        const expected = {
          isDisabled: false,
          type,
          isVisible: true,
          deleteMessageId: 'en.comment.delete',
          idx: 2
        }
        expect(mapStateToProps(state,{ idx: 2, type })).toEqual(expected)
      })
    })
  })
})
