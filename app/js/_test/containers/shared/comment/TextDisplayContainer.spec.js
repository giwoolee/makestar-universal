import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/TextDisplayContainer'

function setup(status = 'Visible') {
  const state = {
    session: {
      session: {
        locale: 'en'
      }
    },
    comments: {
      comments: {
        1: { idx: 1, text: 'Text', status }
      }
    },
    replies: {
      2: { idx: 2, text: 'Text', status }
    }
  }

  return state
}

describe('containers/shared/comment/TextDisplayContainer', () => {
  describe('mapStateToProps', () => {
    context('when the type is comments', () => {
      it('returns the correct props', () => {
        const state = setup()
        const type = 'comments'

        const expected = {
          isVisible: true,
          text: "Text",
          commentDeletedMessageId: 'en.comment.deleted'
        }

        expect(mapStateToProps(state,{ idx: 1, type })).toEqual(expected)
      })
    })

    context('when the type is replies', () => {
      it('returns the correct props', () => {
        const state = setup()
        const type = 'replies'

        const expected = {
          isVisible: true,
          text: "Text",
          commentDeletedMessageId: 'en.comment.deleted'
        }

        expect(mapStateToProps(state,{ idx: 2, type })).toEqual(expected)
      })
    })
  })
})
