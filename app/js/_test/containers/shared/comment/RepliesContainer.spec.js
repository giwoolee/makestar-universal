import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/RepliesContainer'
import commentResponse from '../../../testHelpers/commentResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'

function setup(isLoggedIn = true, isLast = true) {
  const state = {
    comments: {
      comments: {
        1: {
          replies: {
            content: [2],
            pagination: paginationResponse(1,isLast)
          }
        }
      },
      ui: {
        replyOpen: [1]
      }
    },
    replies: {
      2: Object.assign({},commentResponse(2),{ parentIdx: 1 })
    },
    session: {
      session: {
        isLoggedIn,
        locale: 'en'
      }
    },
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1',
      }
    }
  }
  return {
    state
  }
}

describe('containers/shared/comment/RepliesContainer', () => {
  context('when replies is an empty object', () => {
    it('returns the correct props', () => {
      const state = { comments: { comments: { 1: { replies: null } }, ui: { replyOpen: [] }, error: undefined },session: { session: { isLoggedIn: true, locale: 'en'} }, routing: { locationBeforeTransitions: { pathname: '/polls/1' } } }
      expect(mapStateToProps(state,{ parentIdx: 1 })).toEqual({
        idxs: [],
        isLast: true,
        isLoggedIn: true,
        isReplyOpen: false,
        moreMessage: 'en.common.more',
        nextPage: undefined,
        parentIdx: 1
      })
    })
  })

  it('returns the correct props', () => {
    const { state } = setup()

    expect(mapStateToProps(state,{ parentIdx: 1 })).toEqual({
      idxs: [2],
      isLoggedIn: state.session.session.isLoggedIn,
      moreMessage: 'en.common.more',
      isReplyOpen: true,
      nextPage: {
        query: { page: 2 },
        idx: 1,
        pathname: "/polls/1/comments/1/replies",
        type: 'replies',
      },
      isLast: true,
      parentIdx: 1
    })
  })

  it('returns the correct props', () => {
    const { state } = setup(false,true)

    expect(mapStateToProps(state,{ parentIdx: 1 })).toEqual({
      idxs: [2],
      isLoggedIn: state.session.session.isLoggedIn,
      moreMessage: 'en.common.more',
      isReplyOpen: true,
      nextPage: {
        type: 'replies',
        idx: 1,
        query: { page: 2},
        pathname: "/polls/1/comments/1/replies",
        type: 'replies'
      },
      isLast: true,
      parentIdx: 1
    })
  })
})
