import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/ReplyContainer'
import commentResponse from '../../../testHelpers/commentResponse'

function setup (idx=1) {

  const replyOne = Object.assign({},commentResponse(1), { parentIdx: 1 })
  const replyTwo = Object.assign({},commentResponse(2), { parentIdx: 2 })

  const state = {
    comments: {
      ui: {
        editOpen: {
          replies: []
        }
      }
    },
    replies: { 1: replyOne, 2: replyTwo },
    session: {
      session: { isLoggedIn: false, locale: 'en', idx: 1 }
    }
  }

  return {
    idx,
    state,
    replyOne,
    replyTwo
  }
}

describe('containers', () => {
  describe('containers/shared/comment/ReplyContainer', () => {
    context('when the idx is a key in the replies object', () => {
      it('returns the correct props', () => {
        const { idx, state, replyOne, replyTwo } = setup(1,'replies')
        const expected = {
          ...replyOne,
          isLoggedIn: false,
          isEditOpen: false,
          isDeleted: false,
          isMine: true,
          type: 'replies'
        }

        expect(mapStateToProps(state,{ idx: 1, parentIdx: 1 })).toEqual(expected)
      })

      it('returns the correct props', () => {
        const { idx, state, replyOne, replyTwo } = setup(1,'replies')
        const expected = {
          ...replyTwo,
          isLoggedIn: false,
          isEditOpen: false,
          isDeleted: false,
          isMine: true,
          type: 'replies'
        }

        expect(mapStateToProps(state,{ idx: 2, parentIdx: 1 })).toEqual(expected)
      })
    })
  })
})
