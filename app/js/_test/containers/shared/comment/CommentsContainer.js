import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/CommentsContainer'
import commentResponse from '../../../testHelpers/commentResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'

function setup(isLast = true) {
  const comment = commentResponse(1)
  const pagination = paginationResponse(1,isLast)
  const state = {
    comments: {
      comments: { [comment.idx]: comment },
      idxs: [comment.idx],
      pagination
    },
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1'
      }
    },
    session: {
      session: {
        locale: 'en'
      }
    }
  }
  return {
    state,
    comment
  }
}

describe('containers/shared/comment/CommentsContainer',() => {
  context('when isLast is true', () => {
    it('returns the correct props', () => {
      const { state, comment } = setup()
      const idx = 1
      const expectedProps = {
        idxs: [comment.idx],
        idx,
        isLast: true,
        nextPage: null,
        moreMessage: 'en.common.more'
      }

      expect(mapStateToProps(state, { idx })).toEqual(expectedProps)
    })
  })

  context('when isLast is false', () => {
    it('returns the correct props', () => {
      const { state, comment } = setup(false)
      const idx = 1
      const expectedProps = {
        idxs: [comment.idx],
        idx,
        isLast: false,
        moreMessage: 'en.common.more',
        nextPage: {
          pathname: '/polls/1/comments',
          query: { page: 2 },
          type: 'comments'
        }
      }

      expect(mapStateToProps(state,{ idx })).toEqual(expectedProps)
    })
  })
})
