import expect from 'expect'
import { mapStateToProps, mapDispatchToProps, mergeProps } from '../../../../containers/shared/comment/TextInputContainer'
import loremIpsum from 'lorem-ipsum'
import Promise from 'bluebird'

function setup(idx = 1, type = 'comments', isLoggedIn = false, textLength = 0) {
  let text
  let commentsText
  let repliesText
  if(textLength > 0) {
    text = loremIpsum({ count: textLength, units: 'words' })
  } else {
    text = ""
  }
  if(type === 'comments') {
    commentsText = { [`${idx}`]: text  }
  } else {
    commentsText = {}
  }
  if(type === 'replies') {
    repliesText = { [`${idx}`]: text }
  } else {
    repliesText = {}
  }
  const state = {
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1',
      }
    },
    text: { comments: commentsText, replies: repliesText },
    session: {
      session: {
        isLoggedIn,
        locale: 'en'
      }
    }
  }

  const actions = {
    textUpdate: expect.createSpy(),
    createComment: expect.createSpy()
  }

  return {
    idx,
    state,
    actions,
    type
  }
}

describe('containers/shared/comment/TextInputContainer', () => {
  describe('mapStateToProps', () => {

    context('when the type is replies', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup(1,'replies',true,10)

        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: state.text.replies[1] },
          idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments/1/replies`,
          isTextOverLength: false,
          textLength: state.text.replies[1].length,
          leaveComment: "en.comment.leaveReply",
          type: 'replies',
          isLoggedIn: state.session.session.isLoggedIn,
          postMessageId: 'en.comment.post',
          loginPromptMessageId: 'en.comment.loginPrompt',
          textAreaProps: {
            disabled: false,
            value: state.text.replies[1],
          },
          inputBtnProps: {
            disabled: false,
          }
        })
      })
    })

    context('when there is text in the store', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup(1,'comments',true,10)

        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: state.text.comments[1] },
          idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments`,
          isTextOverLength: false,
          textLength: state.text.comments[1].length,
          leaveComment: "en.comment.leaveComment",
          type: 'comments',
          isLoggedIn: state.session.session.isLoggedIn,
          postMessageId: 'en.comment.post',
          loginPromptMessageId: 'en.comment.loginPrompt',
          textAreaProps: {
            disabled: false,
            value: state.text.comments[1],
          },
          inputBtnProps: {
            disabled: false,
          }
        })
      })
    })

    context('when there is text in the store and the user isn\'t logged in', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup(1,'comments',false,10)

        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: "" },
          idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments`,
          isTextOverLength: false,
          textLength: 0,
          leaveComment: 'en.comment.leaveComment',
          type: 'comments',
          isLoggedIn: state.session.session.isLoggedIn,
          loginPromptMessageId: 'en.comment.loginPrompt',
          postMessageId: 'en.comment.post',
          textAreaProps: {
            disabled: true,
            value: "",
          },
          inputBtnProps: {
            disabled: true,
          }
        })
      })
    })

    context('when the text length is over 1000', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup(1,'comments',true,1000)

        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: state.text.comments[1] },
          idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments`,
          isTextOverLength: true,
          textLength: state.text.comments[1].length,
          leaveComment: 'en.comment.leaveComment',
          isLoggedIn: state.session.session.isLoggedIn,
          type: 'comments',
          loginPromptMessageId: 'en.comment.loginPrompt',
          postMessageId: 'en.comment.post',
          textAreaProps: {
            disabled: false,
            value: state.text.comments[1],
          },
          inputBtnProps: {
            disabled: true
          }
        })
      })
    })

    context('when the type is comments', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup()

        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: "" },
          idx: idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments`,
          isTextOverLength: false,
          textLength: state.text.comments[1].length,
          isLoggedIn: state.session.session.isLoggedIn,
          leaveComment: 'en.comment.leaveComment',
          loginPromptMessageId: 'en.comment.loginPrompt',
          postMessageId: 'en.comment.post',
          type: 'comments',
          textAreaProps: {
            disabled: true,
            value: "",
          },
          inputBtnProps: {
            disabled: true
          }
        })
      })
    })

    context('when the user is logged in', () => {
      it('returns the correct props', () => {
        const { state, idx, actions, type } = setup(1,'comments',true)
        expect(mapStateToProps(state,{ idx, type })).toEqual({
          data: { text: "" },
          idx: idx,
          pathname: `${state.routing.locationBeforeTransitions.pathname}/comments`,
          isTextOverLength: false,
          isLoggedIn: state.session.session.isLoggedIn,
          textLength: state.text.comments[1].length,
          leaveComment: 'en.comment.leaveComment',
          type: 'comments',
          loginPromptMessageId: 'en.comment.loginPrompt',
          postMessageId: 'en.comment.post',
          textAreaProps: {
            disabled: false,
            value: "",
          },
          inputBtnProps: {
            disabled: true,
          }
        })
      })
    })
  })

  describe('mergeProps', () => {

    context('when type is comments', () => {
      context('when isLoggedIn is true', () => {
        it('returns a bound submit function', () => {
          const { state, idx, actions, type } = setup(1,'comments',true)
          const stateProps = mapStateToProps(state,{ idx, type })

          const fakeDispatchProps = {
            actions: {
              create: expect.createSpy().andReturn(Promise.resolve()),
              closeReply: expect.createSpy(),
              promptLogin: expect.createSpy(),
              updateText: expect.createSpy(),
            }
          }

          const mergedProps = mergeProps(stateProps,fakeDispatchProps)
          mergedProps.actions.submit()
          expect(fakeDispatchProps.actions.create).toHaveBeenCalledWith({ type, data: stateProps.data, idx: stateProps.idx, pathname: stateProps.pathname })
          expect(fakeDispatchProps.actions.closeReply).toNotHaveBeenCalled()
        })

        it('returns a bound updateText function', () => {
          const { state, idx, actions, type } = setup(1,'comments',true)
          const stateProps = mapStateToProps(state,{ idx, type })

          const fakeDispatchProps = {
            actions: {
              create: expect.createSpy().andReturn((Promise.resolve())),
              promptLogin: expect.createSpy(),
              updateText: expect.createSpy(),
            }
          }

          const mergedProps = mergeProps(stateProps,fakeDispatchProps)
          mergedProps.actions.updateText('text')
          expect(fakeDispatchProps.actions.updateText).toHaveBeenCalledWith({ idx, type, text: 'text' })
        })

        it('returns a promptLogin function', () => {
          const { state, idx, actions, type } = setup(1,'comments',true)
          const stateProps = mapStateToProps(state,{ idx, type })

          const fakeDispatchProps = {
            actions: {
              create: expect.createSpy().andReturn((Promise.resolve())),
              promptLogin: expect.createSpy(),
              updateText: expect.createSpy(),
            }
          }
          const mergedProps = mergeProps(stateProps,fakeDispatchProps)
          mergedProps.actions.updateText('text')
          expect(fakeDispatchProps.actions.promptLogin).toNotHaveBeenCalled()
        })
      })
    })

    context('when type is replies', () => {
      it('returns a bound submit function', () => {
        const { state, idx, actions, type } = setup(1,'replies',true)
        const stateProps = mapStateToProps(state,{ idx, type })

        const fakeDispatchProps = {
          actions: {
            create: expect.createSpy().andReturn(Promise.resolve()),
            closeReply: expect.createSpy(),
            promptLogin: expect.createSpy(),
            updateText: expect.createSpy(),
          }
        }

        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        return mergedProps.actions.submit().then(() => {
          expect(fakeDispatchProps.actions.create).toHaveBeenCalledWith({ type, data: stateProps.data, idx: stateProps.idx, pathname: stateProps.pathname })
          expect(fakeDispatchProps.actions.closeReply).toHaveBeenCalledWith(stateProps.idx)
        })
      })
    })

    context('when isLoggedIn is false', () => {
      it('returns a promptLogin function', () => {
        const { state, idx, actions, type } = setup(1,'comments',false)
        const stateProps = mapStateToProps(state,{ idx, type })

        const fakeDispatchProps = {
          actions: {
            create: expect.createSpy().andReturn((Promise.resolve())),
            promptLogin: expect.createSpy(),
            updateText: expect.createSpy(),
          }
        }
        const mergedProps = mergeProps(stateProps,fakeDispatchProps)
        mergedProps.actions.promptLogin()
        expect(fakeDispatchProps.actions.promptLogin).toHaveBeenCalled()
      })
    })
  })
})
