import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/CommentContainer'
import commentResponse from '../../../testHelpers/commentResponse'
import { omit } from 'lodash'

function setup (idx = 1,type='comments', status='Visible') {
  const commentOne = omit(commentResponse(1),'replies')
  const commentTwo = omit(commentResponse(2),'replies')

  const state = {
    comments: {
      comments: { [commentOne.idx]: Object.assign({},commentOne,{ status, replies: { content: [1,2], pagination: { currentPage: 0, isLast: false } } }), [commentTwo.idx]: Object.assign({},commentTwo,{ status, replies: { content: [3,4], pagination: { currentPage: 1, isLast: true } } }) },
      ui: {
        editOpen: {
          comments: []
        }
      }
    },
    routing: {
      locationBeforeTransitions: {
        pathname: '/polls/1'
      }
    }
  }

  return {
    idx,
    state,
    commentOne,
    commentTwo,
  }
}

describe('containers/shared/comment/CommentContainer', () => {
  context('when the idx is a key in the comments object', () => {
    it('returns the correct props when isLast is false', () => {
      const { idx, state, commentOne, commentTwo, type } = setup(1,'comments')
      const expected = {
        ...commentOne,
        isEditOpen: false,
        nextPage: {
          pathname: '/polls/1/comments/1/reply',
          query: { page: 1 },
          type: 'replies'
        },
        type: 'comments'
      }

      expect(mapStateToProps(state,{ idx, type })).toEqual(expected)
    })

    it('returns the correct props when isLast is true', () => {
      const { idx, state, commentOne, commentTwo, type } = setup(2,'comments')
      const expected = {
        ...commentTwo,
        isEditOpen: false,
        nextPage: undefined,
        type: 'comments'
      }

      expect(mapStateToProps(state,{ idx, type })).toEqual(expected)
    })
  })

  context('when the idx isn\'t a key in the comments object', () => {
    it('returns an empty object', () => {
      const { idx, state, type } = setup(3,'comments')

      expect(mapStateToProps(state,{ idx, type })).toEqual({
        type: 'comments',
        isEditOpen: false,
        nextPage: undefined
      })
    })
  })
})
