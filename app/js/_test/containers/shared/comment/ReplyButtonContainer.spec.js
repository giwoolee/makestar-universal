import expect from 'expect'
import { mapStateToProps } from '../../../../containers/shared/comment/ReplyButtonContainer'
import commentResponse from '../../../testHelpers/commentResponse'

function setup (replyOpen = [],isLoggedIn = false) {
  const state = {
    session: {
      session: {
        isLoggedIn,
        locale: 'en'
      }
    },
    comments: {
      ui: {
        replyOpen
      }
    }
  }

  return state
}

describe('containers/comment/ReplyButtonContainer', () => {

  describe('mapStateToProps', () => {
    context('when the reply is open', () => {
      it('returns the correct props', () => {
        const state = setup([1],true)

        expect(mapStateToProps(state,{ idx: 1 })).toEqual({
          idx: 1,
          replyMessageId: 'en.comment.reply',
          isReplyOpen: true,
          isLoggedIn: true
        })
      })
    })

    context('when the reply is closed', () => {
      it('returns the correct props', () => {
        const state = setup([1],true)

        expect(mapStateToProps(state,{ idx: 2 })).toEqual({
          idx: 2,
          replyMessageId: 'en.comment.reply',
          isReplyOpen: false,
          isLoggedIn: true
        })
      })
    })
  })
})
