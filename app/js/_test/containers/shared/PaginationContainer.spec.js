import expect from 'expect'
import { mapStateToProps, mapDispatchToProps } from '../../../containers/shared/PaginationContainer'
import { routerActions } from 'react-router-redux'
import segmentize from 'segmentize'

function setup(pathname='/polls',query={ status: 'All' },pushType = true,changeLink = undefined,defaultQuery={},centered=true,type='Polls') {
  let state = {
    routing: {
      locationBeforeTransitions: {
        pathname,
        query
      }
    },
    pagination: {
      currentPage: 0,
      isLast: true,
      totalElements: 1,
      totalPages: 1,
    },
  }

  const ownProps = {
    pushType,
    changeLink,
    defaultQuery,
    centered,
    type
  }

  return { state, ownProps }
}

describe('containers',() => {
  describe('PaginationContainer', () => {
    context('when defaultQuery is empty', () => {
      it('sets the correct props', () => {
        const { state,ownProps } = setup()
        expect(mapStateToProps(state,ownProps)).toEqual({
          pathname: state.routing.locationBeforeTransitions.pathname,
          query: state.routing.locationBeforeTransitions.query,
          centered: true,
          type: ownProps.type,
          ...segmentize({
            page: 1,
            pages: state.pagination.totalPages,
            sidePages: 1,
            beginPages: 2,
            endPages: 10
          })
        })
      })
    })

    context('when defaultQuery isn\'t empty and no query is passed', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup('/polls',{},true, undefined,{ status: 'Open' })
        expect(mapStateToProps(state,ownProps)).toEqual({
          pathname: state.routing.locationBeforeTransitions.pathname,
          query: { status: 'Open' },
          centered: true,
          type: ownProps.type,
          ...segmentize({
            page: 1,
            pages: state.pagination.totalPages,
            sidePages: 1,
            beginPages: 2,
            endPages: 10
          })
        })
      })
    })

    context('when defaultQuery isn\'t empty and a query is passed', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup('/polls',{ status: 'Closed' },true, undefined,{ status: 'Open' })
        expect(mapStateToProps(state,ownProps)).toEqual({
          pathname: state.routing.locationBeforeTransitions.pathname,
          query: { status: 'Closed' },
          centered: true,
          type: ownProps.type,
          ...segmentize({
            page: 1,
            pages: state.pagination.totalPages,
            sidePages: 1,
            beginPages: 2,
            endPages: 10
          })
        })
      })
    })

    context('when centered is false', () => {
      it('sets the correct props', () => {
        const { state, ownProps } = setup('/polls',{ status: 'Closed' },true, undefined,{ status: 'Open' },false)
        expect(mapStateToProps(state,ownProps)).toEqual({
          pathname: state.routing.locationBeforeTransitions.pathname,
          query: { status: 'Closed' },
          centered: false,
          type: ownProps.type,
          ...segmentize({
            page: 1,
            pages: state.pagination.totalPages,
            sidePages: 1,
            beginPages: 2,
            endPages: 10
          })
        })
      })
    })

    context('when the pagination is push type', () => {
      it('returns an object with changeLink as a function', () => {
        const { pushType, changeLink } = setup()
        const fakeDispatch = expect.createSpy()

        const result = mapDispatchToProps(fakeDispatch,{ pushType, changeLink })

        expect(result.changeLink).toBeA(Function)
      })

      it('the returned changeLink function calls dispatch with routerActions.push', () => {
        const { state, ownProps } = setup()
        const fakeDispatch = expect.createSpy()

        const result = mapDispatchToProps(fakeDispatch,ownProps)

        result.changeLink('link')
        expect(fakeDispatch).toHaveBeenCalledWith(routerActions.push('link'))
      })
    })

    context('when the pagination is non push type', () => {
      it('returns an object with a changeLink function', () => {
        const fakeChangeLink = expect.createSpy()
        const fakeDispatch = expect.createSpy()
        const { state, ownProps } = setup('/projects/updates',{},false,fakeChangeLink)

        const result = mapDispatchToProps(fakeDispatch,ownProps)
        result.changeLink()
        expect(fakeChangeLink).toHaveBeenCalled()
      })
    })
  })
})

