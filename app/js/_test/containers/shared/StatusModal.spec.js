import expect from 'expect'
import { mapStateToProps } from '../../../containers/shared/StatusModal'
import { COMMENT_CREATE_FAILURE } from '../../../constants/comment/index'
import { POLL_VOTE_FAILURE } from '../../../constants/poll/show/index'

function setup(status = 401, type = POLL_VOTE_FAILURE) {
  let ui
  let session

  if(status !== 200) {
    ui = {
      modal: {
        error: true,
        status,
        type
      }
    }
    session = {
      session: {
        locale: 'en'
      }
    }
  } else {
    ui = {
      modal: {
        error: undefined,
        status: undefined,
        type: undefined
      }
    }
    session = {
      session: {
        locale: 'en'
      }
    }
  }

  const state = {
    ui,
    session
  }
  return state
}

describe('containers/shared/StatusModal',() => {

  context('when there are errors and the type is POLL_VOTE_FAILURE with status 403', () => {
    it('sets the correct props', () => {
      const state = setup(403,POLL_VOTE_FAILURE)
      expect(mapStateToProps(state)).toEqual(
        {
          titleMessageId: 'en.common.verifyFirstTitle',
          bodyMessageId: 'en.common.verifyFirstMessage',
          links: {
            primary: '/user/profile.do'
          },
          show: true
        }
      )
    })
  })
})
