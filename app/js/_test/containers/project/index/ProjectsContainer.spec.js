import expect from 'expect'
import { mapStateToProps } from '../../../../containers/project/index/ProjectsContainer'

function setup() {
  const state = {
    projects: {
      idxs: [1,2,3,4],
    }
  }

  return {
    state
  }
}

describe('containers/project/Project', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state } = setup()
      const expected = {
        idxs: [[1,2],[3,4]]
      }

      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
