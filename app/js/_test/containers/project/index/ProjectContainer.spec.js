import expect from 'expect'
import { mapStateToProps } from '../../../../containers/project/index/ProjectContainer'
import projectResponse from '../../../testHelpers/projectResponse'

function setup() {
  const project = projectResponse()
  const state = {
    projects: {
      projects: { 1: project }
    }
  }

  return {
    state,
    project
  }
}

describe('containers/project/index/ProjectContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { project, state } = setup()
      expect(mapStateToProps(state,{ idx: project.idx })).toEqual(project)
    })
  })
})
