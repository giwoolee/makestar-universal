import expect from 'expect'
import { mapStateToProps } from '../../../../containers/project/index/HelmetContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    }
  }

  return state
}

describe('containers', () => {
  describe('project/index/HelmetContainer',() => {
    it('returns the correct props', () => {
      const state = setup()

      const expected = {
        title: 'Projects',
        meta: [
          {
            'name': 'og:title', 'content': 'Projects',
          },
          {
            'name': 'description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': 'http:///projects'
          },
          {
            'rel': 'alternate', 'href': 'http:///projects?locale=ko', 'hreflang':'ko'
          },
          {
            'rel': 'alternate', 'href': 'http:///projects?locale=ja', 'hreflang':'ja'
          },
          {
            'rel': 'alternate', 'href': 'http:///projects?locale=zh', 'hreflang':'zh'
          }
        ]
      }

      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
