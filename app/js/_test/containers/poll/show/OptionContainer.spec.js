import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/show/OptionContainer'
import normalizedPoll from '../../../testHelpers/normalizedPoll'

function setup() {
  const rank = {
    "idx": 1,
    "imageUrl": "https://dl.dropboxusercontent.com/s/ezguqrhg4p7387d/2016-01-10%2014.35.32.jpg?dl=0",
    "name": "Option 1",
    "myVoteCount": 0,
    "voteCount": 1,
    "linkText": "text"
  }

  const poll = normalizedPoll()

  const state = {
    polls: {
      polls: { 1: poll },
      ranks: { 1: rank }
    },
    session: {
      session: { isLoggedIn: false }
    }
  }

  return {
    state,
    rank,
    poll
  }
}

describe('containers/poll/show/RankContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, rank, poll } = setup()
      const expected = {
        ...rank,
        isAvailable: poll.isAvailable,
        totalVoteCount: poll.voteCount,
        pollIdx: poll.idx,
        isEnd: poll.isEnd,
        isLoggedIn: false
      }

      expect(mapStateToProps(state,{ idx: 1, pollIdx: 1 })).toEqual(expected)
    })
  })
})
