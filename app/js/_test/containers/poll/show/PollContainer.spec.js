import { mapStateToProps } from '../../../../containers/poll/show/PollContainer'
import normalizedPoll from '../../../testHelpers/normalizedPoll'
import expect from 'expect'
import { chunk } from 'lodash'

function setup() {
  const poll = normalizedPoll()

  const state = {
    poll: { idx: 1 },
    session: { session: { locale: 'en' } },
    share: { share: { title: poll.title, description: poll.description, locale: 'en', canonical: 'url', snsimageUrl: 'snsurl' } },
    polls: {
      polls: {
        1: poll
      }
    }
  }

  return {
    poll,
    state
  }
}

describe('containers/poll/show/PollContainer', () => {
  it('returns the correct props', () => {
    const { state, poll } = setup()

    expect(mapStateToProps(state)).toEqual({ ...poll, ranks: chunk(poll.rank,2), locale: 'en' })
  })
})

