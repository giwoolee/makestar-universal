import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/show/HelmetContainer'
import pollResponse from '../../../testHelpers/pollResponse'

function setup() {
  const poll = pollResponse()
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    },
    polls: {
      polls: {
        1: poll
      }
    },
    poll: {
      idx: 1
    }
  }

  return {
    state,
    poll
  }
}


describe('containers',() => {
  describe('poll/show/HelmetContainer',() => {
    it('returns the correct props', () => {
      const { state, poll } = setup()

      const expected = {
        title: poll.title,
        meta: [
          {
            'name': 'og:title', 'content': poll.title
          },
          {
            'name': 'description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:image', 'content': poll.snsimageUrl
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': poll.links[1].href
          },
          {
            'rel': 'alternate', 'href': `${poll.links[1].href}?locale=ko`, 'hreflang': 'ko'
          },
          {
            'rel': 'alternate', 'href': `${poll.links[1].href}?locale=ja`, 'hreflang': 'ja'
          },
          {
            'rel': 'alternate', 'href': `${poll.links[1].href}?locale=zh`, 'hreflang': 'zh'
          }
        ]
      }
      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
