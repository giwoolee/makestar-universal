import { mapStateToProps } from '../../../../containers/poll/index/ThumbnailContainer'
import expect from 'expect'
import normalizedPoll from '../../../testHelpers/normalizedPoll'

function setup() {
  const poll = normalizedPoll(1,'thumbnailurl')

  const state = {
    polls: { polls: { 1: poll } },
    ui: { isInitialLoad: false }
  }
  return {
    poll,
    state
  }
}

describe('containers/poll/index/ThumbnailContainer', () => {

  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, poll } = setup()

      const expected = {
        thumbnailUrl: `url(${poll.thumbnailUrl})`,
        isInitialLoad: false
      }
      expect(mapStateToProps(state,{ idx: 1 })).toEqual(expected)
    })
  })
})
