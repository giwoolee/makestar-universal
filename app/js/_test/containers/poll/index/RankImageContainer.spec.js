import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/index/RankImageContainer'
import normalizedPoll from '../../../testHelpers/normalizedPoll'

function setup(isInitialLoad = false) {
  const poll = normalizedPoll()
  const rank = {
    idx: 1,
    imageUrl: 'url'
  }
  const state = {
    polls: {
      idxs: [poll.idx],
      polls: { [poll.idx]: poll },
      ranks: {
        1: rank, 2: rank, 3: rank, 4: rank, 5: rank
      }
    },
    ui: {
      isInitialLoad
    }
  }

  return {
    poll,
    rank,
    state
  }
}

describe('containers/poll/index/RankImageContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, poll, rank } = setup()

      const expected = {
        imageClass: 'col-xs-6 col-sm-6 col-md-6 col-lg-6 poll-rank-image-split-large',
        backgroundImage: `url(${rank.imageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      }

      const ownProps = {
        index: 0,
        length: 5,
        idx: 1
      }

      expect(mapStateToProps(state,ownProps)).toEqual(expected)
    })

    it('returns the correct props', () => {
      const { state, poll, rank } = setup()

      const expected = {
        imageClass: 'col-xs-4 col-sm-4 col-md-4 col-lg-4 poll-rank-image-split-small',
        backgroundImage: `url(${rank.imageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      }

      const ownProps = {
        index: 2,
        length: 5,
        idx: 3
      }

      expect(mapStateToProps(state,ownProps)).toEqual(expected)
    })

    context('when the length is less than 5 and atleast 3', () => {
      it('returns the correct props', () => {
        const { state, poll, rank } = setup()

        const expected = {
          imageClass: 'col-xs-4 col-sm-4 col-md-4 col-lg-4 poll-rank-image-3-way-split',
          backgroundImage: `url(${rank.imageUrl})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'cover'
        }

        const ownProps = {
          index: 0,
          length: 3,
          idx: 1
        }

        expect(mapStateToProps(state,ownProps)).toEqual(expected)
      })
    })

    context('when the length is less than 3', () => {
      it('returns the correct props', () => {
        const { state, poll, rank } = setup()

        const expected = {
          imageClass: 'col-xs-6 col-sm-6 col-md-6 col-lg-6 poll-rank-image-large',
          backgroundImage: `url(${rank.imageUrl})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'cover'
        }

        const ownProps = {
          index: 0,
          length: 2,
          idx: 1
        }

        expect(mapStateToProps(state,ownProps)).toEqual(expected)
      })
    })
  })
})
