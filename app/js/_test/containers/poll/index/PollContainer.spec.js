import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/index/PollContainer'
import pollResponse from '../../../testHelpers/pollResponse'

function setup() {
  const poll = pollResponse()
  const state = {
    polls: {
      polls: {
        [poll.idx]: poll
      }
    }
  }
  return {
    poll,
    state
  }
}

describe('containers/poll/index/PollContainer', () => {
  it('returns the correct props', () => {
    const { poll, state } = setup()

    expect(mapStateToProps(state,{ idx: poll.idx })).toEqual(poll)
  })
})
