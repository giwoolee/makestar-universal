import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/index/HelmetContainer'

function setup() {
  const state = {
    session: {
      session: {
        locale: 'en',
        isLoggedIn: false
      }
    }
  }

  return state
}


describe('containers',() => {
  describe('poll/index/HelmetContainer',() => {
    it('returns the correct props', () => {
      const state = setup()

      const expected = {
        title: 'Polls',
        meta: [
          {
            'name': 'description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          },
          {
            'name': 'og:description', 'content': 'Global Hallyu crowdfunding. Offering crowdfunding projects and contents in kpop, drama, movies, and publishing.'
          }
        ],
        link: [
          {
            'rel': 'canonical', 'href': 'http:///polls'
          },
          {
            'rel': 'alternate', 'href': 'http:///polls?locale=ko', 'hreflang':'ko'
          },
          {
            'rel': 'alternate', 'href': 'http:///polls?locale=ja', 'hreflang':'ja'
          },
          {
            'rel': 'alternate', 'href': 'http:///polls?locale=zh', 'hreflang':'zh'
          }
        ]
      }
    })
  })
})
