import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/index/RanksContainer'
import pollResponse from '../../../testHelpers/pollResponse'
import { times } from 'lodash'

function setup(numOfRanks = 5) {
  const poll = pollResponse()
  const state = {
    polls: {
      polls: {
        1: {
          rank: times(numOfRanks,i => i + 1)
        }
      },
      ranks: {
        1: poll.rank[0], 2: poll.rank[1], 3: poll.rank[2], 4: poll.rank[3], 5: poll.rank[4]
      }
    }
  }

  return { poll, state }
}

describe('containers/poll/index/RanksContainer', () => {
  describe('mapStateToProps', () => {
    context('when the numOfRanks is >= 5', () => {
      it('returns the correct props', () => {
        const { poll, state } = setup()

        const expected = { ranks: [1,2,3,4,5] }

        expect(mapStateToProps(state,{ idx: 1 })).toEqual(expected)
      })
    })

    context('when the numOfRanks is >= 3 and < 5', () => {
      it('returns the correct props', () => {
        const { poll, state } = setup(4)

        const expected = { ranks: [1,2,3] }

        expect(mapStateToProps(state,{ idx: 1 })).toEqual(expected)
      })
    })

    context('when the numOfRanks is < 3', () => {
      it('returns the correct props', () => {
        const { poll, state } = setup(2)

        const expected = { ranks: [1] }

        expect(mapStateToProps(state,{ idx: 1 })).toEqual(expected)
      })
    })
  })
})

