import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/index/PollsContainer'
import pollResponse from '../../../testHelpers/pollResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import sessionResponse from '../../../testHelpers/sessionResponse'

function setup() {
  const poll = pollResponse()
  const pagination = paginationResponse()
  const session = sessionResponse()
  const state = {
    polls: {
      idxs: [poll.idx],
      selectedStatus: 'All',
      numOfPolls: 1
    },
    pagination
  }

  return {
    poll,
    pagination,
    session,
    state
  }
}

describe('containers/poll/index/PollsContainer', () => {
  describe('mapStateToProps', () => {
    it('returns the correct props', () => {
      const { state, poll, pagination } = setup()
      const expected = {
        idxs: [[poll.idx]],
        selectedStatus: 'All',
        numOfPolls: 1
      }

      expect(mapStateToProps(state)).toEqual(expected)
    })
  })
})
