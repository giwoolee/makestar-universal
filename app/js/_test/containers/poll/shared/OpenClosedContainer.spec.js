import expect from 'expect'
import moment from 'moment'
import { useFakeTimers } from 'sinon'
import { mapStateToProps } from '../../../../containers/poll/shared/OpenClosedContainer'
import pollResponse from '../../../testHelpers/pollResponse'

function setup(isEnd,startDate,endDate) {
  const poll = Object.assign({},pollResponse(),{ isEnd, startDate, endDate })
  const state = {
    polls: {
      polls: {
        [poll.idx]: poll
      }
    },
    session: {
      session: { locale: 'en' }
    }
  }
  return {
    poll,
    state
  }
}

describe('containers/poll/shared/OpenClosedContainer',() => {
  let clock = undefined
  beforeEach(()=> {
    clock = useFakeTimers(moment('2016-01-01').hour(0).minute(0).second(0).valueOf())
  })
  afterEach(()=> {
    clock.restore()
  })
  context('when the poll has ended',() => {
    it('returns the correct props', () => {
      const endDate = moment().add(2,'days').format('YYYY-MM-DD')
      const startDate = moment().format('YYYY-MM-DD')
      const { poll, state } = setup(true,startDate,endDate)
      const expected = {
        isEnd: poll.isEnd,
        timeRemaining: { messageId: 'en.poll.off', values: { days: undefined } }
      }

      expect(mapStateToProps(state,{ idx: poll.idx })).toEqual(expected)
    })
  })
  context('when the poll hasn\'t ended',() => {
    it('returns the correct props', () => {
      const endDate = moment().add(2,'days').format('YYYY-MM-DD')
      const startDate = moment().format('YYYY-MM-DD')
      const { poll, state } = setup(false,startDate,endDate)
      const expected = {
        isEnd: poll.isEnd,
        timeRemaining: { messageId: 'en.poll.on', values: { days: 3 } }
      }

      expect(mapStateToProps(state,{ idx: poll.idx })).toEqual(expected)
    })
  })
  context('when the poll isn\'t defined',() => {
    it('returns the correct props', () => {
      const endDate = moment().add(2,'days').format('YYYY-MM-DD')
      const startDate = moment().format('YYYY-MM-DD')
      const { poll, state } = setup(false,startDate,endDate)
      const expected = {
        isEnd: undefined,
        timeRemaining: {
          messageId: 'en.poll.off',
          values: {
            days: undefined
          }
        }
      }

      expect(mapStateToProps(state,{ idx: undefined })).toEqual(expected)
    })
  })
})
