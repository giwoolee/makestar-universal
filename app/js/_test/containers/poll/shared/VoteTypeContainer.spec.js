import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/shared/VoteTypeContainer'
import pollResponse from '../../../testHelpers/pollResponse'

function setup(votingType) {
  const poll = pollResponse()
  const state = {
    polls: {
      polls: {
        1: { votingType }
      }
    },
    session: {
      session: { locale: 'en' }
    }
  }
  return {
    poll,
    state
  }
}

describe('containers/poll/shared/VoteTypeContainer', () => {

  context('when poll is defined', () => {
    context('when votingType is ONCE',() =>{
      it('returns the correct props', () => {

        const { poll, state } = setup('ONCE')
        const expected = {
          className: 'poll-possible-votes-once',
          messageId: 'en.poll.voteOnce'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
    context('when votingType is RE1',() =>{
      it('returns the correct props', () => {

        const { poll, state } = setup('RE1')
        const expected = {
          className: 'poll-possible-votes-re1',
          messageId: 'en.poll.oneVoteOneHour'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
    context('when votingType is RE6',() =>{
      it('returns the correct props', () => {

        const { poll, state } = setup('RE6')
        const expected = {
          className: 'poll-possible-votes-re6',
          messageId: 'en.poll.oneVoteSixHours'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
    context('when votingType is RE12',() =>{
      it('returns the correct props', () => {

        const { poll, state } = setup('RE12')
        const expected = {
          className: 'poll-possible-votes-re12',
          messageId: 'en.poll.oneVoteTwelveHours'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
    context('when votingType is RE24',() =>{
      it('returns the correct props', () => {

        const { poll, state } = setup('RE24')
        const expected = {
          className: 'poll-possible-votes-re24',
          messageId: 'en.poll.oneVoteOneDay'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
  })

  context('when vote is undefined', () => {
    it('returns the correct props', () => {
      const { poll, state } = setup()
      const expected = {
        className: undefined,
        messageId: 'en.poll.oneVoteOneDay'
      }

      expect(mapStateToProps(state, {idx: 2})).toEqual(expected)
    })
  })
})