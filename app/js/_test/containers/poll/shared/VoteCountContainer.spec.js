import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/shared/VoteCountContainer'

function setup(myVoteCount) {
  const state = {
    polls: {
      polls: {
        1: { myVoteCount }
      }
    },
    session: {
      session: { locale: 'en' }
    }
  }
  return {
    state
  }
}

describe('containers/poll/shared/VoteCountContainer', () => {

  context('when poll is defined', () => {
    context('when myVountCount is > 0',() =>{
      it('returns the correct props', () => {

        const myVoteCount = 1;
        const { state } = setup(myVoteCount)
        const expected = {
          id: 'en.poll.voted',
          myVoteCount,
          iconClass: 'poll-vote-count-on',
          textClass: 'poll-on-text'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })


    })

    context('when myVountCount is === 0',() =>{
      it('returns the correct props', () => {

        const myVoteCount = 0;
        const { state } = setup(myVoteCount)
        const expected = {
          id: 'en.poll.voted',
          myVoteCount,
          iconClass: 'poll-vote-count-off',
          textClass: 'poll-off-text'
        }

        expect(mapStateToProps(state, {idx: 1})).toEqual(expected)
      })
    })
  })

  context('when vote is undefined', () => {
    it('returns the correct props', () => {
      const myVoteCount = 0;
      const { state } = setup(myVoteCount)
      const expected = {
          id: 'en.poll.voted',
          myVoteCount,
          iconClass: 'poll-vote-count-off',
          textClass: 'poll-off-text'
        }

      expect(mapStateToProps(state, {idx: 2})).toEqual(expected)
    })
  })
})