import expect from 'expect'
import { mapStateToProps } from '../../../../containers/poll/shared/StatusContainer'
import pollResponse from '../../../testHelpers/pollResponse'

function setup() {
  const poll = pollResponse()
  const state = {
    polls: {
      polls: {
        [poll.idx]: poll
      }
    }
  }
  return {
    poll,
    state
  }
}

describe('containers/poll/shared/StatusContainer', () => {
  context('when poll is defined', () => {
    it('return the correct props', () => {
      const { poll, state } = setup()
      const expected = {
        idx: poll.idx,
        myVoteCount: poll.myVoteCount,
        voteCount: poll.voteCount
      }

      expect(mapStateToProps(state, { idx: poll.idx })).toEqual(expected)

    })
  })

  context('when poll is undefined', () => {
    it('return the correct props', () => {
      const { poll, state } = setup()
      const expected = {
        idx: undefined,
        myVoteCount: undefined,
        voteCount: undefined
      }

      expect(mapStateToProps(state, { idx: 2 })).toEqual(expected)
    })
  })
})
