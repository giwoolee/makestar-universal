import expect from 'expect'
import contentResponse from '../../testHelpers/contentsResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import { receiveOne, receiveAll } from '../../../schemas/content/index'

function setup() {
  const contentOne = contentResponse(1)
  const contentTwo = contentResponse(2)
  const pagination = paginationResponse(0)

  const multipleResponse = {
    pagination,
    content: [
      contentOne,
      contentTwo
    ]
  }

  const singleResponse = {
    content: contentOne
  }

  return {
    pagination,
    contentOne,
    contentTwo,
    multipleResponse,
    singleResponse
  }
}

describe('schemas/content/index', () => {
  describe('receiveOne', () => {
    it('creates a normalized object', () => {
      const { singleResponse, contentOne, pagination } = setup()

      expect(receiveOne(singleResponse)).toEqual({
        entities: {
          content: { [contentOne.idx]: contentOne }
        },
        result: { content: contentOne.idx }
      })
    })
  })

  describe('receiveAll', () => {
    it('creates a normalized object', () => {
      const { multipleResponse, contentOne, contentTwo, pagination } = setup()

      expect(receiveAll(multipleResponse)).toEqual({
        entities: {
          content: { [contentOne.idx]: contentOne, [contentTwo.idx]: contentTwo }
        },
        result: { content: [contentOne.idx,contentTwo.idx], pagination }
      })
    })
  })
})
