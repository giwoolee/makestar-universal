import expect from 'expect'
import projectUpdateResponse from '../../../testHelpers/projectUpdateResponse'
import paginationResponse from '../../../testHelpers/paginationResponse'
import { receiveOne, receiveAll } from '../../../../schemas/project/update/index'

function setup() {
  const updateOne = projectUpdateResponse(1)
  const updateTwo = projectUpdateResponse(2)
  const pagination = paginationResponse(0)

  const multipleResponse = {
    pagination,
    content: [
      updateOne,
      updateTwo
    ]
  }

  const singleResponse = {
    content: updateOne
  }

  return {
    pagination,
    updateOne,
    updateTwo,
    multipleResponse,
    singleResponse
  }
}

describe('schemas/project/update/index', () => {
  describe('receiveOne', () => {
    it('returns a normalized object', () => {
      const { singleResponse, updateOne } = setup()

      expect(receiveOne(singleResponse)).toEqual({
        entities: {
          projectUpdate: { [updateOne.idx]: updateOne }
        },
        result: { content: updateOne.idx }
      })
    })
  })

  describe('receiveAll', () => {
    it('returns a normalized object', () => {
      const { multipleResponse, updateOne, updateTwo, pagination } = setup()
      expect(receiveAll(multipleResponse)).toEqual({
        entities: {
          projectUpdate: { [updateOne.idx]: updateOne, [updateTwo.idx]: updateTwo }
        },
        result: { content: [updateOne.idx,updateTwo.idx], pagination }
      })
    })
  })
})

