import expect from 'expect'
import projectResponse from '../../testHelpers/projectResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import { receiveOne, receiveAll } from '../../../schemas/project/index'

function setup() {
  const projectOne = projectResponse('stellar',1)
  const projectTwo = projectResponse('singer_nca',2)
  const pagination = paginationResponse(0)

  const multipleResponse = {
    pagination,
    content: [
      projectOne,
      projectTwo
    ]
  }

  const singleResponse = {
    content: projectOne
  }

  return {
    pagination,
    projectOne,
    projectTwo,
    multipleResponse,
    singleResponse
  }
}

describe('schemas/project/index', () => {
  describe('receiveOne', () => {
    it('returns a normalized object', () => {
      const { singleResponse, projectOne } = setup()

      expect(receiveOne(singleResponse)).toEqual({
        entities: {
          project: { [projectOne.idx]: projectOne }
        },
        result: { content: projectOne.idx }
      })
    })
  })

  describe('receiveAll', () => {
    it('returns a normalized object', () => {
      const { multipleResponse, projectOne, projectTwo, pagination } = setup()

      expect(receiveAll(multipleResponse)).toEqual({
        entities: {
          project: { [projectOne.idx]: projectOne, [projectTwo.idx]: projectTwo }
        },
        result: { content: [projectOne.idx,projectTwo.idx], pagination }
      })
    })
  })
})

