import expect from 'expect'
import pollResponse from '../../testHelpers/pollResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import { receiveOne, receiveAll } from '../../../schemas/poll/index'

function setup() {
  const pollOne = pollResponse(1)
  const pollTwo = pollResponse(2)
  const pagination = paginationResponse(0)

  const multipleResponse = {
    pagination,
    content: [
      pollOne,
      pollTwo
    ]
  }

  const singleResponse = {
    content: pollOne
  }

  return {
    pagination,
    pollOne,
    pollTwo,
    multipleResponse,
    singleResponse
  }
}

describe('schemas/poll/index', () => {
  describe('receiveOne', () => {
    it('returns a normalized object', () => {
      const { singleResponse, pollOne } = setup()

      expect(receiveOne(singleResponse)).toEqual({
        entities: {
          poll: { [pollOne.idx]: Object.assign({},pollOne,{ rank: [1,2,3,4,5] }) },
          rank: { 1: pollOne.rank[0], 2: pollOne.rank[1], 3: pollOne.rank[2], 4: pollOne.rank[3], 5: pollOne.rank[4] }
          },
          result: { content: pollOne.idx }
      })
    })
  })

  describe('receiveAll', () => {
    it('returns a normalized object', () => {
      const { multipleResponse, pollOne, pollTwo, pagination } = setup()

      expect(receiveAll(multipleResponse)).toEqual({
        entities: {
          poll: { [ pollOne.idx]: Object.assign({},pollOne,{ rank: [1,2,3,4,5] }), [pollTwo.idx]: Object.assign({},pollTwo,{ rank: [1,2,3,4,5]  }),
          },
          rank: { 1: pollOne.rank[0], 2: pollOne.rank[1], 3: pollOne.rank[2], 4: pollOne.rank[3], 5: pollOne.rank[4] }
          },
          result: { content: [pollOne.idx,pollTwo.idx], pagination }
      })
    })
  })
})

