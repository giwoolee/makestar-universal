import expect from 'expect'
import commentResponse from '../../testHelpers/commentResponse'
import paginationResponse from '../../testHelpers/paginationResponse'
import { receiveOne, receiveAll } from '../../../schemas/comment/index'

function setup() {
  const reply = commentResponse(1)
  const pagination = paginationResponse(0)
  const comment = Object.assign({},commentResponse(1),{ replies: { content:  [reply], pagination } })

  const multipleResponse = {
    pagination,
    content: [
      comment
    ]
  }

  const singleResponse = {
    content: comment
  }

  return {
    comment,
    reply,
    pagination,
    singleResponse,
    multipleResponse
  }
}

describe('schemas/comment/index', () => {
  describe('receiveOne', () => {
    it('creates a normalized object', () => {
      const { singleResponse, comment, reply, pagination } = setup()

      expect(receiveOne(singleResponse)).toEqual({
        entities:
        {
          comment: { [comment.idx]: { ...comment, replies: { content: [reply.idx], pagination } } },
          reply: { [reply.idx]: reply }
        },
        result: { content: comment.idx },
      })
    })
  })

  describe('receiveAll', () => {
    it('creates a normalized object', () => {
      const { multipleResponse, comment, reply, pagination } = setup()

      expect(receiveAll(multipleResponse)).toEqual({
        entities: {
          comment: { [comment.idx]: { ...comment, replies: { content: [reply.idx], pagination } } },
          reply: { [reply.idx]: reply }
        },
        result: { pagination, content: [comment.idx] },
      })
    })
  })
})

