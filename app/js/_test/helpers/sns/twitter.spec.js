import expect from 'expect'
import twitter from '../../../helpers/sns/twitter'

describe('helpers/sns/twitter', () => {
  it('creates a link to twitter', () => {
    expect(twitter({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://twitter.com/intent/tweet?text=Makestar%20Poll&url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1')
  })
})
