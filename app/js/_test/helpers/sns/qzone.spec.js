import expect from 'expect'
import qzone from '../../../helpers/sns/qzone'

describe('helpers/sns/qzone', () => {
  it('creates a share to qzone', () => {
    expect(qzone({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll')
  })
})
