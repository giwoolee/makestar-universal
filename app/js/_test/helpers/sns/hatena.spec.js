import expect from 'expect'
import hatena from '../../../helpers/sns/hatena'

describe('helpers/sns/hatena', () => {
  it('creates a link for hatena', () => {
    expect(hatena({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://b.hatena.ne.jp/entry/http%3A%2F%2Fmakestar.co%2Fpolls%2F1')
  })
})
