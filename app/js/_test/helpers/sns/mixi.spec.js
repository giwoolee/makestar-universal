import expect from 'expect'
import mixi from '../../../helpers/sns/mixi'

describe('helpers/sns/mixi', () => {
  it('creates a share to mixi', () => {
    expect(mixi({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://mixi.jp/share.pl?u=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&t=Makestar%20Poll')
  })
})
