import expect from 'expect'
import ameblo from '../../../helpers/sns/ameblo'

describe('helpers/sns/ameblo',() => {
  it('creates a link for ameblo', () => {
    expect(ameblo({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsimageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://blog.ameba.jp/ucs/entry/srventryinsertinput.do?entry_text=Makestar%20Poll%0D%0AMakestar%20Poll%0D%0A%3Ca%20href%3D%22http%3A%2F%2Fmakestar.co%2Fpolls%2F1%22%3E%3Cimg%20src%3Dhttp%3A%2F%2Fmakestar.co%2Ffile%2Fimages%2Fpolls%2Fsns1.jpg%3E%3C%2Fa%3E')
  })
})
