import expect from 'expect'
import pinterest from '../../../helpers/sns/pinterest'

describe('helpers/sns/pinterest', () => {
  it('creates a share to pinterest', () => {
    expect(pinterest({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsimageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('https://www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&description=Makestar%20Poll&media=http%3A%2F%2Fmakestar.co%2Ffile%2Fimages%2Fpolls%2Fsns1.jpg')
  })
})
