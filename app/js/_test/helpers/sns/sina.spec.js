import expect from 'expect'
import sina from '../../../helpers/sns/sina'

describe('helpers/sns/sina', () => {
  it('creates a link to sina', () => {
    expect(sina({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://service.weibo.com/share/share.php?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll&content=utf8')
  })
})
