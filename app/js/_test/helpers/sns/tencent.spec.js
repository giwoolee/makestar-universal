import expect from 'expect'
import tencent from '../../../helpers/sns/tencent'

describe('helpers/sns/tencent', () => {
  it('creates a link to tencent', () => {
    expect(tencent({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll')
  })
})
