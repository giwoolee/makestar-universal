import expect from 'expect'
import baidu from '../../../helpers/sns/baidu'

describe('helpers/sns/baidu', () => {
  it('creates a share link for baidu', () => {
    expect(baidu({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsimageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://tieba.baidu.com/f/commit/share/openShareApi?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll&desc=Makestar%20Poll&pic=http%3A%2F%2Fmakestar.co%2Ffile%2Fimages%2Fpolls%2Fsns1.jpg')
  })
})
