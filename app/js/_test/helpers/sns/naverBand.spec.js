import expect from 'expect'
import naverBand from '../../../helpers/sns/naverBand'

describe('helpers/sns/naverBand', () => {
  context('when the description and the title match', () => {
    it('creates a share to naverBand', () => {
      expect(naverBand({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
      .toEqual('http://band.us/plugin/share?route=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&body=Makestar%20Poll')
    })
  })
  context('when the description and the title do not match', () => {
    it('creates a share to naverBand', () => {
      expect(naverBand({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll Share', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
      .toEqual('http://band.us/plugin/share?route=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&body=Makestar%20Poll%20Makestar%20Poll%20Share')
    })
  })
})
