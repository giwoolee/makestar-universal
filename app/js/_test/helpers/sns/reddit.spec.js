import expect from 'expect'
import reddit from '../../../helpers/sns/reddit'

describe('helpers/sns/reddit', () => {
  it('creates a link to reddit', () => {
    expect(reddit({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://www.reddit.com/submit/?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll')
  })
})
