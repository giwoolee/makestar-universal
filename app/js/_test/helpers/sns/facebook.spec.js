import expect from 'expect'
import facebook from '../../../helpers/sns/facebook'

describe('helpers/sns/facebook', () => {
  it('creates a link for facebook', () => {
    expect(facebook({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fmakestar.co%2Fpolls%2F1')
  })
})
