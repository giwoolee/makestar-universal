import expect from 'expect'
import kakaoStory from '../../../helpers/sns/kakaoStory'

describe('helpers/sns/kakaoStory', () => {

    it('creates a share to kakaoStory', () => {
      let value = kakaoStory({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' })

      expect(value).toEqual('http://story.kakao.com/s/share?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&text=Makestar%20Poll&desc=Makestar%20Poll&imageurl=http%3A%2F%2Fmakestar.co%2Ffile%2Fimages%2Fpolls%2Fsns1.jpg')
    })
})
