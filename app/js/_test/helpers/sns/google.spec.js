import expect from 'expect'
import google from '../../../helpers/sns/google'

describe('helpers/sns/google', () => {
  it('creates a link for google+', () => {
    expect(google({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('https://plus.google.com/share?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&hl=en')
  })
})
