import expect from 'expect'
import naverLine from '../../../helpers/sns/naverLine'

describe('helpers/sns/naverLine', () => {
  it('creates a share to naverLine', () => {
    expect(naverLine({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsImageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://line.me/R/msg/text/?Makestar%20Poll%20http%3A%2F%2Fmakestar.co%2Fpolls%2F1')
  })
})
