import expect from 'expect'
import qq from '../../../helpers/sns/qq'

describe('helpers/sns/qq', () => {
  it('creates a link to qq', () => {
    expect(qq({ canonical: 'http://makestar.co/polls/1', title: 'Makestar Poll', description: 'Makestar Poll', snsimageUrl: 'http://makestar.co/file/images/polls/sns1.jpg', locale: 'en' }))
    .toEqual('http://connect.qq.com/widget/shareqq/index.html?url=http%3A%2F%2Fmakestar.co%2Fpolls%2F1&title=Makestar%20Poll&desc=Makestar%20Poll&pics=http%3A%2F%2Fmakestar.co%2Ffile%2Fimages%2Fpolls%2Fsns1.jpg')
  })
})
