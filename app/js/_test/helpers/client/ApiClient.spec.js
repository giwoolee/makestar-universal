import expect from 'expect'
import ApiClient from '../../../helpers/ApiClient'
import superagent from 'superagent'
import nock from 'nock'

function setup(server=false,cookie = 'cookie') {
  let req = {
    get: () => { return cookie }
  }

  if(server) {
    let client = new ApiClient(superagent,req)
    return {
      client,
      req
    }
  } else {
    let client = new ApiClient(superagent)
    return {
      client,
      req
    }
  }
}

describe('helpers/ApiClient', () => {
  describe('constructor', () => {

    it('contains a get method', () => {
      let { client } = setup()
      expect(client.get).toExist()
    })

    it('contains a del method', () => {
      let { client } = setup()
      expect(client.del).toExist()
    })

    it('contains a post method', () => {
      let { client } = setup()
      expect(client.post).toExist()
    })

    it('contains a put method', () => {
      let { client } = setup()
      expect(client.put).toExist()
    })
  })

  describe ('get method', () => {
    context('when req is passed', () => {
      it('calls the endpoint with the correct payload', () => {
        let { client } = setup(true)

        nock('http://localhost:8080/')
        .get('/v1/polls')
        .reply(200,{ 'msg': 'this is makestar' })

        return client.get('/polls')
      })
    })

    context('when cookie is null', () => {
      it('shouldn\'t call request.set', () => {

        let { client } = setup(true,undefined)
        nock('http://localhost:8080/')
        .get('/v1/polls')
        .reply(200,{ 'msg': 'this is makestar' })

        return client.get('/polls')
      })
    })

    context('when req isn\'t passed', () => {
      it('calls the endpoint with the correct payload', () => {
        let { client } = setup(false)

        nock('http://localhost:8080/')
        .get('/v1/polls')
        .reply(200,{ msg: 'this is makestar' })

        return client.get('/polls')
      })
    })

    context('when the response is not 200', () => {
      beforeEach(() => {
        nock('http://localhost:8080/')
        .get('/v1/polls')
        .reply(401,{ content: { nickName: null, isLoggedIn: false, locale: "en", profileImage: "/file/user_pic/default.png" } })
      })
      afterEach(() => {
        nock.cleanAll()
      })
      it('rejects the promise with an error', () => {
        let { client } = setup(false)

        const resp = { content: { nickName: null, isLoggedIn: false, locale: "en", profileImage: "/file/user_pic/default.png" } }

        return client.get('/polls').catch( err => {
          expect(err).toBeA(Error)
        })
      })
      it('rejects the promise with an ApiError that includes a status code and a payload', () => {
        let { client } = setup(false)

        const resp = { content: { nickName: null, isLoggedIn: false, locale: "en", profileImage: "/file/user_pic/default.png" } }

        return client.get('/polls').catch( err => {
          expect(err.payload).toEqual(resp)
        })
      })
    })
  })
})
