import ApiError from '../../../helpers/api/ApiError'
import expect from 'expect'

function setup(options) {
  return new ApiError(options)
}

describe('/helpers/api/ApiError', () => {
  context('when the error is 412', () => {
    it('sets the correct message', () => {
      const err = setup({ method: 'PUT', pathname: '/v1/polls/1', status: 412, payload: { content: { isLoggedIn: false } } })

      expect(err.message).toEqual('PUT /v1/polls/1 precondition failed')
    })
  })
  context('when the error is 400', () => {
    it('sets the correct message', () => {
      const err = setup({ method: 'GET', pathname: '/v1/polls/1', status: 400, payload: { content: { isLoggedIn: false } } })

      expect(err.message).toEqual('GET /v1/polls/1 bad request')
    })
  })
  context('when the error is 404', () => {
    it('sets the correct message', () => {
      const err = setup({ method: 'GET', pathname: '/v1/polls/1', status: 404, payload: { content: { isLoggedIn: false } } })

      expect(err.message).toEqual('GET /v1/polls/1 resource doesn\'t exist')
    })
  })
  context('when the error is 401', () => {
    it('sets the correct message', () => {
      const err = setup({ method: 'POST', pathname: '/v1/polls/1', status: 401, payload: { content: { isLoggedIn: false } } })

      expect(err.message).toEqual('POST /v1/polls/1 not authorised')
    })
  })
  context('when the error is 500', () => {
    it('sets the correct message', () => {
      const err = setup({ method: 'PATCH', pathname: '/v1/polls/1', status: 500, payload: { content: { isLoggedIn: false } } })

      expect(err.message).toEqual('PATCH /v1/polls/1 internal server error')
    })
  })
})
