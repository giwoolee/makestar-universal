import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './containers/App'

if (module.hot) {
  require.context('./containers/',true,/\.js$/)
  require.context('./components/',true,/\.js$/)
  require.context('./actions/',true,/\.js$/)
  require.context('./helpers/',true,/\.js$/)
  require.context('./schemas/',true,/\.js$/)
}

const routes = (
  <Route path="/" component={App}>
    <IndexRoute getComponent={(nextState,cb) => {
      System.import('./containers/index/IndexContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/polls" getComponent={(nextState,cb) => {
      System.import('./containers/poll/index/PollsContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/polls/:idx" getComponent={(nextState,cb) => {
      System.import('./containers/poll/show/PollContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/contents/:resource" getComponent={(nextState,cb) => {
      System.import('./containers/content/index/ContentsContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/contents/:resource/:idx" getComponent={(nextState,cb) => {
      System.import('./containers/content/show/ContentContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/projects" getComponent={(nextState,cb) => {
      System.import('./containers/project/index/ProjectsContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/about/:section" getComponent={(nextState,cb) => {
      System.import('./containers/about/AboutPageContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
    <Route path="/main/:section" getComponent={(nextState,cb) => {
      System.import('./containers/main/StarMessageContainer')
      .then((module) => {
        cb(null,module.default)
      })
      }
    }/>
  </Route>)

export default routes
