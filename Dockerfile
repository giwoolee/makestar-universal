FROM node:6.3.1
RUN groupadd -r makestar && useradd -m -r -g makestar makestar
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
COPY npm-shrinkwrap.json /usr/src/app/
RUN npm install
ADD . /usr/src/app
RUN chown -R makestar:makestar /usr/src/app/
USER makestar
RUN NODE_ENV=production ./node_modules/webpack/bin/webpack.js -p
EXPOSE 3000
CMD npm start

