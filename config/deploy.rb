# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'makestar-universal'
set :repo_url, 'git@bitbucket.org:make_star/makestar-universal.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/universal'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
#set :linked_dirs, fetch(:linked_dirs, []).push('node_modules')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  SSHKit.config.command_map[:npm] = "/usr/bin/npm"

  desc 'Clobber node modules'
  task :clobber_node_modules do
    on roles(:web), in: :parallel do
      execute :rm, "-rf #{shared_path}/node_modules"
    end
  end

  desc 'Install modules'
  task :install_modules do
    on roles(:web), in: :parallel do
      within "#{release_path}" do
        execute :npm, "install"
      end
    end
  end

  task :build_apps do
    on roles(:web), in: :parallel do
      SSHKit.config.command_map[:webpack] = "#{release_path}/node_modules/webpack/bin/webpack.js"
      within "#{release_path}" do
        with(NODE_ENV: 'production') { execute :webpack, "-p" }
      end
    end
  end

  task :restart_node do
    on roles(:web), in: :sequence, wait: 5 do
      execute :touch, release_path.join("#{release_path}/restart.txt")
    end
  end

  task :reload_assets do
    on roles(:web), in: :sequence do
      execute :curl, "-s http://127.0.0.1:8080/manage/reload/assets"
    end
  end

  after 'deploy:updated', 'deploy:install_modules'
  after 'deploy:install_modules', 'deploy:build_apps'
  after 'deploy:finished', 'deploy:restart_node'
  after 'deploy:finished', 'deploy:reload_assets'
end
