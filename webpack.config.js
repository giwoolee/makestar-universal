const webpack = require('webpack')
const AssetsPlugin = require('assets-webpack-plugin')
const isProd = process.env.NODE_ENV && process.env.NODE_ENV === 'production' ? true : false
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WebpackMd5Hash = require('webpack-md5-hash')
const _ = require('lodash')

module.exports = {
  devtool: isProd ? 'hidden-source-map' : 'cheap-eval-source-map',
  entry: {
    app: isProd ? ['./app/js/index.js'] : ['react-hot-loader/patch','webpack-hot-middleware/client?reload=true&path=/__webpack_hmr&timeout=20000','./app/js/index.js'],
    comment: './app/js/comment.js',
    vendor: [
      'classnames',
      'flat',
      'moment',
      'qs',
      'react',
      'react-bootstrap',
      'react-helmet',
      'react-dom',
      'react-ga',
      'react-intl',
      'react-lazy-load',
      'react-linkify',
      'react-markdown',
      'react-masonry-component',
      'react-pagify',
      'react-redux',
      'react-router-redux',
      'redial',
      'redux',
      'redux-logger',
      'redux-thunk',
      'reselect',
      'segmentize',
      'superagent',
      'velocity-animate',
      'velocity-react'
    ]
  },
  output: {
    path: __dirname + '/public',
    publicPath: '/public/',
    filename: isProd ? 'js/[name]-[chunkhash].js' : 'js/[name].js'
  },
  module: {
    loaders: _.compact([{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets:["stage-0","es2015","es2016","react"],
        plugins: isProd ? ["transform-decorators-legacy","transform-class-properties","transform-runtime","transform-es2015-classes","lodash"] : ["transform-decorators-legacy","transform-class-properties","transform-runtime","transform-es2015-classes","lodash","react-hot-loader/babel"]
      }
    },
    {
      test: /\.scss$/,
      loader: isProd ? ExtractTextPlugin.extract({ fallbackLoader: 'style-loader',loader: 'css-loader!sass-loader' }) : 'style!css?sourceMap!sass?sourceMap&sourceComments'
    },
    {
      test: /\.css$/,
      loader: isProd ? ExtractTextPlugin.extract({ fallbackLoader: 'style-loader', loader: 'css-loader' }) : 'style!css?sourceMap'
    },
    {
      test: /\.(ttf|woff|woff2|eot|svg)$/,
      loader: isProd ? 'file?name=fonts/[name].[ext]&path=/public' : 'url?limit=100000'
    },{
      test: /\.(png)$/,
      loader: 'url?limit=100000'
    },
    {
      test: /\.(jpg)$/,
      loader: 'file-loader?name=images/[name].[ext]'
    },
    {
      test: /\.json$/,
      loader: 'json'
    }])
  },
  plugins: _.compact([
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: isProd ? 'js/vendor-[chunkhash].js' : 'js/vendor.js'
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|ja|ko|zh/),
    new AssetsPlugin({
      filename: 'assets.json'
    }),
    isProd ? new ExtractTextPlugin({ filename: isProd ? 'css/app-[chunkhash].css' : 'css/app.css', disable: false, allChunks: true }) : null,
    isProd ? new CopyWebpackPlugin([
      {
        context: __dirname + '/app/assets',
        from: '**/*',
        to: './'
      }
    ]) : null,
    new webpack.optimize.OccurrenceOrderPlugin(),
    isProd ? null : new webpack.HotModuleReplacementPlugin(),
    isProd ? null : new webpack.NoErrorsPlugin(),
    isProd ? new WebpackMd5Hash() : null
  ])
}
